package com.taggery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.taggery.R;
import com.taggery.model.TagPeopleList;

import java.util.List;

import Utils.OnItemClickNew;

public class TagListAdapter extends RecyclerView.Adapter<TagListAdapter.ViewHolder> {

    private List<TagPeopleList> tags;
    private Context context;
    private OnItemClickNew listener;

    public TagListAdapter(List<TagPeopleList> tags, Context context) {
        this.tags = tags;
        this.context = context;
    }

    @Override
    public TagListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_taglist, parent, false);

        return new TagListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TagListAdapter.ViewHolder holder, final int position) {
        holder.text_postname.setText(tags.get(position).getName());
        holder.img_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnItemClickNew(position, view);
            }
        });

    }
    public void setItemclickListener(OnItemClickNew listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return tags.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView text_postname;
        public RelativeLayout relative_parent;
        private ImageView img_remove;
        public ViewHolder(View view)
        {
            super(view);
            text_postname=view.findViewById(R.id.text_postname);
            relative_parent = view.findViewById(R.id.relative_parent);
            img_remove = view.findViewById(R.id.img_remove);

        }
    }
}
