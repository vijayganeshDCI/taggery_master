package com.taggery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taggery.model.TaggeryPojo;
import com.taggery.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 9/3/2018.
 */

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.MyViewHolder> {

    private  LinearLayout horizview,addlayout;
    private List<TaggeryPojo> tagList=new ArrayList<>();
    private List<TaggeryPojo> suggestionList=new ArrayList<>();
    Context context;

    public String re="1";
    public String ig="1";
    public String ac="1";
    public String po="1";



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        ImageView img;
        Button accept,reject,post,ignore;
        public LinearLayout addlayout;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            img=(ImageView)view.findViewById(R.id.img);
            post=(Button) view.findViewById(R.id.post);
           /* horizview = (LinearLayout) view.findViewById(R.id.horizview);
            addlayout = (LinearLayout) view.findViewById(R.id.addLayout);*/

        }
    }


    public TagAdapter(List<TaggeryPojo> tagList) {
        this.tagList = tagList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_friend_request_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        TaggeryPojo movie = tagList.get(position);
        holder.title.setText(movie.getName());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));

        holder.accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ac.equalsIgnoreCase("0"))
                {
                    holder.accept.setVisibility(View.VISIBLE);
                    holder.post.setVisibility(View.GONE);
                    ac="1";
                }
                else if(ac.equalsIgnoreCase("1"))
                {
                    holder.post.setVisibility(View.VISIBLE);
                    holder.accept.setVisibility(View.GONE);
                    ac="0";
                }
            }
        });

        holder.post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(po.equalsIgnoreCase("0"))
                {
                    holder.post.setVisibility(View.VISIBLE);
                    holder.accept.setVisibility(View.GONE);
                    po="1";
                }
                else if(po.equalsIgnoreCase("1"))
                {
                    holder.accept.setVisibility(View.VISIBLE);
                    holder.post.setVisibility(View.GONE);
                    po="0";
                }
            }
        });


        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(re.equalsIgnoreCase("0"))
                {
                    holder.reject.setVisibility(View.VISIBLE);
                    holder.ignore.setVisibility(View.GONE);
                    re="1";
                }
                else if(re.equalsIgnoreCase("1"))
                {
                    holder.ignore.setVisibility(View.VISIBLE);
                    holder.reject.setVisibility(View.GONE);
                    re="0";
                }
            }
        });


        holder.ignore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ig.equalsIgnoreCase("0"))
                {
                    holder.ignore.setVisibility(View.VISIBLE);
                    holder.reject.setVisibility(View.GONE);
                    ig="1";
                }
                else if(ig.equalsIgnoreCase("1"))
                {
                    holder.reject.setVisibility(View.VISIBLE);
                    holder.ignore.setVisibility(View.GONE);
                    ig="0";
                }
            }
        });

      /*  suggestionList.clear();
        for(int i=0;i<10;i++){
            TaggeryPojo basicpojo=new TaggeryPojo();
            basicpojo.setName("Name"+i);
            suggestionList.add(basicpojo);
        }

        LayoutInflater layoutinflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
       holder.addlayout.removeAllViews();
        for (int j = 0; j < suggestionList.size(); j++) {
            View view = null;

            CircleImageView sug_profile ;
            Button followBtn;

            view = layoutinflater.inflate(R.layout.suggestion_item, null, false);

            sug_profile = (CircleImageView) view.findViewById(R.id.sug_profile);
            followBtn = (Button) view.findViewById(R.id.follow);

            holder.addlayout.addView(view);
        }


*/

    }

    @Override
    public int getItemCount() {
        return tagList.size();
    }
}