package com.taggery.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.activity.MainActivity;
import com.taggery.adapter.SnapAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.SnapInputParam;
import com.taggery.model.SnapListItemParam;
import com.taggery.model.SnapResponse;
import com.taggery.model.SuggestionItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SnapsFragment extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.image_search)
    ImageView imageSearch;
    @BindView(R.id.image_notification)
    ImageView imageNotification;
    @BindView(R.id.list_snaps)
    RecyclerView listSnaps;
    @BindView(R.id.cons_time_line)
    CoordinatorLayout consTimeLine;
    Unbinder unbinder;
    SnapAdapter snapAdapter;
    List<SnapListItemParam> snapListItemParamList;
    List<SuggestionItem> suggestionItemList;
    @BindView(R.id.text_app_title)
    CustomTextView textAppTitle;
    @BindView(R.id.app_bar_header)
    AppBarLayout appBarHeader;
    @BindView(R.id.swipe_snap)
    SwipeRefreshLayout swipeSnap;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    @BindView(R.id.scroll_nested_snap)
    NestedScrollView scrollNestedSnap;
    private LinearLayoutManager mLayoutManager;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SnapResponse snapResponse;
    private boolean isStarted;
    private boolean isVisible;
    private MainActivity mainActivity;
    private BaseActivity baseActivity;
    private static final int REQUEST_CHECK_SETTINGS = 1;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    public double mLat = 0.0, mLng = 0.0;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;


    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            if (mGoogleApiClient != null) {
                startLocationUpdates();
            } else {
                initiaieGoogleApiClient();
                mGoogleApiClient.connect();
            }
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
            if (mGoogleApiClient != null) {
                startLocationUpdates();
            } else {
                initiaieGoogleApiClient();
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        isLoading = false;
        reachedLastEnd = false;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_snaps, container, false);
        unbinder = ButterKnife.bind(this, view);
        mainActivity = (MainActivity) getActivity();
        baseActivity = (BaseActivity) getActivity();
        TaggeryApplication.getContext().getComponent().inject(this);
        snapListItemParamList = new ArrayList<SnapListItemParam>();
        suggestionItemList = new ArrayList<>();


        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setAutoMeasureEnabled(true);
        listSnaps.setItemAnimator(new DefaultItemAnimator());
        listSnaps.setLayoutManager(mLayoutManager);
        listSnaps.setNestedScrollingEnabled(false);
        snapAdapter = new SnapAdapter(getActivity(), snapListItemParamList, baseActivity);
        listSnaps.setAdapter(snapAdapter);
        scrollNestedSnap.setSmoothScrollingEnabled(true);
        scrollNestedSnap.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight()
                            - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                        if (!isLoading) {
                            if (!reachedLastEnd) {
                                if (snapListItemParamList.size() >= 12) {
                                    getSnapList(++paginationCount);
                                }

                            }
                        }
                    }
                }
            }
        });


        swipeSnap.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeSnap.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (isVisible) {
                            if (mLat != 0.0 && mLng != 0.0)
                                getSnapList(1);
                            else
                                startLocationUpdates();
                            swipeSnap.setRefreshing(false);
                        }
                    }
                }, 000);
            }
        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick({R.id.image_search, R.id.image_notification})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_search:
                break;
            case R.id.image_notification:
                break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {
        if (mGoogleApiClient != null) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
            Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
            task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    // All location settings are satisfied. The client can initialize
                    // location requests here.
                    // ...
                    if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermission();
                        return;
                    } else {
                        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new
                                OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        if (location != null) {
                                            mLat = location.getLatitude();
                                            mLng = location.getLongitude();
                                            getSnapList(1);
                                        }
                                    }
                                });
                    }
                }
            });

            task.addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ResolvableApiException) {
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(getActivity(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        //   finish();
                        break;
                }
                break;
        }
    }


    private void getSnapList(final int paginationCount) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = true;
            SnapInputParam snapInputParam = new SnapInputParam();
            snapInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
//            snapInputParam.setLat(9.9183149);
//            snapInputParam.setLng(78.1489164);
            snapInputParam.setLat(mLat);
            snapInputParam.setLng(mLng);
            snapInputParam.setPage(paginationCount);
            if (paginationCount == 1) {
                snapListItemParamList.clear();
                suggestionItemList.clear();
            }
            taggeryAPI.getSnapList(snapInputParam).enqueue(new Callback<SnapResponse>() {
                @Override
                public void onResponse(Call<SnapResponse> call, Response<SnapResponse> response) {
                    hideProgress();
                    isLoading = false;
                    snapResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (snapResponse.getStatusCode() == 200) {
                            listSnaps.setVisibility(View.VISIBLE);
                            consWarn.setVisibility(View.GONE);
                            picassoImageHolder(imageProPic, snapResponse.getProfilePic() != null ?
                                            snapResponse.getProfilePic() : "",
                                    R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                                    getString(R.string.cloudinary_download_profile_picture));
                            profileRingColor(snapResponse.getUserTotalLikesCount(), imageProPic);
                            //Seen and Unseen
                            if (snapResponse.getPosts() != null && snapResponse.getPosts().size() > 0) {
                                for (int i = 0; i < snapResponse.getPosts().size(); i++) {
                                    snapListItemParamList.add(new SnapListItemParam(
                                            snapResponse.getPosts().get(i).getSnapID(),
                                            snapResponse.getPosts().get(i).getPostImage() != null ?
                                                    snapResponse.getPosts().get(i).getPostImage() : "",
                                            snapResponse.getPosts().get(i).getUserFirstName() != null ?
                                                    snapResponse.getPosts().get(i).getUserFirstName() : "",
                                            snapResponse.getPosts().get(i).getUserLastName() != null ?
                                                    snapResponse.getPosts().get(i).getUserLastName() : "",
                                            snapResponse.getPosts().get(i).getUserPicture() != null ?
                                                    snapResponse.getPosts().get(i).getUserPicture() : "",
                                            snapResponse.getPosts().get(i).getLikeCount(),
                                            snapResponse.getPosts().get(i).getViewCount(),
                                            snapResponse.getPosts().get(i).getLikes(),
                                            snapResponse.getPosts().get(i).getViews(),
                                            snapResponse.getPosts().get(i).getFeedtime() != null ?
                                                    snapResponse.getPosts().get(i).getFeedtime() : "", null,
                                            snapResponse.getPosts().get(i).getPostID(),
                                            snapResponse.getPosts().get(i).getRelatedPostsID(),
                                            snapResponse.getPosts().get(i).getPostMediaType(),
                                            snapResponse.getPosts().get(i).getUserTotalLikesCount()
                                    ));
                                }
                            }

                            //Suggestion
                            if (snapResponse.getSuggestion() != null && snapResponse.getSuggestion().size() > 0) {
                                for (int j = 0; j < snapResponse.getSuggestion().size(); j++) {
                                    if (!snapResponse.getSuggestion().get(j).getStatus().equalsIgnoreCase("2")) {
                                        //Reject adding of already following people from list
                                        suggestionItemList.add(new SuggestionItem(
                                                snapResponse.getSuggestion().get(j).getUserPicture() != null ?
                                                        snapResponse.getSuggestion().get(j).getUserPicture() : "",
                                                snapResponse.getSuggestion().get(j).getId(),
                                                snapResponse.getSuggestion().get(j).getUserFirstName() != null ?
                                                        snapResponse.getSuggestion().get(j).getUserFirstName() : "",
                                                snapResponse.getSuggestion().get(j).getUserLastName() != null ?
                                                        snapResponse.getSuggestion().get(j).getUserLastName() : "",
                                                snapResponse.getSuggestion().get(j).getStatus(),
                                                snapResponse.getSuggestion().get(j).getUserTotalLikesCount()));
                                    }
                                }

                                snapListItemParamList.add(new
                                        SnapListItemParam(2
                                        , null, null, null,
                                        null, 0, 0,
                                        1, 1, null, suggestionItemList, 0,
                                        null, 1, 0));
                            }

                            //Location
                            if (snapResponse.getLocation() != null && snapResponse.getLocation().size() > 0) {
                                for (int k = 0; k < snapResponse.getLocation().size(); k++) {
                                    snapListItemParamList.add(new SnapListItemParam(
                                            snapResponse.getLocation().get(k).getSnapID(),
                                            snapResponse.getLocation().get(k).getPostImage()!=null?
                                                    snapResponse.getLocation().get(k).getPostImage():"", null, null, null,
                                            0, 0, 1, 1,
                                            snapResponse.getLocation().get(k).getPostText()!=null?
                                                    snapResponse.getLocation().get(k).getPostText():"", null,
                                            snapResponse.getLocation().get(k).getId(),
                                            snapResponse.getLocation().get(k).getRelatedPostsID(),
                                            snapResponse.getLocation().get(k).getPostMediaType(), 0
                                    ));
                                }
                            }


                            Collections.sort(snapListItemParamList, new Comparator<SnapListItemParam>() {
                                public int compare(SnapListItemParam obj1, SnapListItemParam obj2) {
                                    // ## Ascending order
//                                    return obj1.firstName.compareToIgnoreCase(obj2.firstName); // To compare string values
                                    return Integer.valueOf(obj1.getSnapID()).compareTo(obj2.getSnapID()); // To compare integer values

                                    // ## Descending order
                                    // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                                    // return Integer.valueOf(obj2.empId).compareTo(obj1.empId); // To compare integer values
                                }
                            });


                            snapAdapter.notifyDataSetChanged();


                        } else if (snapResponse.getStatusCode() == 501) {
                            if (paginationCount != 1) {
                                reachedLastEnd = true;
                                showShackError(snapResponse.getMessage()!=null?snapResponse.getMessage():"", consTimeLine);
                            } else {
                                listSnaps.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(snapResponse.getMessage()!=null?snapResponse.getMessage():"");
                                imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                            }
                        } else {
                            listSnaps.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(snapResponse.getMessage()!=null?snapResponse.getMessage():"");
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }
                    } else {
                        listSnaps.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }


                }

                @Override
                public void onFailure(Call<SnapResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    listSnaps.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }
            });


        } else {
            listSnaps.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }


}
