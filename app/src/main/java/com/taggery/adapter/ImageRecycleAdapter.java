package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.taggery.R;
import com.taggery.activity.ImageshowActivity;
import com.taggery.utils.TaggeryConstants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ImageRecycleAdapter  extends RecyclerView.Adapter<ImageRecycleAdapter.ViewHolder>  {

    private ArrayList<String> imagelist;
    private Context context;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    public ImageRecycleAdapter(ArrayList<String> imagelist, Context context) {
        this.imagelist = imagelist;
        this.context = context;
    }


    @Override
    public ImageRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_galleryitem, parent, false);

        return new ImageRecycleAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageRecycleAdapter.ViewHolder holder, int position) {

        final String path = imagelist.get(position);

        Glide.with(context).load(imagelist.get(position))
                .into(holder.icon);
       /* Picasso.get()
                .load(imagelist.get(position))
                .placeholder(R.mipmap.icon_app_logo)
                .into(holder.icon);*/
       final String imageName ="IMG_" + System.currentTimeMillis();
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ImageshowActivity.class);
                intent.putExtra("mediaPath", path);
                intent.putExtra("mediaFilename",imageName);
                intent.putExtra("isImage",true);
                edit.putString(TaggeryConstants.CHECK_GALLERY, "galley").apply();
                context.startActivity(intent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return imagelist.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {  ImageView icon;

        public ViewHolder(View view)
        {
            super(view);
            icon = (ImageView) view.findViewById(R.id.galleyitem);
            sharedPreferences = context.getSharedPreferences("myprefs", Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            share = context.getSharedPreferences("mytaggery",Context.MODE_PRIVATE);
            edit = share.edit();


        }
    }
}
