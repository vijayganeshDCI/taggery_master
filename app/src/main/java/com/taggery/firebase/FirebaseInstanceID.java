package com.taggery.firebase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.taggery.utils.TaggeryConstants;

import twilio.RegistrationIntentService;

/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseInstanceID extends FirebaseInstanceIdService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        editor.putString(TaggeryConstants.FCMTOKEN,refreshedToken).commit();
    }
    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences=getSharedPreferences(TaggeryConstants.FCMKEYSHAREDPERFRENCES,MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }

}
