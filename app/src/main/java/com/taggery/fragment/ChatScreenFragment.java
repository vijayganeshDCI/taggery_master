package com.taggery.fragment;


import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.adapter.ChatscreenAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.utils.TaggeryConstants;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;
import com.twilio.chat.CallbackListener;
import com.twilio.chat.Channel;
import com.twilio.chat.ChannelListener;
import com.twilio.chat.Channels;
import com.twilio.chat.ErrorInfo;
import com.twilio.chat.Member;
import com.twilio.chat.Message;
import com.twilio.chat.Messages;
import com.twilio.chat.StatusListener;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import twilio.BasicChatClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatScreenFragment extends BaseFragment {


    Channel currentChannel;
    Messages messagesObject;
    List<Message> messageAdd = new ArrayList<>();
    ChatscreenAdapter mAdapter;
    LinearLayoutManager layoutManager;
    BasicChatClient basicChatClient;
    @BindView(R.id.img_addmedia)
    ImageView imgAddmedia;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.text_chatuser)
    CustomTextView textChatuser;
    @BindView(R.id.chatname)
    RelativeLayout chatname;
    @BindView(R.id.recycleview_chat)
    RecyclerView recycleviewChat;
    @BindView(R.id.edit_chatinputmsg)
    CustomEditText editChatinputmsg;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.relative_sendlayout)
    RelativeLayout relativeSendlayout;
    @BindView(R.id.relative_child)
    RelativeLayout relativeChild;
    @BindView(R.id.relative_parent)
    RelativeLayout relativeParent;
    Unbinder unbinder;
    private Channels channelsObject;
    private PickSetup setup;
    private boolean sendMessage = false;
    public String profileMediaName = null, profileMediaPath = null;
    String channelname, channelSID;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat_screen, container, false);
        unbinder = ButterKnife.bind(this, view);

        basicChatClient = TaggeryApplication.get().getBasicClient();
        channelsObject = basicChatClient.getChatClient().getChannels();
        if (getArguments() != null) {
             channelSID = getArguments().getString(TaggeryConstants.EXTRA_CHANNEL_SID);

            if (channelSID!=null){
                basicChatClient.getChatClient().getChannels().getChannel(channelSID, new CallbackListener<Channel>() {
                    @Override
                    public void onSuccess(final Channel channel) {
                        currentChannel = channel;
                        currentChannel.addListener(mDefaultChannelListener);
                        loadData();
                    }
                });


            }
            else {
                channelname = getArguments().getString(TaggeryConstants.USER_CHAT_NAME);
            }




        }
        imgLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  sendMediamessages();
            }
        });

        /*if (Util.isNetworkAvailable()) {

        } else {
            Toast.makeText(this, R.string.network_error_message, Toast.LENGTH_SHORT).show();

        }*/
        return view;
    }


    private void loadData() {
        if (currentChannel != null) {
            // then set the title to channel name
            textChatuser.setText(currentChannel.getFriendlyName());

            if (this.currentChannel.getStatus() == Channel.ChannelStatus.JOINED) {
                loadMessages();
            } else {
                this.currentChannel.join(new StatusListener() {
                    @Override
                    public void onSuccess() {
                        currentChannel.addListener(mDefaultChannelListener);
                        if (sendMessage) {
                            sendMessages();
                        } else {
                            loadMessages();
                        }
                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                    }
                });
            }


        }
    }

    public void loadMessages() {
        messagesObject = currentChannel.getMessages();

        if (messagesObject != null) {
            messagesObject.getLastMessages(80, new CallbackListener<List<Message>>() {
                @Override
                public void onSuccess(List<Message> messageList) {
                    Log.i("chatscreen", "" + messageList.size());
                    messageAdd = messageList;
                    layoutManager = new LinearLayoutManager(getActivity());
                    layoutManager.setStackFromEnd(true);
                    recycleviewChat.setLayoutManager(layoutManager);
                    mAdapter = new ChatscreenAdapter(currentChannel, messageAdd, getActivity());
                    recycleviewChat.setAdapter(mAdapter);
                    editChatinputmsg.requestFocus();
                }
            });
        }
    }


    public void createChannelWithName(String name, final StatusListener handler) {
        this.channelsObject
                .channelBuilder()
                .withFriendlyName(name)
                .withType(Channel.ChannelType.PRIVATE)
                .build(new CallbackListener<Channel>() {
                    @Override
                    public void onSuccess(final Channel newChannel) {
                        currentChannel = newChannel;
                        handler.onSuccess();
                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                        handler.onError(errorInfo);
                    }
                });

    }

    public byte[] getCompressedBitmap1(String imagePath) {
        float maxHeight = 1920.0f;
        float maxWidth = 1080.0f;
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        byte[] byteArray = out.toByteArray();
        return byteArray;
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public byte[] convert(String path) throws IOException {

        FileInputStream fis = new FileInputStream(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[1024];

        for (int readNum; (readNum = fis.read(b)) != -1; ) {
            bos.write(b, 0, readNum);
        }

        byte[] bytes = bos.toByteArray();

        return bytes;
    }

    private void sendMediamessages(String type) {
        String mimeType;
        if (type.equals("image")) {
            mimeType = "image/jpeg";
        } else {
            mimeType = "video/mp4";
        }
        File mediaFile;
        byte[] newfile = new byte[0];
        if (profileMediaName.equals("")) {
            Toast.makeText(getActivity(), "no file selected", Toast.LENGTH_SHORT).show();
        } else {
            mediaFile = new File(profileMediaPath);
            if (mediaFile.exists()) {
                if (type.equals("image")) {
                    newfile = getCompressedBitmap1(profileMediaPath);
                } else {
                    try {
                        newfile = convert(profileMediaPath);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
            Toast.makeText(getActivity(), "started" + profileMediaPath, Toast.LENGTH_SHORT).show();
            if (currentChannel != null) {
                currentChannel.getMessages().sendMessage(Message.options()
                        .withMedia(new ByteArrayInputStream(newfile), mimeType)
                        .withMediaFileName(profileMediaName), new CallbackListener<Message>() {
                    @Override
                    public void onSuccess(Message message) {
                        Toast.makeText(getActivity(), "sent finished", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                        super.onError(errorInfo);
                        Toast.makeText(getActivity(), "sending failed", Toast.LENGTH_SHORT).show();
                        Log.i("chatscreen", "sending failed");

                    }
                });

            }
        }

    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.customalertmedia);
        TextView text_selectphoto, text_selectvideo, text_cancelmedia;
        text_selectphoto = dialog.findViewById(R.id.text_selectphoto);
        text_selectvideo = dialog.findViewById(R.id.text_selectvideo);
        text_cancelmedia = dialog.findViewById(R.id.text_cancelmedia);

        text_selectphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImage();
                dialog.dismiss();
            }
        });
        text_selectvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickVideo();
                dialog.dismiss();
            }
        });
        text_cancelmedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void sendMessages() {
        String messageBody = editChatinputmsg.getText().toString().trim();
        if (messageBody.equals("")) {
            Toast.makeText(getActivity(), "Message Field cannot be empty", Toast.LENGTH_SHORT).show();
        } else {
            if (currentChannel != null) {

                Message.Options options = Message.options().withBody(messageBody);
                Log.d("chatscreen", "Message created");
                currentChannel.getMessages().sendMessage(options, new CallbackListener<Message>() {
                    @Override
                    public void onSuccess(final Message message) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                messageAdd.add(message);
                                loadMessages();
                           /* recycleviewChat.setLayoutManager(layoutManager);
                            mAdapter = new ChatscreenAdapter(messageAdd,ChatActivity.this);
                            recycleviewChat.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();*/
                                editChatinputmsg.setText("");
                            }
                        });
                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                        super.onError(errorInfo);
                        Toast.makeText(getActivity(), errorInfo.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                createChannelWithName(channelname, new StatusListener() {
                    @Override
                    public void onSuccess() {
                        loadData();
                        sendMessage = true;
                        Toast.makeText(getActivity(), "channel created", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    private ChannelListener mDefaultChannelListener = new ChannelListener() {

        @Override
        public void onMessageAdded(final Message message) {
            Log.d("chatscreen", "Message added");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // need to modify user interface elements on the UI thread
                    messageAdd.add(message);
                    loadMessages();
                    //  mAdapter.notifyDataSetChanged();
                }
            });

        }

        @Override
        public void onMessageUpdated(Message message, Message.UpdateReason updateReason) {
            Log.i("chatscreen", "messageupdated");
        }

        @Override
        public void onMessageDeleted(Message message) {

        }

        @Override
        public void onMemberAdded(Member member) {
            Log.i("chatscreen", "memberadded");

        }

        @Override
        public void onMemberUpdated(Member member, Member.UpdateReason updateReason) {

        }

        @Override
        public void onMemberDeleted(Member member) {

        }

        @Override
        public void onTypingStarted(Channel channel, Member member) {
            Log.d("chatscreen", "Started Typing: " + member.getIdentity());
        }

        @Override
        public void onTypingEnded(Channel channel, Member member) {
            Log.d("chatscreen", "Typing Ended: " + member.getIdentity());
        }

        @Override
        public void onSynchronizationChanged(Channel channel) {

        }
    };

    public void pickImage() {

        setup = new PickSetup()
                .setTitle(getString(R.string.choose_photo))
                .setProgressText(getString(R.string.processing))
                .setCancelText(getString(R.string.cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setIconGravity(Gravity.CENTER_HORIZONTAL)
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_camera).setWidth(100).setHeight(100);
        PickImageDialog.build(setup)
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.
                            //   imageProPic.setImageBitmap(r.getBitmap());
                            //Image path
                            //r.getPath();
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            profileMediaName = "Img" + timeStamp;
                            profileMediaPath = r.getPath();
                            sendMediamessages("image");

                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setOnPickCancel(new IPickCancel() {
                    @Override
                    public void onCancelClick() {
                    }
                }).show(getActivity().getSupportFragmentManager());

    }

    private void pickVideo() {
        setup = new PickSetup()
                .setVideo(true)
                .setTitle(getString(R.string.choose_video))
                .setProgressText(getString(R.string.processing))
                .setCancelText(getString(R.string.cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setIconGravity(Gravity.CENTER_HORIZONTAL)
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_camera).setWidth(100).setHeight(100);
        PickImageDialog.build(setup)
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.
                            //   imageProPic.setImageBitmap(r.getBitmap());
                            //Image path
                            //r.getPath();
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            profileMediaName = "VID" + timeStamp;
                            profileMediaPath = r.getPath();
                            String filepath = r.getPath();
                            File file = new File(filepath);
                            long length = file.length();
                            length = length / 1024;

                            if (length > 10000) {
                                Toast.makeText(getActivity(), "Video size should be less than 10MB",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                sendMediamessages("video");
                            }

                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setOnPickCancel(new IPickCancel() {
                    @Override
                    public void onCancelClick() {
                    }
                }).show(getActivity().getSupportFragmentManager());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.img_addmedia, R.id.img_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_addmedia:
             //   showDialog();
                pickImage();
                break;
            case R.id.img_send:
                sendMessages();
                break;
        }
    }
}
