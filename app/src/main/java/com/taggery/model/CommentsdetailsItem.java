package com.taggery.model;

public class CommentsdetailsItem{
	public String getCommentUserName() {
		return CommentUserName;
	}

	public void setCommentUserName(String commentUserName) {
		CommentUserName = commentUserName;
	}

	public String getCommentText() {
		return CommentText;
	}

	public void setCommentText(String commentText) {
		CommentText = commentText;
	}

	public String getCommentLastName() {
		return CommentLastName;
	}

	public void setCommentLastName(String commentLastName) {
		CommentLastName = commentLastName;
	}

	public String getCommentUserFirstName() {
		return CommentUserFirstName;
	}

	public void setCommentUserFirstName(String commentUserFirstName) {
		CommentUserFirstName = commentUserFirstName;
	}

	public String getCommentPicture() {
		return CommentPicture;
	}

	public void setCommentPicture(String commentPicture) {
		CommentPicture = commentPicture;
	}

	public int getCommentID() {
		return CommentID;
	}

	public void setCommentID(int commentID) {
		CommentID = commentID;
	}

	public int getCommentUserID() {
		return CommentUserID;
	}

	public void setCommentUserID(int commentUserID) {
		CommentUserID = commentUserID;
	}


	public CommentsdetailsItem(String commentText,String commentPicture, int commentUserID, String commentCreatedAt) {
		CommentText = commentText;
		CommentPicture = commentPicture;
		CommentUserID = commentUserID;
		CommentCreatedAt = commentCreatedAt;
	}

	private String CommentUserName;
	private String CommentText;
	private String CommentLastName;
	private String CommentUserFirstName;
	private String CommentPicture;
	private int CommentID;
	private int CommentUserID;

	public int getUserTotalLikesCount() {
		return UserTotalLikesCount;
	}

	public void setUserTotalLikesCount(int userTotalLikesCount) {
		UserTotalLikesCount = userTotalLikesCount;
	}

	private int UserTotalLikesCount;

	public int getCommentMentionUserID() {
		return CommentMentionUserID;
	}

	public void setCommentMentionUserID(int commentMentionUserID) {
		CommentMentionUserID = commentMentionUserID;
	}

	public String getCommentMentionUserName() {
		return CommentMentionUserName;
	}

	public void setCommentMentionUserName(String commentMentionUserName) {
		CommentMentionUserName = commentMentionUserName;
	}

	public String getCommentCreatedAt() {
		return CommentCreatedAt;
	}

	public void setCommentCreatedAt(String commentCreatedAt) {
		CommentCreatedAt = commentCreatedAt;
	}

	private int CommentMentionUserID;
	private String CommentMentionUserName;
	private String CommentCreatedAt;

}
