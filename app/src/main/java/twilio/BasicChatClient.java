package twilio;

import android.content.Context;
import android.content.res.Resources;
import android.nfc.Tag;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.taggery.BuildConfig;
import com.taggery.R;
import com.taggery.adapter.ChatscreenAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.fragment.ChannelFragment;
import com.twilio.accessmanager.AccessManager;
import com.twilio.chat.CallbackListener;
import com.twilio.chat.Channel;
import com.twilio.chat.ChatClient;
import com.twilio.chat.ChatClientListener;
import com.twilio.chat.ErrorInfo;
import com.twilio.chat.Message;
import com.twilio.chat.StatusListener;
import com.twilio.chat.internal.HandlerUtil;

import java.util.ArrayList;

public class BasicChatClient  extends CallbackListener<ChatClient>
        implements AccessManager.Listener, AccessManager.TokenUpdateListener {
    private Context context;
    private String accessToken;
    private String fcmToken;
    private AccessManager accessManager;
    private String urlString;
    private String username;
    private LoginListener loginListener;
    private Handler loginListenerHandler;
    final static String TAG = "TwilioChat";
    private ChatClient mChatClient;
    String tokenURL;



    public BasicChatClient(Context context)
    {
        super();
        this.context = context;

        if (BuildConfig.DEBUG) {
            ChatClient.setLogLevel(android.util.Log.DEBUG);
        }
    }

    public ChatClient getChatClient()
    {
        return mChatClient;
    }
    private String getStringResource(int id) {
        Resources resources = TaggeryApplication.get().getResources();
        return resources.getString(id);
    }

    public interface LoginListener {
        public void onLoginStarted();

        public void onLoginFinished();

        public void onLoginError(String errorMessage);

        public void onLogoutFinished();
    }

    public void setFCMToken(String fcmToken)
    {
        this.fcmToken = fcmToken;
        if (mChatClient != null) {
            setupFcmToken();
        }
    }
    public void setClientListener(ChatClientListener listener) {
        if (this.mChatClient != null) {
            this.mChatClient.setListener(listener);
        }
    }

    public void retrieveAccessTokenfromServer(final String token,final String name,final LoginListener listener) {

        if (name == this.username
                && urlString == token
                && loginListener == listener
                && mChatClient != null
                && accessManager != null
                && !accessManager.isTokenExpired())
        {
            onSuccess(mChatClient);
            return;
        }
        this.username = name;
        urlString = token;

        this.loginListener=listener;
        loginListenerHandler = HandlerUtil.setupListenerHandler();
        loginListener = listener;

        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
         tokenURL = token + "?device=" + deviceId + "&identity=" + name;
        Log.d("token",token + "?device=" + deviceId + "&identity=" + name);
        login(tokenURL);

    }
    public void login(String token) {
        Ion.with(context)
                .load(token)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e == null) {
                            accessToken = result.get("token").getAsString();

                            Log.d(TAG, "Retrieved access token from server: " + accessToken);
                            loginListenerHandler.post(new Runnable() {
                                @Override
                                public void run()
                                {
                                    if (loginListener != null) {
                                        loginListener.onLoginStarted();
                                    }
                                }
                            });
                            createAccessManager(accessToken);
                           /* ChatClient.Properties.Builder builder = new ChatClient.Properties.Builder()
                                    .setDeferCertificateTrustToPlatform(true);*/
                            ChatClient.Properties props = new ChatClient.Properties.Builder()
                                    .setDeferCertificateTrustToPlatform(true)
                                    .createProperties();
                            ChatClient.create(context,accessToken,props,mChatClientCallback);
                            accessManager.updateToken(accessToken);

                        } else {
                            Log.e(TAG,e.getMessage(),e);
                            Toast.makeText(context,
                                    R.string.error_retrieving_access_token, Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });

    }

    public CallbackListener<ChatClient> mChatClientCallback =
            new CallbackListener<ChatClient>() {
                @Override
                public void onSuccess(ChatClient chatClient) {
                    mChatClient = chatClient;
                    Log.d(TAG, "Success creating Twilio Chat Client");

                    loginListenerHandler.post(new Runnable() {
                        @Override
                        public void run()
                        {
                            if (loginListener != null) {
                                loginListener.onLoginFinished();
                            }
                        }
                    });
                }

                @Override
                public void onError(ErrorInfo errorInfo) {
                    Log.e(TAG,"Error creating Twilio Chat Client: " + errorInfo.getMessage());
                }
            };


    private void setupFcmToken()
    {
        mChatClient.registerFCMToken(fcmToken, new StatusListener() {
            @Override
            public void onSuccess() {
                Log.i("fcmToken",fcmToken.toString());
                Log.i(TAG,"register successful");
            }

            @Override
            public void onError(ErrorInfo errorInfo) {
                super.onError(errorInfo);
                Log.i("fcmToken",errorInfo.toString());
            }
        });
    }

    public void unregisterFcmToken()
    {
        mChatClient.unregisterFCMToken(fcmToken, new StatusListener() {
            @Override
            public void onSuccess() {
                Log.i(TAG,"unregister successful");
            }
        });

    }

    private void createAccessManager(String token)
    {
        if (accessManager != null) return;
        accessManager = new AccessManager(token, this);
        accessManager.addTokenUpdateListener(this);
    }

    public void shutdown()
    {
        mChatClient.shutdown();
        mChatClient = null; // Client no longer usable after shutdown()
    }

    @Override
    public void onTokenWillExpire(AccessManager accessManager) {
        TaggeryApplication.get().showToast("Token will expire in 3 minutes. Getting new token.");
        login(tokenURL);

    }

    @Override
    public void onTokenExpired(AccessManager accessManager) {
        TaggeryApplication.get().showToast("Token expired. Getting new token.");
        login(tokenURL);


    }

    @Override
    public void onError(final ErrorInfo errorInfo) {
        super.onError(errorInfo);
        TaggeryApplication.get().logErrorInfo("Login error", errorInfo);

        loginListenerHandler.post(new Runnable() {
            @Override
            public void run()
            {
                if (loginListener != null) {
                    loginListener.onLoginError(errorInfo.toString());
                }
            }
        });
    }

    @Override
    public void onError(AccessManager accessManager, String s) {
        TaggeryApplication.get().showToast("AccessManager error: " + s);
    }

    @Override
    public void onTokenUpdated(String token) {
        if (mChatClient == null) return;

       /* mChatClient.updateToken(token, new ToastStatusListener(
                "Client Update Token was successfull",
                "Client Update Token failed"));*/
    }

    @Override
    public void onSuccess(ChatClient chatClient) {
        Log.d(TAG, "Success creating Twilio Chat Client");
        mChatClient = chatClient;
        if (fcmToken != null) {
            setupFcmToken();
        }

        loginListenerHandler.post(new Runnable() {
            @Override
            public void run()
            {
                if (loginListener != null) {
                    loginListener.onLoginFinished();
                }
            }
        });

    }
}
