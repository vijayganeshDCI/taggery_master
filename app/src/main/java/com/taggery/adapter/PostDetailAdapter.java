package com.taggery.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.model.ViewedItem;

import java.util.List;

//import Utils.OnItemClickListener;
import de.hdodenhof.circleimageview.CircleImageView;

public class PostDetailAdapter extends RecyclerView.Adapter<PostDetailAdapter.ViewHolder> {

    private Context context;
    private List<ViewedItem> viewedItemList;

    public PostDetailAdapter(List<ViewedItem> viewedItemList, Context context) {
        this.viewedItemList = viewedItemList;
        this.context = context;
    }

    @Override
    public PostDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_post_details, parent, false);

        return new PostDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostDetailAdapter.ViewHolder holder, final int position) {
        holder.textUserName.setText(viewedItemList.get(position).getUserFirstName());
        picassoImageHolder(holder.imageProfilePic, viewedItemList.get(position).getUserPicture(),
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder, context.getString(R.string.cloudinary_download_profile_picture));
        profileRingColor(viewedItemList.get(position).getUserTotalLikesCount(),holder.imageProfilePic);
        if (viewedItemList.get(position).getLikes() == 1)
            holder.imageLike.setVisibility(View.VISIBLE);
        else
            holder.imageLike.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return viewedItemList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imageProfilePic;
        public ImageView imageLike;
        public RelativeLayout relativeStatus;
        public TextView textUserName;

        public ViewHolder(View view) {
            super(view);
            textUserName = view.findViewById(R.id.status_name);
            imageLike = view.findViewById(R.id.image_like);
            relativeStatus = view.findViewById(R.id.rel_status);
            imageProfilePic = view.findViewById(R.id.img_profileview);
        }
    }


    public void picassoImageHolder(final ImageView imageViewProPic, String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }

    public void profileRingColor(int count, CircleImageView circleImageView) {
        circleImageView.setBorderWidth(3);
        if (count >= 5000 && count < 10000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.colorPrimaryDark));
        } else if (count >= 10000 && count < 15000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.blue));
        } else if (count >= 15000 && count < 25000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.gold));
        } else if (count >= 25000 && count < 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.black));
        } else if (count >= 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.white));
        } else {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.transparent_white));
        }
    }


}
