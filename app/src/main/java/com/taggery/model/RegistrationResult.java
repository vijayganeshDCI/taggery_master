package com.taggery.model;

public class RegistrationResult {
	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getUserMobileNumber() {
		return UserMobileNumber;
	}

	public void setUserMobileNumber(String userMobileNumber) {
		UserMobileNumber = userMobileNumber;
	}

	public String getUserGender() {
		return UserGender;
	}

	public void setUserGender(String userGender) {
		UserGender = userGender;
	}

	public String getUserRelationshipStatus() {
		return UserRelationshipStatus;
	}

	public void setUserRelationshipStatus(String userRelationshipStatus) {
		UserRelationshipStatus = userRelationshipStatus;
	}

	public String getUserPicture() {
		return UserPicture;
	}

	public void setUserPicture(String userPicture) {
		UserPicture = userPicture;
	}

	public String getUserLastName() {
		return UserLastName;
	}

	public void setUserLastName(String userLastName) {
		UserLastName = userLastName;
	}

	public String getUserFirstName() {
		return UserFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		UserFirstName = userFirstName;
	}

	public String getUserEmail() {
		return UserEmail;
	}

	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String UserName;
	private String UserMobileNumber;
	private String UserGender;
	private String UserRelationshipStatus;
	private String UserPicture;
	private String UserLastName;
	private String UserFirstName;
	private String UserEmail;

	public String getUserCountryCode() {
		return UserCountryCode;
	}

	public void setUserCountryCode(String userCountryCode) {
		UserCountryCode = userCountryCode;
	}

	private String UserCountryCode;
	public int getUserProfileType() {
		return UserProfileType;
	}

	public void setUserProfileType(int userProfileType) {
		UserProfileType = userProfileType;
	}

	private int UserProfileType;
	private int id;
}
