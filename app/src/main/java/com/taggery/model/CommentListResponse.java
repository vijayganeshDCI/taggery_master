package com.taggery.model;

import java.util.List;

public class CommentListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<CommentsdetailsItem> getComments() {
		return comments;
	}

	public void setComments(List<CommentsdetailsItem> comments) {
		this.comments = comments;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<CommentsdetailsItem> comments;
	private int StatusCode;
}