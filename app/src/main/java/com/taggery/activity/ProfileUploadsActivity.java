package com.taggery.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.taggery.R;
import com.taggery.adapter.ProfileRecycleAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.ProfileInputParam;
import com.taggery.model.ProfileResponse;
import com.taggery.model.ProfileUpload;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileUploadsActivity extends BaseActivity {


    @BindView(R.id.text_label_related_post)
    CustomTextView textLabelRelatedPost;
    @BindView(R.id.list_profile_uploads)
    RecyclerView listProfileUploads;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    @BindView(R.id.cons_profile_uploads)
    ConstraintLayout consProfileUploads;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    @BindView(R.id.swipe_gallery)
    SwipeRefreshLayout swipeGallery;
    private int ownerID, viwerID;
    private CheckUserNameResponse checkUserNameResponse;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;
    private List<ProfileUpload> profileUpload = new ArrayList<>();
    private ProfileResponse profileUploadListResponse;
    private ProfileRecycleAdapter adapter;
    private int visibleItemCount, totalItemCount, pastVisiblesItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_uploads);
        ButterKnife.bind(this);
        TaggeryApplication.getContext().getComponent().inject(this);

        Intent intent = getIntent();
        if (intent != null) {
            ownerID = intent.getIntExtra("ownerID", 0);
            viwerID = intent.getIntExtra("viewerID", 0);
        }

        final GridLayoutManager mLayoutManager = new GridLayoutManager(ProfileUploadsActivity.this, 3);
        listProfileUploads.setLayoutManager(mLayoutManager);
        listProfileUploads.setItemAnimator(new DefaultItemAnimator());
        adapter = new ProfileRecycleAdapter(ProfileUploadsActivity.this, profileUpload);
        listProfileUploads.setAdapter(adapter);
        listProfileUploads.setNestedScrollingEnabled(false);

        getProfileMediaList(1);

        listProfileUploads.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                if (!isLoading && !reachedLastEnd) {
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount
                            && pastVisiblesItems >= 0
                            && totalItemCount >= 23) {
                        getProfileMediaList(++paginationCount);
                    }
                }

            }

        });

        swipeGallery.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeGallery.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getProfileMediaList(1);
                        swipeGallery.setRefreshing(false);
                    }
                }, 000);
            }
        });


    }


    private void getProfileMediaList(final int paginationCount) {
        final ProfileInputParam profileInputParam = new ProfileInputParam();
        profileInputParam.setOwner_id(ownerID);
        profileInputParam.setViewer_id(viwerID);
        profileInputParam.setPage(paginationCount);
        showProgress();
        isLoading = true;
        if (paginationCount == 1)
            profileUpload.clear();
        taggeryAPI.getProfileMediaList(profileInputParam).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                hideProgress();
                isLoading = false;
                profileUploadListResponse = response.body();
                if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                    if (profileUploadListResponse.getStatusCode() == 200) {
                        consWarn.setVisibility(View.GONE);
                        if (profileUploadListResponse.getAfter12HrsRemaining() != null &&
                                profileUploadListResponse.getAfter12HrsRemaining().size() > 0) {
                            for (int i = 0; i < profileUploadListResponse.getAfter12HrsRemaining().size(); i++) {
                                profileUpload.add(new ProfileUpload(profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostImage() != null ?
                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostImage() : "",
                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostType() != null ?
                                                profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostType() : "",
                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostID(),
                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostUserID(),
                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getId(),
                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostMediaType(),
                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostStatus()));

                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            consWarn.setVisibility(View.VISIBLE);
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            textWarn.setText(getString(R.string.no_media));
                        }

                    } else if (profileUploadListResponse.getStatusCode() == 501) {
                        if (paginationCount != 1) {
                            reachedLastEnd = true;
                            showShackError(profileUploadListResponse.getMessage()!=null?
                                    profileUploadListResponse.getMessage():"", consProfileUploads);
                        } else {
                            listProfileUploads.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }
                    } else {
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }

                } else {
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }

            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                hideProgress();
                isLoading = false;
                consWarn.setVisibility(View.VISIBLE);
                textWarn.setText(getString(R.string.server_error));
                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isLoading = false;
        reachedLastEnd = false;
    }
}
