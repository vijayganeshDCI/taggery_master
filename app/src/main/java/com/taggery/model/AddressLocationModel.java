package com.taggery.model;

public class AddressLocationModel {

    private String locationName;

    public AddressLocationModel(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }


}
