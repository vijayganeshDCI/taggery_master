package com.taggery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.model.TagPeopleList;
import com.taggery.model.TagPeopleResponse;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import Utils.ChatCallback;
import Utils.OnItemClickListener;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {


    private List<TagPeopleResponse.Friend> chatpeople ;
    Context context;
    private OnItemClickListener listener;
    private ChatCallback chatCallback;
    private LinkedHashSet<TagPeopleList> chatlist = new LinkedHashSet<>();

    public ChatListAdapter(List<TagPeopleResponse.Friend> chatpeople, Context context) {
        this.chatpeople = chatpeople;
        this.context = context;
    }

    @Override
    public ChatListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_chatlist, parent, false);

        return new ChatListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChatListAdapter.ViewHolder holder, final int position) {
        final TagPeopleResponse.Friend tagpeople = chatpeople.get(position);
        holder.text_username.setText(tagpeople.getUserName());
        holder.relative_tagpeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // listener.OnItemClick(position, view);

                /*if (tagpeople.getIsSelected() == 0){
                    holder.radio_chat.setChecked(true);
                    tagpeople.setIsSelected(1);
                    chatlist.add(new TagPeopleList(tagpeople.getId(),tagpeople.getUserName()));
                    chatCallback.ChatClick(chatlist);
                    Log.i("listnames", chatlist.toString());
                }
                else {
                    holder.radio_chat.setChecked(false);
                    tagpeople.setIsSelected(0);
                }*/


                if (holder.radio_chat.isChecked()){
                    holder.radio_chat.setChecked(false);
                    chatlist.remove(new TagPeopleList(tagpeople.getId(),tagpeople.getUserName()));
                    chatCallback.ChatClick(chatlist);
                    tagpeople.setIsSelected(0);
                    Log.i("listnames", chatlist.toString());
                }
                else
                {
                    holder.radio_chat.setChecked(true);
                    chatlist.add(new TagPeopleList(tagpeople.getId(),tagpeople.getUserName()));
                    chatCallback.ChatClick(chatlist);
                    Log.i("listnames", chatlist.toString());
                    tagpeople.setIsSelected(1);

                }



            }
        });

    }

    @Override
    public int getItemCount() {
        return chatpeople.size();
    }
    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
    public void setChatList(ChatCallback chatList){
        this.chatCallback =chatList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_profilepic;
        TextView text_username;
        ImageView img_tick;
        RadioButton radio_chat;
        RelativeLayout relative_tagpeople;
        public ViewHolder(View view) {
            super(view);
            img_profilepic= view.findViewById(R.id.img_profilepic);
            text_username = view.findViewById(R.id.text_username);
            img_tick = view.findViewById(R.id.img_tick);
            radio_chat = view.findViewById(R.id.radio_chat);
            relative_tagpeople = view.findViewById(R.id.relative_tagpeople);
        }
    }
}
