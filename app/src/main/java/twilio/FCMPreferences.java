package twilio;

public interface FCMPreferences {
    String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    String REGISTRATION_COMPLETE = "registrationComplete";
}
