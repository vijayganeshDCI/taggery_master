package com.taggery.model;

import java.util.List;

public class SnapResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public int getTotalPage() {
		return TotalPage;
	}

	public void setTotalPage(int totalPage) {
		TotalPage = totalPage;
	}

	public int getCurrentPage() {
		return CurrentPage;
	}

	public void setCurrentPage(int currentPage) {
		CurrentPage = currentPage;
	}

	public List<SuggestionItem> getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(List<SuggestionItem> suggestion) {
		this.suggestion = suggestion;
	}

	public List<PostsItem> getPosts() {
		return Posts;
	}

	public void setPosts(List<PostsItem> posts) {
		Posts = posts;
	}

	public String getProfilePic() {
		return ProfilePic;
	}

	public void setProfilePic(String profilePic) {
		ProfilePic = profilePic;
	}

	public List<LocationItem> getLocation() {
		return location;
	}

	public void setLocation(List<LocationItem> location) {
		this.location = location;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	private String Message;
	private int TotalPage;
	private int CurrentPage;
	private List<SuggestionItem> suggestion;
	private List<PostsItem> Posts;
	private String ProfilePic;

	public int getUserTotalLikesCount() {
		return UserTotalLikesCount;
	}

	public void setUserTotalLikesCount(int userTotalLikesCount) {
		UserTotalLikesCount = userTotalLikesCount;
	}

	private int UserTotalLikesCount;
	private List<LocationItem> location;
	private int StatusCode;
}