package com.taggery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.taggery.R;

import java.util.List;

import Utils.OnItemClickListener;

public class CustomAdapterLocationName extends RecyclerView.Adapter<CustomAdapterLocationName.ViewHolder> {


    private List<Place> locationModel;
    private Context context;
    private OnItemClickListener listener;

    public CustomAdapterLocationName(List<Place> locationModel, Context context) {
        this.locationModel = locationModel;
        this.context = context;
    }

    @Override
    public CustomAdapterLocationName.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_locationname, parent, false);

        return new CustomAdapterLocationName.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomAdapterLocationName.ViewHolder holder, final int position) {

        Place model = locationModel.get(position);
        holder.text_locationname.setText(model.getName());
        holder.text_locationname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnItemClick(position,view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return locationModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView text_locationname;
        public ViewHolder(View view)
        {
            super(view);
            text_locationname=view.findViewById(R.id.text_locationname);

        }
    }
    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
