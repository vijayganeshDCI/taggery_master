package com.taggery.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.taggery.R;
import com.taggery.adapter.CustomAdapterLocationName;
import com.taggery.adapter.CustomAdapterLocationPlace;
import com.taggery.model.AddressLocationModel;

import java.util.ArrayList;
import java.util.List;

import Utils.OnItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * UserProfileUploadAfter12Hrs simple {@link Fragment} subclass.
 */
public class LocationFragment extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnItemClickListener {

    private static final int REQUEST_CHECK_SETTINGS = 1;
    private static final int AUTO_COMP_REQ_CODE = 2;
    @BindView(R.id.recyclerview_place)
    RecyclerView recyclerviewPlace;
    @BindView(R.id.relative_location)
    RelativeLayout relativeLocation;
    Unbinder unbinder;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    protected PlaceDetectionClient placeDetectionClient;
    PlaceAutocompleteFragment place_autocomplete_fragment;

    private LinearLayoutManager mLayoutManager;
    private CustomAdapterLocationPlace mAdapter;
    private List<Place> addressLocation = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        unbinder = ButterKnife.bind(this, view);
        placeDetectionClient = Places.getPlaceDetectionClient(getActivity());
        initiaieGoogleApiClient();
        place_autocomplete_fragment = (PlaceAutocompleteFragment) getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(getActivity());
            startActivityForResult(intent, AUTO_COMP_REQ_CODE);
        } catch (Exception e) {
            Log.e("locationfragment", e.getStackTrace().toString());
        }


        return view;
    }

    private void getCurrentPlaceData() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Task<PlaceLikelihoodBufferResponse> placeResult = placeDetectionClient.getCurrentPlace(null);
        placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                Log.d("loaction fragment", "current location places info");
                PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    addressLocation.add(placeLikelihood.getPlace().freeze());
                }
                likelyPlaces.release();

                mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerviewPlace.setLayoutManager(mLayoutManager);
                recyclerviewPlace.setItemAnimator(new DefaultItemAnimator());
                mAdapter = new CustomAdapterLocationPlace(addressLocation, getActivity());
                mAdapter.setItemclickListener(LocationFragment.this);
                recyclerviewPlace.setAdapter(mAdapter);

            }
        });
    }

    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    /*private void locationData() {
        addressLocation.clear();
        AddressLocationModel AmbigaCinemas = new AddressLocationModel("AmbigaCinemas");
        addressLocation.add(AmbigaCinemas);
        AddressLocationModel AnnaNagar = new AddressLocationModel("Anna Nagar");
        addressLocation.add(AnnaNagar);
        AddressLocationModel KKnagar = new AddressLocationModel("KK nagar");
        addressLocation.add(KKnagar);
        AddressLocationModel Aarapalayam = new AddressLocationModel("Aarapalayam");
        addressLocation.add(Aarapalayam);
        AddressLocationModel Teppakuklam = new AddressLocationModel("Teppakuklam");
        addressLocation.add(Teppakuklam);
        AddressLocationModel Goripalayam = new AddressLocationModel("Goripalayam");
        addressLocation.add(Goripalayam);
        AddressLocationModel Mattuthavani = new AddressLocationModel("Mattuthavani");
        addressLocation.add(Mattuthavani);
        AddressLocationModel Periyar = new AddressLocationModel("Periyar");
        addressLocation.add(Periyar);
        AddressLocationModel LakeView = new AddressLocationModel("Lake View");
        addressLocation.add(LakeView);


    }*/


    /*private void getLocationData() {

        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerviewPlace.setLayoutManager(mLayoutManager);
        recyclerviewPlace.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CustomAdapterLocationPlace(addressLocation, getActivity());
        mAdapter.setItemclickListener(LocationFragment.this);
        recyclerviewPlace.setAdapter(mAdapter);


    }
*/


    private void startLocationUpdates() {

        if (mGoogleApiClient != null) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
            Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
            task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    // All location settings are satisfied. The client can initialize
                    // location requests here.
                    // ...
                    if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermission();
                        return;
                    }
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
                    mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new
                            OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    if (location != null) {
                                        mLat = location.getLatitude();
                                        mLng = location.getLongitude();
                                        Toast.makeText(getActivity(), mLat + "" + mLng, Toast.LENGTH_SHORT).show();

                                       // getCurrentPlaceData();
                                     //   getLocationData();
                                    }
                                }
                            });
                }
            });

            task.addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ResolvableApiException) {
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(getActivity(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        /*startLocationUpdates();
                        getCurrentPlaceData();*/
                       // getLocationData();
                        break;
                    case Activity.RESULT_CANCELED:
                        getActivity().onBackPressed();

                        break;
                }
                break;
        }
        if(requestCode == AUTO_COMP_REQ_CODE){
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Toast.makeText(getActivity(), "place "+place.toString(),
                        Toast.LENGTH_LONG).show();
                Log.i("place",place.toString());
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
      //  startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void OnItemClick(int pos, View view) {

    }

    @Override
    public void OnItemClickSecond(int position, View view) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(place_autocomplete_fragment != null && getActivity() != null && !getActivity().isFinishing()) {
            getActivity().getFragmentManager().beginTransaction().remove(place_autocomplete_fragment).commit();
        }
        unbinder.unbind();
    }
}
