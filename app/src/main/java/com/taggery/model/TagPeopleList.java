package com.taggery.model;

public class TagPeopleList {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TagPeopleList(int id, String name) {
        this.id = id;
        this.name = name;
    }
   /* public int hashCode()
    {
        System.out.println("In hashcode method");
        int hashcode = 0;
        return hashcode;
    }
    public boolean equals(Object obj)
    {
        System.out.println("In equals method");
        if (obj instanceof TagPeopleList)
        {
            TagPeopleList emp = (TagPeopleList) obj;
            return true;
        }
        else
        {
            return false;
        }
    }*/

    private int id;
    private String name;
}
