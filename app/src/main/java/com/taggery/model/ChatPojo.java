package com.taggery.model;

/**
 * Created by keerthana on 9/3/2018.
 */

public class ChatPojo {
    private String img, title, subtitle,time;

    public ChatPojo() {
    }

    public ChatPojo(String img, String title, String subtitle,String time) {
        this.img=img;
        this.title = title;
        this.subtitle = subtitle;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}