package twilio;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.taggery.R;
import com.taggery.application.TaggeryApplication;
import com.taggery.fragment.ChannelFragment;
import com.twilio.chat.StatusListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegistrationIntentService";
    private static final String FCM_BINDING_TYPE = "fcm";
    private Intent bindingResultIntent;
    private SharedPreferences sharedPreferences;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        bindingResultIntent = new Intent(ChannelFragment.BINDING_REGISTRATION);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String identity = sharedPreferences.getString(BindingSharedPreferences.IDENTITY, null);
        String address = sharedPreferences.getString(BindingSharedPreferences.ADDRESS, null);

        String newIdentity = intent.getStringExtra(BindingSharedPreferences.IDENTITY);
        if (newIdentity == null) {
            // If no identity was provided to us then we use the identity stored in shared preferences.
            if (identity != null) {
                newIdentity = identity;
            } else {
                /*
                 * When the application is first installed onTokenRefresh() may be called without the
                 * user providing an identity. In this case we ignore the request to bind.
                 */
                Log.w("registraionintent", "No identity was provided.");
                return;
            }
        }
        String endpoint = sharedPreferences.getString(BindingSharedPreferences.ENDPOINT + newIdentity, null);

        String newAddress = FirebaseInstanceId.getInstance().getToken();

        boolean sameBinding =
                newIdentity.equals(identity) && newAddress.equals(address);


        if (newIdentity == null) {
            Log.i("registraionintent", "UserProfileUploadAfter12Hrs new binding registration was not performed because" +
                    " the identity cannot be null.");
            bindingResultIntent.putExtra(ChannelFragment.BINDING_SUCCEEDED, false);
            bindingResultIntent.putExtra(ChannelFragment.BINDING_RESPONSE, "Binding identity was null");
            // Notify the MainActivity that the registration ended
            LocalBroadcastManager.getInstance(RegistrationIntentService.this)
                    .sendBroadcast(bindingResultIntent);
        } else if (sameBinding) {
            Log.i("registraionintent", "UserProfileUploadAfter12Hrs new binding registration was not performed because" +
                    "the binding values are the same as the last registered binding.");
            bindingResultIntent.putExtra(ChannelFragment.BINDING_SUCCEEDED, true);
            bindingResultIntent.putExtra(ChannelFragment.BINDING_RESPONSE, "Binding already registered");
            // Notify the MainActivity that the registration ended
            LocalBroadcastManager.getInstance(RegistrationIntentService.this)
                    .sendBroadcast(bindingResultIntent);
        } else {
            /*
             * Clear all the existing bindings from SharedPreferences and attempt to register
             * the new binding values.
             */
            sharedPreferences.edit().remove(BindingSharedPreferences.IDENTITY).commit();
            sharedPreferences.edit().remove(BindingSharedPreferences.ENDPOINT + newIdentity).commit();
            sharedPreferences.edit().remove(BindingSharedPreferences.ADDRESS).commit();
            final Binding binding = new Binding(newIdentity, endpoint, newAddress, FCM_BINDING_TYPE);
            registerBinding(binding);
        }


        Intent registrationComplete = new Intent(FCMPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

    }

    private void registerBinding(final Binding binding) {
        Log.i("registraionintent", "Binding with" +
                " identity: " + binding.identity +
                " endpoint: " + binding.endpoint +
                " address: " + binding.Address);

        if (TwilioFunctionsAPI.BASE_SERVER_URL.equals(getResources().getString(R.string.base_url_string))) {
            String message = "Set the BASE_SERVER_URL in TwilioFunctionsAPI.java";
            Log.e("registraionintent", message);
            bindingResultIntent.putExtra(ChannelFragment.BINDING_SUCCEEDED, false);
            bindingResultIntent.putExtra(ChannelFragment.BINDING_RESPONSE, message);
            // Notify the MainActivity that the registration ended
            LocalBroadcastManager.getInstance(RegistrationIntentService.this)
                    .sendBroadcast(bindingResultIntent);
            return;
        }

        Call<CreateBindingResponse> call = TwilioFunctionsAPI.registerBinding(binding);
        call.enqueue(new Callback<CreateBindingResponse>() {
            @Override
            public void onResponse(Call<CreateBindingResponse> call, Response<CreateBindingResponse> response) {
                if(response.isSuccessful()) {
                    // Store binding in SharedPreferences upon success
                    sharedPreferences.edit().putString(BindingSharedPreferences.IDENTITY, binding.identity).commit();
                    sharedPreferences.edit().putString(BindingSharedPreferences.ENDPOINT + binding.identity, response.body().endpoint).commit();
                    sharedPreferences.edit().putString(BindingSharedPreferences.ADDRESS, binding.Address).commit();

                    bindingResultIntent.putExtra(ChannelFragment.BINDING_SUCCEEDED, true);
                } else {
                    String message = "Binding failed " + response.code() + " " + response.message();
                    Log.e("registraionintent", message);
                    bindingResultIntent.putExtra(ChannelFragment.BINDING_SUCCEEDED, false);
                    bindingResultIntent.putExtra(ChannelFragment.BINDING_RESPONSE, message);
                }

                // Notify the MainActivity that the registration ended
                LocalBroadcastManager.getInstance(RegistrationIntentService.this)
                        .sendBroadcast(bindingResultIntent);
            }

            @Override
            public void onFailure(Call<CreateBindingResponse> call, Throwable t) {
                String message = "Binding failed " + t.getMessage();
                Log.e("registraionintent", message);
                bindingResultIntent.putExtra(ChannelFragment.BINDING_SUCCEEDED, false);
                bindingResultIntent.putExtra(ChannelFragment.BINDING_RESPONSE, message);
                // Notify the MainActivity that the registration ended
                LocalBroadcastManager.getInstance(RegistrationIntentService.this)
                        .sendBroadcast(bindingResultIntent);
            }
        });
    }
}
