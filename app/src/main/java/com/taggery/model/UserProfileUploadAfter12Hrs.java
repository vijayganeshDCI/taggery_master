package com.taggery.model;

import java.util.List;

public class UserProfileUploadAfter12Hrs {
	private String first_page_url;
	private String path;
	private int per_page;
	private int total;
	private List<ProfileUpload> data;
	private int last_page;
	private String last_page_url;
	private String next_page_url;
	private int from;
	private int to;
	private Object prev_page_url;
	private int current_page;

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	private int StatusCode;
	private String Status;


	public String getFirst_page_url() {
		return first_page_url;
	}

	public void setFirst_page_url(String first_page_url) {
		this.first_page_url = first_page_url;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getPer_page() {
		return per_page;
	}

	public void setPer_page(int per_page) {
		this.per_page = per_page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<ProfileUpload> getData() {
		return data;
	}

	public void setData(List<ProfileUpload> data) {
		this.data = data;
	}

	public int getLast_page() {
		return last_page;
	}

	public void setLast_page(int last_page) {
		this.last_page = last_page;
	}

	public String getLast_page_url() {
		return last_page_url;
	}

	public void setLast_page_url(String last_page_url) {
		this.last_page_url = last_page_url;
	}

	public String getNext_page_url() {
		return next_page_url;
	}

	public void setNext_page_url(String next_page_url) {
		this.next_page_url = next_page_url;
	}

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public Object getPrev_page_url() {
		return prev_page_url;
	}

	public void setPrev_page_url(Object prev_page_url) {
		this.prev_page_url = prev_page_url;
	}

	public int getCurrent_page() {
		return current_page;
	}

	public void setCurrent_page(int current_page) {
		this.current_page = current_page;
	}
}