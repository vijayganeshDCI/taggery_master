package com.taggery.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.taggery.R;
import com.taggery.adapter.ViewPagerAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.fragment.CameraFragment;
import com.taggery.fragment.ChannelFragment;
import com.taggery.fragment.ContactListFragment;
import com.taggery.fragment.ProfileFragment;
import com.taggery.fragment.SnapsFragment;
import com.taggery.utils.TaggeryConstants;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity {

    @BindView(R.id.viewpager_main)
    ViewPager viewpagerMain;
    @BindView(R.id.linear_main)
    LinearLayout linearMain;
    private boolean doubleBackToExitPressedOnce;
    private Handler mHandler = new Handler();
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    File deletefile;
    int pos;
    private RunTimePermission runTimePermission;
    private boolean isFromAddPost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        TaggeryApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        editor.putInt(TaggeryConstants.LOGIN_STATUS, 1).commit();
        deletefile = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Taggery" + File.separator + "Local");
        Intent intent = getIntent();
        if (intent != null) {
            isFromAddPost = intent.getBooleanExtra("isFromAddpost", false);
        }

        runTimePermission = new RunTimePermission(this);
        runTimePermission.requestPermission(new String[]{Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        }, new RunTimePermission.RunTimePermissionListener() {

            @Override
            public void permissionGranted() {
                // First we need to check availability of play services
                setupViewPager(viewpagerMain);
                if (deletefile != null) {
                    deleteDir(deletefile);
                }

            }

            @Override
            public void permissionDenied() {

                finish();
            }
        });
        viewpagerMain.setOffscreenPageLimit(4);
        viewpagerMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                pos = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });



    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setupViewPager(ViewPager viewpager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new ChannelFragment(), "ChannelFragment");
        adapter.addFrag(new CameraFragment(), "CameraFragment");
        adapter.addFrag(new SnapsFragment(), "SnapsFragment");
        adapter.addFrag(new ProfileFragment(), "ProfileFragment");
        viewpager.setAdapter(adapter);
        if (isFromAddPost)
            viewpagerMain.setCurrentItem(2);
        else
            viewpagerMain.setCurrentItem(1);


    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//
//    }

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (viewpagerMain.getCurrentItem() != 0)
                viewpagerMain.setCurrentItem(2, true);
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacks(mRunnable);
        }

    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    public void closeappdialog() {

        final Toast toast = Toast.makeText(this, getString(R.string.tap_again_exit), Toast.LENGTH_SHORT);
        if (doubleBackToExitPressedOnce) {
            toast.cancel();
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        toast.show();
        mHandler.postDelayed(mRunnable, 2000);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (runTimePermission != null) {
            runTimePermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}
