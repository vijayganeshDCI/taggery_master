package com.taggery.fragment;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cloudinary.Transformation;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.taggery.R;
import com.taggery.activity.ImageshowActivity;
import com.taggery.adapter.ImageFilterAdpater;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.FilterThumModel;
import com.taggery.model.OnSwipeTouchListener;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import Utils.OnItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * UserProfileUploadAfter12Hrs simple {@link Fragment} subclass.
 */
public class FilterFragment extends BaseFragment implements OnItemClickListener {


    @BindView(R.id.img_show)
    ImageView imgShow;
    @BindView(R.id.video_player)
    SimpleExoPlayerView videoPlayer;
    @BindView(R.id.video_layout)
    FrameLayout video_layout;
    @BindView(R.id.text_filterlabel)
    CustomTextView textFilterlabel;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.linear_filters)
    LinearLayout fliterListView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.rel_filter)
    RelativeLayout relFilter;
    @BindView(R.id.progress_applyfilter)
    ProgressBar progressApplyfilter;

    private File imagefile, videofile;
    private LinearLayoutManager mLayoutManager;
    private ImageFilterAdpater mFliterImageAdapter;
    private ImageshowActivity imageshowActivity;
    private String mediaPath, mediaFilename;
    private List<FilterThumModel> filterThumModelList = new ArrayList<>();
    private String resourcetype, image_id;
    private boolean isUp = false;
    private boolean isImage = false;
    private Bitmap bitmap, finalImage;
    private Bundle bundle;
    private SimpleExoPlayer player;
    Unbinder unbinder;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    SharedPreferences share;
    SharedPreferences.Editor edit;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        share = getActivity().getSharedPreferences("mytaggery", Context.MODE_PRIVATE);
        edit = share.edit();
        imageshowActivity = (ImageshowActivity) getActivity();
        fliterListView.setVisibility(View.INVISIBLE);
        initializeCloudinary();
        getBundleData();
          /*textFilterlabel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        view.setX(motionEvent.getRawX());
                        view.setY(motionEvent.getRawY());
                        break;

                }

                return false;
            }
        });*/

        relFilter.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                slideUpandDown(fliterListView, 1);
            }

            public void onSwipeBottom() {
                slideUpandDown(fliterListView, 2);
            }

            public void onClick(MotionEvent event) {
                slideUpandDown(fliterListView, 2);
            }

        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (share.getString(TaggeryConstants.CHECK_GALLERY, "camera").equals("camera")) {
                        if (imagefile != null) {
                            File file = new File(String.valueOf(imagefile));
                            file.delete();
                        } else {
                            File file = new File(String.valueOf(videofile));
                            file.delete();
                        }
                    }

                }
                return false;
            }
        });


        return view;
    }

    @OnClick({R.id.text_filterlabel, R.id.fab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_filterlabel:
                if (isUp) {
                    slideUpandDown(fliterListView, 2);
                } else {
                    slideUpandDown(fliterListView, 1);
                }
                isUp = !isUp;
                break;
            case R.id.fab:
                filterData();
                break;
        }
    }

    @Override
    public void OnItemClick(int pos, View view) {
        universalImageLoader(pos);
    }

    @Override
    public void OnItemClickSecond(int position, View view) {

    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null)
            player.release();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        edit.putString(TaggeryConstants.FILTERS_NEW, "nonewfilters").commit();
        unbinder.unbind();
    }

    private void getBundleData() {

        bundle = getArguments();
        if (getArguments() != null) {
            mediaPath = bundle.getString("mediaPath");
            mediaFilename = bundle.getString("mediaFilename");
            isImage = bundle.getBoolean("isImage");

            if (isImage) {
                //Image File
                imagefile = new File(mediaPath);
                resourcetype = "image";
                if (imagefile.exists()) {
                    try {
                        bitmap = getCompressedBitmap(imagefile.getAbsolutePath());
                        if (bitmap != null) {
                            imgShow.setImageBitmap(bitmap);
                            cloudUpload();
                        }

                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();

                    }
                }
            } else {
                //Video file
                videofile = new File(mediaPath);
                resourcetype = "video";
                if (Util.isNetworkAvailable())
                    cloudinaryPostUpload();
                else
                    Toast.makeText(getActivity(), R.string.turnoninternetconnection, Toast.LENGTH_SHORT).show();
                video_layout.setVisibility(View.VISIBLE);
                videoPlayer.setVisibility(View.VISIBLE);
                textFilterlabel.setVisibility(View.GONE);
                recyclerview.setVisibility(View.GONE);
                try {
                    exoVideoPlayer(mediaPath, videoPlayer, player);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }


    private void cloudUpload() {

        if (share.getString(TaggeryConstants.FILTERS_NEW, "newfilters").equals("newfilters")) {
            // for apply filter
            if (Util.isNetworkAvailable()) {
                cloudinaryPostUpload();
            } else {
                Toast.makeText(getActivity(), R.string.turnoninternetconnection, Toast.LENGTH_SHORT).show();
            }
        } else {
            image_id = sharedPreferences.getString(TaggeryConstants.FILTERS_APPLY, "");
            if (!image_id.equals("")) {
                filterThumModelList.clear();
                addFilter(image_id);
            }
        }
    }


    private void cloudinaryPostUpload() {
        showProgress();
        MediaManager.get().upload(mediaPath)
                .option("public_id", mediaFilename)
                .option("resource_type", resourcetype)
                .option("folder", getString(R.string.cloudinary_upload_add_post))
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {
                       /* Double progress = (double) bytes/totalBytes;
                        String numberD = String.valueOf(progress);
                        numberD = numberD.substring ( numberD.indexOf ( "." ) );
                        Toast.makeText(getActivity(), numberD, Toast.LENGTH_SHORT).show();*/
                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        hideProgress();
                        if (isImage) {
                            filterThumModelList.clear();
                            addFilter(resultData.get("public_id").toString());
                        } else {
                            MediaManager.get().url().transformation(new Transformation().quality(50)).
                                    resourceType("video").generate(resultData.get("public_id").toString());
                        }
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        hideProgress();
                        Toast.makeText(getActivity(), error.getCode(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {
                    }
                }).dispatch();

    }


    private void addFilter(String imageId) {
        editor.putString(TaggeryConstants.FILTERS_APPLY, imageId).commit();
        String[] effects = {"athena", "red_rock", "fes", "zorro", "eucalyptus", "peacock", "ukulele", "primavera",
                "daguerre", "audrey", "incognito"};
        FilterThumModel thumbModel;
        String imageUrl, imageThumb;
        for (String effect : effects) {
            imageUrl = MediaManager.get().url().transformation(new Transformation().
                    gravity("auto").crop("fit").quality("auto:low").effect("art:" + effect)).generate(imageId);
            imageThumb = MediaManager.get().url().transformation(new Transformation().
                    width(150).height(150).crop("thumb").effect("art:" + effect)).generate(imageId);
            thumbModel = new FilterThumModel(effect, imageUrl, imageThumb);
            filterThumModelList.add(thumbModel);
        }
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setNestedScrollingEnabled(false);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        mFliterImageAdapter = new ImageFilterAdpater(filterThumModelList, getActivity());
        mFliterImageAdapter.setItemclickListener(FilterFragment.this);
        recyclerview.setAdapter(mFliterImageAdapter);
    }

    private void filterData() {
        PostFragment postFragment = new PostFragment();
        Bundle bundle = new Bundle();
        if (isImage) {
            //Filtered Image
            if (finalImage != null)
                mediaPath = saveImage(finalImage);
        }
        bundle.putBoolean("isImage", isImage);
        bundle.putString("mediaPath", mediaPath);
        bundle.putString("mediaFilename", mediaFilename);
        postFragment.setArguments(bundle);
        imageshowActivity.push(postFragment, "PostFragment");

    }

    private void universalImageLoader(int pos) {
        FilterThumModel model = filterThumModelList.get(pos);
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .build();
        imageLoader.displayImage(model.getImageurl(), imgShow, options);
        imageLoader.loadImage(model.getImageurl(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                progressApplyfilter.setVisibility(View.GONE);
                finalImage = loadedImage;
            }

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                super.onLoadingStarted(imageUri, view);
                progressApplyfilter.setVisibility(View.VISIBLE);
            }
        });
    }

    private void slideUpandDown(View view, int slideType) {
        TranslateAnimation animate = null;
        view.setVisibility(View.VISIBLE);
        if (slideType == 1) {
//            slideUp
            scaleAnimUpandDown(0.75f, 0.75f);
            recyclerview.setVisibility(View.VISIBLE);
            fab.setVisibility(View.INVISIBLE);
            animate = new TranslateAnimation(
                    0,                 // fromXDelta
                    0,                 // toXDelta
                    view.getHeight(),  // fromYDelta
                    0);                // toYDelta
        } else {
            //slideDown
            scaleAnimUpandDown(1f, 1f);
            fab.setVisibility(View.VISIBLE);
            recyclerview.setVisibility(View.GONE);
            animate = new TranslateAnimation(0,                 // fromXDelta
                    0,                 // toXDelta
                    0,                 // fromYDelta
                    view.getHeight()); // toYDelta
        }
        animate.setDuration(100);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    private void scaleAnimUpandDown(float x, float y) {
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(imgShow, "scaleX", x);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(imgShow, "scaleY", y);
        scaleDownX.setDuration(100);
        scaleDownY.setDuration(100);
        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);
        scaleDown.start();
    }

}
