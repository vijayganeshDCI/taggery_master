package com.taggery.model;

import java.util.List;

public class ContactListInputParam {

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<String> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<String> numbers) {
        this.numbers = numbers;
    }

    private int user_id;

    public int getPostID() {
        return PostID;
    }

    public void setPostID(int postID) {
        PostID = postID;
    }

    private int PostID;
    private int page;
    private List<String> numbers;

    public int getReportStatus() {
        return ReportStatus;
    }

    public void setReportStatus(int reportStatus) {
        ReportStatus = reportStatus;
    }

    private int ReportStatus;


}
