package com.taggery.model;

public class UserDetails {
    public String getUserRelationshipStatus() {
        return UserRelationshipStatus;
    }

    public void setUserRelationshipStatus(String userRelationshipStatus) {
        UserRelationshipStatus = userRelationshipStatus;
    }

    public String getUserLastName() {
        return UserLastName;
    }

    public void setUserLastName(String userLastName) {
        UserLastName = userLastName;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }

    public int getUserStatus() {
        return UserStatus;
    }

    public void setUserStatus(int userStatus) {
        UserStatus = userStatus;
    }

    public int getUserPrivateProfile() {
        return UserPrivateProfile;
    }

    public void setUserPrivateProfile(int userPrivateProfile) {
        UserPrivateProfile = userPrivateProfile;
    }

    public String getUserCoverPhoto() {
        return UserCoverPhoto;
    }

    public void setUserCoverPhoto(String userCoverPhoto) {
        UserCoverPhoto = userCoverPhoto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserMobileNumber() {
        return UserMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        UserMobileNumber = userMobileNumber;
    }

    public String getUserGender() {
        return UserGender;
    }

    public void setUserGender(String userGender) {
        UserGender = userGender;
    }

    public String getUserPicture() {
        return UserPicture;
    }

    public void setUserPicture(String userPicture) {
        UserPicture = userPicture;
    }

    public String getUserPassword() {
        return UserPassword;
    }

    public void setUserPassword(String userPassword) {
        UserPassword = userPassword;
    }

    public String getUserFirstName() {
        return UserFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        UserFirstName = userFirstName;
    }

    private String UserRelationshipStatus;
    private String UserLastName;
    private String UserEmail;
    private int UserStatus;
    private int UserPrivateProfile;

    public String getUserDescription() {
        return UserDescription;
    }

    public void setUserDescription(String userDescription) {
        UserDescription = userDescription;
    }

    private String UserDescription;

    public int getUserProfileType() {
        return UserProfileType;
    }

    public void setUserProfileType(int userProfileType) {
        UserProfileType = userProfileType;
    }

    private int UserProfileType;
    private String UserCoverPhoto;
    private int id;
    private String UserName;
    private String UserMobileNumber;
    private String UserGender;
    private String UserPicture;
    private String UserPassword;
    private String UserFirstName;
    public String getUserCountryCode() {
        return UserCountryCode;
    }

    public void setUserCountryCode(String userCountryCode) {
        UserCountryCode = userCountryCode;
    }

    private String UserCountryCode;
}
