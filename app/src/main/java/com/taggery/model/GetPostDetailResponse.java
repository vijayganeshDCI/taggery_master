package com.taggery.model;

public class GetPostDetailResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}



	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;

	public com.taggery.model.Post getPost() {
		return Post;
	}

	public void setPost(com.taggery.model.Post post) {
		Post = post;
	}

	private Post Post;
	private int StatusCode;
}
