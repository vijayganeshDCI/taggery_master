package com.taggery.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.taggery.R;
import com.taggery.model.FilterThumModel;

import java.util.List;

import Utils.OnItemClickListener;

public class ImageFilterAdpater extends RecyclerView.Adapter<ImageFilterAdpater.ViewHolder> {

    private List<FilterThumModel> filterThumModels;
    private Context context;
    private OnItemClickListener listener;
    private int selectedIndex = 0;

    public ImageFilterAdpater(List<FilterThumModel> filterThumModels, Context context) {
        this.filterThumModels = filterThumModels;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_filters, parent, false);

        return new ImageFilterAdpater.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            final FilterThumModel filter = filterThumModels.get(position);
            holder.textFilterName.setText(filter.getFilterName());

            if (holder.imageFilter != null) {
                ImageLoader imageLoader = ImageLoader.getInstance();
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisk(true).resetViewBeforeLoading(true).build();
                imageLoader.displayImage(filter.getThumb(), holder.imageFilter, options, new SimpleImageLoadingListener(){
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        holder.progress_filter.setVisibility(View.GONE);
                    }
                });
            }
            holder.linearlayoutParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.OnItemClick(position, view);
                    notifyDataSetChanged();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return filterThumModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageFilter;
        public RelativeLayout linearlayoutParent;
        public TextView textFilterName;
        ProgressBar progress_filter;

        public ViewHolder(View view) {
            super(view);
            imageFilter = view.findViewById(R.id.filterimg);
            textFilterName = view.findViewById(R.id.filtername);
            linearlayoutParent = view.findViewById(R.id.linlay);
            progress_filter = view.findViewById(R.id.progress_filter);

        }
    }

    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
