package com.taggery.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.model.TagPeopleResponse;

import java.util.ArrayList;
import java.util.List;

import Utils.OnItemClickListener;
import de.hdodenhof.circleimageview.CircleImageView;

public class TagPeopleAdapter extends RecyclerView.Adapter<TagPeopleAdapter.ViewHolder> {


    private List<TagPeopleResponse.Friend> tagPeople ;
    private List<String> names = new ArrayList<>();
    Context context;
    boolean longclick=false,click=false;
    private int selectedCount = 0;
    private boolean isMultipleUserSelected;
    private OnItemClickListener listener;

    public TagPeopleAdapter(List<TagPeopleResponse.Friend> tagPeople, Context context) {
        this.tagPeople = tagPeople;
        this.context = context;
    }

    @Override
    public TagPeopleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_tagpeople, parent, false);

        return new TagPeopleAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TagPeopleAdapter.ViewHolder holder, final int position) {

        final TagPeopleResponse.Friend tagpeople = tagPeople.get(position);
        holder.text_username.setText(tagpeople.getUserName());

        picassoImageHolder(holder.img_profilepic,tagpeople.getUserPicture(), R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                context.getString(R.string.cloudinary_download_snap_image_add_post));

        if (tagPeople.get(position).getIsSelected()==0){
            holder.img_tick.setVisibility(View.GONE);
        }


        holder.relative_tagpeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnItemClickSecond(position, view);
                    if (tagPeople.get(position).getIsSelected() == 0) {
                        holder.img_tick.setVisibility(View.VISIBLE);
                        tagPeople.get(position).setIsSelected(1);

                    } else {
                        holder.img_tick.setVisibility(View.GONE);
                        tagPeople.get(position).setIsSelected(0);

                    }

            }
        });
       /* holder.relative_tagpeople.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                longclick=true;
                listener.OnItemClick(position, view);
                holder.img_tick.setVisibility(View.VISIBLE);
                tagPeople.get(position).setIsSelected(1);
                return true;
            }
        });*/

    }
    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void picassoImageHolder(final ImageView imageViewProPic, String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target=new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }

    @Override
    public int getItemCount() {
        return tagPeople.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_profilepic;
        TextView text_username;
        ImageView img_tick;
        RelativeLayout relative_tagpeople;
        public ViewHolder(View view) {
            super(view);
            img_profilepic= view.findViewById(R.id.img_profilepic);
            text_username = view.findViewById(R.id.text_username);
            img_tick = view.findViewById(R.id.img_tick);
            relative_tagpeople = view.findViewById(R.id.relative_tagpeople);
        }
    }
}
