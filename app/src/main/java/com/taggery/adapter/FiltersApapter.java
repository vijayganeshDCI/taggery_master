package com.taggery.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.taggery.R;
import com.taggery.model.StatusPojo;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.utils.ThumbnailItem;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
/*

public class FiltersApapter extends RecyclerView.Adapter<FiltersApapter.ViewHolder>  {

    private List<ThumbnailItem> thumbnailItemList;
    private ThumbnailsAdapterListener listener;
    private Context context;
    private int selectedIndex = 0;

    public FiltersApapter( List<ThumbnailItem> thumbnailItemList, Context context,ThumbnailsAdapterListener listener) {
        this.thumbnailItemList = thumbnailItemList;
        this.context = context;
        this.listener=listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_filters, parent, false);

        return new FiltersApapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            final ThumbnailItem thumbnailItem = thumbnailItemList.get(position);
            holder.textFilterName.setText(thumbnailItem.filterName);
            holder.imageFilter.setImageBitmap(thumbnailItem.image);
            holder.linearlayoutParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onFilterSelected(thumbnailItem.filter);
                    selectedIndex = position;
                    notifyDataSetChanged();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return thumbnailItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView imageFilter;
        public LinearLayout linearlayoutParent;
        public TextView textFilterName;
        public ViewHolder(View view)
        {
            super(view);
            imageFilter=view.findViewById(R.id.imageFilter);
            textFilterName=view.findViewById(R.id.textFilterName);
            linearlayoutParent=view.findViewById(R.id.linearlayoutParent);

        }
    }
    public interface ThumbnailsAdapterListener {
        void onFilterSelected(Filter filter);
    }
}
*/
