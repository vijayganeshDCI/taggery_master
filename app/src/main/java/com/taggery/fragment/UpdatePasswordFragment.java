package com.taggery.fragment;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.activity.LoginActivity;
import com.taggery.activity.SettingsActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.RegistrationParam;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UpdatePasswordFragment extends BaseFragment {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.edit_password)
    CustomEditText editPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout textInputPassword;
    @BindView(R.id.edit_confirm_password)
    CustomEditText editConfirmPassword;
    @BindView(R.id.text_input_confirm_password)
    TextInputLayout textInputConfirmPassword;
    @BindView(R.id.cons_password)
    ConstraintLayout consPassword;
    @BindView(R.id.button_submit)
    CustomButton buttonSubmit;
    @BindView(R.id.cons_update_password)
    ConstraintLayout consUpdatePassword;
    Unbinder unbinder;
    LoginActivity loginActivity;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    TaggeryAPI taggeryAPI;
    @BindView(R.id.edit_current_password)
    CustomEditText editCurrentPassword;
    @BindView(R.id.text_input_current_password)
    TextInputLayout textInputCurrentPassword;
    @BindView(R.id.text_label_title)
    CustomTextView textLabelTitle;
    private String phoneNumber, countryCode;
    CheckUserNameResponse checkUserNameResponse;
    private boolean isForChangePassword;
    private SettingsActivity settingsActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upate_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getActivity() instanceof LoginActivity)
            loginActivity = (LoginActivity) getActivity();
        else
            settingsActivity = (SettingsActivity) getActivity();
        TaggeryApplication.getContext().getComponent().inject(this);
        textInputCurrentPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        textInputPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        textInputConfirmPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        buttonSubmit.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        Bundle bundle = getArguments();
        if (getArguments() != null) {
            isForChangePassword = bundle.getBoolean("isForChangePassword");
            if (!isForChangePassword) {
                //ForgetPassword
                textLabelTitle.setText(getString(R.string.reset_password));
                textInputCurrentPassword.setVisibility(View.GONE);
                phoneNumber = bundle.getString("phoneNumber");
                countryCode = bundle.getString("countryCode");
            } else {
                //ChangePassword
                textLabelTitle.setText(getString(R.string.change_password));
                textInputCurrentPassword.setVisibility(View.VISIBLE);
            }
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button_submit)
    public void onViewClicked() {
        updatePasswordValidation();
    }

    private void updatePasswordValidation() {
        if (!isForChangePassword) {
            //ForgetPassword
            if (editPassword.getText().toString().length() > 0 &&
                    editConfirmPassword.getText().toString().length() > 0 &&
                    editConfirmPassword.getText().toString().equals(editPassword.getText().toString())) {
                textInputPassword.setErrorEnabled(false);
                textInputConfirmPassword.setErrorEnabled(false);
                forgetPassword();
            } else {
                if (editPassword.getText().toString().length() == 0)
                    textInputPassword.setError(getString(R.string.password_error));
                else
                    textInputPassword.setErrorEnabled(false);

                if (editPassword.getText().toString().length() > 0 &&
                        editConfirmPassword.getText().toString().length() > 0 &&
                        !editConfirmPassword.getText().toString().equals(editPassword.getText().toString())) {
                    textInputConfirmPassword.setError(getString(R.string.pass_mis_match_error));
                } else {
                    if (editConfirmPassword.getText().toString().length() == 0)
                        textInputConfirmPassword.setError(getString(R.string.confirm_password_error));
                    else
                        textInputConfirmPassword.setErrorEnabled(false);
                }
            }
        } else {
            //ChangePassword
            if (editCurrentPassword.getText().toString().length() > 0 && editPassword.getText().toString().length() > 0 &&
                    editConfirmPassword.getText().toString().length() > 0 &&
                    editConfirmPassword.getText().toString().equals(editPassword.getText().toString())) {
                textInputPassword.setErrorEnabled(false);
                textInputConfirmPassword.setErrorEnabled(false);
                textInputCurrentPassword.setErrorEnabled(false);
                changePassword();
            } else {
                if (editPassword.getText().toString().length() == 0)
                    textInputPassword.setError(getString(R.string.password_error));
                else
                    textInputPassword.setErrorEnabled(false);

                if (editCurrentPassword.getText().toString().length() == 0)
                    textInputCurrentPassword.setError(getString(R.string.current_password_Error));
                else
                    textInputCurrentPassword.setErrorEnabled(false);

                if (editPassword.getText().toString().length() > 0 &&
                        editConfirmPassword.getText().toString().length() > 0 &&
                        !editConfirmPassword.getText().toString().equals(editPassword.getText().toString())) {
                    textInputConfirmPassword.setError(getString(R.string.pass_mis_match_error));
                } else {
                    if (editConfirmPassword.getText().toString().length() == 0)
                        textInputConfirmPassword.setError(getString(R.string.confirm_password_error));
                    else
                        textInputConfirmPassword.setErrorEnabled(false);
                }
            }
        }
    }

    private void forgetPassword() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setCountryCode("+" + countryCode);
            registrationParam.setPhoneNumber(phoneNumber);
            registrationParam.setPassword(editConfirmPassword.getText().toString());
            taggeryAPI.forgetPassword(registrationParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            Toast.makeText(getActivity(), checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", Toast.LENGTH_SHORT).show();
                            loginActivity.push(new LoginFragment());
                            loginActivity.popAllBackstack();
                        } else {
                            showShackError(checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", consUpdatePassword);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consUpdatePassword);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consUpdatePassword);

                }
            });
        } else {
            showShackError(getString(R.string.no_network), consUpdatePassword);
        }


    }

    private void changePassword() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setCurrent_password(editCurrentPassword.getText().toString());
            registrationParam.setNew_password(editPassword.getText().toString());
            registrationParam.setConfirm_password(editConfirmPassword.getText().toString());
            registrationParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.changePassword(registrationParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            Toast.makeText(getActivity(), checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        } else {
                            showShackError(checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", consUpdatePassword);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consUpdatePassword);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consUpdatePassword);
                }
            });
        } else {
            showShackError(getString(R.string.no_network), consUpdatePassword);
        }


    }


}
