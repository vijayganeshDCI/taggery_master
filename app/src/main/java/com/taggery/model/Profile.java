package com.taggery.model;

import java.util.List;

public class Profile{
	private String UserName;
	private String UserGender;
	private String UserPicture;
	private String UserRelationshipStatus;
	private int Posts;
	private String UserLastName;
	private String UserFirstName;
	private int Followings;
	private int Followers;
	private int UserProfileType;
	private String UserCoverPhoto;
	private String UserDescription;
	private int Likes;

	public int getProfileOwner() {
		return ProfileOwner;
	}

	public void setProfileOwner(int profileOwner) {
		ProfileOwner = profileOwner;
	}

	public int getOurfriendrequestscount() {
		return ourfriendrequestscount;
	}

	public void setOurfriendrequestscount(int ourfriendrequestscount) {
		this.ourfriendrequestscount = ourfriendrequestscount;
	}

	public int getThosetwofriends() {
		return thosetwofriends;
	}

	public void setThosetwofriends(int thosetwofriends) {
		this.thosetwofriends = thosetwofriends;
	}

	public int getLast_page() {
		return last_page;
	}

	public void setLast_page(int last_page) {
		this.last_page = last_page;
	}

	private int ProfileOwner;
	private int ourfriendrequestscount;
	private int thosetwofriends;
	private int last_page;
	private List<PostDetails> RelatedPostsID;




	public List<PostDetails> getRelatedPostsID() {
		return RelatedPostsID;
	}

	public void setRelatedPostsIDs(List<PostDetails> relatedPostsID) {
		RelatedPostsID = relatedPostsID;
	}

	public void setUserName(String userName){
		this.UserName = userName;
	}

	public String getUserName(){
		return UserName;
	}

	public void setUserGender(String userGender){
		this.UserGender = userGender;
	}

	public String getUserGender(){
		return UserGender;
	}

	public void setUserPicture(String userPicture){
		this.UserPicture = userPicture;
	}

	public String getUserPicture(){
		return UserPicture;
	}

	public void setUserRelationshipStatus(String userRelationshipStatus){
		this.UserRelationshipStatus = userRelationshipStatus;
	}

	public String getUserRelationshipStatus(){
		return UserRelationshipStatus;
	}

	public void setPosts(int posts){
		this.Posts = posts;
	}

	public int getPosts(){
		return Posts;
	}

	public void setUserLastName(String userLastName){
		this.UserLastName = userLastName;
	}

	public String getUserLastName(){
		return UserLastName;
	}

	public void setUserFirstName(String userFirstName){
		this.UserFirstName = userFirstName;
	}

	public String getUserFirstName(){
		return UserFirstName;
	}

	public void setFollowings(int followings){
		this.Followings = followings;
	}

	public int getFollowings(){
		return Followings;
	}

	public void setFollowers(int followers){
		this.Followers = followers;
	}

	public int getFollowers(){
		return Followers;
	}

	public void setUserProfileType(int userProfileType){
		this.UserProfileType = userProfileType;
	}

	public int getUserProfileType(){
		return UserProfileType;
	}

	public void setUserCoverPhoto(String userCoverPhoto){
		this.UserCoverPhoto = userCoverPhoto;
	}

	public String getUserCoverPhoto(){
		return UserCoverPhoto;
	}

	public void setUserDescription(String userDescription){
		this.UserDescription = userDescription;
	}

	public String getUserDescription(){
		return UserDescription;
	}

	public void setLikes(int likes){
		this.Likes = likes;
	}

	public int getLikes(){
		return Likes;
	}
}
