package com.taggery.fragment;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.taggery.R;
import com.taggery.adapter.ImageRecycleAdapter;
import com.yashoid.instacropper.InstaCropperView;

import java.io.File;
import java.util.ArrayList;

public class TestFragment extends Fragment {

    InstaCropperView instaCropperView;


    public TestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test, container, false);
        instaCropperView = view.findViewById(R.id.instacropper);

        Bundle bundle = getArguments();
        String imagepath;
        if (getArguments()!=null){
            imagepath = bundle.getString("filterimage");
            File imgFile = new File(imagepath);
            instaCropperView.setImageUri(Uri.fromFile(new File(imgFile.getAbsolutePath())));


        }


        return view;
    }



}
