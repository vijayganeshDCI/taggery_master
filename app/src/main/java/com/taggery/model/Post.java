package com.taggery.model;

import java.util.List;

public class Post{
	public String getPostImage() {
		return PostImage;
	}

	public void setPostImage(String postImage) {
		PostImage = postImage;
	}

	public String getPostLocation() {
		return PostLocation;
	}

	public void setPostLocation(String postLocation) {
		PostLocation = postLocation;
	}

	public String getPostType() {
		return PostType;
	}

	public void setPostType(String postType) {
		PostType = postType;
	}

	public int getViews() {
		return Views;
	}

	public void setViews(int views) {
		Views = views;
	}

	public int getLikestcount() {
		return likestcount;
	}

	public void setLikestcount(int likestcount) {
		this.likestcount = likestcount;
	}

	public int getViewscount() {
		return viewscount;
	}

	public void setViewscount(int viewscount) {
		this.viewscount = viewscount;
	}

	public int getPostID() {
		return PostID;
	}

	public void setPostID(int postID) {
		PostID = postID;
	}

	public int getPostUserID() {
		return PostUserID;
	}

	public void setPostUserID(int postUserID) {
		PostUserID = postUserID;
	}

	public List<String> getPostTagList() {
		return PostTagList;
	}

	public void setPostTagList(List<String> postTagList) {
		PostTagList = postTagList;
	}

	public String getPostLongitude() {
		return PostLongitude;
	}

	public void setPostLongitude(String postLongitude) {
		PostLongitude = postLongitude;
	}

	public String getPostText() {
		return PostText;
	}

	public void setPostText(String postText) {
		PostText = postText;
	}

	public List<ViewedItem> getViewed() {
		return viewed;
	}

	public void setViewed(List<ViewedItem> viewed) {
		this.viewed = viewed;
	}

	public int getPostTimeFrame() {
		return PostTimeFrame;
	}

	public void setPostTimeFrame(int postTimeFrame) {
		PostTimeFrame = postTimeFrame;
	}

	public int getLikes() {
		return Likes;
	}

	public void setLikes(int likes) {
		Likes = likes;
	}

	public int getPostMediaType() {
		return PostMediaType;
	}

	public void setPostMediaType(int postMediaType) {
		PostMediaType = postMediaType;
	}

	public int getPostStatus() {
		return PostStatus;
	}

	public void setPostStatus(int postStatus) {
		PostStatus = postStatus;
	}

	public String getPostLatitude() {
		return PostLatitude;
	}

	public void setPostLatitude(String postLatitude) {
		PostLatitude = postLatitude;
	}

	private String PostImage;
	private String PostLocation;
	private String PostType;
	private int Views;
	private int likestcount;
	private int viewscount;

	public int getCommentscount() {
		return commentscount;
	}

	public void setCommentscount(int commentscount) {
		this.commentscount = commentscount;
	}

	private int commentscount;
	private int PostID;
	private int PostUserID;
	private List<String> PostTagList;
	private String PostLongitude;
	private String PostText;
	private List<ViewedItem> viewed;

	public List<CommentsdetailsItem> getCommentsdetails() {
		return comments;
	}

	public void setCommentsdetails(List<CommentsdetailsItem> commentsdetails) {
		this.comments = commentsdetails;
	}

	private List<CommentsdetailsItem> comments;
	private int PostTimeFrame;
	private int Likes;
	private int PostMediaType;
	private int PostStatus;
	private String PostLatitude;



	public String getPostUserFName() {
		return PostUserFName;
	}

	public void setPostUserFName(String postUserFName) {
		PostUserFName = postUserFName;
	}

	private String PostUserFName;

	public String getPostUserLName() {
		return PostUserLName;
	}

	public void setPostUserLName(String postUserLName) {
		PostUserLName = postUserLName;
	}

	public String getPostUserProfileImage() {
		return PostUserProfileImage;
	}

	public void setPostUserProfileImage(String postUserProfileImage) {
		PostUserProfileImage = postUserProfileImage;
	}

	private String PostUserLName;
	private String PostUserProfileImage;

	public String getPostedUserName() {
		return PostedUserName;
	}

	public void setPostedUserName(String postedUserName) {
		PostedUserName = postedUserName;
	}

	public String getFeedtime() {
		return Feedtime;
	}

	public void setFeedtime(String feedtime) {
		Feedtime = feedtime;
	}

	private String PostedUserName;
	private String Feedtime;

	public int getReportStatus() {
		return ReportStatus;
	}

	public void setReportStatus(int reportStatus) {
		ReportStatus = reportStatus;
	}

	private int ReportStatus;

	public String getPostcreateddate() {
		return Postcreateddate;
	}

	public void setPostcreateddate(String postcreateddate) {
		Postcreateddate = postcreateddate;
	}

	private String Postcreateddate;

	public int getUserTotalLikesCount() {
		return UserTotalLikesCount;
	}

	public void setUserTotalLikesCount(int userTotalLikesCount) {
		UserTotalLikesCount = userTotalLikesCount;
	}

	private int UserTotalLikesCount;



}