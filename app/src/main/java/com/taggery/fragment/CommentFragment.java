package com.taggery.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.taggery.R;
import com.taggery.adapter.CommentAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.AddComment;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.CommentListResponse;
import com.taggery.model.CommentsdetailsItem;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.GetPostDetailResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentFragment extends BaseFragment {

    @BindView(R.id.list_comment)
    ListView listComment;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.edit_add_comment)
    CustomEditText editAddComment;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.relative_add_comment)
    ConstraintLayout relativeAddComment;
    @BindView(R.id.cons_comment)
    ConstraintLayout consComment;
    Unbinder unbinder;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    CommentListResponse getPostDetailResponse;
    ArrayList<CommentsdetailsItem> commentsdetailsItemArrayList;
    CommentAdapter commentAdapter;
    private int postID;
    CheckUserNameResponse checkUserNameResponse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comment, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        commentsdetailsItemArrayList = new ArrayList<CommentsdetailsItem>();
        Bundle bundle = getArguments();
        if (bundle != null)
            postID = bundle.getInt("postID");
        getCommentList(postID);
        picassoImageHolder(imageProPic, sharedPreferences.getString(TaggeryConstants.USER_PROFILE_PIC, ""),
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                getString(R.string.cloudinary_download_profile_picture));
        profileRingColor(sharedPreferences.getInt(TaggeryConstants.USER_LIKE_COUNT, 0), imageProPic);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.img_send)
    public void onViewClicked() {
        if (editAddComment.getText().length() > 0)
            addComment();
        else
            editAddComment.setError(getString(R.string.add_comment));
    }

    private void getCommentList(int postID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setPostID(postID);
            taggeryAPI.getCommentList(contactListInputParam).enqueue(new Callback<CommentListResponse>() {
                @Override
                public void onResponse(Call<CommentListResponse> call, Response<CommentListResponse> response) {
                    hideProgress();
                    getPostDetailResponse = response.body();
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (getPostDetailResponse.getStatusCode() == 200) {
                            if (getPostDetailResponse.getComments() != null &&
                                    getPostDetailResponse.getComments().size() > 0) {
                                consWarn.setVisibility(View.GONE);
                                listComment.setVisibility(View.VISIBLE);
                                commentsdetailsItemArrayList.addAll(getPostDetailResponse.getComments());
                                commentAdapter = new CommentAdapter(getActivity(), commentsdetailsItemArrayList);
                                listComment.setAdapter(commentAdapter);
                                listComment.smoothScrollToPosition(listComment.getAdapter().getCount() - 1);
                            } else {
                                listComment.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.No_comments));
                                imageWarnIcon.setImageResource(R.mipmap.icon_no_comment);
                            }

                        } else {
                            listComment.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }

                    } else {
                        listComment.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }

                }

                @Override
                public void onFailure(Call<CommentListResponse> call, Throwable t) {
                    hideProgress();
                    listComment.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }
            });


        } else {
            listComment.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }

    }


    private void addComment() {
        if (com.taggery.utils.Util.isNetworkAvailable()) {
            showProgress();
            AddComment addComment = new AddComment();
            addComment.setCommentPostID(postID);
            addComment.setCommentText(editAddComment.getText().toString());
            addComment.setCommentUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.addComment(addComment).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            commentsdetailsItemArrayList.add(new CommentsdetailsItem(
                                    editAddComment.getText().toString(),
                                    sharedPreferences.getString(TaggeryConstants.USER_PROFILE_PIC, ""),
                                    sharedPreferences.getInt(TaggeryConstants.USER_ID, 0),
                                    getString(R.string.now)));
                            commentAdapter.notifyDataSetChanged();
                            listComment.smoothScrollToPosition(listComment.getAdapter().getCount() - 1);
                            editAddComment.setText("");
                            showShackError(checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", relativeAddComment);
                        } else {
                            showShackError(getString(R.string.server_error), relativeAddComment);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), relativeAddComment);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeAddComment);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), relativeAddComment);
        }
    }
}
