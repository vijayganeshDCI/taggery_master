package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.taggery.model.ChatPojo;
import com.taggery.R;


import java.util.List;


 public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private List<ChatPojo> chatList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle = (TextView) view.findViewById(R.id.subtitle);
            time = (TextView) view.findViewById(R.id.time);
            img=(ImageView)view.findViewById(R.id.img);
            lin=view.findViewById(R.id.lin);
        }
    }


    public ChatAdapter(List<ChatPojo> chatList,Context context) {
        this.chatList = chatList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ChatPojo movie = chatList.get(position);
        holder.title.setText(movie.getTitle());
        holder.subtitle.setText(movie.getSubtitle());
        holder.time.setText(movie.getTime());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
       /* holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context, ChatActivity.class);
                context.startActivity(intent);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }
}