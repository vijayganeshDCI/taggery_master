package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.activity.GalleryPreviewActivity;
import com.taggery.model.FItem;
import com.taggery.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder>  {

    private List<FItem> image;
    private Context context;


    public StatusAdapter(List<FItem> status, Context context) {
        this.image = status;
        this.context = context;
    }

    @Override
    public StatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                 .inflate(R.layout.item_after_recent_list, parent, false);
        return new StatusAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StatusAdapter.ViewHolder holder, final int position) {

        FItem model = image.get(position);

        if (image.size()!=0){
            if (model.getPostMediaType()==1){
                holder.img_video.setVisibility(View.INVISIBLE);
                picassoImageHolder(holder.status_image, model.getPostImage(),
                        R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                        context.getString(R.string.cloudinary_download_post));
            }
            else if (model.getPostMediaType()==2){
                holder.img_video.setVisibility(View.VISIBLE);
                picassoImageHolder(holder.status_image, model.getPostImage()+".jpg",
                        R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                        context.getString(R.string.cloudinary_download_video_add_post));

            }

        }
        else {
            Toast.makeText(context, "no image", Toast.LENGTH_SHORT).show();

        }
        holder.status_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GalleryPreviewActivity.class);
                intent.putExtra("postID",image.get(position).getPostID());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return image.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public CircleImageView status_image;
        private ImageView img_video;
        public ViewHolder(View view)
        {
            super(view);
            status_image=view.findViewById(R.id.status_image);
            img_video = view.findViewById(R.id.img_video_icon);
        }
    }
    public void picassoImageHolder(final ImageView imageViewProPic, String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target=new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }
}
