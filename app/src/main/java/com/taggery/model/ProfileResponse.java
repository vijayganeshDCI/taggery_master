package com.taggery.model;

import android.widget.ListView;

import java.util.List;

public class ProfileResponse{
	private String Status;

	public List<ProfileUpload> getAfter12HrsRemaining() {
		return After12HrsRemaining;
	}

	public void setAfter12HrsRemaining(List<ProfileUpload> after12HrsRemaining) {
		After12HrsRemaining = after12HrsRemaining;
	}

	private List<ProfileUpload> After12HrsRemaining;
	private List<FItem> After12HrsTop3;
	private List<SuggestionItem> ourfriendrequests;
	private Profile profile;
	private int StatusCode;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	private String Message;

	public List<SuggestionItem> getOurfriendrequests() {
		return ourfriendrequests;
	}

	public void setOurfriendrequests(List<SuggestionItem> ourfriendrequests) {
		this.ourfriendrequests = ourfriendrequests;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}



	public List<FItem> getAfter12HrsTop3() {
		return After12HrsTop3;
	}

	public void setAfter12HrsTop3(List<FItem> after12HrsTop3) {
		this.After12HrsTop3 = after12HrsTop3;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}
}