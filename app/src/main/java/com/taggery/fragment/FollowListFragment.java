package com.taggery.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.adapter.FollowerListAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.ContactListResponse;
import com.taggery.model.SuggestionItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowListFragment extends BaseFragment {

    @BindView(R.id.image_app_logo)
    ImageView imageAppLogo;
    @BindView(R.id.text_label_title)
    CustomTextView textLabelTitle;
    @BindView(R.id.list_contacts)
    ListView listContacts;
    @BindView(R.id.swipe_contact_list)
    SwipeRefreshLayout swipeFollowList;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    @BindView(R.id.cons_follow_list)
    ConstraintLayout consFollowList;
    Unbinder unbinder;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    FollowerListAdapter followListAdapter;
    ArrayList<SuggestionItem> followItemArrayList;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.search_follow)
    SearchView searchFollow;
    private ContactListResponse contactListResponse;
    private BaseActivity loginActivity;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;
    boolean isSearch = false,backpress=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follow_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        followItemArrayList = new ArrayList<>();
        loginActivity = (BaseActivity) getActivity();
        followListAdapter = new FollowerListAdapter(getActivity(),
                followItemArrayList, loginActivity, 1);
        listContacts.setAdapter(followListAdapter);
        final Intent intent = getActivity().getIntent();
        if (intent != null) {
            if (intent.getIntExtra("follow", 0) == 1) {
                getFollowerList(1);
                textLabelTitle.setText(getString(R.string.Followers));
            } else {
                getFollowingList(1);
                textLabelTitle.setText(getString(R.string.following));
            }
        }


        swipeFollowList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeFollowList.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (intent.getIntExtra("follow", 0) == 1)
                            getFollowerList(1);
                        else
                            getFollowingList(1);
                        swipeFollowList.setRefreshing(false);
                    }
                }, 000);
            }
        });

        listContacts.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!isLoading && !reachedLastEnd) {
                    if ((visibleItemCount + firstVisibleItem) >= totalItemCount
                            && firstVisibleItem >= 0
                            && totalItemCount >= 29) {
                        if (intent.getIntExtra("follow", 0) == 1)
                            getFollowerList(++paginationCount);
                        else
                            getFollowingList(++paginationCount);
                    }
                }
            }
        });

        searchFollow.setQueryHint("search here");
        EditText searchEditText = (EditText) searchFollow.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        ImageView searchIcon = searchFollow.findViewById(android.support.v7.appcompat.R.id.search_button);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));
        searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.searchnew));
        searchFollow.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                backpress = true;
                return false;
            }
        });
       /* searchFollow.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus){
                    backpress = true;
                }
                else {
                    backpress = false;
                }

            }
        });*/
        searchFollow.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                filter("");
                return false;
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    if (backpress){
                        Log.i("backpress","filter");
                        filter("");
                        searchFollow.onActionViewCollapsed();
                        searchFollow.setVisibility(View.GONE);
                        backpress = false;
                    }
                    else {
                        Log.i("backpress","nofilter");
                        getActivity().finish();
                    }


                }
                return true;
            }
        });


        return view;
    }

    private void filter(String text) {
        if (followItemArrayList != null && followItemArrayList.size() != 0) {
            ArrayList<SuggestionItem> temp = new ArrayList();
            for (SuggestionItem d : followItemArrayList) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getUserFirstName().toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d);
                }
            }
            if (temp.size() != 0) {
                listContacts.setVisibility(View.VISIBLE);
                followListAdapter.updateList(temp);
            } else {
                listContacts.setVisibility(View.GONE);
                showShackError("no results",consFollowList);
            }

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoading = false;
        reachedLastEnd = false;
        unbinder.unbind();

    }


    private void getFollowingList(final int paginationCount) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = true;
            if (paginationCount == 1)
                followItemArrayList.clear();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setPage(paginationCount);
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.getFollowingList(contactListInputParam).enqueue(new Callback<ContactListResponse>() {
                @Override
                public void onResponse(Call<ContactListResponse> call, Response<ContactListResponse> response) {
                    hideProgress();
                    isLoading = false;
                    contactListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (contactListResponse.getStatusCode() == 200) {
                            if (contactListResponse.getFollowings() != null &&
                                    contactListResponse.getFollowings().size() != 0) {
                                listContacts.setVisibility(View.VISIBLE);
                                consWarn.setVisibility(View.GONE);
                                followItemArrayList.addAll(contactListResponse.getFollowings());
                                followListAdapter = new FollowerListAdapter(getActivity(),
                                        followItemArrayList, loginActivity, 2);
                                listContacts.setAdapter(followListAdapter);
                            } else {
                                listContacts.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.No_following));
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            }

                        } else if (contactListResponse.getStatusCode() == 501) {
                            if (paginationCount != 1) {
                                reachedLastEnd = true;
                                showShackError(contactListResponse.getMessage() != null ?
                                        contactListResponse.getMessage() : "", consFollowList);
                            } else {
                                listContacts.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.server_error));
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            }
                        } else {
                            listContacts.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }

                    } else {
                        listContacts.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }
                }

                @Override
                public void onFailure(Call<ContactListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    listContacts.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }
            });

        } else {
            listContacts.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }

    private void getFollowerList(final int paginationCount) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = true;
            if (paginationCount == 1)
                followItemArrayList.clear();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setPage(paginationCount);
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.getFollowerList(contactListInputParam).enqueue(new Callback<ContactListResponse>() {
                @Override
                public void onResponse(Call<ContactListResponse> call, Response<ContactListResponse> response) {
                    hideProgress();
                    isLoading = false;
                    contactListResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (contactListResponse.getStatusCode() == 200) {
                            if (contactListResponse.getFollowers() != null &&
                                    contactListResponse.getFollowers().size() != 0) {
                                listContacts.setVisibility(View.VISIBLE);
                                consWarn.setVisibility(View.GONE);
                                followItemArrayList.addAll(contactListResponse.getFollowers());
                                followListAdapter.notifyDataSetChanged();
                            } else {
                                listContacts.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.No_followers));
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            }

                        } else if (contactListResponse.getStatusCode() == 501) {
                            if (paginationCount != 1) {
                                reachedLastEnd = true;
                                showShackError(contactListResponse.getMessage() != null ?
                                        contactListResponse.getMessage() : "", consFollowList);
                            } else {
                                listContacts.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.server_error));
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            }
                        } else {
                            listContacts.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }

                    } else {
                        listContacts.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }
                }

                @Override
                public void onFailure(Call<ContactListResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    listContacts.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }
            });

        } else {
            listContacts.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }

    @OnClick(R.id.img_search)
    public void onViewClicked() {
        if (isSearch) {
            searchFollow.setVisibility(View.GONE);
            searchFollow.onActionViewCollapsed();
            isSearch = false;

        } else {
            searchFollow.setVisibility(View.VISIBLE);
            searchFollow.onActionViewExpanded();
            isSearch = true;
        }
    }
}
