package com.taggery.fragment;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.util.IOUtils;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.model.UserError;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vijayaganesh on 10/17/2017.
 */

public class BaseFragment extends Fragment {

    private static final float PICTURE_SIZE = 640;
    String blockCharacterSet = "~#^|$%&*!)(?/-+@:,.;'=_1234567890#{}[]";
    public AmazonS3Client s3Client;
    public BasicAWSCredentials credentials;
    public TransferUtility transferUtility;
    int REQUEST_STORAGE = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_SMS, Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.RECORD_AUDIO};
    public String[] imageFormat = {"tif", "tiff", "gif", "jpeg", "jpg", "jif", "jfif"
            , "jp2", "jpx", "j2k", "j2c"
            , "fpx", "pcd", "png", "pdf"};
    public String[] audioFormat = {"3gp", "aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb"
            , "amr", "ape", "au", "awb", "dct", "dss", "dvf", "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b",
            "m4p", "mmf", "mp3", "mpc", "msv", "nsf", "ogg", "opus", "ra", "raw", "sln", "tta"
            , "vox", "wav", "wma", "webm", "8svx"};
    public String[] audioimageFormat = {"tif", "tiff", "gif", "jpeg", "jpg", "jif", "jfif"
            , "jp2", "jpx", "j2k", "j2c"
            , "fpx", "pcd", "png", "pdf", "aa", "aac", "aax", "act", "aiff", "amr", "ape", "au", "awb"
            , "amr", "ape", "au", "awb", "dct", "dss", "dvf", "dvf", "flac", "gsm", "iklax", "ivs", "m4a", "m4b",
            "m4p", "mmf", "mp3", "mpc", "msv", "nsf", "ogg", "opus", "ra", "raw", "sln", "tta"
            , "vox", "wav", "wma", "webm", "8svx"};

    private boolean isRevealEnabled = true;
    private PickSetup setup;
    public String profileImageName = null, profileImagePath = null;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        credentials = new BasicAWSCredentials(getString(R.string.aws_access_key),
//                getString(R.string.aws_secret_key));
//        s3Client = new AmazonS3Client(credentials);
//        transferUtility =
//                TransferUtility.builder()
//                        .context(getActivity())
//                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
//                        .s3Client(s3Client)
//                        .build();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public void showProgress() {
        showProgress(R.string.loading);
    }

    public void showProgress(int stringId) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showProgress(stringId);
        }
    }

    public void hideProgress() {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideProgress();
        }
    }

    public void showError(int errorType, UserError error) {
        ((BaseActivity) getActivity()).showError(errorType, error);
    }

    public String parseOrderDate(long date) {
//        convert requried date format
        SimpleDateFormat mSdf = new SimpleDateFormat("yyyy-MM-dd");
//        getCurrentDate
        Calendar currentDate = Calendar.getInstance();
//        getDayBeforDate
        Calendar dayBeforeDate = Calendar.getInstance();
        dayBeforeDate.roll(Calendar.DATE, -1);
//        order date
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
//        setOrderDate in Calendar
        Calendar orderPlacedCalendar = Calendar.getInstance();
        Date orderPlacedDate = null;
        orderPlacedDate = calendar.getTime();
        orderPlacedCalendar.setTime(orderPlacedDate);
//        compare dates
        if (isSameDay(currentDate, orderPlacedCalendar)) {
            return "Today, " + mSdf.format(orderPlacedDate);
        } else if (isSameDay(dayBeforeDate, orderPlacedCalendar)) {
            return "Yesterday, " + mSdf.format(orderPlacedDate);
        } else {
            return "" + mSdf.format(orderPlacedDate);
        }
    }

    private boolean isSameDay(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }

    public boolean onBackPressed() {
        return false;
    }

    public static void setViewAndChildrenEnabled(View view, boolean enabled) {
        view.setEnabled(enabled);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                setViewAndChildrenEnabled(child, enabled);
            }
        }
    }

    public String getBase64Image(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new RectF(0, 0, PICTURE_SIZE, PICTURE_SIZE),
                Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] bytes = stream.toByteArray();
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }


    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public InputFilter editTextRestrictSmileys() {

        InputFilter restrictSmileys = new InputFilter() {

            public CharSequence filter(CharSequence src, int start,
                                       int end, Spanned dst, int dstart, int dend) {
                if (src.equals("")) { // for backspace
                    return src;
                }
                if (src.toString().matches("[\\x00-\\x7F]+")) {
                    return src;
                }
                return "";
            }
        };
        return restrictSmileys;
    }

    public InputFilter editTextRestrictLength() {

        InputFilter setLength = new InputFilter.LengthFilter(30);
        return setLength;
    }

    public InputFilter editTextEmailRestrictLength() {

        InputFilter setLength = new InputFilter.LengthFilter(30);
        return setLength;
    }

    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, REQUEST_STORAGE);
        }
    }


    public InputFilter editTextRestrictSpecialCharacters() {
        InputFilter restrictSpecialCharacter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source != null && blockCharacterSet.contains(("" + source))) {
                    return "";
                }
                return null;
            }
        };
        return restrictSpecialCharacter;
    }

    public void startCountAnimation(int count, final TextView textView) {
        ValueAnimator animator = ValueAnimator.ofInt(0, count);
        animator.setDuration(100);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                textView.setText(animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }

    public String getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);

        String ageS = ageInt.toString();

        return ageS;
    }

    public void picassoImageHolder(final ImageView imageViewProPic, String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                BitmapDrawable image = new BitmapDrawable(getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }

    public void universalImageLoader(ImageView imageView, String image, int loadingImage,
                                     int emptyURIImage) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(emptyURIImage)
                .showImageOnFail(emptyURIImage)
                .showImageOnLoading(loadingImage).build();
        imageLoader.displayImage(image, imageView, options);
    }

    //For Single Images
    public static void compressInputImage(Intent data, Context context, ImageView newIV) {
        Bitmap bitmap;
        Uri inputImageData = data.getData();
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), inputImageData);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
                newIV.setImageBitmap(bitmap);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
                newIV.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    //For Multiple Images
    public static Bitmap compressInputImage(Uri uri, Context context) {
        Bitmap bitmap = null;
        try {
            Bitmap bitmapInputImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() > 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1920, 1200, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() > 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, 1024, 1280, true);
            } else if (bitmapInputImage.getWidth() < 2048 && bitmapInputImage.getHeight() < 2048) {
                bitmap = Bitmap.createScaledBitmap(bitmapInputImage, bitmapInputImage.getWidth(), bitmapInputImage.getHeight(), true);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }
        return bitmap;
    }

    // UPDATED!
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET UserProfileUploadAfter12Hrs NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    public String getDateFormat(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    public String getDateFormatWitoutTime(String date) {
        SimpleDateFormat simpleDateFormat, simpleDateFormat1;
        Date dob = null;
        simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat = new SimpleDateFormat();
        try {
            dob = simpleDateFormat1.parse(date);
            simpleDateFormat.applyPattern("dd-MMM-yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return simpleDateFormat.format(dob);
    }

    //s3 upload image
    public void uploadFile(Uri uri, String s3bucketPath, String imageName, boolean isAudioFromRecord) {
        if (uri != null) {
            final File destination;
            if (!isAudioFromRecord)
                destination = new File(getActivity().getExternalFilesDir(null), imageName);
            else
                destination = new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString() +
                        "/Voice Recorder/RECORDING_"
                        + imageName);
            createFile(getActivity(), uri, destination);
            transferUtility =
                    TransferUtility.builder()
                            .context(getActivity())
                            .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                            .s3Client(s3Client)
                            .build();
            TransferObserver uploadObserver =
                    transferUtility.upload(s3bucketPath + imageName, destination);
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        destination.delete();
                    } else if (TransferState.FAILED == state) {
                        destination.delete();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                }

                @Override
                public void onError(int id, Exception ex) {
                    ex.printStackTrace();
                }

            });
        }
    }

    //to get path from uri
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    // to get URI from bitmap
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage
                (inContext.getContentResolver(), inImage, "Title", null);
        return path != null ? Uri.parse(path) : null;
    }

    //to get file name from URI
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    // to get file type
    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    // to create file
    public void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isAudioFile(String fileNamePath) {
        for (int i = 0; i < audioFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(audioFormat[i])) {
                return true;
            }
        }
        return false;
    }

//    public static boolean isAudioFile(String path) {
//        String mimeType = URLConnection.guessContentTypeFromName(path);
//        return mimeType != null && mimeType.endsWith(".mp3");
//    }

    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }


    public boolean isImageFile(String fileNamePath) {
        for (int i = 0; i < imageFormat.length; i++) {
            if (getFileExt(fileNamePath).equalsIgnoreCase(imageFormat[i])) {
                return true;
            }
        }
        return false;
    }

    public boolean getFileSize(Uri file) {
        File path = null;

        path = new File(file.getPath());


        long expectedSizeInMB = 20;
        long expectedSizeInBytes = 1024 * 1024 * expectedSizeInMB;

        long sizeInBytes = -1;
        sizeInBytes = path.length();

        if (sizeInBytes > expectedSizeInBytes) {
            System.out.println("Bigger than " + expectedSizeInMB + " MB");
            return false;
        } else {
            System.out.println("Not bigger than " + expectedSizeInMB + " MB");
            return true;
        }

    }

    //get Image Path
    public String getImagePath(Uri uri) {
        String path = null;
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            String document_id = cursor.getString(0);
            document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
            cursor.close();

            cursor = getActivity().getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            cursor.moveToFirst();
            path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();


        }

        return path;
    }

    public void showShackError(String errorMessage, ViewGroup view) {
        Snackbar snackbarError = Snackbar.make(view, errorMessage, Snackbar.LENGTH_SHORT);
        snackbarError.show();
    }

//    public void cloudinaryUpload(String path, String profileImageName, String folderName) {
//        showProgress();
//        initializeCloudinary();
//        MediaManager.get().upload(path)
//                .option("public_id", profileImageName)
//                .option("resource_type", "image")
//                .option("folder", folderName)
//                .callback(new UploadCallback() {
//                    @Override
//                    public void onStart(String requestId) {
//
//                    }
//
//                    @Override
//                    public void onProgress(String requestId, long bytes, long totalBytes) {
//                       /* Double progress = (double) bytes/totalBytes;
//                        String numberD = String.valueOf(progress);
//                        numberD = numberD.substring ( numberD.indexOf ( "." ) );
//                        Toast.makeText(getActivity(), numberD, Toast.LENGTH_SHORT).show();*/
//                    }
//
//                    @Override
//                    public void onSuccess(String requestId, Map resultData) {
//                        hideProgress();
//
//                        //  Toast.makeText(getActivity(), R.string.fileuploadedtocloud, Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onError(String requestId, ErrorInfo error) {
//                        hideProgress();
////                        Toast.makeText(getActivity(), error.getCode(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onReschedule(String requestId, ErrorInfo error) {
//                    }
//                }).dispatch();
//
//    }

    public void initializeCloudinary() {
        try {
            Map config = new HashMap();
            config.put("cloud_name", getString(R.string.cloud_name));
            config.put("api_key", getString(R.string.cloud_apikey));
            config.put("api_secret", getString(R.string.cloud_apisecret));
            MediaManager.init(getActivity(), config);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }


    public void pickImageFromGalleryandCamera(final ImageView imageProPic) {
        setup = new PickSetup()
                .setTitle(getString(R.string.choose_photo))
                .setProgressText(getString(R.string.processing))
                .setCancelText(getString(R.string.cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_camera).setWidth(100).setHeight(100);
        PickImageDialog.build(setup)
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.
                            imageProPic.setImageBitmap(r.getBitmap());
                            //Image path
                            //r.getPath();
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            profileImageName = "ProfileImg" + timeStamp;
                            profileImagePath = r.getPath();
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setOnPickCancel(new IPickCancel() {
                    @Override
                    public void onCancelClick() {
                    }
                }).show(getActivity().getSupportFragmentManager());

    }

    public String saveImage(Bitmap finalImage) {
        File destFile;
        String mediaPath = null;
        String state = Environment.getExternalStorageState();
        if (!state.equals(Environment.MEDIA_MOUNTED)) {
            return null;
        } else {
            File folder_gui1 = new File(Environment.getExternalStorageDirectory() + File.separator +
                    "Taggery" + File.separator + "Filter");

            if (!folder_gui1.exists()) {
                folder_gui1.mkdirs();

            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG" + timeStamp + "_";

            destFile = new File(folder_gui1, imageFileName + ".jpg");
            try {
                FileOutputStream out = new FileOutputStream(destFile);
                finalImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
                mediaPath = destFile.getAbsolutePath();
                galleryAddPic1(mediaPath);
                out.flush();
                out.close();
                //  Toast.makeText(getActivity(), "filtered image saved ", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mediaPath;
    }

    public void galleryAddPic1(String m1) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f1 = new File(m1);
        Uri contentUri = Uri.fromFile(f1);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public Bitmap getCompressedBitmap(String imagePath) {
        float maxHeight = 1920.0f;
        float maxWidth = 1080.0f;
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        /*ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

        byte[] byteArray = out.toByteArray();

        Bitmap updatedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        return updatedBitmap;
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public void exoVideoPlayer(String path, SimpleExoPlayerView videoPlayer, SimpleExoPlayer player) {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        //Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        //Initialize simpleExoPlayerView
        videoPlayer.setPlayer(player);
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(getActivity(),
                        com.google.android.exoplayer2.util.Util.getUserAgent(getActivity(), "CloudinaryExoplayer"));
        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        // This is the MediaSource representing the media to be played.
        Uri videoUri = Uri.parse(path);
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);
        LoopingMediaSource loopingSource = new LoopingMediaSource(videoSource);
        // Prepare the player with the source.
        player.prepare(videoSource);
    }


    public void profileRingColor(int count, CircleImageView circleImageView) {
        circleImageView.setBorderWidth(5);
        if (count >= 5000 && count < 10000) {
            circleImageView.setBorderColor(getResources().getColor(R.color.colorPrimaryDark));
        } else if (count >= 10000 && count < 15000) {
            circleImageView.setBorderColor(getResources().getColor(R.color.blue));
        } else if (count >= 15000 && count < 25000) {
            circleImageView.setBorderColor(getResources().getColor(R.color.gold));
        } else if (count >= 25000 && count < 50000) {
            circleImageView.setBorderColor(getResources().getColor(R.color.black));
        } else if (count >= 50000) {
            circleImageView.setBorderColor(getResources().getColor(R.color.white));
        } else {
            circleImageView.setBorderColor(getResources().getColor(R.color.transparent_white));
        }
    }


}
