package com.taggery.fragment;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.android.MediaManager;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;
import com.taggery.R;
import com.taggery.activity.LoginActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.RegistrationParam;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;
import com.taggery.view.CustomTextViewBold;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SigupFragment extends BaseFragment {

    @BindView(R.id.image_logo)
    ImageView imageLogo;
    @BindView(R.id.text_label_reg)
    CustomTextViewBold textLabelReg;
    @BindView(R.id.text_label_reg_fill)
    CustomTextView textLabelRegFill;
    @BindView(R.id.edit_first_name)
    CustomEditText editFirstName;
    @BindView(R.id.text_input_first_name)
    TextInputLayout textInputFirstName;
    @BindView(R.id.edit_last_name)
    CustomEditText editLastName;
    @BindView(R.id.text_input_last_name)
    TextInputLayout textInputLastName;
    @BindView(R.id.edit_user_name)
    CustomEditText editUserName;
    @BindView(R.id.text_input_user_name)
    TextInputLayout textInputUserName;
    @BindView(R.id.spinner_country_code)
    MaterialSpinner spinnerCountryCode;
    @BindView(R.id.edit_phone_number)
    CustomEditText editPhoneNumber;
    @BindView(R.id.text_input_phonenumber)
    TextInputLayout textInputPhonenumber;
    @BindView(R.id.spinner_gender)
    MaterialSpinner spinnerGender;
    @BindView(R.id.spinner_relation_ship)
    MaterialSpinner spinnerRelationShip;
    @BindView(R.id.edit_password)
    CustomEditText editPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout textInputPassword;
    @BindView(R.id.edit_confirm_password)
    CustomEditText editConfirmPassword;
    @BindView(R.id.text_input_confirm_password)
    TextInputLayout textInputConfirmPassword;
    @BindView(R.id.text_label_terms)
    CustomTextView textLabelTerms;
    @BindView(R.id.button_sign_up)
    CustomButton buttonSignUp;
    @BindView(R.id.text_label_already_have_acc)
    CustomTextView textLabelAlreadyHaveAcc;
    @BindView(R.id.text_label_login)
    CustomTextViewBold textLabelLogin;
    @BindView(R.id.cons_reg)
    ConstraintLayout consReg;
    @BindView(R.id.scroll_reg)
    ScrollView scrollReg;
    Unbinder unbinder;
    LoginActivity loginActivity;
    Bundle bundle;
    @BindView(R.id.spinner_country)
    CountryCodePicker spinnerCountry;
    @BindView(R.id.edit_email)
    CustomEditText editEmail;
    @BindView(R.id.text_input_email)
    TextInputLayout textInputEmail;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    private String mCountryIso, profileImageName, profileImagePath;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String countryCode, gender, relationShipStatus;
    private CheckUserNameResponse checkUserNameResponse;
    private boolean isUserNameAvailable, isPhoneNumberAvailable;
    private PickSetup setup;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        loginActivity = (LoginActivity) getActivity();
        editor = sharedPreferences.edit();
        initializeCloudinary();
        editFirstName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editLastName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editUserName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editPhoneNumber.setFilters(new InputFilter[]{editTextRestrictSmileys()});
        editPassword.setFilters(new InputFilter[]{editTextRestrictSmileys()});
        editConfirmPassword.setFilters(new InputFilter[]{editTextRestrictSmileys()});
        textInputPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        textInputConfirmPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        buttonSignUp.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        ArrayAdapter<String> adapterGender = new
                ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().
                getStringArray(R.array.gender));
        spinnerGender.setAdapter(adapterGender);
        ArrayAdapter<String> adapterRelationshipStatus = new
                ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().
                getStringArray(R.array.relation_ship_status));
        spinnerRelationShip.setAdapter(adapterRelationshipStatus);


        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getCount() > 0 && isAdded() && adapterView != null && view != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.white));
                gender = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerRelationShip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getCount() > 0 && isAdded() && adapterView != null && view != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.white));

                relationShipStatus = (String) adapterView.getSelectedItem();
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinnerCountry.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                countryCode = selectedCountry.getPhoneCode();
            }
        });
        countryCode = spinnerCountry.getDefaultCountryCode();
        editUserName.addTextChangedListener(userNameTextWatcher);
        editConfirmPassword.addTextChangedListener(confirmPasswordTextWatcher);
        editPhoneNumber.addTextChangedListener(phoneNumberTextWatcher);

        setup = new PickSetup()
                .setTitle(getString(R.string.choose_photo))
                .setProgressText(getString(R.string.processing))
                .setCancelText(getString(R.string.cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_camera).setWidth(100).setHeight(100);


        return view;


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_sign_up, R.id.text_label_login, R.id.image_pro_pic})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_sign_up:
                signupValidation();
                break;
            case R.id.text_label_login:
                loginActivity.push(new LoginFragment());
                loginActivity.popAllBackstack();
                break;
            case R.id.image_pro_pic:
                PickImageDialog.build(setup)
                        .setOnPickResult(new IPickResult() {
                            @Override
                            public void onPickResult(PickResult r) {
                                if (r.getError() == null) {
                                    //If you want the Uri.
                                    //Mandatory to refresh image from Uri.
                                    //getImageView().setImageURI(null);

                                    //Setting the real returned image.
                                    //getImageView().setImageURI(r.getUri());

                                    //If you want the Bitmap.
                                    imageProPic.setImageBitmap(r.getBitmap());
                                    //Image path
                                    //r.getPath();
                                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                                    profileImageName = "ProfileImg" + timeStamp;
                                    profileImagePath = r.getPath();
                                } else {
                                    //Handle possible errors
                                    //TODO: do what you have to do with r.getError();
                                    Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                                }

                            }
                        })
                        .setOnPickCancel(new IPickCancel() {
                            @Override
                            public void onCancelClick() {
                            }
                        }).show(getActivity().getSupportFragmentManager());
                break;
        }
    }


    private void signupValidation() {
        if (editFirstName.getText().toString().length() > 0 &&
                editLastName.getText().toString().length() > 0 &&
                editUserName.getText().toString().length() > 0 &&
                editPhoneNumber.getText().toString().length() > 0 &&
                editPassword.getText().toString().length() > 0 &&
                editConfirmPassword.getText().toString().length() > 0 &&
                spinnerCountryCode.getSelectedItemPosition() != 0 &&
                spinnerRelationShip.getSelectedItemPosition() != 0 &&
                spinnerGender.getSelectedItemPosition() != 0 &&
                editConfirmPassword.getText().toString().equals(editPassword.getText().toString()) &&
                editEmail.getText().toString().length() > 0 &&
                Util.isValidEmailAddress(editEmail.getText().toString()) && !isUserNameAvailable && !isPhoneNumberAvailable) {
            textInputFirstName.setErrorEnabled(false);
            textInputLastName.setErrorEnabled(false);
            textInputUserName.setErrorEnabled(false);
            textInputPhonenumber.setErrorEnabled(false);
            textInputPassword.setErrorEnabled(false);
            textInputEmail.setErrorEnabled(false);
            textInputConfirmPassword.setErrorEnabled(false);
            spinnerGender.setEnableErrorLabel(false);
            spinnerRelationShip.setEnableErrorLabel(false);
            spinnerCountryCode.setEnableErrorLabel(false);
            openActivity(getE164Number());


        } else {
            if (editFirstName.getText().toString().length() == 0)
                textInputFirstName.setError(getString(R.string.first_name_error));
            else
                textInputFirstName.setErrorEnabled(false);
            if (editLastName.getText().toString().length() == 0)
                textInputLastName.setError(getString(R.string.last_name_error));
            else
                textInputLastName.setErrorEnabled(false);

            if (editUserName.getText().toString().length() == 0)
                textInputUserName.setError(getString(R.string.user_name_error));
            else {
                if (isUserNameAvailable)
                    textInputUserName.setError(getString(R.string.user_name_vaild_error));
                else
                    textInputUserName.setErrorEnabled(false);
            }


            if (editPhoneNumber.getText().toString().length() == 0)
                textInputPhonenumber.setError(getString(R.string.phone_number_error));
            else {
                if (isPhoneNumberAvailable)
                    textInputPhonenumber.setError(getString(R.string.phone_number_available_error));
                else
                    textInputPhonenumber.setErrorEnabled(false);
            }

            if (editPassword.getText().toString().length() == 0)
                textInputPassword.setError(getString(R.string.password_error));
            else
                textInputPassword.setErrorEnabled(false);

            if (spinnerCountryCode.getSelectedItemPosition() == 0) {
                spinnerCountryCode.setError(getString(R.string.country_code_error));
            } else {
                spinnerCountryCode.setEnableErrorLabel(false);
            }
            if (spinnerRelationShip.getSelectedItemPosition() == 0) {
                spinnerRelationShip.setError(getString(R.string.relationship_status_error));
            } else {
                spinnerRelationShip.setEnableErrorLabel(false);
            }
            if (spinnerGender.getSelectedItemPosition() == 0) {
                spinnerGender.setError(getString(R.string.gender_error));
            } else {
                spinnerGender.setEnableErrorLabel(false);
            }
            if (editPassword.getText().toString().length() > 0 &&
                    editConfirmPassword.getText().toString().length() > 0 &&
                    !editConfirmPassword.getText().toString().equals(editPassword.getText().toString())) {
                textInputConfirmPassword.setError(getString(R.string.pass_mis_match_error));
            } else {
                if (editConfirmPassword.getText().toString().length() == 0)
                    textInputConfirmPassword.setError(getString(R.string.confirm_password_error));
                else
                    textInputConfirmPassword.setErrorEnabled(false);
            }
            if (editEmail.getText().toString().length() == 0) {
                textInputEmail.setError(getString(R.string.Email_error));
            } else {
                if (!Util.isValidEmailAddress(editEmail.getText().toString())) {
                    textInputEmail.setError(getString(R.string.Email_vaild_error));
                } else {
                    textInputEmail.setErrorEnabled(false);
                }
            }


        }
    }


    private void openActivity(String phoneNumber) {
        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phoneNumber", phoneNumber);
        bundle.putString("countryCode", countryCode);
        bundle.putString("firstName", editFirstName.getText().toString());
        bundle.putString("lastName", editLastName.getText().toString());
        bundle.putString("userName", editUserName.getText().toString());
        bundle.putString("gender", gender);
        bundle.putString("relationShipStatus", relationShipStatus);
        bundle.putString("password", editConfirmPassword.getText().toString());
        bundle.putString("email", editEmail.getText().toString());
        bundle.putString("profilePicture", profileImageName);
        bundle.putString("profilePicturePath", profileImagePath);
        bundle.putBoolean("forForgotPassword", false);
        otpVerificationFragment.setArguments(bundle);
        loginActivity.push(otpVerificationFragment, "otpVerificationFragment");
    }

    private String getE164Number() {
        return editPhoneNumber.getText().toString().replaceAll("\\D", "").trim();
        // return PhoneNumberUtils.formatNumberToE164(mPhoneNumber.getText().toString(), mCountryIso);
    }


    private void checkUserNameAvailable(final int type) {
        if (Util.isNetworkAvailable()) {
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setType(type);
            if (type == 1)
                registrationParam.setUserName(editUserName.getText().toString());
            else {
                registrationParam.setCountryCode("+" + countryCode);
                registrationParam.setPhoneNumber(editPhoneNumber.getText().toString());
            }

            taggeryAPI.checkUserName(registrationParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    checkUserNameResponse = response.body();
                    if (response.body() != null) {
                        if (response.code() == 200 && checkUserNameResponse.getStatusCode() == 200) {
                            //Not available
                            if (type == 1) {
                                //UserName check
                                isUserNameAvailable = false;
                                textInputUserName.setErrorEnabled(false);
                                editUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_correct, 0);
                                editUserName.setCompoundDrawablePadding(10);
                            } else {
                                //PhoeneNumber check
                                isPhoneNumberAvailable = false;
                                textInputPhonenumber.setErrorEnabled(false);
                                editPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_correct, 0);
                                editPhoneNumber.setCompoundDrawablePadding(10);
                            }
                        } else {
                            //Already available
                            if (type == 1) {
                                isUserNameAvailable = true;
                                editUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            } else {
                                isPhoneNumberAvailable = true;
                                editPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            }
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consReg);
                    }

                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    showShackError(getString(R.string.server_error), consReg);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), consReg);
        }
    }

    TextWatcher userNameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (count > 0) {
                checkUserNameAvailable(1);
            } else {
                editUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    TextWatcher phoneNumberTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (count > 0) {
                checkUserNameAvailable(2);
            } else {
                editPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher confirmPasswordTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (count > 0 && editConfirmPassword.getText().toString().equals(editPassword.getText().toString())) {
                editConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.icon_correct, 0);
            } else {
                editConfirmPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


}
