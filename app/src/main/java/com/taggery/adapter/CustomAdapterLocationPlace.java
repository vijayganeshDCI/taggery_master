package com.taggery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.taggery.R;
import com.taggery.model.AddressLocationModel;

import java.util.List;

import Utils.OnItemClickListener;

public class CustomAdapterLocationPlace extends RecyclerView.Adapter<CustomAdapterLocationPlace.ViewHolder> {

    private List<Place> locationModel;
    private Context context;
    private OnItemClickListener listener;

    public CustomAdapterLocationPlace(List<Place> locationModel, Context context) {
        this.locationModel = locationModel;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_locationplace, parent, false);

        return new CustomAdapterLocationPlace.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Place model = locationModel.get(position);
        holder.text_locationplace.setText(model.getName());
        holder.text_locationplace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnItemClick(position,view);
            }
        });

    }

    @Override
    public int getItemCount() {
        return locationModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView text_locationplace;
        public ViewHolder(View view)
        {
            super(view);
            text_locationplace=view.findViewById(R.id.text_locationplace);

        }
    }
    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
