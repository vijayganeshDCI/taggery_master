package com.taggery.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.taggery.R;
import com.taggery.adapter.LocationListAdapter;
import com.taggery.adapter.ProfileRecycleAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.LocationItem;
import com.taggery.model.SnapInputParam;
import com.taggery.model.SnapResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationSnapActivity extends BaseActivity implements OnMapReadyCallback {


    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    FusedLocationProviderClient mFusedLocationClient;
    @BindView(R.id.list_loaction_post)
    RecyclerView listLoactionPost;
    @BindView(R.id.cons_loaction)
    ConstraintLayout consLoaction;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    private double lat, lng;
    SnapResponse snapResponse;
    LocationListAdapter locationListAdapter;
    ArrayList<LocationItem> locationItemArrayList;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;
    private int visibleItemCount, totalItemCount, pastVisiblesItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_snap);
        ButterKnife.bind(this);
        TaggeryApplication.getContext().getComponent().inject(this);
        locationItemArrayList = new ArrayList<LocationItem>();

        final GridLayoutManager mLayoutManager = new GridLayoutManager(LocationSnapActivity.this, 4);
        listLoactionPost.setLayoutManager(mLayoutManager);
        listLoactionPost.setItemAnimator(new DefaultItemAnimator());
        listLoactionPost.setNestedScrollingEnabled(false);
        locationListAdapter = new LocationListAdapter(LocationSnapActivity.this,
                locationItemArrayList);
        listLoactionPost.setAdapter(locationListAdapter);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_location);
        mapFrag.getMapAsync(this);

        listLoactionPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                if (!isLoading && !reachedLastEnd) {
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount
                            && pastVisiblesItems >= 0
                            && totalItemCount >= 23) {
                        getLocationList(++paginationCount);
                    }
                }

            }

        });


    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mGoogleMap.setMyLocationEnabled(true);

            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //Place current location marker
                lat = location.getLatitude();
                lng = location.getLongitude();
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Current Position");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

                //move map camera
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
                getLocationList(1);
            }
        }
    };

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(LocationSnapActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                                Looper.myLooper());
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getLocationList(final int paginationCount) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            isLoading = true;
            if (paginationCount == 1)
                locationItemArrayList.clear();
            SnapInputParam snapInputParam = new SnapInputParam();
            snapInputParam.setLat(lat);
            snapInputParam.setLng(lng);
            snapInputParam.setPage(paginationCount);
            taggeryAPI.getLocationList(snapInputParam).enqueue(new Callback<SnapResponse>() {
                @Override
                public void onResponse(Call<SnapResponse> call, Response<SnapResponse> response) {
                    hideProgress();
                    isLoading = false;
                    snapResponse = response.body();
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (snapResponse.getStatusCode() == 200) {
                            if (snapResponse.getLocation() != null &&
                                    snapResponse.getLocation().size() > 0) {
                                locationItemArrayList.addAll(snapResponse.getLocation());
                                locationListAdapter.notifyDataSetChanged();
                            } else {
                                listLoactionPost.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                                textWarn.setText(snapResponse.getMessage());
                            }

                        } else if (snapResponse.getStatusCode() == 501) {
                            if (paginationCount != 1) {
                                reachedLastEnd = true;
                                showShackError(snapResponse.getMessage() != null ?
                                        snapResponse.getMessage() : "", consLoaction);
                            } else {
                                listLoactionPost.setVisibility(View.GONE);
                                consWarn.setVisibility(View.VISIBLE);
                                textWarn.setText(getString(R.string.server_error));
                                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            }
                        } else {
                            listLoactionPost.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }
                    } else {
                        listLoactionPost.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }
                }

                @Override
                public void onFailure(Call<SnapResponse> call, Throwable t) {
                    hideProgress();
                    isLoading = false;
                    listLoactionPost.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }
            });

        } else {
            listLoactionPost.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isLoading = false;
        reachedLastEnd = false;
    }
}
