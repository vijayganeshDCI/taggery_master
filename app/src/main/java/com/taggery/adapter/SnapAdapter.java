package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.jcminarro.roundkornerlayout.RoundKornerRelativeLayout;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.activity.LocationSnapActivity;
import com.taggery.activity.PreviewActivity;
import com.taggery.model.SnapListItemParam;
import com.taggery.model.SuggestionItem;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SnapAdapter extends RecyclerView.Adapter<SnapAdapter.SnapViewHolder> {


    public SnapAdapter(Context context, List<SnapListItemParam> snapListItemParamsList, BaseActivity baseActivity) {
        this.context = context;
        this.snapListItemParamsList = snapListItemParamsList;
        this.baseActivity = baseActivity;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        followListParamList = new ArrayList<SuggestionItem>();

    }

    Context context;
    List<SnapListItemParam> snapListItemParamsList;
    LayoutInflater mInflater;
    private LinearLayoutManager linearLayoutManager;
    List<SuggestionItem> followListParamList;
    FollowListAdapter followListAdapter;
    private BaseActivity baseActivity;


    @Override
    public SnapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_snaps_list, parent, false);
        return new SnapAdapter.SnapViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SnapViewHolder viewHolder, final int position) {


        switch (snapListItemParamsList.get(position).getSnapID()) {
            case 1:
                //Unseen post
                viewHolder.recyclerViewFollow.setVisibility(View.GONE);
                viewHolder.conSnapview.setVisibility(View.VISIBLE);
                viewHolder.imageProPic.setVisibility(View.VISIBLE);
                viewHolder.constraintLayoutLikeVisible.setVisibility(View.VISIBLE);
                viewHolder.textPostedName.setVisibility(View.VISIBLE);
                viewHolder.textLabelDate.setVisibility(View.VISIBLE);
                viewHolder.textLabelDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                viewHolder.textPostedName.setText(snapListItemParamsList.get(position).getFirstName());
                profileRingColor(snapListItemParamsList.get(position).getUserlikeCount(),viewHolder.imageProPic);

                if (snapListItemParamsList.get(position).getIsLiked() == 1) {
                    viewHolder.imageLike.setImageResource(R.mipmap.icon_heart_like);
                } else {
                    viewHolder.imageLike.setImageResource(R.mipmap.icon_unlike);
                }


                if (snapListItemParamsList.get(position).getMediaType() == 1) {
                    //image
                    viewHolder.imagePlay.setVisibility(View.GONE);
                    try {
                        picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap, snapListItemParamsList.get(position).getSnapPicture(),
                                R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder, 1,
                                context.getString(R.string.cloudinary_download_snap_image_add_post));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (snapListItemParamsList.get(position).getMediaType() == 2) {
                    //video
                    viewHolder.imagePlay.setVisibility(View.VISIBLE);
                    try {
                        picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap,
                                snapListItemParamsList.get(position).getSnapPicture() + ".jpg",
                                R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder, 1,
                                context.getString(R.string.cloudinary_download_snap_video_add_post));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    viewHolder.imagePlay.setVisibility(View.GONE);
                    try {
                        picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap, snapListItemParamsList.get(position).getSnapPicture(),
                                R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder, 1,
                                context.getString(R.string.cloudinary_download_snap_image_add_post));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap,
                            snapListItemParamsList.get(position).getUserProPic(),
                            R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder, 2,
                            context.getString(R.string.cloudinary_download_profile_picture));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                viewHolder.textLabelLikeCount.setText("" + snapListItemParamsList.get(position).getLikeCount());
                viewHolder.textLabelVisibleCount.setText("" + snapListItemParamsList.get(position).getViewCount());
                viewHolder.textLabelDate.setText("" + snapListItemParamsList.get(position).getPostedTime());
                break;
            case 2:
                //Suggestion
                linearLayoutManager = new LinearLayoutManager(context);
                linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                linearLayoutManager.setAutoMeasureEnabled(true);
                viewHolder.recyclerViewFollow.setLayoutManager(linearLayoutManager);
                viewHolder.recyclerViewFollow.setVisibility(View.VISIBLE);
                viewHolder.conSnapview.setVisibility(View.GONE);
                followListParamList.clear();
                followListParamList.addAll(snapListItemParamsList.get(position).getSuggestionItemList());
                followListAdapter = new FollowListAdapter(context, followListParamList, baseActivity);
                viewHolder.recyclerViewFollow.setAdapter(followListAdapter);
                viewHolder.imagePlay.setVisibility(View.GONE);
                break;
            case 3:
                //Location post
                viewHolder.recyclerViewFollow.setVisibility(View.GONE);
                viewHolder.conSnapview.setVisibility(View.VISIBLE);
                viewHolder.imageProPic.setVisibility(View.GONE);
                viewHolder.constraintLayoutLikeVisible.setVisibility(View.GONE);
                viewHolder.textPostedName.setVisibility(View.GONE);
                viewHolder.textLabelDate.setVisibility(View.VISIBLE);
                viewHolder.imagePlay.setVisibility(View.GONE);

                try {
                    picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap, snapListItemParamsList.get(position).getSnapPicture(),
                            R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder, 1,
                            context.getString(R.string.cloudinary_download_snap_image_add_post));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                viewHolder.textLabelDate.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_location, 0, 0, 0);
                viewHolder.textLabelDate.setCompoundDrawablePadding(10);
                if (snapListItemParamsList.get(position).getPostedTime()==null&&
                        snapListItemParamsList.get(position).getPostedTime().equals(""))
                viewHolder.textLabelDate.setText(context.getString(R.string.location));
                else
                viewHolder.textLabelDate.setText("" + snapListItemParamsList.get(position).getPostedTime());
                break;
            case 4:
                //Seen
                viewHolder.recyclerViewFollow.setVisibility(View.GONE);
                viewHolder.conSnapview.setVisibility(View.VISIBLE);
                viewHolder.imageProPic.setVisibility(View.VISIBLE);
                viewHolder.constraintLayoutLikeVisible.setVisibility(View.VISIBLE);
                viewHolder.textPostedName.setVisibility(View.VISIBLE);
                viewHolder.textLabelDate.setVisibility(View.VISIBLE);
                viewHolder.textLabelDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                viewHolder.textPostedName.setText(snapListItemParamsList.get(position).getFirstName());
                profileRingColor(snapListItemParamsList.get(position).getUserlikeCount(),viewHolder.imageProPic);

                if (snapListItemParamsList.get(position).getIsLiked() == 1) {
                    viewHolder.imageLike.setImageResource(R.mipmap.icon_heart_like);
                } else {
                    viewHolder.imageLike.setImageResource(R.mipmap.icon_unlike);
                }


                if (snapListItemParamsList.get(position).getMediaType() == 1) {
                    //image
                    viewHolder.imagePlay.setVisibility(View.GONE);
                    try {
                        picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap, snapListItemParamsList.get(position).getSnapPicture(),
                                R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder, 1,
                                context.getString(R.string.cloudinary_download_snap_image_add_post));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (snapListItemParamsList.get(position).getMediaType() == 2) {
                    //video
                    viewHolder.imagePlay.setVisibility(View.VISIBLE);
                    try {
                        picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap,
                                snapListItemParamsList.get(position).getSnapPicture() + ".jpg",
                                R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder, 1,
                                context.getString(R.string.cloudinary_download_snap_video_add_post));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    viewHolder.imagePlay.setVisibility(View.GONE);
                    try {
                        picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap, snapListItemParamsList.get(position).getSnapPicture(),
                                R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder, 1,
                                context.getString(R.string.cloudinary_download_snap_image_add_post));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    picassoImageHolder(viewHolder.imageProPic, viewHolder.roundCornerSnap,
                            snapListItemParamsList.get(position).getUserProPic(),
                            R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder, 2,
                            context.getString(R.string.cloudinary_download_profile_picture));
                } catch (Exception e) {
                    e.printStackTrace();
                }


                viewHolder.textLabelLikeCount.setText("" + snapListItemParamsList.get(position).getLikeCount());
                viewHolder.textLabelVisibleCount.setText("" + snapListItemParamsList.get(position).getViewCount());
                viewHolder.textLabelDate.setText("" + snapListItemParamsList.get(position).getPostedTime());
                break;
        }


        viewHolder.roundCornerSnap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (snapListItemParamsList.get(position).getReleatedPost() != null &&
                        snapListItemParamsList.get(position).getReleatedPost().size() > 0) {
                    if (snapListItemParamsList.get(position).getSnapID() != 3) {
                        Intent intent = new Intent(context, PreviewActivity.class);
                        Gson gson = new Gson();
                        String listValue = gson.toJson(snapListItemParamsList.get(position).getReleatedPost());
                        intent.putExtra("relatedPostList", listValue);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, LocationSnapActivity.class);
                        context.startActivity(intent);
                    }
                }
            }
        });

        viewHolder.imageProPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (snapListItemParamsList.get(position).getReleatedPost() != null &&
                        snapListItemParamsList.get(position).getReleatedPost().size() > 0) {
                    if (snapListItemParamsList.get(position).getSnapID() != 3) {
                        Intent intent = new Intent(context, PreviewActivity.class);
                        Gson gson = new Gson();
                        String listValue = gson.toJson(snapListItemParamsList.get(position).getReleatedPost());
                        intent.putExtra("relatedPostList", listValue);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, LocationSnapActivity.class);
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return snapListItemParamsList.size();
    }


    public class SnapViewHolder extends RecyclerView.ViewHolder {

        CustomTextView textPostedName;
        LinearLayout LinearPostcount;
        ImageView imageLike, imageSnapView;
        CustomTextView textLabelLikeCount;
        ImageView imageVisible, imagePlay;
        CustomTextView textLabelVisibleCount;
        CustomTextView textLabelDate;
        CircleImageView imageProPic;
        RecyclerView recyclerViewFollow;
        ConstraintLayout conSnapview, constraintLayoutLikeVisible;
        RoundKornerRelativeLayout roundCornerSnap;

        public SnapViewHolder(View itemView) {
            super(itemView);
            textPostedName = (CustomTextView) itemView.findViewById(R.id.text_posted_name);
            textLabelLikeCount = (CustomTextView) itemView.findViewById(R.id.text_label_like_count);
            textLabelVisibleCount = (CustomTextView) itemView.findViewById(R.id.text_label_visible_count);
            textLabelDate = (CustomTextView) itemView.findViewById(R.id.text_label_date);
            LinearPostcount = (LinearLayout) itemView.findViewById(R.id.Linear_postcount);
            imageLike = (ImageView) itemView.findViewById(R.id.image_like);
            imagePlay = (ImageView) itemView.findViewById(R.id.image_play);
            imageVisible = (ImageView) itemView.findViewById(R.id.image_visible);
            imageSnapView = (ImageView) itemView.findViewById(R.id.image_snap_view);
            imageProPic = (CircleImageView) itemView.findViewById(R.id.image_pro_pic);
            recyclerViewFollow = (RecyclerView) itemView.findViewById(R.id.recycler_follow);
            conSnapview = (ConstraintLayout) itemView.findViewById(R.id.cons_card_view);
            constraintLayoutLikeVisible = (ConstraintLayout) itemView.findViewById(R.id.cons_like_visible);
            roundCornerSnap = (RoundKornerRelativeLayout) itemView.findViewById(R.id.round_corner_snap);
        }
    }


    public void picassoImageHolder(final ImageView imageViewProPic,
                                   final RoundKornerRelativeLayout roundKornerRelativeLayout, String imageName, int loadingImage,
                                   int emptyURIImage, final int imageType, String imageUrl) {

        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                if (imageType == 1)
                    //Snap
                    roundKornerRelativeLayout.setBackground(image);
                else
                    //ProPic
                    imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                if (imageType == 1)
                    roundKornerRelativeLayout.setBackground(errorDrawable);
                else
                    imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (imageType == 1)
                    roundKornerRelativeLayout.setBackground(placeHolderDrawable);
                else
                    imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };

        roundKornerRelativeLayout.setTag(target);
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }

    public void profileRingColor(int count, CircleImageView circleImageView) {
        circleImageView.setBorderWidth(3);
        if (count >= 5000 && count < 10000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.colorPrimaryDark));
        } else if (count >= 10000 && count < 15000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.blue));
        } else if (count >= 15000 && count < 25000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.gold));
        } else if (count >= 25000 && count < 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.black));
        } else if (count >= 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.white));
        } else {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.transparent_white));
        }
    }

}
