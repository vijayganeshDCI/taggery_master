package com.taggery.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.cloudinary.Cloudinary;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.taggery.R;
import com.taggery.activity.ImageshowActivity;
import com.taggery.activity.MainActivity;
import com.taggery.adapter.CustomAdapterLocationName;
import com.taggery.adapter.PostDurationAdapter;
import com.taggery.adapter.TagListAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.AddpostInputParam;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.TagPeopleList;
import com.taggery.model.TimeFrameResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.yashoid.instacropper.InstaCropperView;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import Utils.OnItemClickListener;
import Utils.OnItemClickNew;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnItemClickListener, OnItemClickNew,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    @BindView(R.id.img_crop)
    ImageView imgCrop;
    @BindView(R.id.instacropper)
    InstaCropperView instacropper;
    @BindView(R.id.relative_top)
    RelativeLayout relativeTop;
    @BindView(R.id.text_tag)
    CustomTextView textTag;
    @BindView(R.id.text_addMore)
    CustomTextView textAddMore;
    @BindView(R.id.edit_input_msg)
    CustomEditText editInputMsg;
    @BindView(R.id.text_addLocation)
    CustomTextView textAddLocation;
    @BindView(R.id.view_location)
    View viewLocation;
    @BindView(R.id.recyclerview_location)
    RecyclerView recyclerviewLocation;
    @BindView(R.id.text_sharelocation)
    CustomTextView textSharelocation;
    @BindView(R.id.text_calendar)
    CustomTextView textCalendar;
    @BindView(R.id.text_timer)
    CustomTextView textTimer;
    @BindView(R.id.linear_timer)
    LinearLayout linearTimer;
    @BindView(R.id.view_sharepost)
    View viewSharepost;
    @BindView(R.id.img_post)
    ImageView imgPost;
    @BindView(R.id.text_post)
    CustomTextView textPost;
    @BindView(R.id.relative_post)
    RelativeLayout relativePost;
    @BindView(R.id.relative_bottom)
    RelativeLayout relativeBottom;
    @BindView(R.id.scrollView_parent)
    ScrollView scrollViewParent;
    Unbinder unbinder;
    @BindView(R.id.relative_videoview)
    RelativeLayout relativeVideoview;
    @BindView(R.id.recyclerview_postduration)
    RecyclerView recyclerviewPostduration;
    @BindView(R.id.text_postvisible)
    CustomTextView textPostvisible;
    @BindView(R.id.text_visibletime)
    CustomTextView textVisibletime;
    @BindView(R.id.relative_posttime)
    RelativeLayout relativePosttime;
    @BindView(R.id.view_visible_post)
    View viewVisiblePost;
    @BindView(R.id.video_player)
    SimpleExoPlayerView videoPlayer;
    SimpleExoPlayer player;
    @BindView(R.id.recyclerview_tagpeople)
    RecyclerView recyclerviewTagpeople;
    @BindView(R.id.linear_parent)
    LinearLayout linearParent;
    @BindView(R.id.progress_loadimg)
    ProgressBar progressLoadimg;
    @BindView(R.id.view_tagpeople)
    View viewTagpeople;
    @BindView(R.id.view_add_more)
    View viewAddMore;

    private LinearLayoutManager mLayoutManager;
    private CustomAdapterLocationName mAdapter;
    private List<Place> addressLocation = new ArrayList<>();

    protected PlaceDetectionClient placeDetectionClient;
    private int mYear, mMonth, mDay, mHour, mMinute;
    ImageshowActivity imageshowActivity;
    Bitmap filterimage;
    String mediaPath, mediaFilename;
    MediaController controller;
    private File imageFile, videofile;
    Cloudinary cloudinary;
    boolean isImage;
    int n = 1;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    private double mLat, mLng;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long INTERVAL = 1000 * 10;
    private static final int REQUEST_CHECK_SETTINGS = 1;
    private static final int AUTO_COMP_REQ_CODE = 2;
    private static final int PLACE_PICKER_REQUEST = 3;
    private LatLngBounds bounds;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private CheckUserNameResponse checkUserNameResponse;
    private Calendar calendardate, calendarTime, currentD, currentT;
    private SimpleDateFormat simpleDateFormat;
    private String postDate, postTime;
    private String postDateFormat;
    private int monthOfYear;
    private Date dob;
    private PostDurationAdapter postDurationAdapter;

    private TagListAdapter tagListAdapter;
    private List<Integer> postDuration = new ArrayList<>();
    LinkedHashSet<TagPeopleList> tagPeopleSet = TagFragment.tagPeopleStaticSelectedList;
    private List<TagPeopleList> tagPeopleList;
    private TimeFrameResponse timeFrameResponse;
    private int postDur;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        TaggeryApplication.getContext().getComponent().inject(this);
        unbinder = ButterKnife.bind(this, view);
//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        placeDetectionClient = Places.getPlaceDetectionClient(getActivity());
        simpleDateFormat = new SimpleDateFormat();
        textCalendar.setText(new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(new Date()));
        textTimer.setText(new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()));
        tagPeopleList = new ArrayList<TagPeopleList>();
        initiaieGoogleApiClient();
        imageshowActivity = (ImageshowActivity) getActivity();
        tagList();
        /*textAddLocation.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textAddLocation.setSingleLine(true);
        textAddLocation.setSelected(true);*/
        Bundle bundle = getArguments();
       // getPostDurationList();
        // locationData();

        if (getArguments() != null) {
            if (bundle.getBoolean("isImage")) {
                relativeVideoview.setVisibility(View.GONE);
                isImage = true;
                mediaPath = bundle.getString("mediaPath");
                mediaFilename = bundle.getString("mediaFilename");
                imageFile = new File(mediaPath);
                if (imageFile.exists()) {


                    ImageLoader imageLoader = ImageLoader.getInstance();
                    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                            .cacheOnDisk(true).resetViewBeforeLoading(true)
                            .build();
                    imageLoader.displayImage(getString(R.string.cloudinary_base_url)+getString(R.string.cloudinary_download_post) + mediaFilename, imgCrop, options);
                    imageLoader.loadImage(getString(R.string.cloudinary_base_url)+getString(R.string.cloudinary_download_post) + mediaFilename, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            super.onLoadingComplete(imageUri, view, loadedImage);
                            progressLoadimg.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            super.onLoadingStarted(imageUri, view);
                            progressLoadimg.setVisibility(View.VISIBLE);
                        }
                    });



                    /*picassoImageHolder(imgCrop,mediaFilename,
                            R.mipmap.icon_loading_64,R.mipmap.icon_no_image_64,getString(R.string.cloudinary_download_post)
                            );*/
                   /* Bitmap myBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    // instacropper.setImageUri(Uri.fromFile(new File(imageFile.getAbsolutePath())));
                    imgCrop.setImageBitmap(myBitmap);*/

                }
            } else {
                mediaPath = bundle.getString("mediaPath");
                mediaFilename = bundle.getString("mediaFilename");
                videofile = new File(mediaPath);
                isImage = false;
                relativeVideoview.setVisibility(View.VISIBLE);
                try {
                    initializePlayer(mediaPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return view;
    }

    private void tagList() {
        tagPeopleList.clear();
        Log.i("taglist", tagPeopleSet.toString());
        tagPeopleList.addAll(tagPeopleSet);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerviewTagpeople.setLayoutManager(mLayoutManager);
        recyclerviewTagpeople.setItemAnimator(new DefaultItemAnimator());
        tagListAdapter = new TagListAdapter(tagPeopleList, getActivity());
        tagListAdapter.setItemclickListener(PostFragment.this);
        recyclerviewTagpeople.setAdapter(tagListAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.text_addMore, R.id.text_addLocation, R.id.text_tag, R.id.linear_timer, R.id.relative_post})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_tag:
                tagPeople();
                break;
            case R.id.text_addLocation:
                pickYourPlace();
                break;
            case R.id.linear_timer:
                datePickerDialog();
                break;
            case R.id.relative_post:
                addPost();
                break;
        }
    }

    @Override
    public void OnItemClick(int pos, View view) {
        Place model = addressLocation.get(pos);
        textAddLocation.setText(model.getName());
        mLat = model.getLatLng().latitude;
        mLng = model.getLatLng().longitude;

    }

    @Override
    public void OnItemClickSecond(int position, View view) {
        postDur = postDuration.get(position);
        if (postDuration.get(position) == 0)
            textVisibletime.setText(getString(R.string.infinity));
        else
            textVisibletime.setText(postDuration.get(position) + " sec");
    }

    @Override
    public void OnItemClickNew(int pos, View view) {
        TagPeopleList model = tagPeopleList.get(pos);
        tagPeopleSet.remove(model);
        tagListAdapter.notifyDataSetChanged();
        tagList();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        startLocationUpdates();
                        getCurrentPlaceData();
                        // getLocationData();
                        break;
                    case Activity.RESULT_CANCELED:
                        getActivity().onBackPressed();
                        break;
                }
                break;
        }
//        if (requestCode == AUTO_COMP_REQ_CODE) {
//            if (resultCode == RESULT_OK) {
//                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
//               /* Toast.makeText(getActivity(), "place "+place.getName(),
//                        Toast.LENGTH_LONG).show();*/
//                textAddLocation.setText(place.getName());
//                Log.i("place", place.toString());
//            }
//        }

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(getActivity(), data);
            final String name = place.getName().toString();
            final String address = place.getAddress().toString();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            mLat = place.getLatLng().latitude;
            mLng = place.getLatLng().longitude;
            if (mLat != 0 && mLng != 0) {
                if (!name.contains("9"))
                    textAddLocation.setText(name);
                else
                    textAddLocation.setText("");
            } else
                textAddLocation.setText("");

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void initializePlayer(String mediaPath) {
        videoPlayer.setVisibility(View.VISIBLE);
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);


        //Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        player.setPlayWhenReady(true);
        player.setRepeatMode(2);

        //Initialize simpleExoPlayerView

        videoPlayer.setPlayer(player);


        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(getActivity(), com.google.android.exoplayer2.util.Util.getUserAgent(getActivity(), "CloudinaryExoplayer"));

        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        // This is the MediaSource representing the media to be played.
        Uri videoUri = Uri.parse(mediaPath);
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);
        LoopingMediaSource loopingSource = new LoopingMediaSource(videoSource);

        // Prepare the player with the source.
        player.prepare(videoSource);
    }

    private String getrTime() {
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();

        String aTime = new StringBuilder().append(today.year).append("-").append(today.month)
                .append("-").append(today.monthDay).append(" ").append(today.format("%k:%M")).toString();

        return aTime;

    }


    private void initiaieGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

//    private void getPostDurationList() {
//        if (Util.isNetworkAvailable()) {
//            showProgress();
//            postDuration.clear();
//            taggeryAPI.getTimeFrameList().enqueue(new Callback<TimeFrameResponse>() {
//                @Override
//                public void onResponse(Call<TimeFrameResponse> call, Response<TimeFrameResponse> response) {
//                    hideProgress();
//                    timeFrameResponse = response.body();
//                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
//                        if (timeFrameResponse.getTimeframe() != null
//                                && timeFrameResponse.getTimeframe().size() > 0) {
//                            postDuration.addAll(timeFrameResponse.getTimeframe());
//                            mLayoutManager = new LinearLayoutManager(getActivity(),
//                                    LinearLayoutManager.HORIZONTAL, false);
//                            recyclerviewPostduration.setLayoutManager(mLayoutManager);
//                            recyclerviewPostduration.setItemAnimator(new DefaultItemAnimator());
//                            postDurationAdapter = new PostDurationAdapter(postDuration, getActivity());
//                            postDurationAdapter.setItemclickListener(PostFragment.this);
//                            recyclerviewPostduration.setAdapter(postDurationAdapter);
//                        }
//                    } else {
//                        showShackError(getString(R.string.server_error), relativeParent);
//                    }
//
//                }
//
//                @Override
//                public void onFailure(Call<TimeFrameResponse> call, Throwable t) {
//                    hideProgress();
//                    showShackError(getString(R.string.server_error), relativeParent);
//
//                }
//            });
//        } else {
//            showShackError(getString(R.string.no_network), relativeParent);
//        }
//
//
//    }

    private void getCurrentPlaceData() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
       /* List<String> filters = new ArrayList<>();
        filters.add(String.valueOf(Place.TYPE_HOSPITAL));
        PlaceFilter placeFilter = new PlaceFilter(false, filters);*/
        Task<PlaceLikelihoodBufferResponse> placeResult = placeDetectionClient.getCurrentPlace(null);
        placeResult.addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {
                Log.d("loaction fragment", "current location places info");
                PlaceLikelihoodBufferResponse likelyPlaces = task.getResult();
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    addressLocation.add(placeLikelihood.getPlace().freeze());
                }
                likelyPlaces.release();
                try {
                    mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                    recyclerviewLocation.setLayoutManager(mLayoutManager);
                    recyclerviewLocation.setItemAnimator(new DefaultItemAnimator());
                    mAdapter = new CustomAdapterLocationName(addressLocation, getActivity());
                    mAdapter.setItemclickListener(PostFragment.this);
                    recyclerviewLocation.setAdapter(mAdapter);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    private void tagPeople() {
        if (mediaPath != null) {
            TagFragment tagFragment = new TagFragment();
            Bundle bundle = new Bundle();
            if (isImage) {
                bundle.putString("mediaPath", mediaPath);
                bundle.putBoolean("isImage", true);
            } else {
                bundle.putString("mediaPath", mediaPath);
                bundle.putBoolean("isImage", false);
            }
            tagFragment.setArguments(bundle);
            imageshowActivity.push(tagFragment, "TagFragment");
        }
    }


    private void pickYourPlace() {
        try {
            if (bounds != null) {
                PlacePicker.IntentBuilder intentBuilder =
                        new PlacePicker.IntentBuilder();
                intentBuilder.setLatLngBounds(bounds);
                Intent intent = intentBuilder.build(getActivity());
                startActivityForResult(intent, PLACE_PICKER_REQUEST);
            }

        } catch (GooglePlayServicesRepairableException
                | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    private void startLocationUpdates() {

        if (mGoogleApiClient != null) {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
            Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
            task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    // All location settings are satisfied. The client can initialize
                    // location requests here.
                    // ...
                    if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermission();
                        return;
                    }
                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
                    mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new
                            OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    if (location != null) {
                                        mLat = location.getLatitude();
                                        mLng = location.getLongitude();
//                                        Toast.makeText(getActivity(), mLat + "" + mLng, Toast.LENGTH_SHORT).show();
                                        getCurrentPlaceData();
                                        //   getLocationData();
                                    }
                                }
                            });
                    double radiusDegrees = 0.001;
                    LatLng center = new LatLng(mLat, mLng);
                    LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
                    LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
                    bounds = LatLngBounds.builder()
                            .include(northEast)
                            .include(southWest)
                            .build();
                }
            });

            task.addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ResolvableApiException) {
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(getActivity(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                    }
                }
            });
        }
    }


    private void addPost() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddpostInputParam addpostInputParam = new AddpostInputParam();
            addpostInputParam.setPostImage(mediaFilename);
            addpostInputParam.setPostLatitude(mLat);
            addpostInputParam.setPostLongitude(mLng);
            if (isImage)
                addpostInputParam.setMediaType(1);
            else
                addpostInputParam.setMediaType(2);
            addpostInputParam.setPostLocation(textAddLocation.getText().toString());
            List<Integer> tagListID = new ArrayList<Integer>();
            for (int i = 0; i < tagPeopleList.size(); i++) {
                tagListID.add(tagPeopleList.get(i).getId());
            }
            addpostInputParam.setPostTagList(tagListID);
            addpostInputParam.setPostText(editInputMsg.getText().toString());
            addpostInputParam.setPostTimeFrame(3000);
            addpostInputParam.setPostType(sharedPreferences.getInt(TaggeryConstants.USER_PROFILE_TYPE, 0));
            addpostInputParam.setPostUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            if (postDateFormat != null && postTime != null)
                addpostInputParam.setPostTime(postDateFormat + "" + postTime);
            else {
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
                try {
                    dob = simpleDateFormat1.parse(textCalendar.getText().toString());
                    simpleDateFormat.applyPattern("yyyy-MM-dd");
                    postDateFormat = simpleDateFormat.format(dob);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                addpostInputParam.setPostTime(postDateFormat + " " + textTimer.getText().toString());
            }
            Log.i("addpost", addpostInputParam.toString());

            taggeryAPI.addPost(addpostInputParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.code() == 200 && response.body() != null && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.putExtra("isFromAddpost", true);
                            intent.putExtra("Lat", mLat);
                            intent.putExtra("Lng", mLng);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            Toast.makeText(getActivity(), R.string.imageposted, Toast.LENGTH_SHORT).show();
                        } else {
                            showShackError(checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", linearParent);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), linearParent);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), linearParent);
                }
            });


        }
    }

    private void datePickerDialog() {
        currentD = Calendar.getInstance();
        DatePickerDialog datePickerDialog =
                newInstance(this,
                        currentD.get(Calendar.YEAR), currentD.get(Calendar.MONTH),
                        currentD.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
        datePickerDialog.setMinDate(currentD);
        datePickerDialog.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void timePickerDialog() {
        currentT = Calendar.getInstance();
        TimePickerDialog timePickerDialog =
                TimePickerDialog.newInstance(this,
                        currentT.get(Calendar.HOUR_OF_DAY), currentT.get(Calendar.MINUTE), currentT.get(Calendar.SECOND), false);
        timePickerDialog.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
        timePickerDialog.show(getActivity().getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendardate = Calendar.getInstance();
        calendardate.set(Calendar.MONTH, monthOfYear);
        calendardate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendardate.set(Calendar.YEAR, year);
        simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        postDate = simpleDateFormat.format(calendardate.getTime());
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            dob = simpleDateFormat1.parse(postDate);
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            postDateFormat = simpleDateFormat.format(dob);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        timePickerDialog();
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        calendarTime = Calendar.getInstance();
        calendarTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendarTime.set(Calendar.MINUTE, minute);
        simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        postTime = simpleDateFormat.format(calendarTime.getTime());
        textCalendar.setText(postDate);
        textTimer.setText(postTime);
    }


}

