package com.taggery.model;

public class StatusPojo {
    public StatusPojo(String statusimg) {
        this.statusimg = statusimg;

    }

    public String getStatusimg() {
        return statusimg;
    }

    public void setStatusimg(String statusimg) {
        this.statusimg = statusimg;
    }

    String statusimg;

}
