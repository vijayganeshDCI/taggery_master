package com.taggery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.taggery.R;

import java.util.List;

import Utils.OnItemClickListener;

public class PostDurationAdapter extends RecyclerView.Adapter<PostDurationAdapter.ViewHolder> {

    private List<Integer> postDuration;
    private Context context;
    private OnItemClickListener listener;

    public PostDurationAdapter(List<Integer> postDuration, Context context) {
        this.postDuration = postDuration;
        this.context = context;
    }


    @Override
    public PostDurationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_posttime, parent, false);

        return new PostDurationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostDurationAdapter.ViewHolder holder, final int position) {
        if (postDuration.get(position) == 0)
            holder.text_postname.setText(context.getString(R.string.infinity));
        else
            holder.text_postname.setText(postDuration.get(position)+" sec");

        holder.text_postname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnItemClickSecond(position, view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return postDuration.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text_postname;

        public ViewHolder(View view) {
            super(view);
            text_postname = view.findViewById(R.id.text_postname);

        }
    }

    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
