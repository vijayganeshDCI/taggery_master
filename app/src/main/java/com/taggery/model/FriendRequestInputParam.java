package com.taggery.model;

public class FriendRequestInputParam {

    public int getFollowerUserID() {
        return FollowerUserID;
    }

    public void setFollowerUserID(int followerUserID) {
        FollowerUserID = followerUserID;
    }

    public int getFollowingUserID() {
        return FollowingUserID;
    }

    public void setFollowingUserID(int followingUserID) {
        FollowingUserID = followingUserID;
    }

    private int FollowerUserID;
    private int FollowingUserID;

    public int getRequestAcceptStatus() {
        return RequestAcceptStatus;
    }

    public void setRequestAcceptStatus(int requestAcceptStatus) {
        RequestAcceptStatus = requestAcceptStatus;
    }

    private int RequestAcceptStatus;
}
