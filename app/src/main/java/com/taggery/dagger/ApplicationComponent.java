package com.taggery.dagger;


import android.content.SharedPreferences;

import com.taggery.activity.BaseActivity;
import com.taggery.activity.LocationSnapActivity;
import com.taggery.activity.ProfileUploadsActivity;
import com.taggery.adapter.AnalyticalAdapter;
import com.taggery.adapter.FollowerListAdapter;
import com.taggery.fragment.AnalyticalListFragment;
import com.taggery.fragment.FollowListFragment;
import com.taggery.fragment.GalleryPreviewFragment;
import com.taggery.activity.LoginActivity;
import com.taggery.activity.MainActivity;
import com.taggery.activity.ProfileDetailActivity;
import com.taggery.activity.SplashActivity;
import com.taggery.adapter.CommentAdapter;
import com.taggery.adapter.ContactListAdapter;
import com.taggery.adapter.FollowListAdapter;
import com.taggery.adapter.FriendRequestListAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.fragment.AddChatFragment;
import com.taggery.fragment.CommentFragment;
import com.taggery.fragment.ContactListFragment;
import com.taggery.fragment.EditProfileFragment;
import com.taggery.fragment.FilterFragment;
import com.taggery.activity.FriendRequestActivity;
import com.taggery.fragment.LoginFragment;
import com.taggery.fragment.OtpVerificationFragment;
import com.taggery.fragment.PostFragment;
import com.taggery.fragment.PreviewFragment;
import com.taggery.fragment.ProfileFragment;
import com.taggery.fragment.SettingsFragment;
import com.taggery.fragment.SigupFragment;
import com.taggery.fragment.SnapsFragment;
import com.taggery.fragment.StatsFragment;
import com.taggery.fragment.TagFragment;
import com.taggery.fragment.UpdatePasswordFragment;
import com.taggery.retrofit.RetrofitModule;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;


@Singleton
// modules for perform dependency injection on below classes
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    void inject(TaggeryApplication application);

    SharedPreferences sharedPreferences();

    Retrofit retrofit();

    void inject(BaseActivity baseActivity);

    void inject(SplashActivity splashActivity);

    void inject(LoginActivity loginActivity);

    void inject(MainActivity mainActivity);

    void inject(OtpVerificationFragment otpVerificationFragment);

    void inject(SigupFragment sigupFragment);

    void inject(LoginFragment loginFragment);

    void inject(UpdatePasswordFragment updatePasswordFragment);

    void inject(EditProfileFragment editProfileFragment);

    void inject(SettingsFragment settingsFragment);

    void inject(SnapsFragment snapsFragment);

    void inject(PostFragment postFragment);

    void inject(ContactListFragment contactListFragment);

    void inject(FilterFragment filterFragment);

    void inject(ContactListAdapter contactListAdapter);

    void inject(FollowListAdapter followListAdapter);

    void inject(ProfileFragment profileFragment);

    void inject(TagFragment tagFragment);

    void inject(AddChatFragment addChatFragment);
    void inject(PreviewFragment previewFragment);

    void inject(StatsFragment statsFragment);

    void inject(FriendRequestListAdapter friendRequestListAdapter);

    void inject(FriendRequestActivity friendRequestActivity);

    void inject(ProfileDetailActivity profileDetailActivity);

    void inject(GalleryPreviewFragment galleryPreviewFragment);

    void inject(CommentAdapter commentAdapter);

    void inject(CommentFragment commentFragment);

    void inject(AnalyticalAdapter analyticalAdapter);

    void inject(AnalyticalListFragment analyticalListFragment);

    void inject(FollowListFragment followListFragment);

    void inject(FollowerListAdapter followerListAdapter);

    void inject(LocationSnapActivity locationSnapActivity);

    void inject(ProfileUploadsActivity profileUploadsActivity);
}
