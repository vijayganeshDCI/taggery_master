package com.taggery.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taggery.activity.ChatActivity;
import com.taggery.adapter.ChannelAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.R;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.twilio.chat.CallbackListener;
import com.twilio.chat.Channel;
import com.twilio.chat.ChannelDescriptor;
import com.twilio.chat.Channels;
import com.twilio.chat.ChatClient;
import com.twilio.chat.ChatClientListener;
import com.twilio.chat.ErrorInfo;
import com.twilio.chat.Paginator;
import com.twilio.chat.StatusListener;
import com.twilio.chat.User;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Utils.OnItemClickListener;
import twilio.BasicChatClient;

import twilio.BindingSharedPreferences;
import twilio.ChannelManager;
import twilio.ChannelModel;
import twilio.RegistrationIntentService;

import static android.content.Context.MODE_PRIVATE;


public class ChannelFragment extends BaseFragment implements BasicChatClient.LoginListener, ChatClientListener , OnItemClickListener {

    private RecyclerView recyclerview;
    private ChannelAdapter mAdapter;
    private boolean isStarted;
    private boolean isVisible;
    private ImageView img_search,logo;
    private BasicChatClient basicClient;
    private Map<String, ChannelModel> channels = new HashMap<String, ChannelModel>();
    private List<ChannelModel> adapterContents = new ArrayList<>();
    private Channels channelsObject;
    private BasicChatClient basicChatClient;
    private ChannelManager channelManager;
    SwipeRefreshLayout swipe_chat;
    private String fcmToken;
    FloatingActionButton floating_add;
    private SharedPreferences fcmSharedPerf;
    public static final String BINDING_REGISTRATION = "BINDING_REGISTRATION";
    public static final String BINDING_SUCCEEDED = "BINDING_SUCCEEDED";
    public static final String BINDING_RESPONSE = "BINDING_RESPONSE";

    String SERVER_TOKEN_URL = getStringResource(R.string.serverToken);
    private String mIdentity = "UserAjith";
    private WakefulBroadcastReceiver bindingBroadcastReceiver;


    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {

            channelManager = ChannelManager.getInstance();
            checkTwilioClient();
            basicClient = TaggeryApplication.get().getBasicClient();
            basicClient.setClientListener(ChannelFragment.this);
            setupRecyclerView();
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {


            channelManager = ChannelManager.getInstance();
            checkTwilioClient();
            basicClient = TaggeryApplication.get().getBasicClient();
            basicClient.setClientListener(ChannelFragment.this);
            setupRecyclerView();
            isStarted = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_chat, container, false);
        recyclerview=(RecyclerView)view.findViewById(R.id.chat_recycle);
        swipe_chat = view.findViewById(R.id.swipe_chat);
        floating_add = view.findViewById(R.id.floating_add);
        swipe_chat.setColorSchemeResources(R.color.colorAccent);
        logo = view.findViewById(R.id.logo);
        img_search =view.findViewById(R.id.img_search);
        fcmSharedPerf = getActivity().getSharedPreferences(TaggeryConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        fcmToken = fcmSharedPerf.getString(TaggeryConstants.FCMTOKEN, "");


        bindingBroadcastReceiver = new WakefulBroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean succeeded = intent.getBooleanExtra(BINDING_SUCCEEDED, false);
                String message = intent.getStringExtra(BINDING_RESPONSE);
                if (message == null) {
                    message = "";
                }

                if (succeeded) {
                    Toast.makeText(getActivity(), "Binding registered"+message, Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), "Binding failed"+message, Toast.LENGTH_SHORT).show();

                }

            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(bindingBroadcastReceiver,
                new IntentFilter(BINDING_REGISTRATION));


        //  Chatdata();

        floating_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ChatActivity.class);
                i.putExtra("isFromFloating", true);
                getActivity().startActivity(i);
            }
        });
        swipe_chat.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getChannels();
                swipe_chat.setRefreshing(false);
            }
        });
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerBinding();
            }
        });


        return view;
    }

   /* @Override
    public void onResume() {
        super.onResume();
        getChannels();
    }*/
   public class MyStartServiceReceiver extends BroadcastReceiver {

       @Override
       public void onReceive(Context context, Intent intent) {

       }
   }

    private void registerBinding() {
        Intent intent = new Intent(getActivity(), RegistrationIntentService.class);
        intent.putExtra(BindingSharedPreferences.IDENTITY, mIdentity);
        getActivity().startService(intent);
    }

    private void checkTwilioClient() {
        if (Util.isNetworkAvailable()){
            basicChatClient = TaggeryApplication.get().getBasicClient();
            if (basicChatClient.getChatClient()==null){
                TaggeryApplication.get().getBasicClient().retrieveAccessTokenfromServer(SERVER_TOKEN_URL,mIdentity,ChannelFragment.this);
            }
            else {
                getChannels();
            }
        }
        else {
            Toast.makeText(getActivity(), R.string.network_error_message, Toast.LENGTH_SHORT).show();
        }

    }

    private void createChannelWithName(String name) {
        channelManager.createChannelWithName(name, new StatusListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getActivity(),"channel created", Toast.LENGTH_SHORT).show();
                getChannels();

            }

            @Override
            public void onError(ErrorInfo errorInfo) {
                Toast.makeText(getActivity(),errorInfo.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void deleteCurrentChannel(Channel channel) {
        channelManager.deleteChannelWithHandler(channel, new StatusListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getActivity(), "Channel deleted", Toast.LENGTH_SHORT).show();
                getChannels();
            }

            @Override
            public void onError(ErrorInfo errorInfo) {
                Toast.makeText(getActivity(), "Channel not deleted", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupRecyclerView() {
        recyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mAdapter = new ChannelAdapter( adapterContents,getActivity(),ChannelFragment.this);
        mAdapter.setItemclickListener(ChannelFragment.this);

        // attach the adapter to the recycler view
        recyclerview.setAdapter(mAdapter);
        // set layout manager to position the item
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));

    }
    private String getStringResource(int id) {
        Resources resources = TaggeryApplication.get().getResources();
        return resources.getString(id);
    }


    @Override
    public void onLoginStarted() {
        showProgress();
       // Toast.makeText(getActivity(), "login started", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginFinished() {
        hideProgress();
       // Toast.makeText(getActivity(), "login finished", Toast.LENGTH_SHORT).show();
        getChannels();

    }

    public void getChannels() {
        if (channels == null) return;
        if (basicClient == null || basicClient.getChatClient() == null) return;

        channelsObject = basicClient.getChatClient().getChannels();

        channels.clear();

        /*channelsObject.getPublicChannelsList(new CallbackListener<Paginator<ChannelDescriptor>>() {
            @Override
            public void onSuccess(Paginator<ChannelDescriptor> channelDescriptorPaginator) {

                getChannelsPage(channelDescriptorPaginator);
                *//*for (ChannelDescriptor channel : channelPaginator.getItems()) {

                    // add the channel to the list
                    mAdapterContents.add(channel);

                    Log.d(TAG, "Channel named: " + channel.getFriendlyName());
                    channel.getChannel(new CallbackListener<Channel>() {
                        @Override
                        public void onSuccess(Channel channel) {
                            Log.d(TAG, channel.getSid());
                        }
                    });

                }*//*
            }
        });*/

        channelsObject.getUserChannelsList(new CallbackListener<Paginator<ChannelDescriptor>>() {
            @Override
            public void onSuccess(Paginator<ChannelDescriptor> channelDescriptorPaginator) {
                getChannelsPage(channelDescriptorPaginator);
            }
        });
    }

    public void createChannelWithName(String name, final StatusListener handler) {
        this.channelsObject
                .channelBuilder()
                .withFriendlyName(name)
                .withType(Channel.ChannelType.PRIVATE)
                .build(new CallbackListener<Channel>() {
                    @Override
                    public void onSuccess(final Channel newChannel) {
                        handler.onSuccess();
                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                        handler.onError(errorInfo);
                    }
                });
    }


    private void getChannelsPage(Paginator<ChannelDescriptor> paginator){
        for (ChannelDescriptor cd : paginator.getItems()) {
            Log.e("Chatfrag","Adding channel descriptor for sid|"+cd.getSid()+"| friendlyName "+cd.getFriendlyName());
            channels.put(cd.getSid(), new ChannelModel(cd));
        }

        refreshChannelList();

        Log.e("HASNEXTPAGE", String.valueOf(paginator.getItems().size()));
        Log.e("HASNEXTPAGE", paginator.hasNextPage() ? "YES" : "NO");

        if (paginator.hasNextPage()) {
            paginator.requestNextPage(new CallbackListener<Paginator<ChannelDescriptor>>() {
                @Override
                public void onSuccess(Paginator<ChannelDescriptor> channelDescriptorPaginator) {
                    getChannelsPage(channelDescriptorPaginator);
                }
            });
        } else {
            // Get subscribed channels last - so their status will overwrite whatever we received
            // from public list. Ugly workaround for now.
            channelsObject = basicClient.getChatClient().getChannels();
            List<Channel> ch = channelsObject.getSubscribedChannels();
            for (Channel channel : ch) {
                channels.put(channel.getSid(), new ChannelModel(channel));
            }
            refreshChannelList();
        }

    }
    private void refreshChannelList()
    {
        adapterContents.clear();
        adapterContents.addAll(channels.values());
        Collections.sort(adapterContents, new CustomChannelComparator());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnItemClick(int pos, View view) {
        ChannelModel channelModel = adapterContents.get(pos);
        Log.d("ChannelsAdapter", channelModel.getSid());
        Intent i = new Intent(getActivity(), ChatActivity.class); // instead of v.getContext()->context can also be used
        i.putExtra(TaggeryConstants.EXTRA_CHANNEL_SID, channelModel.getSid());
        getActivity().startActivity(i);
    }

    private void showDialog(String delete, String copy, final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_alertdialog);

        TextView remove = (TextView) dialog.findViewById(R.id.text_removemsg);
        TextView copytext = dialog.findViewById(R.id.text_copytext);
        remove.setText(delete);
        copytext.setVisibility(View.GONE);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChannelModel channelModel = adapterContents.get(position);
                channelModel.getChannel(new CallbackListener<Channel>() {
                    @Override
                    public void onSuccess(Channel channel) {
                        deleteCurrentChannel(channel);
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                        dialog.dismiss();
                    }
                });
            }
        });

        dialog.show();
    }

    @Override
    public void OnItemClickSecond(int position, View view) {
        showDialog("Delete Chat","Copy text",position);
    }

    private class CustomChannelComparator implements Comparator<ChannelModel>
    {
        @Override
        public int compare(ChannelModel lhs, ChannelModel rhs)
        {
            return lhs.getFriendlyName().compareTo(rhs.getFriendlyName());
        }
    }

    @Override
    public void onLoginError(String errorMessage) {
        hideProgress();
        TaggeryApplication.get().showToast("Error logging in : " + errorMessage, Toast.LENGTH_LONG);

    }

    @Override
    public void onLogoutFinished() {
        TaggeryApplication.get().showToast("Log out finished");
    }



    @Override
    public void onChannelJoined(Channel channel) {

        Log.d("chat","Received onChannelJoined callback for channel |" + channel.getFriendlyName() + "|");
        channels.put(channel.getSid(), new ChannelModel(channel));
        refreshChannelList();
    }

    @Override
    public void onChannelInvited(Channel channel) {
        channels.put(channel.getSid(), new ChannelModel(channel));
        refreshChannelList();
    }

    @Override
    public void onChannelAdded(Channel channel) {
        Log.d("chat","Received onChannelAdd callback for channel |" + channel.getFriendlyName() + "|");
        channels.put(channel.getSid(), new ChannelModel(channel));
        refreshChannelList();
    }

    @Override
    public void onChannelUpdated(Channel channel, Channel.UpdateReason updateReason) {
        Log.d("chat","Received onChannelChange callback for channel |" + channel.getFriendlyName()
                + "| with reason " + updateReason.toString());
        channels.put(channel.getSid(), new ChannelModel(channel));
        refreshChannelList();
    }

    @Override
    public void onChannelDeleted(Channel channel) {
        Log.d("chat","Received onChannelDelete callback for channel |" + channel.getFriendlyName() + "|");
        channels.remove(channel.getSid());
        refreshChannelList();
    }

    @Override
    public void onChannelSynchronizationChange(Channel channel) {
        Log.d("chat","Received onChannelSynchronizationChange callback for channel |"
                + channel.getFriendlyName()
                + "| with new status " + channel.getStatus().toString());
        refreshChannelList();
    }

    @Override
    public void onError(ErrorInfo errorInfo) {
        TaggeryApplication.get().showError("Received error", errorInfo);
    }

    @Override
    public void onUserUpdated(User user, User.UpdateReason updateReason) {
        Log.d("chat","Received onUserUpdated callback for "+updateReason.toString());

    }

    @Override
    public void onUserSubscribed(User user) {
        Log.d("chat","Received onUserSubscribed callback");    }

    @Override
    public void onUserUnsubscribed(User user) {
        Log.d("chat","Received onUserUnsubscribed callback");
    }

    @Override
    public void onClientSynchronization(ChatClient.SynchronizationStatus synchronizationStatus) {
        Log.d("chat","Received onClientSynchronization callback " + synchronizationStatus.toString());

    }

    @Override
    public void onNewMessageNotification(String s, String s1, long l) {
        TaggeryApplication.get().showToast("Received new push notification");
    }

    @Override
    public void onAddedToChannelNotification(String s) {

    }

    @Override
    public void onInvitedToChannelNotification(String s) {

    }

    @Override
    public void onRemovedFromChannelNotification(String s) {

    }

    @Override
    public void onNotificationSubscribed() {
        TaggeryApplication.get().showToast("Subscribed to push notifications");
    }

    @Override
    public void onNotificationFailed(ErrorInfo errorInfo) {
        TaggeryApplication.get().showError("Failed to subscribe to push notifications", errorInfo);
    }

    @Override
    public void onConnectionStateChange(ChatClient.ConnectionState connectionState) {
       // TaggeryApplication.get().showToast("Transport state changed to "+connectionState.toString());

    }
}
