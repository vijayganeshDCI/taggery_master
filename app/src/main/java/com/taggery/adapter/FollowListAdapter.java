package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.activity.ProfileDetailActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.fragment.ProfileFragment;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.FriendRequestInputParam;
import com.taggery.model.SuggestionItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomTextView;

import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowListAdapter extends RecyclerView.Adapter<FollowListAdapter.FollowListViewHolder> {


    public FollowListAdapter(Context context, List<SuggestionItem> followListParamList, BaseActivity baseActivity) {
        this.context = context;
        this.followListParamList = followListParamList;
        this.baseActivity = baseActivity;
        TaggeryApplication.getContext().getComponent().inject(this);
    }


    private CheckUserNameResponse checkUserNameResponse;
    Context context;
    List<SuggestionItem> followListParamList;
    BaseActivity baseActivity;
    private View itemView;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;

    @Override
    public FollowListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_follow_list, parent, false);
        return new FollowListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowListViewHolder holder, final int position) {
        holder.buttonFollow.setTypeface(Typeface.createFromAsset(context.getAssets(), "fontName/Montserrat-Regular.ttf"));
        holder.textViewUserName.setText(followListParamList.get(position).getUserFirstName());
        profileRingColor(followListParamList.get(position).getUserTotalLikesCount(), holder.imageProPic);

        try {
            baseActivity.picassoImageHolder(holder.imageProPic, followListParamList.get(position).getUserPicture(),
                    R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                    context.getString(R.string.cloudinary_download_profile_picture));
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (followListParamList.get(position).getStatus()) {
//            1->requested, 2->following, 3->follow
            case "1":
                buttonFollowStyle(holder.buttonFollow, context.getString(R.string.requested),
                        context.getResources().getColor(R.color.white),
                        context.getResources().getColor(R.color.colorPrimaryDark), true);
                break;
            case "2":
                buttonFollowStyle(holder.buttonFollow, context.getString(R.string.following),
                        context.getResources().getColor(R.color.colorPrimaryDark),
                        context.getResources().getColor(R.color.white), true);
                break;
            case "3":
                buttonFollowStyle(holder.buttonFollow, context.getString(R.string.follow),
                        context.getResources().getColor(R.color.colorPrimaryDark),
                        context.getResources().getColor(R.color.white), true);
                break;
            default:
                break;


        }

        holder.buttonFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (followListParamList.get(position).getStatus()) {
                    case "1":
                        //requested
                        alertDialog(1, position, holder);
                        break;
                    case "2":
                        //following
                        alertDialog(2, position, holder);
                        break;
                    case "3":
                        //follow
                        friendRequest(holder.constraintFollowList,
                                followListParamList.get(position).getId(), holder.buttonFollow,position);
                        break;
                    default:
                        break;


                }
            }
        });

        holder.imageProPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileDetailActivity.class);
                intent.putExtra("postUserID", followListParamList.get(position).getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return followListParamList.size();
    }

    public class FollowListViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imageProPic;
        CustomButton buttonFollow;
        ConstraintLayout constraintFollowList;
        CustomTextView textViewUserName;

        public FollowListViewHolder(View itemView) {
            super(itemView);
            imageProPic = (CircleImageView) itemView.findViewById(R.id.image_pro_pic);
            textViewUserName = (CustomTextView) itemView.findViewById(R.id.text_user_name);
            buttonFollow = (CustomButton) itemView.findViewById(R.id.button_follow);
            constraintFollowList = (ConstraintLayout) itemView.findViewById(R.id.cons_item_follow_list);
        }
    }

    private void buttonFollowStyle(CustomButton button, String status, int bgColor, int bgTextColor, boolean isEnable) {
        button.setText(status);
        button.setTextColor(bgTextColor);
        button.setEnabled(isEnable);
        buttonShapeColor(bgColor, button);
    }

    private void buttonShapeColor(int color, CustomButton customButton) {
        ShapeDrawable footerBackground = new ShapeDrawable();
        float[] radii = new float[8];
        for (int i = 0; i < 8; i++) {
            radii[i] = 20;
        }
        footerBackground.setShape(new RoundRectShape(radii, null, null));
        footerBackground.getPaint().setColor(color);
        customButton.setBackgroundDrawable(footerBackground);
    }

    public void profileRingColor(int count, CircleImageView circleImageView) {
        circleImageView.setBorderWidth(3);
        if (count >= 5000 && count < 10000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.colorPrimaryDark));
        } else if (count >= 10000 && count < 15000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.blue));
        } else if (count >= 15000 && count < 25000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.gold));
        } else if (count >= 25000 && count < 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.black));
        } else if (count >= 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.white));
        } else {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.transparent_white));
        }
    }

    public void friendRequest(final ConstraintLayout consItemContactList,
                              int id, final CustomButton button, final int position) {

        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            FriendRequestInputParam friendRequestInputParam = new FriendRequestInputParam();
            friendRequestInputParam.setFollowerUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            friendRequestInputParam.setFollowingUserID(id);
            taggeryAPI.friendRequest(friendRequestInputParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    baseActivity.hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            if (checkUserNameResponse.getRequestAcceptStatus() == 1) {
                                //requested
                                buttonFollowStyle(button, context.getString(R.string.requested),
                                        context.getResources().getColor(R.color.white),
                                        context.getResources().getColor(R.color.colorPrimaryDark), true);
                                followListParamList.get(position).setStatus("1");
                            } else if (checkUserNameResponse.getRequestAcceptStatus() == 2) {
                                // following
                                buttonFollowStyle(button, context.getString(R.string.following),
                                        context.getResources().getColor(R.color.colorPrimaryDark),
                                        context.getResources().getColor(R.color.white), true);
                                followListParamList.get(position).setStatus("2");
                            } else if (checkUserNameResponse.getRequestAcceptStatus() == 3) {
                                //follow
                                buttonFollowStyle(button, context.getString(R.string.follow),
                                        context.getResources().getColor(R.color.colorPrimaryDark),
                                        context.getResources().getColor(R.color.white), true);
                                followListParamList.get(position).setStatus("3");
                            }

                        } else {
                            Snackbar snackbarError = Snackbar.make(consItemContactList,
                                    context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                            snackbarError.show();
                        }
                    } else {
                        Snackbar snackbarError = Snackbar.make(consItemContactList,
                                context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                        snackbarError.show();
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Snackbar snackbarError = Snackbar.make(consItemContactList,
                            context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                    snackbarError.show();
                }
            });

        } else {
            Snackbar snackbarError = Snackbar.make(consItemContactList,
                    context.getString(R.string.no_network), Snackbar.LENGTH_SHORT);
            snackbarError.show();
        }
    }

    private void alertDialog(int status, final int position, final FollowListViewHolder holder) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        View dialogView = itemView.inflate(context, R.layout.alert_unfollow, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        ContactListAdapter.dialogViewHolder viewHolder = new ContactListAdapter.dialogViewHolder(dialogView);
        viewHolder.buttonOk.setTypeface(Typeface.createFromAsset(context.getAssets(),
                "fontName/Montserrat-Regular.ttf"));
        viewHolder.buttonCancel.setTypeface(Typeface.createFromAsset(context.getAssets(),
                "fontName/Montserrat-Regular.ttf"));
        try {
            baseActivity.picassoImageHolder(viewHolder.imageProPic,
                    followListParamList.get(position).getUserPicture(),
                    R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                    context.getString(R.string.cloudinary_download_profile_picture));
        } catch (Exception e) {
            e.printStackTrace();
        }
        profileRingColor(followListParamList.get(position).getUserTotalLikesCount(), viewHolder.imageProPic);
        if (status == 1) {
            //requested
            viewHolder.textUserName.setText(context.getString(R.string.Confirm_to_back_request));
            viewHolder.buttonOk.setText(context.getString(R.string.Undo));
        } else {
            //following
            viewHolder.textUserName.setText(context.getString(R.string.Confirm_to_unfollow_));
            viewHolder.buttonOk.setText(context.getString(R.string.UnFollow));
        }

        viewHolder.buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                friendRequest(holder.constraintFollowList,
                        followListParamList.get(position).getId(), holder.buttonFollow,position);
            }
        });
        viewHolder.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


}
