package com.taggery.model;

import java.util.List;

public class LocationItem {
    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getPostLocation() {
        return PostLocation;
    }

    public void setPostLocation(String postLocation) {
        PostLocation = postLocation;
    }

    public String getPostType() {
        return PostType;
    }

    public void setPostType(String postType) {
        PostType = postType;
    }



    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getPostID() {
        return PostID;
    }

    public void setPostID(int postID) {
        PostID = postID;
    }

    public int getPostUserID() {
        return PostUserID;
    }

    public void setPostUserID(int postUserID) {
        PostUserID = postUserID;
    }

    public String getPostTagList() {
        return PostTagList;
    }

    public void setPostTagList(String postTagList) {
        PostTagList = postTagList;
    }

    public String getPostLongitude() {
        return PostLongitude;
    }

    public void setPostLongitude(String postLongitude) {
        PostLongitude = postLongitude;
    }

    public String getPostText() {
        return PostText;
    }

    public void setPostText(String postText) {
        PostText = postText;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostTimeFrame() {
        return PostTimeFrame;
    }

    public void setPostTimeFrame(int postTimeFrame) {
        PostTimeFrame = postTimeFrame;
    }

    public String getPostcreateddate() {
        return Postcreateddate;
    }

    public void setPostcreateddate(String postcreateddate) {
        Postcreateddate = postcreateddate;
    }

    public int getPostStatus() {
        return PostStatus;
    }

    public void setPostStatus(int postStatus) {
        PostStatus = postStatus;
    }

    public String getPostLatitude() {
        return PostLatitude;
    }

    public void setPostLatitude(String postLatitude) {
        PostLatitude = postLatitude;
    }

    private String PostImage;
    private String PostLocation;
    private String PostType;

    public int getSnapID() {
        return SnapID;
    }

    public void setSnapID(int snapID) {
        SnapID = snapID;
    }

    private int SnapID;
    private String created_at;
    private int PostID;
    private int PostUserID;
    private String PostTagList;
    private String PostLongitude;
    private String PostText;
    private String updated_at;
    private int id;
    private int PostTimeFrame;
    private String Postcreateddate;
    private int PostStatus;
    private String PostLatitude;

    public int getPostMediaType() {
        return PostMediaType;
    }

    public void setPostMediaType(int postMediaType) {
        PostMediaType = postMediaType;
    }

    private int PostMediaType;

    public List<PostDetails> getRelatedPostsID() {
        return RelatedPostsID;
    }

    public void setRelatedPostsID(List<PostDetails> relatedPostsID) {
        RelatedPostsID = relatedPostsID;
    }

    private List<PostDetails> RelatedPostsID;
}
