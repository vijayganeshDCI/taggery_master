package com.taggery.model;

public class AnalyticalUserDetail {

    public String getUserPicture() {
        return UserPicture;
    }

    public void setUserPicture(String userPicture) {
        UserPicture = userPicture;
    }

    public int getActuallike() {
        return Actuallike;
    }

    public void setActuallike(int actuallike) {
        Actuallike = actuallike;
    }

    public int getLikes() {
        return Likes;
    }

    public void setLikes(int likes) {
        Likes = likes;
    }

    public int getKadd1() {
        return kadd1;
    }

    public void setKadd1(int kadd1) {
        this.kadd1 = kadd1;
    }

    public int getNearBigValue() {
        return NearBigValue;
    }

    public void setNearBigValue(int nearBigValue) {
        NearBigValue = nearBigValue;
    }

    public int getKadd2() {
        return kadd2;
    }

    public void setKadd2(int kadd2) {
        this.kadd2 = kadd2;
    }

    private String UserPicture;
    private int Actuallike;
    private int Likes;
    private int kadd1;
    private int NearBigValue;
    private int kadd2;

    public int getUserTotalLikesCount() {
        return UserTotalLikesCount;
    }

    public void setUserTotalLikesCount(int userTotalLikesCount) {
        UserTotalLikesCount = userTotalLikesCount;
    }

    private int UserTotalLikesCount;

}
