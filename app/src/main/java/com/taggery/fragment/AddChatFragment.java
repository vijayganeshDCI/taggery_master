package com.taggery.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.activity.ChatActivity;
import com.taggery.adapter.ChatListAdapter;
import com.taggery.adapter.TagListAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.TagInputParam;
import com.taggery.model.TagPeopleList;
import com.taggery.model.TagPeopleResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import Utils.ChatCallback;
import Utils.OnItemClickListener;
import Utils.OnItemClickNew;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddChatFragment extends BaseFragment implements OnItemClickListener, OnItemClickNew, ChatCallback {


    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.text_chatuser)
    CustomTextView textChatuser;
    @BindView(R.id.relative_newmessage)
    RelativeLayout relativeNewmessage;
    @BindView(R.id.text_to)
    CustomTextView textTo;
    @BindView(R.id.recyclerview_topeople)
    RecyclerView recyclerviewTopeople;
    @BindView(R.id.relative_tomessage)
    RelativeLayout relativeTomessage;
    @BindView(R.id.recyclerview_listpeople)
    RecyclerView recyclerviewListpeople;
    Unbinder unbinder;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    RelativeLayout consWarn;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    TagPeopleResponse tagPeopleResponse;
    @BindView(R.id.relative_chat)
    RelativeLayout relativeChat;
    private ChatListAdapter mAdapter;
    TagListAdapter tagListAdapter;
    List<TagPeopleList> finalchatlist = new ArrayList<>();
    List<TagPeopleResponse.Friend> tagPeople = new ArrayList<>();
    String name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_chat, container, false);
        unbinder = ButterKnife.bind(this, view);

        TaggeryApplication.getContext().getComponent().inject(this);

        getTaglist("");


        return view;
    }


    private void getTaglist(String name) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            final TagInputParam tagInputParam = new TagInputParam();
            tagInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            tagInputParam.setSearchvalue(name);
            taggeryAPI.tagpeopleresponse(tagInputParam).enqueue(new Callback<TagPeopleResponse>() {
                @Override
                public void onResponse(Call<TagPeopleResponse> call, Response<TagPeopleResponse> response) {
                    hideProgress();
                    tagPeopleResponse = response.body();

                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (tagPeopleResponse.getStatusCode() == 200) {
                            if (response.body().getFriends() != null && response.body().getFriends().size() > 0){
                                tagPeople = response.body().getFriends();
                                recyclerviewListpeople.setVisibility(View.VISIBLE);
                                consWarn.setVisibility(View.GONE);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                recyclerviewListpeople.setLayoutManager(mLayoutManager);
                                recyclerviewListpeople.setItemAnimator(new DefaultItemAnimator());
                                mAdapter = new ChatListAdapter(tagPeople, getActivity());
                                mAdapter.setItemclickListener(AddChatFragment.this);
                                mAdapter.setChatList(AddChatFragment.this);
                                recyclerviewListpeople.setAdapter(mAdapter);

                            } else {
                                recyclerviewListpeople.setVisibility(View.GONE);
                            }


                        } else {
                            recyclerviewListpeople.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }

                    } else {
                        recyclerviewListpeople.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }


                }

                @Override
                public void onFailure(Call<TagPeopleResponse> call, Throwable t) {

                    hideProgress();
                    recyclerviewListpeople.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);

                }
            });

        } else {
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);

        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void OnItemClick(int pos, View view) {

    }

    @Override
    public void OnItemClickSecond(int position, View view) {

    }


    @Override
    public void ChatClick(Set<TagPeopleList> list) {
        finalchatlist.clear();
        finalchatlist.addAll(list);

        Toast.makeText(getActivity(), "" + name, Toast.LENGTH_SHORT).show();
        if (list.size() > 0) {
            name = finalchatlist.get(0).getName();
            relativeChat.setVisibility(View.VISIBLE);
        } else {
            relativeChat.setVisibility(View.GONE);
        }
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerviewTopeople.setLayoutManager(mLayoutManager);
        recyclerviewTopeople.setItemAnimator(new DefaultItemAnimator());
        tagListAdapter = new TagListAdapter(finalchatlist, getActivity());
        tagListAdapter.setItemclickListener(AddChatFragment.this);
        recyclerviewTopeople.setAdapter(tagListAdapter);
        Log.i("chatcallback", finalchatlist.toString());
    }

    @Override
    public void OnItemClickNew(int pos, View view) {


    }

    @OnClick(R.id.relative_chat)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), ChatActivity.class);
        intent.putExtra(TaggeryConstants.USER_CHAT_NAME, name);
        startActivity(intent);
        getActivity().finish();
    }
}
