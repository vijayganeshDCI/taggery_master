package com.taggery.model;

import java.util.List;

public class Analytical{
	public List<AnalyticalListItem> getAnalyticalList() {
		return AnalyticalList;
	}

	public void setAnalyticalList(List<AnalyticalListItem> analyticalList) {
		AnalyticalList = analyticalList;
	}

	public AnalyticalUserDetail getUserdetails() {
		return Userdetails;
	}

	public void setUserdetails(AnalyticalUserDetail userdetails) {
		Userdetails = userdetails;
	}

	private List<AnalyticalListItem> AnalyticalList;
	private AnalyticalUserDetail Userdetails;
}