package com.taggery.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.msg91.sendotp.library.PhoneNumberUtils;
import com.msg91.sendotp.library.internal.Iso2Phone;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;
import com.taggery.R;
import com.taggery.activity.LoginActivity;
import com.taggery.activity.MainActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.LoginResponse;
import com.taggery.model.RegistrationParam;
import com.taggery.model.RegistrationResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;
import com.taggery.view.CustomTextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class LoginFragment extends BaseFragment {

    @BindView(R.id.image_login_logo)
    ImageView imageLoginLogo;
    @BindView(R.id.edit_phone_number)
    CustomEditText editPhoneNumber;
    @BindView(R.id.text_input_phonenumber)
    TextInputLayout textInputPhonenumber;
    @BindView(R.id.edit_password)
    CustomEditText editPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout textInputPassword;
    @BindView(R.id.text_label_forgot_pass)
    CustomTextView textLabelForgotPass;
    @BindView(R.id.button_login)
    CustomButton buttonLogin;
    @BindView(R.id.text_label_have_acc)
    CustomTextView textLabelHaveAcc;
    @BindView(R.id.text_label_sign_up)
    CustomTextViewBold textLabelSignUp;
    @BindView(R.id.cons_login)
    ConstraintLayout consLogin;
    Unbinder unbinder;
    LoginActivity loginActivity;
    @BindView(R.id.spinner_country)
    CountryCodePicker spinnerCountry;
    private String mCountryIso;
    private String countryCode;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    LoginResponse loginResponse;
    private CheckUserNameResponse checkUserNameResponse;
    private SharedPreferences fcmSharedPerf;
    private String fcmToken;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPerf = getActivity().getSharedPreferences(TaggeryConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        fcmToken = fcmSharedPerf.getString(TaggeryConstants.FCMTOKEN, "");
        textInputPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        buttonLogin.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        editPhoneNumber.setFilters(new InputFilter[]{editTextRestrictSmileys()});
        editPassword.setFilters(new InputFilter[]{editTextRestrictSmileys()});
        loginActivity = (LoginActivity) getActivity();
//        mCountryIso = PhoneNumberUtils.getDefaultCountryIso(getActivity());

        spinnerCountry.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
                countryCode = selectedCountry.getPhoneCode();
            }
        });
        countryCode = spinnerCountry.getDefaultCountryCode();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.text_label_forgot_pass, R.id.button_login, R.id.text_label_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_label_forgot_pass:
                Bundle bundle;
                if (editPhoneNumber.getText().toString().length() != 0) {
                    checkPhoneNumberAvailable();
                } else
                    textInputPhonenumber.setError(getString(R.string.phone_number_error));
                break;
            case R.id.button_login:
                loginNullCheck();
                break;
            case R.id.text_label_sign_up:
                loginActivity.push(new SigupFragment(), "SigupFragment");
                break;
        }
    }

    private void loginNullCheck() {
        if (editPassword.getText().toString().length() > 0 &&
                editPhoneNumber.getText().toString().length() > 0) {
            textInputPhonenumber.setErrorEnabled(false);
            textInputPassword.setErrorEnabled(false);
            login();
        } else {
            if (editPhoneNumber.getText().toString().length() == 0) {
                textInputPhonenumber.setError(getString(R.string.phone_number_error));
            } else {
                textInputPhonenumber.setErrorEnabled(false);
            }
            if (editPassword.getText().toString().length() == 0) {
                textInputPassword.setError(getString(R.string.password_error));
            } else {
                textInputPassword.setErrorEnabled(false);
            }

        }
    }

    private void openActivity(String phoneNumber) {
        OtpVerificationFragment otpVerificationFragment = new OtpVerificationFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phoneNumber", phoneNumber);
        bundle.putString("countryCode", countryCode);
        bundle.putBoolean("forForgotPassword", true);
        otpVerificationFragment.setArguments(bundle);
        loginActivity.push(otpVerificationFragment, "otpVerificationFragment");
    }

    private String getE164Number() {
        return editPhoneNumber.getText().toString().replaceAll("\\D", "").trim();
        // return PhoneNumberUtils.formatNumberToE164(mPhoneNumber.getText().toString(), mCountryIso);
    }

    private void login() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setCountryCode("+" + countryCode);
            registrationParam.setPhoneNumber(editPhoneNumber.getText().toString());
            registrationParam.setPassword(editPassword.getText().toString());
            registrationParam.setFCMKey(fcmToken);
            taggeryAPI.login(registrationParam).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    hideProgress();
                    loginResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (loginResponse.getStatusCode() == 200) {
                            if (loginResponse.getUserDetails() != null) {
                                editor.putString(TaggeryConstants.PHONE_NUMBER,
                                        loginResponse.getUserDetails().getUserMobileNumber() != null ?
                                                loginResponse.getUserDetails().getUserMobileNumber() : "").commit();
                                editor.putString(TaggeryConstants.COUNTRY_CODE_PHONE_NUMBER,
                                        loginResponse.getUserDetails().getUserCountryCode() != null ?
                                                loginResponse.getUserDetails().getUserCountryCode() : "").commit();
                                editor.putInt(TaggeryConstants.USER_ID,
                                        loginResponse.getUserDetails().getId()).commit();
                                editor.putString(TaggeryConstants.USER_NAME,
                                        loginResponse.getUserDetails().getUserName() != null ?
                                                loginResponse.getUserDetails().getUserName() : "").commit();
                                editor.putString(TaggeryConstants.FIRST_NAME,
                                        loginResponse.getUserDetails().getUserFirstName() != null ?
                                                loginResponse.getUserDetails().getUserFirstName() : "").commit();
                                editor.putString(TaggeryConstants.LAST_NAME,
                                        loginResponse.getUserDetails().getUserLastName() != null ?
                                                loginResponse.getUserDetails().getUserLastName() : "").commit();
                                editor.putString(TaggeryConstants.USER_EMAIL,
                                        loginResponse.getUserDetails().getUserEmail() != null ?
                                                loginResponse.getUserDetails().getUserEmail() : "").commit();
                                editor.putString(TaggeryConstants.USER_GENDER,
                                        loginResponse.getUserDetails().getUserGender() != null ?
                                                loginResponse.getUserDetails().getUserGender() : "").commit();
                                editor.putString(TaggeryConstants.USER_RELATION_SHIP_STATUS,
                                        loginResponse.getUserDetails().getUserRelationshipStatus() != null ?
                                                loginResponse.getUserDetails().getUserRelationshipStatus() : "").commit();
                                editor.putInt(TaggeryConstants.USER_PROFILE_TYPE,
                                        loginResponse.getUserDetails().getUserProfileType()).commit();
                                editor.putString(TaggeryConstants.USER_PROFILE_PIC,
                                        loginResponse.getUserDetails().getUserPicture() != null ?
                                                loginResponse.getUserDetails().getUserPicture() : "").commit();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                Toast.makeText(getActivity(), loginResponse.getMessage()!=null?
                                        loginResponse.getMessage():"", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            showShackError(loginResponse.getMessage()!=null?
                                    loginResponse.getMessage():"", consLogin);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), consLogin);
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consLogin);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), consLogin);
        }
    }

    private void checkPhoneNumberAvailable() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setType(2);
            registrationParam.setCountryCode("+" + countryCode);
            registrationParam.setPhoneNumber(editPhoneNumber.getText().toString());
            taggeryAPI.checkUserName(registrationParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null) {
                        if (response.code() == 200 && checkUserNameResponse.getStatusCode() == 200) {
                            //Not available
                            showShackError(getString(R.string.phone_number_not_available), consLogin);
                        } else {
                            //Already available
                            openActivity(getE164Number());
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consLogin);
                    }

                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consLogin);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), consLogin);
        }
    }
}
