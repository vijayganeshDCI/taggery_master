package com.taggery.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.taggery.R;
import com.taggery.activity.PreviewActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.AddComment;
import com.taggery.model.AddLike;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.PostDetails;
import com.taggery.model.ViewPost;
import com.taggery.model.ViewPostResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.shts.android.storiesprogressview.StoriesProgressView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * UserProfileUploadAfter12Hrs simple {@link Fragment} subclass.
 */
public class PreviewFragment extends BaseFragment implements StoriesProgressView.StoriesListener {

    @BindView(R.id.profile_name)
    CustomTextView textProfileName;
    @BindView(R.id.text_posttime)
    CustomTextView textPosttime;
    @BindView(R.id.relative_name)
    RelativeLayout relativeName;
    @BindView(R.id.img_infinity)
    ImageView imgInfinity;
    @BindView(R.id.img_flag)
    ImageView imgFlagReport;
    @BindView(R.id.relative_timelimit)
    RelativeLayout relativeTimelimit;
    @BindView(R.id.img_bottomup)
    ImageView imgBottomup;
    @BindView(R.id.img_mappin)
    ImageView imgMappin;
    @BindView(R.id.edit_addcomment)
    CustomEditText editAddcomment;
    @BindView(R.id.text_viewcount)
    CustomTextView textViewcount;
    @BindView(R.id.text_like_count)
    CustomTextView textLikecount;
    @BindView(R.id.text_status)
    CustomTextView textStatus;
    @BindView(R.id.relative_status)
    RelativeLayout relativeStatus;
    @BindView(R.id.linear_bottom)
    LinearLayout linearBottom;
    @BindView(R.id.relative_child)
    RelativeLayout relativeChild;
    @BindView(R.id.relative_view_post)
    RelativeLayout relativeParent;
    @BindView(R.id.linear_name)
    LinearLayout linearName;
    Unbinder unbinder;
    @BindView(R.id.image_like_button)
    ImageView imageLikeButton;
    @BindView(R.id.video_player_post)
    SimpleExoPlayerView videoPlayer;
    @BindView(R.id.progress_post)
    StoriesProgressView progressPost;
    @BindView(R.id.img_post)
    ImageView imgPost;
    PreviewActivity previewActivity;
    boolean isPause = false;
    boolean swipeTop = false, swipeBottom = false;
    @BindView(R.id.relative_addcomment)
    ConstraintLayout relativeAddcomment;
    @BindView(R.id.view_reverse)
    View viewReverse;
    @BindView(R.id.view_skip)
    View viewSkip;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    private List<PostDetails> relatedPostArray;
    private String relatedPost;
    private Type type;
    private long pressTime = 0L;
    private long limit = 500L;
    private int counter = 0, postPosition = 0;
    private SimpleExoPlayer player;
    private DefaultExtractorsFactory extractorsFactory;
    private DefaultDataSourceFactory dataSourceFactory;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    CheckUserNameResponse checkUserNameResponse;
    ViewPostResponse viewPostResponse;
    private boolean isCommentFocused = false;
    private boolean isLiked = false, isReported = false;
    private int likeCount, viewCount;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview, container, false);
        ButterKnife.bind(this, view);
        previewActivity = (PreviewActivity) getActivity();
        TaggeryApplication.getContext().getComponent().inject(this);
        type = new TypeToken<ArrayList<PostDetails>>() {
        }.getType();
        relatedPostArray = new ArrayList<PostDetails>();
        viewSkip.setOnTouchListener(onTouchListener);
        viewReverse.setOnTouchListener(onTouchListener);
        Intent intent = getActivity().getIntent();
        if (intent != null)
            relatedPost = intent.getStringExtra("relatedPostList");

        if (relatedPost != null)
            relatedPostArray = new Gson().fromJson(relatedPost, type);

        progressPost.setStoriesCount(relatedPostArray.size());
        initializePlayer();
        viewPost(postPosition);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (isCommentFocused) {
                        progressPost.resume();
                        return true;
                    } else
                        return false;
                }
                return false;
            }
        });


//        viewSwipe.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
//            public void onSwipeTop() {
//                progressPost.pause();
//                if (swipeTop) {
//                    StatsFragment statsFragment = new StatsFragment();
//                    previewActivity.push(statsFragment, "StatsFragment");
//                    swipeTop = false;
//                } else {
//                    linearBottom.setVisibility(View.VISIBLE);
//                    swipeTop = true;
//                    swipeBottom = true;
//                }
//
//            }
//
//            public void onSwipeBottom() {
//                if (swipeBottom) {
//                    swipeTop = false;
//                    linearBottom.setVisibility(View.GONE);
//                    swipeBottom = false;
//                    progressPost.resume();
//                } else {
//                    progressPost.pause();
//                    progressPost.destroy();
//                    Intent intent = new Intent(getActivity(), MainActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    previewActivity.finish();
//                    previewActivity.overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
//                }
//
//
//            }
//
//            public void onClick(MotionEvent event) {
//                if (isPause) {
//                    progressPost.resume();
//                    isPause = false;
//                } else {
//                    progressPost.pause();
//                    isPause = true;
//                }
//
//            }
//        });

        return view;
    }

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    progressPost.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    progressPost.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };


    @Override
    public void onDestroy() {
        // Very important !
        progressPost.destroy();
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        swipeTop = false;
        swipeBottom = false;
    }

    @OnClick({R.id.linear_name, R.id.img_bottomup, R.id.image_like_button, R.id.view_skip, R.id.view_reverse,
            R.id.img_send, R.id.edit_addcomment, R.id.img_flag})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_name:
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundleProfile = new Bundle();
                bundleProfile.putBoolean("isGuestUser", true);
                bundleProfile.putInt("postUserID", relatedPostArray.get(postPosition).getPostUserID());
                profileFragment.setArguments(bundleProfile);
                previewActivity.push(profileFragment, "ProfileFragment");
                break;
            case R.id.img_bottomup:
                StatsFragment statsFragment = new StatsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("postID", relatedPostArray.get(postPosition).getPostID());
                statsFragment.setArguments(bundle);
                previewActivity.push(statsFragment, "StatsFragment");
                break;
            case R.id.view_skip:
                progressPost.skip();
                break;
            case R.id.view_reverse:
                progressPost.reverse();
                break;
            case R.id.image_like_button:
                addLike(postPosition);
                break;
            case R.id.img_send:
                progressPost.resume();
                if (editAddcomment.getText().length() > 0)
                    addComment(postPosition);
                else
                    editAddcomment.setError(getString(R.string.add_comment));
                break;
            case R.id.edit_addcomment:
                isCommentFocused = true;
                progressPost.pause();
                break;
            case R.id.img_flag:
                addReport(postPosition);
                break;
        }
    }

    @Override
    public void onNext() {
        postPosition = ++counter;
        viewPost(postPosition);
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
        postPosition = --counter;
        viewPost(postPosition);
    }

    @Override
    public void onComplete() {
        if (player != null) {
            player.release();
            player = null;
        }
        getActivity().onBackPressed();
    }


    private void initializePlayer() {
        // Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        //Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        //Initialize simpleExoPlayerView
        videoPlayer.setPlayer(player);
        // Produces DataSource instances through which media data is loaded.
        dataSourceFactory =
                new DefaultDataSourceFactory(getActivity(), Util.getUserAgent(getActivity(),
                        "taggery"));
        // Produces Extractor instances for parsing the media data.
        extractorsFactory = new DefaultExtractorsFactory();

    }

    private void loadData(int position, ViewPostResponse viewPostResponse) {
//        if (relatedPostArray.get(position).getPostTimeFrame() != 0)
//            progressPost.setStoryDuration(relatedPostArray.get(position).getPostTimeFrame() * 1000L);
//        else
//            progressPost.setStoryDuration(30000L);

        progressPost.setStoryDuration(8000L);
        progressPost.setStoriesListener(this);
        progressPost.startStories(position);


        if (viewPostResponse.getPost().getPostMediaType() == 1) {
            //image
            imgPost.setVisibility(View.VISIBLE);
            videoPlayer.setVisibility(View.GONE);
            picassoImageHolder(imgPost, viewPostResponse.getPost().getPostImage() != null ?
                            viewPostResponse.getPost().getPostImage() : "",
                    R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                    getString(R.string.cloudinary_download_post));
        } else if (viewPostResponse.getPost().getPostMediaType() == 2) {
            //video
            // This is the MediaSource representing the media to be played.
            imgPost.setVisibility(View.GONE);
            videoPlayer.setVisibility(View.VISIBLE);
            if (viewPostResponse.getPost().getPostImage() != null) {
                Uri videoUri = Uri.parse(getString(R.string.cloudinary_base_url) +
                        getString(R.string.cloudinary_download_video_add_post) +
                        viewPostResponse.getPost().getPostImage());
                MediaSource videoSource = new ExtractorMediaSource(videoUri,
                        dataSourceFactory, extractorsFactory, null, null);
                // Prepare the player with the source.
                player.prepare(videoSource);
                player.setPlayWhenReady(true);
            }
        } else {
            picassoImageHolder(imgPost, relatedPostArray.get(position).getPostImage() != null ?
                            relatedPostArray.get(position).getPostImage() : "",
                    R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                    getString(R.string.cloudinary_download_post));
        }

        textStatus.setText(viewPostResponse.getPost().getPostText() != null ?
                viewPostResponse.getPost().getPostText() : "");
        String[] postTime = new String[0];
        if (viewPostResponse.getPost().getPostcreateddate() != null)
            postTime = viewPostResponse.getPost().getPostcreateddate().split(" ");
        textPosttime.setText((postTime[0] != null ? postTime[0] : "") + " " + (postTime[1] != null ? postTime[0] : ""));
        likeCount = viewPostResponse.getPost().getLikestcount();
        viewCount = viewPostResponse.getPost().getViewscount();
        textLikecount.setText("" + likeCount);
        textViewcount.setText("" + viewCount);
        textProfileName.setText(viewPostResponse.getPost().getPostUserFName() != null ?
                viewPostResponse.getPost().getPostUserFName() : "");
        picassoImageHolder(imageProPic, viewPostResponse.getPost().getPostUserProfileImage() != null ?
                        viewPostResponse.getPost().getPostUserProfileImage() : "",
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder, getString(R.string.cloudinary_download_profile_picture));
        profileRingColor(viewPostResponse.getPost().getUserTotalLikesCount(), imageProPic);
        if (viewPostResponse.getPost().getLikes() == 1) {
            //like
            isLiked = true;
            imageLikeButton.setImageResource(R.mipmap.icon_heart_like);
        } else if (viewPostResponse.getPost().getLikes() == 2) {
            //unlike
            isLiked = false;
            imageLikeButton.setImageResource(R.mipmap.icon_heart_un_like);
        }
        if (viewPostResponse.getPost().getReportStatus() == 1) {
            //Report
            isReported = true;
            imgFlagReport.setImageResource(R.mipmap.icon_flag_red);
        } else if (viewPostResponse.getPost().getReportStatus() == 2) {
            //Revert
            isReported = false;
            imgFlagReport.setImageResource(R.mipmap.icon_flag_white);
        }
    }


    private void addLike(final int postPosition) {
        if (com.taggery.utils.Util.isNetworkAvailable()) {
            showProgress();
            AddLike addLike = new AddLike();
            addLike.setLikePostID(relatedPostArray.get(postPosition).getPostID());
            addLike.setLikeUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            if (isLiked)
                //unlike
                addLike.setLikeStatus(2);
            else
                //like
                addLike.setLikeStatus(1);

            taggeryAPI.addLike(addLike).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            likeCount = viewPostResponse.getPost().getLikestcount();
                            if (checkUserNameResponse.getLikeStatus() == 1) {
                                //like
                                isLiked = true;
                                imageLikeButton.setImageResource(R.mipmap.icon_heart_like);
                                relatedPostArray.get(postPosition).setLikes(1);
                                viewPostResponse.getPost().setLikestcount(++likeCount);
                            } else if (checkUserNameResponse.getLikeStatus() == 2) {
                                //unlike
                                isLiked = false;
                                imageLikeButton.setImageResource(R.mipmap.icon_heart_un_like);
                                relatedPostArray.get(postPosition).setLikes(2);
                                viewPostResponse.getPost().setLikestcount(--likeCount);
                            }
                            textLikecount.setText("" + viewPostResponse.getPost().getLikestcount());
                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }
    }

    private void addComment(int postPosition) {
        if (com.taggery.utils.Util.isNetworkAvailable()) {
            showProgress();
            AddComment addComment = new AddComment();
            addComment.setCommentPostID(relatedPostArray.get(postPosition).getPostID());
            addComment.setCommentText(editAddcomment.getText().toString());
            addComment.setCommentUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.addComment(addComment).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            editAddcomment.setText("");
                            showShackError(checkUserNameResponse.getMessage(), relativeParent);
                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }
    }

    private void viewPost(final int postPosition) {
        if (com.taggery.utils.Util.isNetworkAvailable()) {
            showProgress();
            final ViewPost viewPost = new ViewPost();
            viewPost.setViewPostID(relatedPostArray.get(postPosition).getPostID());
            viewPost.setViewUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.viewPost(viewPost).enqueue(new Callback<ViewPostResponse>() {
                @Override
                public void onResponse(Call<ViewPostResponse> call, Response<ViewPostResponse> response) {
                    hideProgress();
                    viewPostResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (viewPostResponse.getStatusCode() == 200) {
                            loadData(postPosition, viewPostResponse);
                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<ViewPostResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });


        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }
    }

    private void addReport(final int postPosition) {
        if (com.taggery.utils.Util.isNetworkAvailable()) {
            showProgress();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setPostID(relatedPostArray.get(postPosition).getPostID());
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            if (isReported)
                //revert
                contactListInputParam.setReportStatus(2);
            else
                //report
                contactListInputParam.setReportStatus(1);

            taggeryAPI.addReport(contactListInputParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            if (checkUserNameResponse.getReportStatus() == 1) {
                                //Report
                                isReported = true;
                                imgFlagReport.setImageResource(R.mipmap.icon_flag_red);
                            } else if (checkUserNameResponse.getReportStatus() == 2) {
                                //Revert
                                isLiked = false;
                                imgFlagReport.setImageResource(R.mipmap.icon_flag_white);
                            }
                            showShackError(checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", relativeParent);

                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
