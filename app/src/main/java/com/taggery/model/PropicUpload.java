package com.taggery.model;

public class PropicUpload {

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public PropicUpload(String imagePath, String imageName) {
        this.imagePath = imagePath;
        this.imageName = imageName;
    }

    private String imagePath;
    private String imageName;
}
