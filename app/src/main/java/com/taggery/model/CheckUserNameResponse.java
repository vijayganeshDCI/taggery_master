package com.taggery.model;

public class CheckUserNameResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private String Message;
	private int StatusCode;

	public int getRequestAcceptStatus() {
		return RequestAcceptStatus;
	}

	public void setRequestAcceptStatus(int requestAcceptStatus) {
		RequestAcceptStatus = requestAcceptStatus;
	}

	private int RequestAcceptStatus;

	public int getLikeStatus() {
		return LikeStatus;
	}

	public void setLikeStatus(int likeStatus) {
		LikeStatus = likeStatus;
	}

	private int LikeStatus;

	public int getReportStatus() {
		return ReportStatus;
	}

	public void setReportStatus(int reportStatus) {
		ReportStatus = reportStatus;
	}

	private int ReportStatus;
}
