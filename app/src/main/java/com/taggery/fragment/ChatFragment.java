package com.taggery.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.taggery.model.ChatPojo;
import com.taggery.R;
import com.taggery.adapter.ChatAdapter;

import java.util.ArrayList;
import java.util.List;


public class ChatFragment extends BaseFragment {

    private RecyclerView recyclerview;
    private ChatAdapter mAdapter;
    private List<ChatPojo> chatList=new ArrayList<>();
    private boolean isStarted;
    private boolean isVisible;

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isVisible && isStarted) {
            isStarted = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isStarted = false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_chat, container, false);
        recyclerview=(RecyclerView)view.findViewById(R.id.chat_recycle);
        recyclerview.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mAdapter = new ChatAdapter(chatList,getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(mAdapter);
        Chatdata();


        return view;
    }
    private void Chatdata() {

        ChatPojo s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("David");
        s.setSubtitle("Where are you now?");
        s.setTime("11.30Am");
        chatList.add(s);


        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Daniel");
        s.setSubtitle("What are you doing?");
        s.setTime("11.40Am");
        chatList.add(s);

        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Caroline");
        s.setSubtitle("Hi?");
        s.setTime("10.30Am");
        chatList.add(s);

        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Albert");
        s.setSubtitle("Where are you now?");
        s.setTime("1.30Pm");
        chatList.add(s);

        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Sofia");
        s.setSubtitle("Hello");
        s.setTime("9.30Am");
        chatList.add(s);

        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Lucia");
        s.setSubtitle("Iam in meeting");
        s.setTime("8.30Pm");
        chatList.add(s);
        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Lucia");
        s.setSubtitle("Iam in meeting");
        s.setTime("8.30Pm");
        chatList.add(s);
        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Lucia");
        s.setSubtitle("Iam in meeting");
        s.setTime("8.30Pm");
        chatList.add(s);
        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Lucia");
        s.setSubtitle("Iam in meeting");
        s.setTime("8.30Pm");
        chatList.add(s);
        s=new ChatPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Lucia");
        s.setSubtitle("Iam in meeting");
        s.setTime("8.30Pm");
        chatList.add(s);

    }



}
