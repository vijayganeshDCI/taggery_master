package com.taggery.view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.activity.ImageshowActivity;
import com.taggery.activity.MainActivity;
import com.taggery.fragment.TagFragment;
import com.taggery.utils.TaggeryConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


public class CameraPreview extends SurfaceView implements
        SurfaceHolder.Callback, View.OnClickListener {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    Camera.Parameters param;
    Camera.Size mPreviewSize;
    Context context;
    private List<Camera.Size> mSupportedPreviewSizes;
    int currentCameraId = 0;
    File ouputfile;
    String mCurrentPhotoPath;
    boolean flashmode = false;
    private static final int FOCUS_AREA_SIZE = 300;
    private Canvas canvas;
    private Paint paint;
    private static final float STROKE_WIDTH = 5f;
    private boolean meteringAreaSupported;
    private Matrix matrix;
    private boolean isRecording = false;
    private MediaRecorder mMediaRecorder;
    private GestureDetector mDetector;

    private Handler customHandler = new Handler();
    int flag = 0;
    private File tempFile = null;
    private Camera.PictureCallback mPictureCallback;
    int MAX_VIDEO_SIZE_UPLOAD = 25; //MB

    OrientationEventListener myOrientationEventListener;
    int iOrientation = 0;
    int mOrientation = 90;
    private int mPhotoAngle = 90;
    MainActivity mainActivity;
    private File folder = null;
    private ImageView imgCapture;
    private ImageView imgFlashOnOff;
    private ImageView imgSwipeCamera;
    int flashType = 2;
    private TextView textCounter,text_swipegalley;
    private MediaRecorder mediaRecorder;
    private String videoFilename = null,imageName;
    private TextView hintTextView;
    View view;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    FrameLayout preview;
    File imageFilename;
    SharedPreferences share;
    SharedPreferences.Editor edit;


    public CameraPreview(Context context, Camera mCamera, View view) {
        super(context);
        this.mCamera = mCamera;
        this.context = context;
        this.view = view;
        mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
        /*for (Camera.Size str : mSupportedPreviewSizes)
            param = mCamera.getParameters();
        param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        param.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);*/
        mainActivity = (MainActivity) context;
        sharedPreferences = mainActivity.getSharedPreferences("myprefs", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        share = mainActivity.getSharedPreferences("mytaggery",Context.MODE_PRIVATE);
        edit = share.edit();
        //create a folder to get image
        folder = new File(Environment.getExternalStorageDirectory() + File.separator + "Taggery" + File.separator +"Local");
        if (!folder.exists()) {
            folder.mkdirs();
        }
        imageName ="IMG_" + System.currentTimeMillis();
        imageFilename = new File(folder.getAbsolutePath() + "/" + imageName + ".jpg");
        //capture image on callback
        OrientationDetection();

        initialize();
        captureImageCallback();


    }

    private void initialize() {
        imgCapture = view.findViewById(R.id.img_capture);
        imgFlashOnOff = view.findViewById(R.id.img_flash);
        textCounter = view.findViewById(R.id.textCounter);
        imgSwipeCamera = view.findViewById(R.id.img_switch);
        hintTextView = view.findViewById(R.id.hintTextView);
        preview = view.findViewById(R.id.camera_preview);
        text_swipegalley = view.findViewById(R.id.text_swipegalley);
        imgSwipeCamera.setOnClickListener(this);

        textCounter.setVisibility(View.GONE);

        imgFlashOnOff.setOnClickListener(this);


        activeCameraCapture();
        if (mCamera != null) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                imgFlashOnOff.setVisibility(View.GONE);
            }
        }
        preview.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                try {
                    focusOnTouch(motionEvent);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        });

        //capture image on callback
    }

    private void captureImageCallback() {

        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mPictureCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                refreshCamera();
                SavePicture(data,getPhotoRotation());
            }
        };
    }
    private void SavePicture(byte[] data, int photoRotation) {
        String result = null;
        try {
             result =  saveToSDCard(data, photoRotation);
        } catch (Exception e) {
            e.printStackTrace();

        }
        activeCameraCapture();

        tempFile = new File(result);
        Log.d("outputfile", String.valueOf(tempFile));

        String path = tempFile.getAbsolutePath();
        Intent intent = new Intent(context, ImageshowActivity.class);
        intent.putExtra("mediaPath", path);
        intent.putExtra("mediaFilename",imageName);
        intent.putExtra("isImage",true);
        context.startActivity(intent);

    }

    private void SaveVideo() {
        File thumbFilename = null;
        imgCapture.setOnTouchListener(null);
        textCounter.setVisibility(View.GONE);
        imgSwipeCamera.setVisibility(View.VISIBLE);
        text_swipegalley.setVisibility(VISIBLE);
        imgFlashOnOff.setVisibility(View.VISIBLE);
            try {
                myOrientationEventListener.enable();
                customHandler.removeCallbacksAndMessages(null);
                mediaRecorder.stop();
                releaseMediaRecorder();
                tempFile = new File(folder.getAbsolutePath() + "/" + videoFilename + ".mp4");
             //   thumbFilename = new File(folder.getAbsolutePath(), "t_" + videoFilename + ".jpeg");
                //  generateVideoThmb(tempFile.getPath(), thumbFilename);

            } catch (Exception e) {
                e.printStackTrace();
            }

        if (tempFile != null){
                String videopath = tempFile.getAbsolutePath();

                Log.d("outputfile", String.valueOf(videopath));
                String path = videopath.toString();
                Intent intent = new Intent(context, ImageshowActivity.class);
                intent.putExtra("mediaPath", path);
                intent.putExtra("    ",videoFilename);
                intent.putExtra("isImage",false);

            context.startActivity(intent);
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_flash:
                //  flashToggle();
                break;
            case R.id.img_switch:
                switchCamera();
                break;
            default:
                break;
        }

    }

    public void switchCamera() {
        mCamera.stopPreview();
        mCamera.release();
        if (flag == 0) {
            imgFlashOnOff.setVisibility(View.GONE);
            flag = 1;
        } else {
            imgFlashOnOff.setVisibility(View.VISIBLE);
            flag = 0;
        }
        surfaceCreated(mHolder);

    }

    private void activeCameraCapture() {
        if (imgCapture != null) {
            imgCapture.setAlpha(1.0f);
            imgCapture.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    hintTextView.setVisibility(View.INVISIBLE);
                    try {
                        if (prepareMediaRecorder()) {
                            myOrientationEventListener.disable();
                            mediaRecorder.start();
                            startTime = SystemClock.uptimeMillis();
                            customHandler.postDelayed(updateTimerThread, 0);
                        } else {
                            return false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    textCounter.setVisibility(View.VISIBLE);
                    imgSwipeCamera.setVisibility(View.GONE);
                    imgFlashOnOff.setVisibility(View.GONE);
                    text_swipegalley.setVisibility(INVISIBLE);
                    scaleUpAnimation();
                    imgCapture.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() == MotionEvent.ACTION_BUTTON_PRESS) {
                                return true;
                            }
                            if (event.getAction() == MotionEvent.ACTION_UP) {

                                scaleDownAnimation();
                                hintTextView.setVisibility(View.VISIBLE);
                                SaveVideo();

                                return true;
                            }
                            return true;

                        }
                    });
                    return true;
                }

            });
            imgCapture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isSpaceAvailable()) {
                        captureImage();
                    } else {
                        Toast.makeText(context, R.string.memorynotavailable, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }

    private void captureImage() {
        try {
            if (mCamera != null) {
                mCamera.takePicture(new Camera.ShutterCallback() {
                    @Override
                    public void onShutter() {
                        AudioManager mgr = (AudioManager) context.getSystemService
                                (Context.AUDIO_SERVICE);
                        mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
                    }
                }, null, mPictureCallback);

            }
            inActiveCameraCapture();
            edit.putString(TaggeryConstants.CHECK_GALLERY, "camera").apply();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    private void inActiveCameraCapture() {
        if (imgCapture != null) {
            imgCapture.setAlpha(0.5f);
            imgCapture.setOnClickListener(null);
        }
    }

    public void flashOpen() {

        if (flashType == 1) {
            Toast.makeText(context, R.string.flashauto, Toast.LENGTH_SHORT).show();
            flashType = 3;
        } else if (flashType == 2) {
            Toast.makeText(context, R.string.flashon, Toast.LENGTH_SHORT).show();
            flashType = 1;
        } else {
            Toast.makeText(context, R.string.flashoff, Toast.LENGTH_SHORT).show();
            flashType = 2;
        }

        refreshCamera();
    }

    public void refreshCamera() {

        if (mHolder.getSurface() == null) {
            return;
        }
        try {
            mCamera.stopPreview();
            Camera.Parameters param = mCamera.getParameters();
            if (flag == 0) {
                if (flashType == 1) {
                    param.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                    imgFlashOnOff.setImageResource(R.mipmap.icon_flash_on);
                } else if (flashType == 2) {
                    param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    imgFlashOnOff.setImageResource(R.mipmap.icon_flash_off);
                } else {
                    param.setFlashMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    imgFlashOnOff.setImageResource(R.mipmap.icon_flash_auto);
                }
            }
            refrechCameraPreview(param);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refrechCameraPreview(Camera.Parameters param) {
        try {
            mCamera.setParameters(param);
            setCameraDisplayOrientation(0);

            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCameraDisplayOrientation(int cameraId) {

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        int rotation = mainActivity.getWindowManager().getDefaultDisplay().getRotation();

        if (Build.MODEL.equalsIgnoreCase("Nexus 6") && flag == 1) {
            rotation = Surface.ROTATION_180;
        }
        int degrees = 0;
        switch (rotation) {

            case Surface.ROTATION_0:

                degrees = 0;
                break;

            case Surface.ROTATION_90:

                degrees = 90;
                break;

            case Surface.ROTATION_180:

                degrees = 180;
                break;

            case Surface.ROTATION_270:

                degrees = 270;
                break;
        }

        int result;
        if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {

            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror

        } else {
            result = (info.orientation - degrees + 360) % 360;

        }

        mCamera.setDisplayOrientation(result);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            if (flag == 0) {
                mCamera = Camera.open(0);
            } else {
                mCamera = Camera.open(1);
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
            return;
        }

        try {
            Camera.Parameters param;
            param = mCamera.getParameters();
            List<Camera.Size> sizes = param.getSupportedPreviewSizes();
            //get diff to get perfact preview sizes
            DisplayMetrics displaymetrics = new DisplayMetrics();
            mainActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            int height = displaymetrics.heightPixels;
            int width = displaymetrics.widthPixels;
            long diff = (height * 1000 / width);
            long cdistance = Integer.MAX_VALUE;
            int idx = 0;
            for (int i = 0; i < sizes.size(); i++) {
                long value = (long) (sizes.get(i).width * 1000) / sizes.get(i).height;
                if (value > diff && value < cdistance) {
                    idx = i;
                    cdistance = value;
                }
                Log.e("Camera", "width=" + sizes.get(i).width + " height=" + sizes.get(i).height);
            }
            Log.e("Camera", "INDEX:  " + idx);
            Camera.Size cs = sizes.get(idx);
            param.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            //  param.setPictureSize(mPreviewSize.width,mPreviewSize.height);
            param.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
            /*if (flag==1){
                param.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            }
            else {
                param.setPreviewSize(cs.width, cs.height);
            }*/
            //param.setPreviewSize(cs.width, cs.height);
          //  param.setPictureSize(cs.width, cs.height);  // adjust picture size
            param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            mCamera.setParameters(param);
            setCameraDisplayOrientation(0);

            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int
            height) {
        refreshCamera();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        try {

            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private long timeInMilliseconds = 0L, startTime = SystemClock.uptimeMillis(), updatedTime = 0L, timeSwapBuff = 0L;
    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            int hrs = mins / 60;

            secs = secs % 60;
            textCounter.setText(String.format("%02d", mins) + ":" + String.format("%02d", secs));
            customHandler.postDelayed(this, 0);

        }
    };

    private void scaleUpAnimation() {
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(imgCapture, "scaleX", 2f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(imgCapture, "scaleY", 2f);
        scaleDownX.setDuration(100);
        scaleDownY.setDuration(100);
        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);

        scaleDownX.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                View p = (View) imgCapture.getParent();
                p.invalidate();
            }
        });
        scaleDown.start();
    }

    private void scaleDownAnimation() {
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(imgCapture, "scaleX", 1f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(imgCapture, "scaleY", 1f);
        scaleDownX.setDuration(100);
        scaleDownY.setDuration(100);
        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);

        scaleDownX.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                View p = (View) imgCapture.getParent();
                p.invalidate();
            }
        });
        scaleDown.start();
    }

    public void focusOnTouch(MotionEvent event) {
        if (mCamera != null) {

            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters.getMaxNumMeteringAreas() > 0) {
                Log.i("camerafragment", "focus");
                Rect rect = calculateTapArea(event.getX(), event.getY());

                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
                meteringAreas.add(new Camera.Area(rect, 800));
                parameters.setFocusAreas(meteringAreas);

                try {
                    mCamera.setParameters(parameters);
                    mCamera.autoFocus(mAutoFocusTakePictureCallback);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                mCamera.autoFocus(mAutoFocusTakePictureCallback);
            }
        }

    }

    private Rect calculateTapArea(float x, float y) {
        int left = clamp(Float.valueOf((x / view.getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
        int top = clamp(Float.valueOf((y / view.getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

        return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper) + focusAreaSize / 2 > 1000) {
            if (touchCoordinateInCameraReper > 0) {
                result = 1000 - focusAreaSize / 2;
            } else {
                result = -1000 + focusAreaSize / 2;
            }
        } else {
            result = touchCoordinateInCameraReper - focusAreaSize / 2;
        }
        return result;
    }

    private Camera.AutoFocusCallback mAutoFocusTakePictureCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            if (success) {

                // do something...
                Log.i("tap_to_focus", "success!");
            } else {
                // do something...
                Log.i("tap_to_focus", "fail!");
            }
        }
    };


    public String saveToSDCard(byte[] data, int rotation) throws IOException {
        String imagePath = "";
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, options);

            DisplayMetrics metrics = new DisplayMetrics();
            mainActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

            int reqHeight = metrics.heightPixels;
            int reqWidth = metrics.widthPixels;

            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            options.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);


            if (rotation != 0) {
                Matrix mat = new Matrix();
                mat.postRotate(rotation);
                Bitmap bitmap1 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), mat, true);

                if (bitmap != bitmap1) {
                    bitmap.recycle();
                }
                imagePath = getSavePhotoLocal(bitmap1);
                if (bitmap1 != null) {
                    bitmap1.recycle();
                }
            } else {
                imagePath = getSavePhotoLocal(bitmap);
                if (bitmap != null) {
                    bitmap.recycle();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagePath;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private String getSavePhotoLocal(Bitmap bitmap) {
        String path = "";
        try {
            OutputStream output;

            try {
                output = new FileOutputStream(imageFilename);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, output);
                output.flush();
                output.close();
                path = imageFilename.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    //--------------------------CHECK FOR MEMORY -----------------------------//

    public int getFreeSpacePercantage() {
        int percantage = (int) (freeMemory() * 100 / totalMemory());
        int modValue = percantage % 5;
        return percantage - modValue;
    }

    public double totalMemory() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double sdAvailSize = (double) stat.getBlockCount() * (double) stat.getBlockSize();
        return sdAvailSize / 1073741824;
    }

    public double freeMemory() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double sdAvailSize = (double) stat.getAvailableBlocks() * (double) stat.getBlockSize();
        return sdAvailSize / 1073741824;
    }

    public boolean isSpaceAvailable() {
        if (getFreeSpacePercantage() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    //-------------------END METHODS OF CHECK MEMORY--------------------------//


    protected boolean prepareMediaRecorder() throws IOException {

        mediaRecorder = new MediaRecorder(); // Works well
        mCamera.stopPreview();
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        if (flag == 1) {
            mediaRecorder.setProfile(CamcorderProfile.get(1, CamcorderProfile.QUALITY_HIGH));
        } else {
            mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        }
        mediaRecorder.setPreviewDisplay(mHolder.getSurface());

        mediaRecorder.setOrientationHint(mOrientation);

        if (Build.MODEL.equalsIgnoreCase("Nexus 6") && flag == 1) {

            if (mOrientation == 90) {
                mediaRecorder.setOrientationHint(mOrientation);
            } else if (mOrientation == 180) {
                mediaRecorder.setOrientationHint(0);
            } else {
                mediaRecorder.setOrientationHint(180);
            }

        } else if (mOrientation == 90 && flag == 1) {
            mediaRecorder.setOrientationHint(270);
        } else if (flag == 1) {
            mediaRecorder.setOrientationHint(mOrientation);
        }
        videoFilename = "VID_" + System.currentTimeMillis();

        /*mediaRecorder.setVideoEncodingBitRate(690000);
        mediaRecorder.setVideoFrameRate(30);
        mediaRecorder.setVideoSize(640, 480);*/

        mediaRecorder.setVideoEncodingBitRate(1000000);
        mediaRecorder.setVideoSize(1280, 720);
        mediaRecorder.setVideoFrameRate(18);

        mediaRecorder.setOutputFile(folder.getAbsolutePath() + "/" + videoFilename + ".mp4"); // Environment.getExternalStorageDirectory()

        mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {

            public void onInfo(MediaRecorder mr, int what, int extra) {
                // TODO Auto-generated method stub

                if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {

                    long downTime = 0;
                    long eventTime = 0;
                    float x = 0.0f;
                    float y = 0.0f;
                    int metaState = 0;
                    MotionEvent motionEvent = MotionEvent.obtain(
                            downTime,
                            eventTime,
                            MotionEvent.ACTION_UP,
                            0,
                            0,
                            metaState
                    );

                    imgCapture.dispatchTouchEvent(motionEvent);

                    Toast.makeText(context, "You reached to Maximum(25MB) video size.", Toast.LENGTH_SHORT).show();
                }


            }
        });

       // mediaRecorder.setMaxFileSize(1000 * 25 * 1000);
         mediaRecorder.setMaxDuration(30000);

        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            releaseMediaRecorder();
            e.printStackTrace();
            return false;
        }
        return true;

    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = new MediaRecorder();
        }
    }

    public void resumeMethod() {
        try {
            if (myOrientationEventListener != null)
                myOrientationEventListener.enable();
            if (mCamera == null) {
                mCamera.startPreview();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    private int normalize(int degrees) {
        if (degrees > 315 || degrees <= 45) {
            return 0;
        }

        if (degrees > 45 && degrees <= 135) {
            return 90;
        }

        if (degrees > 135 && degrees <= 225) {
            return 180;
        }

        if (degrees > 225 && degrees <= 315) {
            return 270;
        }

        throw new RuntimeException("Error....");
    }
//
//    public void generateVideoThmb(String srcFilePath, File destFile) {
//        try {
//            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(srcFilePath, 120);
//            FileOutputStream out = new FileOutputStream(destFile);
//            ThumbnailUtils.extractThumbnail(bitmap, 200, 200).compress(Bitmap.CompressFormat.JPEG, 100, out);
//            out.flush();
//            out.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    private int getPhotoRotation() {
        int rotation;
        int orientation = mPhotoAngle;

        Camera.CameraInfo info = new Camera.CameraInfo();
        if (flag == 0) {
            Camera.getCameraInfo(0, info);
        } else {
            Camera.getCameraInfo(1, info);
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            rotation = (info.orientation - orientation + 360) % 360;
        } else {
            rotation = (info.orientation + orientation) % 360;
        }
        return rotation;
    }

    private void OrientationDetection() {

        myOrientationEventListener = new OrientationEventListener(context, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int iAngle) {

                final int iLookup[] = {0, 0, 0, 90, 90, 90, 90, 90, 90, 180, 180,
                        180, 180, 180, 180, 270, 270, 270, 270, 270, 270, 0, 0, 0}; // 15-degree increments
                if (iAngle != ORIENTATION_UNKNOWN) {

                    int iNewOrientation = iLookup[iAngle / 15];
                    if (iOrientation != iNewOrientation) {
                        iOrientation = iNewOrientation;
                        if (iOrientation == 0) {
                            mOrientation = 90;
                        } else if (iOrientation == 270) {
                            mOrientation = 0;
                        } else if (iOrientation == 90) {
                            mOrientation = 180;
                        }

                    }
                    mPhotoAngle = normalize(iAngle);
                }
            }
        };

        if (myOrientationEventListener.canDetectOrientation()) {
            myOrientationEventListener.enable();
        }

    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int height = resolveSize(getSuggestedMinimumHeight(),
                heightMeasureSpec);
        setMeasuredDimension(width, height);


        if (mSupportedPreviewSizes != null) {
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width,
                    height);
        }
        if (mPreviewSize != null) {
            float ratio;
            //  initFocusDrawingTools(mPreviewSize.width, mPreviewSize.height);
            if (mPreviewSize.height >= mPreviewSize.width)
                ratio = (float) mPreviewSize.height / (float) mPreviewSize.width;
            else
                ratio = (float) mPreviewSize.width / (float) mPreviewSize.height;

            // One of these methods should be used, second method squishes preview
            setMeasuredDimension(width, (int) (width * ratio));
            //        setMeasuredDimension((int) (width * ratio), height);
        }


    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }



}
