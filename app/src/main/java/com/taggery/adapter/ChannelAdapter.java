package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.fragment.ChannelFragment;
import com.taggery.utils.TaggeryConstants;
import com.twilio.chat.CallbackListener;
import com.twilio.chat.Channel;
import com.twilio.chat.Message;
import com.twilio.chat.Messages;
import com.twilio.chat.StatusListener;

import java.util.List;

import Utils.OnItemClickListener;
import twilio.ChannelModel;
import twilio.DateFormatter;


public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.MyViewHolder> {

    // store channels Descriptor list
    private List<ChannelModel> mChannelModels;
    // store context for easy access
    private Context mContext;
    public ChannelFragment fragment;
    OnItemClickListener onItemClickListener;

    public ChannelAdapter(List<ChannelModel> mChannelModels, Context mContext,ChannelFragment fragment) {
        this.mChannelModels = mChannelModels;
        this.mContext = mContext;
        this.fragment =fragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView text_username,text_recentchat,text_time,text_unreadmsgcount;
        private RelativeLayout relative_child;

        public MyViewHolder(View view) {
            super(view);
            text_username= view.findViewById(R.id.text_username);
            text_recentchat = view.findViewById(R.id.text_recentchat);
            relative_child = view.findViewById(R.id.relative_child);
            text_time = view.findViewById(R.id.text_time);
            text_unreadmsgcount = view.findViewById(R.id.text_unreadmsgcount);
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_item, parent, false);

        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // get the data model based on position
        ChannelModel model = mChannelModels.get(position);
        holder.text_username.setText(model.getFriendlyName());
        holder.text_time.setText("12.30pm");
        model.getUnconsumedMessagesCount(new CallbackListener<Long>() {
            @Override
            public void onSuccess(Long aLong) {
                holder.text_unreadmsgcount.setText(""+aLong);
            }
        });
        model.getChannel(new CallbackListener<Channel>() {
            @Override
            public void onSuccess(Channel channel) {
               Messages messagesObject = channel.getMessages();
               final String msg="";

                if (messagesObject != null) {
                    messagesObject.getLastMessages(1, new CallbackListener<List<Message>>() {
                        @Override
                        public void onSuccess(List<Message> messageList) {
                            Log.i("chatscreen", "" + messageList.size());
                            if (messageList.get(0).hasMedia()){
                                holder.text_recentchat.setText(messageList.get(0).getMedia().getType());
                            }
                            else {
                                holder.text_recentchat.setText(messageList.get(0).getMessageBody());
                            }


                        }
                    });
                }

            }
        });
       /* holder.relative_child.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ChannelModel channelModel = mChannelModels.get(position);
                channelModel.getChannel(new CallbackListener<Channel>() {
                    @Override
                    public void onSuccess(Channel channel) {
                        deleteChannelWithHandler(channel, new StatusListener() {
                            @Override
                            public void onSuccess() {
                                fragment.getChannels();
                                Toast.makeText(mContext, "channel deleted", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
                return true;
            }
        });*/
        holder.relative_child.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onItemClickListener.OnItemClickSecond(position,view);
                return true;
            }
        });
        holder.relative_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onItemClickListener.OnItemClick(position,view);

               /* ChannelModel channelModel = mChannelModels.get(position);
                channelModel.getChannel(new CallbackListener<Channel>() {
                    @Override
                    public void onSuccess(Channel channel) {
                        Log.d("ChannelsAdapter", channel.getSid());
                        Intent i = new Intent(mContext, ChatActivity.class); // instead of v.getContext()->context can also be used
                        i.putExtra(TaggeryConstants.EXTRA_CHANNEL_SID, channel.getSid());
                        i.putExtra(TaggeryConstants.EXTRA_CHANNEL, (Parcelable) channel);
                        // start the activity
                        mContext.startActivity(i);
                    }
                });
*/
            }
        });

    }

    public void setItemclickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public void deleteChannelWithHandler(Channel channel, StatusListener handler) {
        channel.destroy(handler);
    }

    @Override
    public int getItemCount() {
        return mChannelModels.size();
    }
}