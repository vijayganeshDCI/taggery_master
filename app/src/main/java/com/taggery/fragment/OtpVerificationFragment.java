package com.taggery.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;
import com.taggery.R;
import com.taggery.activity.LoginActivity;
import com.taggery.activity.MainActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.RegistrationParam;
import com.taggery.model.RegistrationResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.UiUtils;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;
import com.taggery.view.CustomTextViewBold;

import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by iyyapparajr on 4/19/2018.
 */

public class OtpVerificationFragment extends BaseFragment implements VerificationListener {


    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.relative_otp_head)
    RelativeLayout relativeOtpHead;
    @BindView(R.id.edit_otp_one)
    CustomEditText editOtpOne;
    @BindView(R.id.edit_otp_two)
    CustomEditText editOtpTwo;
    @BindView(R.id.edit_otp_three)
    CustomEditText editOtpThree;
    @BindView(R.id.edit_otp_four)
    CustomEditText editOtpFour;
    @BindView(R.id.text_verification_hint)
    CustomTextView textVerificationHint;
    @BindView(R.id.text_verification_nuumber)
    CustomTextView textVerificationNuumber;
    @BindView(R.id.button_resend)
    CustomButton buttonResend;
    @BindView(R.id.button_submit)
    CustomButton buttonSubmit;
    @BindView(R.id.cons_otp)
    ConstraintLayout consOtp;
    Unbinder unbinder;
    boolean forForgotPassword;
    @BindView(R.id.cons_otp_view)
    ConstraintLayout consOtpView;
    @BindView(R.id.edit_password)
    CustomEditText editPassword;
    @BindView(R.id.text_input_password)
    TextInputLayout textInputPassword;
    @BindView(R.id.edit_confirm_password)
    CustomEditText editConfirmPassword;
    @BindView(R.id.text_input_confirm_password)
    TextInputLayout textInputConfirmPassword;
    @BindView(R.id.cons_password)
    ConstraintLayout consPassword;
    @BindView(R.id.text_label_title)
    CustomTextViewBold textLabelTitle;
    @BindView(R.id.text_verification_timer)
    CustomTextView textVerificationTimer;
    private String phoneNumber, firstName, lastName;
    private String userName, password, gender;
    private String relationShipStatus, email, proPic;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private Bundle bundle;
    private String countryCode;
    private Verification mVerification;
    LoginActivity loginActivity;
    private CountDownTimer countDownTimer;
    @Inject
    TaggeryAPI taggeryAPI;
    RegistrationResponse registrationResponse;
    SharedPreferences fcmSharedPerf;
    private String fcmToken, profilePicturePath;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp_verification, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        loginActivity = (LoginActivity) getActivity();
        editor = sharedPreferences.edit();
        fcmSharedPerf = getActivity().getSharedPreferences(TaggeryConstants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        fcmToken = fcmSharedPerf.getString(TaggeryConstants.FCMTOKEN, "");
        bundle = getArguments();
        startTimer();
        editWatcher();
        buttonResend.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        buttonSubmit.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        textInputPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        textInputConfirmPassword.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fontName/Montserrat-Regular.ttf"));
        Bundle bundle = getArguments();
        if (getArguments() != null) {
            forForgotPassword = bundle.getBoolean("forForgotPassword", false);
            phoneNumber = bundle.getString("phoneNumber");
            countryCode = bundle.getString("countryCode");
        }

        if (forForgotPassword) {
            //update password
            textLabelTitle.setText(getString(R.string.reset_password));
        } else {
            //registration
            textLabelTitle.setText(getString(R.string.verification_code));
            if (bundle != null) {
                firstName = bundle.getString("firstName");
                lastName = bundle.getString("lastName");
                userName = bundle.getString("userName");
                gender = bundle.getString("gender");
                relationShipStatus = bundle.getString("relationShipStatus");
                email = bundle.getString("email");
                proPic = bundle.getString("profilePicture");
                password = bundle.getString("password");
                profilePicturePath = bundle.getString("profilePicturePath");
            }
        }

        initiateVerification();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        countDownTimer.cancel();
        unbinder.unbind();
    }


    @OnClick({R.id.button_resend, R.id.button_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_resend:
                resendCode();
                break;
            case R.id.button_submit:
                countDownTimer.cancel();
                otpValidation();
                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0])) {
                Toast.makeText(getActivity(), "This application needs permission to read your SMS to automatically verify your "
                        + "phone, you may disable the permission once you have been verified.", Toast.LENGTH_LONG)
                        .show();
            }
        }
        initiateVerificationAndSuppressPermissionCheck();
    }

    void initiateVerification() {
        initiateVerification(false);
    }

    void initiateVerificationAndSuppressPermissionCheck() {
        initiateVerification(true);
    }

    void initiateVerification(boolean skipPermissionCheck) {
        if (bundle != null) {
            textVerificationNuumber.setText("+" + countryCode + "-" + phoneNumber);
            createVerification(phoneNumber, skipPermissionCheck, countryCode);
        }
    }

    public void resendCode() {
        startTimer();
        initiateVerification();
    }

    void createVerification(String phoneNumber, boolean skipPermissionCheck, String countryCode) {
        if (!skipPermissionCheck && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS) ==
                PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, 0);
            hideProgress();
        } else {
            mVerification = SendOtpVerification.createSmsVerification
                    (SendOtpVerification
                            .config(countryCode + phoneNumber)
                            .context(getActivity())
                            .autoVerification(false)
                            .otplength("4")
                            .expiry("5")
                            .httpsConnection(false)
                            .senderId("TAGGERY")
                            .build(), this);
            mVerification.initiate();
        }
    }

    @Override
    public void onInitiated(String response) {

    }

    @Override
    public void onInitiationFailed(Exception paramException) {
        hideProgress();
    }

    @Override
    public void onVerified(String response) {
        hideProgress();
        countDownTimer.cancel();
        if (forForgotPassword) {
            UpdatePasswordFragment updatePasswordFragment = new UpdatePasswordFragment();
            Bundle bundle = new Bundle();
            bundle.putString("countryCode", countryCode);
            bundle.putString("phoneNumber", phoneNumber);
            updatePasswordFragment.setArguments(bundle);
            loginActivity.push(updatePasswordFragment);
        } else
            cloudinaryUpload(profilePicturePath, proPic, getString(R.string.Profile),
                    getString(R.string.image));
    }

    @Override
    public void onVerificationFailed(Exception paramException) {
        hideProgress();
        countDownTimer.cancel();
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater1 = this.getLayoutInflater();
        final View dialogView = inflater1.inflate(R.layout.alert_otp_verified, null);
        alertDialog.setView(dialogView);
        TextView textView = (TextView) dialogView.findViewById(R.id.text_alert_mes);
        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_alert_icon);
        Button button = (Button) dialogView.findViewById(R.id.button_ok);
        textView.setText(getString(R.string.otp_verification_failed));
//        imageView.setImageResource(R.drawable.animated_vector_cross);
        //imageView.setImageResource(R.mipmap.icon_failed);
        final AlertDialog show = alertDialog.show();
//        ((Animatable) imageView.getDrawable()).start();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        textVerificationTimer.setVisibility(View.GONE);
        buttonResend.setVisibility(View.VISIBLE);
    }


    public void otpValidation() {
        if (editOtpOne.getText().length() != 0 &&
                editOtpTwo.getText().length() != 0 &&
                editOtpThree.getText().length() != 0 &&
                editOtpFour.getText().length() != 0) {
            if (mVerification != null) {
                showProgress();
                mVerification.verify(editOtpOne.getText().toString() + editOtpTwo.getText().toString() +
                        editOtpThree.getText().toString() + editOtpFour.getText().toString());
            }

        } else {
            Toast.makeText(getActivity(), getString(R.string.invaild_otp), Toast.LENGTH_SHORT).show();
        }

    }

    private void editWatcher() {

        editOtpOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if (s.length() == 1) {
                    editOtpTwo.requestFocus();
                }
            }
        });

        editOtpTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() == 1) {
                    editOtpThree.requestFocus();
                } else if (s.length() == 0) {
                    editOtpOne.requestFocus();
                }
            }
        });

        editOtpThree.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() == 1) {
                    editOtpFour.requestFocus();
                } else if (s.length() == 0) {
                    editOtpTwo.requestFocus();
                }
            }
        });
        editOtpFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                if (s.length() == 0) {
                    editOtpThree.requestFocus();
                }
                UiUtils.hideKeyboard(getActivity());
            }
        });

    }

    private void startTimer() {
        textVerificationTimer.setVisibility(View.VISIBLE);
        buttonResend.setVisibility(View.GONE);
        countDownTimer = new CountDownTimer(60000, 1000) {
            int secondsLeft = 0;

            public void onTick(long ms) {
                if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                    secondsLeft = Math.round((float) ms / 1000.0f);
                    textVerificationTimer.setText(getString(R.string.otp_expries) + " " +
                            secondsLeft + " " + getString(R.string.otp_expries_seconds));
                }
            }

            public void onFinish() {
                textVerificationTimer.setVisibility(View.GONE);
                buttonResend.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    private void userRegistration() {
        if (Util.isNetworkAvailable()) {
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setFirstName(firstName);
            registrationParam.setLastName(lastName);
            registrationParam.setUserName(userName);
            registrationParam.setCountryCode("+" + countryCode);
            registrationParam.setPhoneNumber(phoneNumber);
            registrationParam.setGender(gender);
            registrationParam.setRelationShipStatus(relationShipStatus);
            registrationParam.setPassword(password);
            registrationParam.setEmailID(email);
            registrationParam.setProfilePic(proPic);
            registrationParam.setFCMKey(fcmToken);
            registrationParam.setDeviceID(sharedPreferences.getString(TaggeryConstants.DEVICEID, ""));
            registrationParam.setDeviceType("Android");
            taggeryAPI.registration(registrationParam).enqueue(new Callback<RegistrationResponse>() {
                @Override
                public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                    hideProgress();
                    registrationResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (registrationResponse.getStatusCode() == 200) {
                            if (registrationResponse.getResult() != null) {
                                loginActivity.push(new ContactListFragment());
                                editor.putString(TaggeryConstants.PHONE_NUMBER,
                                        registrationResponse.getResult().getUserMobileNumber() != null ?
                                                registrationResponse.getResult().getUserMobileNumber() : "").commit();
                                editor.putString(TaggeryConstants.COUNTRY_CODE_PHONE_NUMBER,
                                        registrationResponse.getResult().getUserCountryCode() != null ?
                                                registrationResponse.getResult().getUserCountryCode() : "").commit();
                                editor.putInt(TaggeryConstants.USER_ID,
                                        registrationResponse.getResult().getId()).commit();
                                editor.putString(TaggeryConstants.USER_NAME,
                                        registrationResponse.getResult().getUserName() != null ?
                                                registrationResponse.getResult().getUserName() : "").commit();
                                editor.putString(TaggeryConstants.FIRST_NAME,
                                        registrationResponse.getResult().getUserFirstName() != null ?
                                                registrationResponse.getResult().getUserFirstName() : "").commit();
                                editor.putString(TaggeryConstants.LAST_NAME,
                                        registrationResponse.getResult().getUserLastName() != null ?
                                                registrationResponse.getResult().getUserLastName() : "").commit();
                                editor.putString(TaggeryConstants.USER_EMAIL,
                                        registrationResponse.getResult().getUserEmail() != null ?
                                                registrationResponse.getResult().getUserEmail() : "").commit();
                                editor.putString(TaggeryConstants.USER_GENDER,
                                        registrationResponse.getResult().getUserGender() != null ?
                                                registrationResponse.getResult().getUserGender() : "").commit();
                                editor.putString(TaggeryConstants.USER_RELATION_SHIP_STATUS,
                                        registrationResponse.getResult().getUserRelationshipStatus() != null ?
                                                registrationResponse.getResult().getUserRelationshipStatus() : "").commit();
                                editor.putInt(TaggeryConstants.USER_PROFILE_TYPE,
                                        registrationResponse.getResult().getUserProfileType()).commit();
                                editor.putString(TaggeryConstants.USER_PROFILE_PIC,
                                        registrationResponse.getResult().getUserPicture() != null ?
                                                registrationResponse.getResult().getUserPicture() : "").commit();
                            }

                        } else {
                            showShackError(registrationResponse.getMessage(), consOtp);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consOtp);
                    }
                }

                @Override
                public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consOtp);
                }
            });


        } else {
            showShackError(getString(R.string.no_network), consOtp);
        }
    }

    private void cloudinaryUpload(String path, final String profileImageName, String folderName, String mediaType) {
        showProgress();
        initializeCloudinary();
        MediaManager.get().upload(path)
                .option("public_id", profileImageName)
                .option("resource_type", mediaType)
                .option("folder", folderName)
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {
                       /* Double progress = (double) bytes/totalBytes;
                        String numberD = String.valueOf(progress);
                        numberD = numberD.substring ( numberD.indexOf ( "." ) );
                        Toast.makeText(getActivity(), numberD, Toast.LENGTH_SHORT).show();*/
                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        userRegistration();
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        hideProgress();
                        showShackError(getString(R.string.media_upload_error), consOtp);
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {
                    }
                }).dispatch();

    }


}
