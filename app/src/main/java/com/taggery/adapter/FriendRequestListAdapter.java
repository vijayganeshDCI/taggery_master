package com.taggery.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.activity.FriendRequestActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.FriendRequestInputParam;
import com.taggery.model.SuggestionItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendRequestListAdapter extends BaseAdapter {

    Context context;
    ArrayList<SuggestionItem> friendRequestLists;
    private LayoutInflater mInflater;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    CheckUserNameResponse checkUserNameResponse;
    private FriendRequestActivity baseActivity;


    public FriendRequestListAdapter(ArrayList<SuggestionItem> friendRequestLists,
                                    FriendRequestActivity baseActivity) {
        this.context = baseActivity;
        this.friendRequestLists = friendRequestLists;
        this.baseActivity = baseActivity;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TaggeryApplication.getContext().getComponent().inject(this);
    }

    @Override
    public int getCount() {
        return friendRequestLists.size();
    }

    @Override
    public SuggestionItem getItem(int position) {
        return friendRequestLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_friend_request_list, null);
            viewHolder = new ViewHolder(convertView);
            viewHolder.buttonAccept.setTypeface(Typeface.createFromAsset(context.getAssets(),
                    "fontName/Montserrat-Regular.ttf"));
            viewHolder.buttonReject.setTypeface(Typeface.createFromAsset(context.getAssets(),
                    "fontName/Montserrat-Regular.ttf"));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        buttonFollowStyle(viewHolder.buttonAccept, context.getString(R.string.accept),
                context.getResources().getColor(R.color.colorPrimaryDark),
                context.getResources().getColor(R.color.white), true);
        buttonFollowStyle(viewHolder.buttonReject, context.getString(R.string.reject),
                context.getResources().getColor(R.color.white),
                context.getResources().getColor(R.color.colorPrimaryDark), true);


        viewHolder.textUserName.setText(friendRequestLists.get(position).getUserFirstName());
        picassoImageHolder(viewHolder.imageProPic,
                friendRequestLists.get(position).getUserPicture(),
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                context.getString(R.string.cloudinary_download_profile_picture));


        viewHolder.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptAndRejectRequest(viewHolder.consItemFriendReqList,
                        friendRequestLists.get(position).getId(),viewHolder.buttonAccept,1,position);
            }
        });
        viewHolder.buttonReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptAndRejectRequest(viewHolder.consItemFriendReqList,
                        friendRequestLists.get(position).getId(),viewHolder.buttonAccept,2,position);
            }
        });

        return convertView;
    }

    private void buttonFollowStyle(CustomButton button, String status, int bgColor, int bgTextColor, boolean isEnable) {
        button.setText(status);
        button.setTextColor(bgTextColor);
        button.setEnabled(isEnable);
        buttonShapeColor(bgColor, button);
    }


    private void buttonShapeColor(int color, CustomButton customButton) {
        ShapeDrawable footerBackground = new ShapeDrawable();
        float[] radii = new float[8];
        for (int i = 0; i < 8; i++) {
            radii[i] = 20;
        }
        footerBackground.setShape(new RoundRectShape(radii, null, null));
        footerBackground.getPaint().setColor(color);
        customButton.setBackgroundDrawable(footerBackground);
    }


    public void picassoImageHolder(final ImageView imageViewProPic,
                                   String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                //ProPic
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }

    public void acceptAndRejectRequest(final ConstraintLayout consItemFriendReqList,
                                       int id, final CustomButton butto, int requestStatus , final int pos) {

        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            FriendRequestInputParam friendRequestInputParam = new FriendRequestInputParam();
            friendRequestInputParam.setFollowerUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            friendRequestInputParam.setFollowingUserID(id);
            friendRequestInputParam.setRequestAcceptStatus(requestStatus);
            taggeryAPI.acceptAndRejectRequest(friendRequestInputParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    baseActivity.hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            Snackbar snackbarError = Snackbar.make(consItemFriendReqList,
                                    checkUserNameResponse.getMessage()!=null?
                                            checkUserNameResponse.getMessage():"", Snackbar.LENGTH_SHORT);
                            snackbarError.show();
                            friendRequestLists.remove(pos);
                            notifyDataSetChanged();
                            if (friendRequestLists.size()==0)
                                baseActivity.onBackPressed();
                        } else {
                            Snackbar snackbarError = Snackbar.make(consItemFriendReqList,
                                    context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                            snackbarError.show();
                        }
                    } else {
                        Snackbar snackbarError = Snackbar.make(consItemFriendReqList,
                                context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                        snackbarError.show();
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Snackbar snackbarError = Snackbar.make(consItemFriendReqList,
                            context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                    snackbarError.show();
                }
            });

        } else {
            Snackbar snackbarError = Snackbar.make(consItemFriendReqList,
                    context.getString(R.string.no_network), Snackbar.LENGTH_SHORT);
            snackbarError.show();
        }
    }

    static class ViewHolder {
        @BindView(R.id.image_pro_pic)
        CircleImageView imageProPic;
        @BindView(R.id.text_user_name)
        CustomTextView textUserName;
        @BindView(R.id.button_accept)
        CustomButton buttonAccept;
        @BindView(R.id.button_reject)
        CustomButton buttonReject;
        @BindView(R.id.cons_item_friend_req_list)
        ConstraintLayout consItemFriendReqList;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
