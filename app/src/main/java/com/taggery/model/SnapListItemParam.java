package com.taggery.model;

import java.util.List;

public class SnapListItemParam {

    public int getSnapID() {
        return snapID;
    }

    public void setSnapID(int snapID) {
        this.snapID = snapID;
    }

    public String getSnapPicture() {
        return snapPicture;
    }

    public void setSnapPicture(String snapPicture) {
        this.snapPicture = snapPicture;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserProPic() {
        return userProPic;
    }

    public void setUserProPic(String userProPic) {
        this.userProPic = userProPic;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

    public int getIsViewed() {
        return isViewed;
    }

    public void setIsViewed(int isViewed) {
        this.isViewed = isViewed;
    }

    public String getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(String postedTime) {
        this.postedTime = postedTime;
    }

    public List<SuggestionItem> getSuggestionItemList() {
        return suggestionItemList;
    }

    public void setSuggestionItemList(List<SuggestionItem> suggestionItemList) {
        this.suggestionItemList = suggestionItemList;
    }

    public SnapListItemParam(int snapID, String snapPicture, String firstName, String lastName, String userProPic,
                             int likeCount, int viewCount, int isLiked,
                             int isViewed, String postedTime, List<SuggestionItem> suggestionItemList, int id,
                             List<PostDetails> releatedPost,int mediaType,int UserlikeCount) {
        this.snapID = snapID;
        this.snapPicture = snapPicture;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userProPic = userProPic;
        this.likeCount = likeCount;
        this.viewCount = viewCount;
        this.isLiked = isLiked;
        this.isViewed = isViewed;
        this.postedTime = postedTime;
        this.suggestionItemList = suggestionItemList;
        this.id = id;
        this.releatedPost = releatedPost;
        this.mediaType = mediaType;
        this.UserlikeCount=UserlikeCount;
    }

    private int snapID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    private String snapPicture;
    private String firstName;
    private String lastName;
    private String userProPic;
    private int likeCount;

    public int getUserlikeCount() {
        return UserlikeCount;
    }

    public void setUserlikeCount(int userlikeCount) {
        UserlikeCount = userlikeCount;
    }

    private int UserlikeCount;
    private int viewCount;

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    private int mediaType;
    private int isLiked;
    private int isViewed;
    private String postedTime;

    public List<PostDetails> getReleatedPost() {
        return releatedPost;
    }

    public void setReleatedPost(List<PostDetails> releatedPost) {
        this.releatedPost = releatedPost;
    }

    private List<PostDetails> releatedPost;
    private List<SuggestionItem> suggestionItemList;
}
