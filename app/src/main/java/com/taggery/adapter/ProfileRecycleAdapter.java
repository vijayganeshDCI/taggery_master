package com.taggery.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.activity.GalleryPreviewActivity;
import com.taggery.model.ProfileUpload;

import java.util.List;

public class ProfileRecycleAdapter extends RecyclerView.Adapter<ProfileRecycleAdapter.ViewHolder> {

    Context context;
    private List<ProfileUpload> image;

    public ProfileRecycleAdapter(Context context, List<ProfileUpload> image) {
        this.context = context;
        this.image = image;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_profile_upload, parent, false);

        return new ProfileRecycleAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        ProfileUpload model = image.get(position);

        if (image.size() != 0) {
            if (model.getPostMediaType() == 2) {
                holder.img_video.setVisibility(View.VISIBLE);
                picassoImageHolder(holder.icon, model.getPostImage() + ".jpg",
                        R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                        context.getString(R.string.cloudinary_download_video_add_post));

            } else {
                holder.img_video.setVisibility(View.INVISIBLE);
                picassoImageHolder(holder.icon, model.getPostImage(),
                        R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                        context.getString(R.string.cloudinary_download_post));
            }

        } else {
            Toast.makeText(context, "no image", Toast.LENGTH_SHORT).show();

        }

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GalleryPreviewActivity.class);
                intent.putExtra("postID",image.get(position).getPostID());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return image.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon, img_video;

        public ViewHolder(View view) {
            super(view);
            icon = view.findViewById(R.id.image_upload_media);
            img_video = view.findViewById(R.id.img_video_icon);
        }
    }

    public void picassoImageHolder(final ImageView imageViewProPic, String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }
}
