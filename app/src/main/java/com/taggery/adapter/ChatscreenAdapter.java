package com.taggery.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.jsibbold.zoomage.ZoomageView;
import com.taggery.R;
import com.twilio.chat.Channel;
import com.twilio.chat.ErrorInfo;
import com.twilio.chat.Message;
import com.twilio.chat.ProgressListener;
import com.twilio.chat.StatusListener;

import java.io.ByteArrayOutputStream;
import java.util.List;

import twilio.DateFormatter;


public class ChatscreenAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private static final int VIEW_TYPE_MEDIA_SENT = 3;
    private static final int VIEW_TYPE_MEDIA_RECEIVE = 4;
    Channel channel;
    boolean sendRecive = false;
    Bitmap bitmap;
    ProgressDialog progressDialog;
    SimpleExoPlayerView video_player_chat;
    SimpleExoPlayer player;


    private List<Message> messages;
    private Context context;
    public ChatscreenAdapter(Channel channel ,List<Message> messages, Context context) {
        this.messages = messages;
        this.context=context;
        this.channel =channel;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.childlayout_sendchat, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.childlayout_receivechat, parent, false);
            return new ReceivedMessageHolder(view);
        }
        else if (viewType == VIEW_TYPE_MEDIA_SENT){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.childlayout_sendmedia, parent, false);
            return new SentMediaHolder(view);

        }
        else if (viewType == VIEW_TYPE_MEDIA_RECEIVE){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.childlayout_recievemedia, parent, false);
            return new ReceivedMediaHolder(view);

        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Message message = (Message) messages.get(position);


        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MEDIA_SENT:
                ((SentMediaHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MEDIA_RECEIVE :
                ((ReceivedMediaHolder)holder).bind(message);
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        Message message = (Message) messages.get(position);

        if (message.getAuthor().equals("UserAjith")) {
            if (message.hasMedia()){
                return VIEW_TYPE_MEDIA_SENT;
            }
            else {
                return VIEW_TYPE_MESSAGE_SENT;
            }

        } else {
            if (message.hasMedia()){
                return VIEW_TYPE_MEDIA_RECEIVE;
            }
            else {
                return VIEW_TYPE_MESSAGE_RECEIVED;
            }
            // If some other user sent the message

        }
    }
    /* public void addMessage(Message message) {
        messages.add(new UserMessage(message));
        notifyDataSetChanged();
    }
    public void setMessages(List<Message> messages) {
        this.messages = convertTwilioMessages(messages);
        this.statusMessageSet.clear();
        notifyDataSetChanged();
    }
    private List<ChatMessage> convertTwilioMessages(List<Message> messages) {
        List<ChatMessage> chatMessages = new ArrayList<>();
        for (Message message : messages) {
            chatMessages.add(new UserMessage(message));
        }
        return chatMessages;
    }*/

    @Override
    public int getItemCount() {

        return messages.size();
    }
    /*public class ViewHolder extends RecyclerView.ViewHolder
    {
        RelativeLayout leftMsgLayout;
        TextView leftMsgTextView,text_sendtimestamp;

        public ViewHolder(View itemView)
        {
            super(itemView);
            if(itemView!=null) {
                leftMsgLayout = (RelativeLayout) itemView.findViewById(R.id.chat_left_msg_layout);
                leftMsgTextView = (TextView) itemView.findViewById(R.id.chat_left_msg_text_view);
                text_sendtimestamp = itemView.findViewById(R.id.text_receivetimestamp);

            }

        }
    }*/
    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage;
        RelativeLayout chat_recieve_msg_layout;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageText = (TextView) itemView.findViewById(R.id.chat_left_msg_text_view);
            timeText = (TextView) itemView.findViewById(R.id.text_receivetimestamp);
            chat_recieve_msg_layout = itemView.findViewById(R.id.chat_recieve_msg_layout);

        }

        void bind(final Message message) {
            messageText.setText(message.getMessageBody());
            chat_recieve_msg_layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    sendRecive = true;
                    showDialog("","Copy text",message,sendRecive);
                    return true;
                }
            });

            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateFormatter.getFormattedDateFromISOString(message.getDateCreated()));

            // Insert the profile image from the URL into the ImageView.
        }
    }
    private class ReceivedMediaHolder extends RecyclerView.ViewHolder {
        TextView timeText, nameText;
        ImageView profileImage,img_reciever;
        RelativeLayout chat_recieve_msg_layout;

        ReceivedMediaHolder(View itemView) {
            super(itemView);

            timeText = (TextView) itemView.findViewById(R.id.text_receivetimestamp);
            chat_recieve_msg_layout = itemView.findViewById(R.id.chat_recieve_msg_layout);
            img_reciever = itemView.findViewById(R.id.img_reciever);

        }
        void bind(final Message message) {
            if (message.hasMedia()){
                Message.Media media = message.getMedia();
                String sid = media.getSid();
                String type = media.getType();
                String fn = media.getFileName();
                long size = media.getSize();
                Log.i("chatadpater","This is a media message with SID "+sid+", type "+type+", name "+fn+", and size "+size);
                if (type.contentEquals("image/jpeg")){
                    final ByteArrayOutputStream out = new ByteArrayOutputStream();
                    media.download(out, new StatusListener() {
                        @Override
                        public void onSuccess() {
                            String content = out.toString();
                            byte b [] = out.toByteArray();
                            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                            bitmap.compress(Bitmap.CompressFormat.JPEG,10,out);
                            img_reciever.setImageBitmap(bitmap);
                            Log.i("chatadpater","Downloaded media");
                        }

                        @Override
                        public void onError(ErrorInfo errorInfo) {
                            super.onError(errorInfo);
                            Log.i("chatadpater","Error downloading media"+errorInfo);
                        }
                    }, new ProgressListener() {
                        @Override
                        public void onStarted() {
                            Log.i("chatadpater","Download started");


                        }

                        @Override
                        public void onProgress(long bytes) {
                            Log.i("chatadpater","Downloaded "+bytes+" bytes");

                        }

                        @Override
                        public void onCompleted(String s) {
                            Log.i("chatadpater","Download completed");

                        }
                    });
                }
            }

            img_reciever.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDialog("Remove message","",message, sendRecive);
                }
            });

            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateFormatter.getFormattedDateFromISOString(message.getDateCreated()));

            // Insert the profile image from the URL into the ImageView.
        }
    }
    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage;
        RelativeLayout chat_send_msg_layout;

        SentMessageHolder(View itemView) {
            super(itemView);
            messageText = (TextView) itemView.findViewById(R.id.chat_right_msg_text_view);
            timeText = (TextView) itemView.findViewById(R.id.text_sendtimestamp);
            chat_send_msg_layout = itemView.findViewById(R.id.chat_send_msg_layout);

        }

        void bind(final Message message) {
            messageText.setText(message.getMessageBody());
            chat_send_msg_layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    sendRecive = false;
                   // showDialog("Remove message","Copy text",message, sendRecive);
                    return true;
                }
            });


            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateFormatter.getFormattedDateFromISOString(message.getDateCreated()));

            // Insert the profile image from the URL into the ImageView.
        }
    }
    private class SentMediaHolder extends RecyclerView.ViewHolder {
        TextView timeText;
        ImageView profileImage,img_sender;
        RelativeLayout chat_send_msg_layout;



        SentMediaHolder(View itemView) {
            super(itemView);
            timeText = (TextView) itemView.findViewById(R.id.text_sendtimestamp);
            chat_send_msg_layout = itemView.findViewById(R.id.chat_send_msg_layout);
            img_sender = itemView.findViewById(R.id.img_sender);
            video_player_chat = itemView.findViewById(R.id.video_player_chat);

        }

        void bind(final Message message) {
            if (message.hasMedia()){
                Message.Media media = message.getMedia();
                String sid = media.getSid();
                String type = media.getType();
                String fn = media.getFileName();
                long size = media.getSize();
                Log.i("chatadpater","This is a media message with SID "+sid+", type "+type+", name "+fn+", and size "+size);
                if (type.contentEquals("image/jpeg")){
                    final ByteArrayOutputStream out = new ByteArrayOutputStream();
                    media.download(out, new StatusListener() {
                        @Override
                        public void onSuccess() {
                            String content = out.toString();
                            byte b [] = out.toByteArray();
                            bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                            bitmap.compress(Bitmap.CompressFormat.JPEG,10,out);
                            img_sender.setImageBitmap(bitmap);
                            Log.i("chatadpater","Downloaded media");
                        }

                        @Override
                        public void onError(ErrorInfo errorInfo) {
                            super.onError(errorInfo);
                            Log.i("chatadpater","Error downloading media"+errorInfo);
                        }
                    }, new ProgressListener() {
                        @Override
                        public void onStarted() {
                            Log.i("chatadpater","Download started");

                        }

                        @Override
                        public void onProgress(long bytes) {
                            Log.i("chatadpater","Downloaded "+bytes+" bytes");

                        }

                        @Override
                        public void onCompleted(String s) {
                            Log.i("chatadpater","Download completed");

                        }
                    });
                }
            }

            chat_send_msg_layout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    sendRecive = false;
                 //   showDialog("Remove message","Copy text",message, sendRecive);
                    return true;
                }
            });
            img_sender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showDialog("Remove message","",message, sendRecive);
                }
            });
            img_sender.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    //showDialog("Remove message","Copy text",message, sendRecive);
                    return true;

                }
            });
            // Format the stored timestamp into a readable String using method.
            timeText.setText(DateFormatter.getFormattedDateFromISOString(message.getDateCreated()));

            // Insert the profile image from the URL into the ImageView.
        }
    }

    private void showDialog(String delete, String copy, final Message message, boolean sendRecive) {
        progressDialog = new ProgressDialog(context);
        final Dialog dialog = new Dialog(context,android.R.style.Theme_Light);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_alertdialog);
        TextView remove = (TextView) dialog.findViewById(R.id.text_removemsg);
        TextView copytext = dialog.findViewById(R.id.text_copytext);
        RelativeLayout relative_viewfull = dialog.findViewById(R.id.relative_viewfull);
        final ZoomageView img_viewfull = dialog.findViewById(R.id.img_viewfull);
        RelativeLayout relative_content = dialog.findViewById(R.id.relative_content);



        if (copy.equals("")){
            relative_viewfull.setVisibility(View.VISIBLE);
            relative_content.setVisibility(View.GONE);

            if (message.getMedia().getType().contentEquals("image/jpeg")){
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.getMedia().download(out, new StatusListener() {
                    @Override
                    public void onSuccess() {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        byte b [] = out.toByteArray();
                       bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,10,out);
                        img_viewfull.setImageBitmap(bitmap);
                        Log.i("chatadpater","Downloaded media");
                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                        super.onError(errorInfo);
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Log.i("chatadpater","Error downloading media"+errorInfo);
                    }
                }, new ProgressListener() {
                    @Override
                    public void onStarted() {
                        progressDialog.setMessage("loading...");
                        progressDialog.show();
                        Log.i("chatadpater","Download started");


                    }

                    @Override
                    public void onProgress(long bytes) {
                        Log.i("chatadpater","Downloaded "+bytes+" bytes");

                    }

                    @Override
                    public void onCompleted(String s) {
                        Log.i("chatadpater","Download completed");

                    }
                });
            }
            else if (message.getMedia().getType().contentEquals("video/mp4")){
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                message.getMedia().download(out, new StatusListener() {
                    @Override
                    public void onSuccess() {
                        byte b [] = out.toByteArray();
                        Toast.makeText(context, "video", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(ErrorInfo errorInfo) {
                        super.onError(errorInfo);
                        Log.i("chatadpater","Error downloading media"+errorInfo);
                    }
                }, new ProgressListener() {
                    @Override
                    public void onStarted() {
                        Log.i("chatadpater","Download started");

                    }

                    @Override
                    public void onProgress(long l) {
                        Log.i("chatadpater","Downloaded "+l+" bytes");

                    }

                    @Override
                    public void onCompleted(String s) {
                        Log.i("chatadpater","Download completed");

                    }
                });


                   /* try {
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/
            }

        }
        else {
            relative_content.setVisibility(View.VISIBLE);
            relative_viewfull.setVisibility(View.GONE);
            if (sendRecive){
                remove.setVisibility(View.GONE);
                copytext.setText(copy);
            }
            else {
                remove.setText(delete);
                copytext.setText(copy);
            }

            copytext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "Copied", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });

            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    channel.getMessages().removeMessage(message, new StatusListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(context, "deleted", Toast.LENGTH_SHORT).show();
                           // ((ChatActivity)context).loadMessages();
                            dialog.dismiss();
                        }

                        @Override
                        public void onError(ErrorInfo errorInfo) {
                            super.onError(errorInfo);
                            dialog.dismiss();
                            Toast.makeText(context, errorInfo.toString(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            });
        }

        dialog.show();
    }
}
