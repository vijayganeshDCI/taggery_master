package com.taggery.dagger;


import android.content.Context;
import android.content.SharedPreferences;

import com.taggery.application.TaggeryApplication;
import com.taggery.retrofit.TaggeryAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;


//provides instance or objects for a class
@Module
public class AppModule {

    public static final String TAGGERY_PREFS = "dmk";

    private final TaggeryApplication taggeryApp;

    public AppModule(TaggeryApplication app) {
        this.taggeryApp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return taggeryApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return taggeryApp.getSharedPreferences(TAGGERY_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public TaggeryAPI provideDMKApiInterface(Retrofit retrofit) {
        return retrofit.create(TaggeryAPI.class);
    }


}
