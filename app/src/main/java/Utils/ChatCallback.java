package Utils;

import android.view.View;

import com.taggery.model.TagPeopleList;

import java.util.List;
import java.util.Set;

public interface ChatCallback {
    public void ChatClick(Set<TagPeopleList> list);
}
