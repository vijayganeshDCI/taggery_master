package com.taggery.model;

public class RegistrationResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public RegistrationResult getResult() {
		return Result;
	}

	public void setResult(RegistrationResult result) {
		Result = result;
	}

	private String Status;
	private String Message;
	private int StatusCode;
	private RegistrationResult Result;
}
