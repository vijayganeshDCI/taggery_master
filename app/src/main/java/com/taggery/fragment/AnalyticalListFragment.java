package com.taggery.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;

import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.adapter.AnalyticalAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.AnalyticalListItem;
import com.taggery.model.AnalyticalListResponse;
import com.taggery.model.ContactListInputParam;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;
import com.taggery.view.CustomTextViewBold;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnalyticalListFragment extends BaseFragment {

    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.list_analytical)
    ListView listAnalytical;
    Unbinder unbinder;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    @BindView(R.id.swipe_analytical_list)
    SwipeRefreshLayout swipeAnalyticalList;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    AnalyticalListResponse analyticalListResponse;
    ArrayList<AnalyticalListItem> analyticalListItemArrayList;
    AnalyticalAdapter analyticalAdapter;
    BaseActivity baseActivity;
    @BindView(R.id.text_label_likes)
    CustomTextViewBold textLabelLikes;
    @BindView(R.id.text_label_likes_count)
    CustomTextView textLabelLikesCount;
    @BindView(R.id.text_label_likes_total_count)
    CustomTextView textLabelLikesTotalCount;
    @BindView(R.id.seekLikeCount)
    SeekBar seekLikeCount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_analytical_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        analyticalListItemArrayList = new ArrayList<AnalyticalListItem>();
        baseActivity = (BaseActivity) getActivity();
        getAnalyticalList();

        swipeAnalyticalList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeAnalyticalList.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAnalyticalList();
                        swipeAnalyticalList.setRefreshing(false);
                    }
                }, 000);
            }
        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void getAnalyticalList() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            analyticalListItemArrayList.clear();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.getAnalyticalList(contactListInputParam).enqueue(new Callback<AnalyticalListResponse>() {
                @Override
                public void onResponse(Call<AnalyticalListResponse> call, Response<AnalyticalListResponse> response) {
                    hideProgress();
                    analyticalListResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (analyticalListResponse.getStatusCode() == 200) {
                            listAnalytical.setVisibility(View.VISIBLE);
                            consWarn.setVisibility(View.GONE);
                            if (analyticalListResponse.getAnalytical() != null) {
                                if (analyticalListResponse.getAnalytical().getUserdetails() != null) {
                                    picassoImageHolder(imageProPic, analyticalListResponse.getAnalytical().getUserdetails().getUserPicture() != null ?
                                                    analyticalListResponse.getAnalytical().getUserdetails().getUserPicture() : "",
                                            R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                                            getString(R.string.cloudinary_download_profile_picture));
                                    profileRingColor(analyticalListResponse.getAnalytical().getUserdetails().getUserTotalLikesCount(), imageProPic);

                                    seekLikeCount.setMax(analyticalListResponse.getAnalytical().getUserdetails().getNearBigValue());
                                    seekLikeCount.setSecondaryProgress(analyticalListResponse.getAnalytical().getUserdetails().getLikes());
                                    textLabelLikesCount.setText("" + analyticalListResponse.getAnalytical().getUserdetails().getLikes());
                                    textLabelLikesTotalCount.setText(" / " + analyticalListResponse.getAnalytical().getUserdetails().getNearBigValue());
                                }

                                if (analyticalListResponse.getAnalytical().getAnalyticalList() != null &&
                                        analyticalListResponse.getAnalytical().getAnalyticalList().size() > 0) {
                                    analyticalListItemArrayList.addAll(analyticalListResponse.getAnalytical().getAnalyticalList());
                                    analyticalAdapter = new AnalyticalAdapter(getActivity(), analyticalListItemArrayList, baseActivity);
                                    listAnalytical.setAdapter(analyticalAdapter);

                                } else {
                                    listAnalytical.setVisibility(View.GONE);
                                    consWarn.setVisibility(View.VISIBLE);
                                    textWarn.setText(getString(R.string.no_recent_post));
                                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                                }
                            }


                        } else {
                            listAnalytical.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }

                    } else {
                        listAnalytical.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }
                }

                @Override
                public void onFailure(Call<AnalyticalListResponse> call, Throwable t) {
                    hideProgress();
                    listAnalytical.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                }
            });

        } else {
            listAnalytical.setVisibility(View.GONE);
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }
}
