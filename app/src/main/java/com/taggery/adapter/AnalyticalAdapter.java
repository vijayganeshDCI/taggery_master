package com.taggery.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.activity.BaseActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.AnalyticalListItem;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.FriendRequestInputParam;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomTextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AnalyticalAdapter extends BaseAdapter {


    Context context;
    List<AnalyticalListItem> analyticalListItemList;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    private LayoutInflater mInflater;
    private BaseActivity baseActivity;
    private CheckUserNameResponse checkUserNameResponse;

    public AnalyticalAdapter(Context context, List<AnalyticalListItem> analyticalListItemList,
                             BaseActivity baseActivity) {
        this.context = context;
        this.analyticalListItemList = analyticalListItemList;
        this.baseActivity = baseActivity;
        TaggeryApplication.getContext().getComponent().inject(this);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return analyticalListItemList.size();
    }

    @Override
    public AnalyticalListItem getItem(int position) {
        return analyticalListItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_analytical_list, null);
            viewHolder = new ViewHolder(convertView);
            viewHolder.buttonFollow.setTypeface(Typeface.createFromAsset(context.getAssets(),
                    "fontName/Montserrat-Regular.ttf"));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        picassoImageHolder(viewHolder.imageProPic,
                analyticalListItemList.get(position).getNotification_created_UserPicture(),
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                context.getString(R.string.cloudinary_download_profile_picture));
        profileRingColor(analyticalListItemList.get(position).getUserTotalLikesCount(),viewHolder.imageProPic);

        viewHolder.textAnalytical.setText(analyticalListItemList.get(position).getNotification_text());
        String[] time = analyticalListItemList.get(position).getNotification_created_at().split(" ");
        viewHolder.textAnalyticalTime.setText(time[0] + " " + time[1]);


        if (analyticalListItemList.get(position).getNotification_caption() == 0) {
            //post
            viewHolder.buttonFollow.setVisibility(View.INVISIBLE);
            viewHolder.imagePost.setVisibility(View.VISIBLE);
            if (analyticalListItemList.get(position).getNotification_postImageType() == 1) {
                viewHolder.imagePlay.setVisibility(View.GONE);
                picassoImageHolder(viewHolder.imagePost,
                        analyticalListItemList.get(position).getNotification_postImage(),
                        R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                        context.getString(R.string.cloudinary_download_snap_image_add_post));
            } else {
                viewHolder.imagePlay.setVisibility(View.VISIBLE);
                picassoImageHolder(viewHolder.imagePost,
                        analyticalListItemList.get(position).getNotification_postImage() + ".jpg",
                        R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                        context.getString(R.string.cloudinary_download_snap_video_add_post));
            }


        } else if (analyticalListItemList.get(position).getNotification_caption() == 1) {
            //follow
            viewHolder.buttonFollow.setVisibility(View.VISIBLE);
            viewHolder.imagePost.setVisibility(View.INVISIBLE);
            viewHolder.imagePlay.setVisibility(View.GONE);
            buttonFollowStyle(viewHolder.buttonFollow, context.getString(R.string.follow),
                    context.getResources().getColor(R.color.colorPrimaryDark),
                    context.getResources().getColor(R.color.white), true);

        } else if (analyticalListItemList.get(position).getNotification_caption() == 2) {
            //following
            viewHolder.buttonFollow.setVisibility(View.VISIBLE);
            viewHolder.imagePost.setVisibility(View.INVISIBLE);
            viewHolder.imagePlay.setVisibility(View.GONE);
            buttonFollowStyle(viewHolder.buttonFollow, context.getString(R.string.following),
                    context.getResources().getColor(R.color.white),
                    context.getResources().getColor(R.color.colorPrimaryDark), true);
        } else if (analyticalListItemList.get(position).getNotification_caption() == 3) {
            //Requested
            viewHolder.buttonFollow.setVisibility(View.VISIBLE);
            viewHolder.imagePost.setVisibility(View.INVISIBLE);
            viewHolder.imagePlay.setVisibility(View.GONE);
            buttonFollowStyle(viewHolder.buttonFollow, context.getString(R.string.requested),
                    context.getResources().getColor(R.color.white),
                    context.getResources().getColor(R.color.colorPrimaryDark), true);

        }

        viewHolder.buttonFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (analyticalListItemList.get(position).getNotification_caption()) {
                    case 1:
                        //follow
                        friendRequest(viewHolder.consAnalyList,
                                analyticalListItemList.get(position).getNotification_created_UserID(),
                                viewHolder.buttonFollow,position);
                        break;
                    case 2:
                        //following
                        alertDialog(2, position, viewHolder);
                        break;
                    case 3:
                        //requested
                        alertDialog(1, position, viewHolder);
                        break;
                    default:
                        break;


                }







            }
        });


        return convertView;
    }


    private void buttonFollowStyle(CustomButton button, String status, int bgColor, int bgTextColor, boolean isEnable) {
        button.setText(status);
        button.setTextColor(bgTextColor);
        button.setEnabled(isEnable);
        buttonShapeColor(bgColor, button);
    }


    private void buttonShapeColor(int color, CustomButton customButton) {
        ShapeDrawable footerBackground = new ShapeDrawable();
        float[] radii = new float[8];
        for (int i = 0; i < 8; i++) {
            radii[i] = 20;
        }
        footerBackground.setShape(new RoundRectShape(radii, null, null));
        footerBackground.getPaint().setColor(color);
        customButton.setBackgroundDrawable(footerBackground);
    }

    static class ViewHolder {
        @BindView(R.id.image_pro_pic)
        CircleImageView imageProPic;
        @BindView(R.id.text_analytical)
        CustomTextView textAnalytical;
        @BindView(R.id.text_analytical_time)
        CustomTextView textAnalyticalTime;
        @BindView(R.id.image_post)
        ImageView imagePost;
        @BindView(R.id.image_play)
        ImageView imagePlay;
        @BindView(R.id.button_follow)
        CustomButton buttonFollow;
        @BindView(R.id.cons_analy_list)
        ConstraintLayout consAnalyList;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void picassoImageHolder(final ImageView imageViewProPic,
                                   String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                //ProPic
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }


    public void
    friendRequest(final ConstraintLayout consItemContactList,
                  int id, final CustomButton button, final int position) {

        if (Util.isNetworkAvailable()) {
            baseActivity.showProgress();
            FriendRequestInputParam friendRequestInputParam = new FriendRequestInputParam();
            friendRequestInputParam.setFollowerUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            friendRequestInputParam.setFollowingUserID(id);
            taggeryAPI.friendRequest(friendRequestInputParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    baseActivity.hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            if (checkUserNameResponse.getRequestAcceptStatus() == 1) {
                                //requested
                                buttonFollowStyle(button, context.getString(R.string.requested),
                                        context.getResources().getColor(R.color.white),
                                        context.getResources().getColor(R.color.colorPrimaryDark), true);
                                analyticalListItemList.get(position).setNotification_caption(3);
                            } else if (checkUserNameResponse.getRequestAcceptStatus() == 2) {
                                // following
                                buttonFollowStyle(button, context.getString(R.string.following),
                                        context.getResources().getColor(R.color.white),
                                        context.getResources().getColor(R.color.colorPrimaryDark), true);
                                analyticalListItemList.get(position).setNotification_caption(2);
                            } else {
                                //follow
                                buttonFollowStyle(button, context.getString(R.string.follow),
                                        context.getResources().getColor(R.color.colorPrimaryDark),
                                        context.getResources().getColor(R.color.white), true);
                                analyticalListItemList.get(position).setNotification_caption(1);
                            }

                        } else {
                            Snackbar snackbarError = Snackbar.make(consItemContactList,
                                    context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                            snackbarError.show();
                        }
                    } else {
                        Snackbar snackbarError = Snackbar.make(consItemContactList,
                                context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                        snackbarError.show();
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    baseActivity.hideProgress();
                    Snackbar snackbarError = Snackbar.make(consItemContactList,
                            context.getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                    snackbarError.show();
                }
            });

        } else {
            Snackbar snackbarError = Snackbar.make(consItemContactList,
                    context.getString(R.string.no_network), Snackbar.LENGTH_SHORT);
            snackbarError.show();
        }
    }

    public void profileRingColor(int count, CircleImageView circleImageView) {
        circleImageView.setBorderWidth(3);
        if (count >= 5000 && count < 10000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.colorPrimaryDark));
        } else if (count >= 10000 && count < 15000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.blue));
        } else if (count >= 15000 && count < 25000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.gold));
        } else if (count >= 25000 && count < 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.black));
        } else if (count >= 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.white));
        } else {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.transparent_white));
        }
    }

    private void alertDialog(int status, final int position, final ViewHolder holder) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        View dialogView = mInflater.inflate(R.layout.alert_unfollow, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        ContactListAdapter.dialogViewHolder viewHolder = new ContactListAdapter.dialogViewHolder(dialogView);
        viewHolder.buttonOk.setTypeface(Typeface.createFromAsset(context.getAssets(),
                "fontName/Montserrat-Regular.ttf"));
        viewHolder.buttonCancel.setTypeface(Typeface.createFromAsset(context.getAssets(),
                "fontName/Montserrat-Regular.ttf"));
        try {
            picassoImageHolder(viewHolder.imageProPic,
                    analyticalListItemList.get(position).getNotification_created_UserPicture(),
                    R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                    context.getString(R.string.cloudinary_download_profile_picture));
        } catch (Exception e) {
            e.printStackTrace();
        }
        profileRingColor(analyticalListItemList.get(position).getUserTotalLikesCount(), viewHolder.imageProPic);
        if (status == 1) {
            //requested
            viewHolder.textUserName.setText(context.getString(R.string.Confirm_to_back_request));
            viewHolder.buttonOk.setText(context.getString(R.string.Undo));
        } else {
            //following
            viewHolder.textUserName.setText(context.getString(R.string.Confirm_to_unfollow_));
            viewHolder.buttonOk.setText(context.getString(R.string.UnFollow));
        }

        viewHolder.buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                friendRequest(holder.consAnalyList,
                        analyticalListItemList.get(position).getNotification_created_UserID(),
                        holder.buttonFollow,position);
            }
        });
        viewHolder.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


}