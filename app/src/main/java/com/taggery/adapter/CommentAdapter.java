package com.taggery.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.taggery.R;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CommentsdetailsItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.view.CustomTextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class CommentAdapter extends BaseAdapter {

    Context context;
    List<CommentsdetailsItem> commentsdetailsItemList;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    private LayoutInflater mInflater;

    public CommentAdapter(Context context, List<CommentsdetailsItem> commentsdetailsItemList) {
        this.context = context;
        this.commentsdetailsItemList = commentsdetailsItemList;
        TaggeryApplication.getContext().getComponent().inject(this);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return commentsdetailsItemList.size();
    }

    @Override
    public CommentsdetailsItem getItem(int position) {
        return commentsdetailsItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_comment, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        picassoImageHolder(viewHolder.imageProPic,
                commentsdetailsItemList.get(position).getCommentPicture(),
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                context.getString(R.string.cloudinary_download_profile_picture));
        profileRingColor(commentsdetailsItemList.get(position).getUserTotalLikesCount(),viewHolder.imageProPic);

        if (commentsdetailsItemList.get(position).getCommentUserID() ==
                sharedPreferences.getInt(TaggeryConstants.USER_ID, 0)) {
            viewHolder.textUserName.setText(context.getString(R.string.You));
        } else {
            viewHolder.textUserName.setText(commentsdetailsItemList.get(position).getCommentUserFirstName());
        }

        viewHolder.textComment.setText(commentsdetailsItemList.get(position).getCommentText());

        viewHolder.textComment.setText(commentsdetailsItemList.get(position).getCommentText());
        if (!commentsdetailsItemList.get(position).getCommentCreatedAt().equals(context.getString(R.string.now))){
            String[] postTime = commentsdetailsItemList.get(position).getCommentCreatedAt().split(" ");
            viewHolder.textCommentPostTime.setText("- " + postTime[0] + " " + postTime[1]);
        }else {
            viewHolder.textCommentPostTime.setText("- " + context.getString(R.string.now));
        }

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.image_pro_pic)
        CircleImageView imageProPic;
        @BindView(R.id.text_user_name)
        CustomTextView textUserName;
        @BindView(R.id.text_comment)
        CustomTextView textComment;
        @BindView(R.id.text_comment_post_time)
        CustomTextView textCommentPostTime;
        @BindView(R.id.cons_com_item)
        ConstraintLayout consComItem;
        @BindView(R.id.cons_time_detail_footer)
        ConstraintLayout consTimeDetailFooter;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void picassoImageHolder(final ImageView imageViewProPic,
                                   String imageName, int loadingImage,
                                   int emptyURIImage, String imageUrl) {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                //ProPic
                BitmapDrawable image = new BitmapDrawable(context.getResources(), bitmap);
                imageViewProPic.setImageDrawable(image);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                imageViewProPic.setImageDrawable(errorDrawable);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                imageViewProPic.setImageDrawable(placeHolderDrawable);
            }
        };
        imageViewProPic.setTag(target);
        Picasso.get().load(context.getString(R.string.cloudinary_base_url) + imageUrl + imageName)
                .placeholder(loadingImage).error(emptyURIImage).into(target);
    }

    public void profileRingColor(int count, CircleImageView circleImageView) {
        circleImageView.setBorderWidth(3);
        if (count >= 5000 && count < 10000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.colorPrimaryDark));
        } else if (count >= 10000 && count < 15000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.blue));
        } else if (count >= 15000 && count < 25000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.gold));
        } else if (count >= 25000 && count < 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.black));
        } else if (count >= 50000) {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.white));
        } else {
            circleImageView.setBorderColor(context.getResources().getColor(R.color.transparent_white));
        }
    }
}
