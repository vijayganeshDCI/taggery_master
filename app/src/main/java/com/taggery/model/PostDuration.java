package com.taggery.model;

public class PostDuration {

    public PostDuration(String posttime) {
        this.posttime = posttime;
    }

    public String getPosttime() {
        return posttime;
    }

    public void setPosttime(String posttime) {
        this.posttime = posttime;
    }

    String posttime;
}
