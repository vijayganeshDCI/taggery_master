package com.taggery.model;

import java.util.List;

public class PostsItem{
	public String getPostImage() {
		return PostImage;
	}

	public void setPostImage(String postImage) {
		PostImage = postImage;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getFeedtime() {
		return Feedtime;
	}

	public void setFeedtime(String feedtime) {
		Feedtime = feedtime;
	}

	public int getViews() {
		return Views;
	}

	public void setViews(int views) {
		Views = views;
	}

	public String getUserPicture() {
		return UserPicture;
	}

	public void setUserPicture(String userPicture) {
		UserPicture = userPicture;
	}

	public int getViewCount() {
		return ViewCount;
	}

	public void setViewCount(int viewCount) {
		ViewCount = viewCount;
	}

	public int getSeenStatus() {
		return SeenStatus;
	}

	public void setSeenStatus(int seenStatus) {
		SeenStatus = seenStatus;
	}

	public int getLikeCount() {
		return LikeCount;
	}

	public void setLikeCount(int likeCount) {
		LikeCount = likeCount;
	}

	public int getLikes() {
		return Likes;
	}

	public void setLikes(int likes) {
		Likes = likes;
	}

	public int getPostID() {
		return PostID;
	}

	public void setPostID(int postID) {
		PostID = postID;
	}

	private String PostImage;
	private String UserName;
	private String Feedtime;
	private int Views;
	private String UserPicture;
	private int ViewCount;
	private int SeenStatus;
	private int LikeCount;
	private int Likes;
	private int PostID;

	public String getUserFirstName() {
		return UserFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		UserFirstName = userFirstName;
	}

	public String getUserLastName() {
		return UserLastName;
	}

	public void setUserLastName(String userLastName) {
		UserLastName = userLastName;
	}

	public int getSnapID() {
		return SnapID;
	}

	public void setSnapID(int snapID) {
		SnapID = snapID;
	}

	private String UserFirstName;
	private String UserLastName;
	private int SnapID;


	public int getUserTotalLikesCount() {
		return UserTotalLikesCount;
	}

	public void setUserTotalLikesCount(int userTotalLikesCount) {
		UserTotalLikesCount = userTotalLikesCount;
	}

	private int UserTotalLikesCount;

	public int getPostMediaType() {
		return PostMediaType;
	}

	public void setPostMediaType(int postMediaType) {
		PostMediaType = postMediaType;
	}

	private int PostMediaType;

	public List<PostDetails> getRelatedPostsID() {
		return RelatedPostsID;
	}

	public void setRelatedPostsIDs(List<PostDetails> relatedPostsID) {
		RelatedPostsID = relatedPostsID;
	}

	private List<PostDetails> RelatedPostsID;
}
