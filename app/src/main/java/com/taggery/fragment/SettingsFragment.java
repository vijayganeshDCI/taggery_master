package com.taggery.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.taggery.R;
import com.taggery.activity.LoginActivity;
import com.taggery.activity.SettingsActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.RegistrationParam;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends BaseFragment {

    RelativeLayout relativeEditProfile, relativeChangePassword, relativeBusinessProfile;
    RelativeLayout relativePrivacySettings, relativeDeleteAccount, relativeLogout;
    ConstraintLayout consSettings;
    SwitchCompat switchCompatPrivateProfile, switchCompatBusinessProfile;
    SwitchCompat switchCompatDeactivateLikesandRanks;
    SettingsActivity settingsActivity;
    //    1-Pubilc 2-Private 3-Business
    private int profileType = 1;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    TaggeryAPI taggeryAPI;
    CheckUserNameResponse checkUserNameResponse;
    private boolean isPrivateProfileSelected = false, isBusinessProfileSelected = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        TaggeryApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        settingsActivity = (SettingsActivity) getActivity();
        relativeEditProfile = (RelativeLayout) view.findViewById(R.id.relative_edit_profile);
        consSettings = (ConstraintLayout) view.findViewById(R.id.cons_settings);
        relativeChangePassword = (RelativeLayout) view.findViewById(R.id.relative_change_password);
        relativeBusinessProfile = (RelativeLayout) view.findViewById(R.id.relative_business_profile);
        relativePrivacySettings = (RelativeLayout) view.findViewById(R.id.relative_privacy_settings);
        relativeDeleteAccount = (RelativeLayout) view.findViewById(R.id.relative_delete_acount);
        relativeLogout = (RelativeLayout) view.findViewById(R.id.relative_logout);
        switchCompatPrivateProfile = (SwitchCompat) view.findViewById(R.id.switch_private_profile);
        switchCompatBusinessProfile = (SwitchCompat) view.findViewById(R.id.switch_business_profile);
        switchCompatDeactivateLikesandRanks = (SwitchCompat) view.findViewById(R.id.switch_deactivate_likes);


        switch (sharedPreferences.getInt(TaggeryConstants.USER_PROFILE_TYPE, 0)) {
            case 1:
                //public
                switchCompatPrivateProfile.setChecked(false);
                break;
            case 2:
                //private
                switchCompatPrivateProfile.setChecked(true);
                break;
            case 3:
                //business
                switchCompatBusinessProfile.setChecked(true);
                break;
            default:
                switchCompatBusinessProfile.setChecked(true);
                switchCompatPrivateProfile.setChecked(true);
                break;

        }


        relativeEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsActivity.push(new EditProfileFragment(), "EditProfileFragment");
            }
        });
        relativeChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdatePasswordFragment updatePasswordFragment = new UpdatePasswordFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isForChangePassword", true);
                updatePasswordFragment.setArguments(bundle);
                settingsActivity.push(updatePasswordFragment, "updatePasswordFragment");


            }
        });

        relativePrivacySettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsActivity.push(new PrivacySettingsFragment(), "PrivacySettingsFragment");
            }
        });
        relativeDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setMessage(R.string.delete_account);
                alertDialogBuilder.setPositiveButton((getString(R.string.yes)),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                deleteAccount();
                            }
                        });

                alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();


            }
        });
        relativeLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setMessage(R.string.are_you_sure_want_to_logout);
                alertDialogBuilder.setPositiveButton((getString(R.string.yes)),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                Toast.makeText(getActivity(), getString(R.string.logged_out), Toast.LENGTH_SHORT).show();
                                Intent intent_admin_logout = new Intent(getActivity(), LoginActivity.class);
                                intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                getActivity().finish();
                                startActivity(intent_admin_logout);
                            }
                        });

                alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

            }
        });


        switchCompatPrivateProfile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPrivateProfileSelected = isChecked;
                if (isChecked) {
                    profileType = 2;
                    switchCompatBusinessProfile.setChecked(false);
                    updateProfileType();
                } else {
                    if (!isBusinessProfileSelected) {
                        profileType = 1;
                        updateProfileType();
                    }
                }

            }
        });
        switchCompatBusinessProfile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isBusinessProfileSelected = isChecked;
                if (isChecked) {
                    profileType = 3;
                    switchCompatPrivateProfile.setChecked(false);
                    updateProfileType();
                } else {
                    if (!isPrivateProfileSelected) {
                        profileType = 1;
                        updateProfileType();
                    }
                }
            }
        });

        return view;
    }

    private void updateProfileType() {
        if (Util.isNetworkAvailable()) {
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            registrationParam.setProfileType(profileType);
            taggeryAPI.updateProfileType(registrationParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    checkUserNameResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            switch (profileType) {
                                case 1:
                                    showShackError(getString(R.string.pubilc_mode_on), consSettings);
                                    break;
                                case 2:
                                    showShackError(getString(R.string.private_mode_on), consSettings);
                                    break;
                                case 3:
                                    showShackError(getString(R.string.business_mode_on), consSettings);
                                    break;
                            }
                            editor.putInt(TaggeryConstants.USER_PROFILE_TYPE, profileType).commit();
                        } else {
                            showShackError(checkUserNameResponse.getMessage()!=null?
                                    checkUserNameResponse.getMessage():"", consSettings);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consSettings);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    showShackError(getString(R.string.server_error), consSettings);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), consSettings);
        }

    }

    private void deleteAccount() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.deleteAccount(registrationParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            editor.clear().commit();
                            Toast.makeText(getActivity(), checkUserNameResponse.getMessage()!=null?
                                    checkUserNameResponse.getMessage():"", Toast.LENGTH_SHORT).show();
                            Intent intent_admin_logout = new Intent(getActivity(), LoginActivity.class);
                            intent_admin_logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            getActivity().finish();
                            startActivity(intent_admin_logout);
                        } else {
                            showShackError(checkUserNameResponse.getMessage()!=null?
                                    checkUserNameResponse.getMessage():"", consSettings);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consSettings);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consSettings);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), consSettings);

        }
    }


}
