package com.taggery.model;

import java.util.List;

public class TagPeopleResponse {
    private String Status;
    private Integer StatusCode;
    private String Message;
    private List<Friend> Friends = null;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Integer getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(Integer statusCode) {
        StatusCode = statusCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public List<Friend> getFriends() {
        return Friends;
    }

    public void setFriends(List<Friend> friends) {
        Friends = friends;
    }
    public class Friend {

        private Integer id;
        private String UserFirstName;
        private String UserLastName;
        private String UserName;
        private String UserPicture;

        public int getIsSelected() {
            return IsSelected;
        }

        public void setIsSelected(int isSelected) {
            IsSelected = isSelected;
        }

        private int IsSelected;


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUserFirstName() {
            return UserFirstName;
        }

        public void setUserFirstName(String userFirstName) {
            UserFirstName = userFirstName;
        }

        public String getUserLastName() {
            return UserLastName;
        }

        public void setUserLastName(String userLastName) {
            UserLastName = userLastName;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getUserPicture() {
            return UserPicture;
        }

        public void setUserPicture(String userPicture) {
            UserPicture = userPicture;
        }


    }
}
