package com.taggery.model;

public class RegistrationParam {

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getFCMKey() {
        return FCMKey;
    }

    public void setFCMKey(String FCMKey) {
        this.FCMKey = FCMKey;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public String getRelationShipStatus() {
        return RelationShipStatus;
    }

    public void setRelationShipStatus(String relationShipStatus) {
        RelationShipStatus = relationShipStatus;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public int getCountryID() {
        return CountryID;
    }

    public void setCountryID(int countryID) {
        CountryID = countryID;
    }

    private String FirstName;
    private String LastName;
    private String UserName;
    private String PhoneNumber;
    private String Gender;
    private String Password;
    private String FCMKey;
    private String DeviceType;
    private String RelationShipStatus;
    private String DeviceID;

    public String getUserDescription() {
        return UserDescription;
    }

    public void setUserDescription(String userDescription) {
        UserDescription = userDescription;
    }

    private String UserDescription;

    public String getUserPicture() {
        return UserPicture;
    }

    public void setUserPicture(String userPicture) {
        UserPicture = userPicture;
    }

    private String UserPicture;

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    private String CountryCode;

    public int getProfileType() {
        return ProfileType;
    }

    public void setProfileType(int profileType) {
        ProfileType = profileType;
    }

    private int ProfileType;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    private int user_id;

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    private int Type;

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getProfilePic() {
        return ProfilePic;
    }

    public void setProfilePic(String profilePic) {
        ProfilePic = profilePic;
    }

    private String EmailID;
    private String ProfilePic;

    public String getUserCoverPhoto() {
        return UserCoverPhoto;
    }

    public void setUserCoverPhoto(String userCoverPhoto) {
        UserCoverPhoto = userCoverPhoto;
    }

    private String UserCoverPhoto;

    public String getCurrent_password() {
        return current_password;
    }

    public void setCurrent_password(String current_password) {
        this.current_password = current_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    private String current_password;
    private String new_password;
    private String confirm_password;
    private int CountryID;
}
