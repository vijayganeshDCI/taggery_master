package com.taggery.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.taggery.R;
import com.taggery.activity.MainActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.LoginResponse;
import com.taggery.model.PropicUpload;
import com.taggery.model.RegistrationParam;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;
import com.taggery.view.CustomTextViewBold;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.enums.EPickType;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileFragment extends BaseFragment {


    @BindView(R.id.image_logo)
    ImageView imageLogo;
    @BindView(R.id.text_label_reg)
    CustomTextViewBold textLabelEdit;
    @BindView(R.id.image_update)
    ImageView imageUpdate;
    @BindView(R.id.image_pro_pic)
    CircleImageView imageProPic;
    @BindView(R.id.edit_first_name)
    CustomEditText editFirstName;
    @BindView(R.id.text_input_first_name)
    TextInputLayout textInputFirstName;
    @BindView(R.id.edit_last_name)
    CustomEditText editLastName;
    @BindView(R.id.text_input_last_name)
    TextInputLayout textInputLastName;
    @BindView(R.id.edit_user_name)
    CustomEditText editUserName;
    @BindView(R.id.text_input_user_name)
    TextInputLayout textInputUserName;
    @BindView(R.id.edit_email)
    CustomEditText editEmail;
    @BindView(R.id.text_input_email)
    TextInputLayout textInputEmail;
    @BindView(R.id.spinner_gender)
    MaterialSpinner spinnerGender;
    @BindView(R.id.spinner_relation_ship)
    MaterialSpinner spinnerRelationShip;
    Unbinder unbinder;
    @BindView(R.id.cons_edit_profile)
    ConstraintLayout consEditProfile;
    @BindView(R.id.edit_user_decription)
    CustomEditText editUserDecription;
    @BindView(R.id.text_input_user_description)
    TextInputLayout textInputUserDescription;
    @BindView(R.id.img_cover_photo)
    ImageView imgCoverPhoto;
    @BindView(R.id.text_post)
    CustomTextView textPost;
    @BindView(R.id.img_post)
    ImageView imgPost;
    @BindView(R.id.relative_post)
    RelativeLayout relativePost;
    private String gender, relationShipStatus;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    @Inject
    TaggeryAPI taggeryAPI;
    private CheckUserNameResponse checkUserNameResponse;
    private boolean isUserNameAvailable;
    private ArrayAdapter<String> adapterGender, adapterRelationshipStatus;
    private LoginResponse loginResponse;
    private boolean isfromProfile = false;
    private PickSetup setup;
    public String profilePicImageName = null, profilePicImagePath = null;
    public String coverPicImageName = null, coverPicImagePath = null;
    private ArrayList<PropicUpload> propicUploadArrayList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_edit_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        propicUploadArrayList = new ArrayList<PropicUpload>();
        editFirstName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editLastName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        editUserName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys()});
        adapterGender = new
                ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().
                getStringArray(R.array.gender));
        spinnerGender.setAdapter(adapterGender);
        adapterRelationshipStatus = new
                ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, getResources().
                getStringArray(R.array.relation_ship_status));
        spinnerRelationShip.setAdapter(adapterRelationshipStatus);

        Intent intent = getActivity().getIntent();
        if (intent != null) {
            isfromProfile = intent.getBooleanExtra("isfromProfile", false);
        }

        getUserDetail();


        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getCount() > 0 && isAdded() && adapterView != null && view != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.white));
                gender = (String) adapterView.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerRelationShip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (adapterView.getCount() > 0 && isAdded() && adapterView != null && view != null)
                    ((TextView) adapterView.getChildAt(0)).setTextColor(getResources().getColor(R.color.white));

                relationShipStatus = (String) adapterView.getSelectedItem();
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        editUserName.addTextChangedListener(userNameTextWatcher);

        setup = new PickSetup()
                .setTitle(getString(R.string.choose_photo))
                .setProgressText(getString(R.string.processing))
                .setCancelText(getString(R.string.cancel))
                .setPickTypes(EPickType.GALLERY, EPickType.CAMERA)
                .setCameraButtonText(getString(R.string.camera))
                .setGalleryButtonText(getString(R.string.gallery))
                .setButtonOrientation(LinearLayoutCompat.HORIZONTAL)
                .setGalleryIcon(R.mipmap.icon_gallery)
                .setCameraIcon(R.mipmap.icon_camera).setWidth(100).setHeight(100);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.image_update, R.id.image_pro_pic, R.id.img_cover_photo, R.id.relative_post})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_update:
                editProfileValidation();
                break;
            case R.id.image_pro_pic:
                pickImageFromGalleryandCamera(imageProPic, 1);
                break;
            case R.id.img_cover_photo:
                pickImageFromGalleryandCamera(imgCoverPhoto, 2);
                break;
            case R.id.relative_post:
                editProfileValidation();
                break;
        }
    }

    TextWatcher userNameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (count > 0) {
                checkUserNameAvailable(1);
            } else {
                editUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void checkUserNameAvailable(final int type) {
        if (Util.isNetworkAvailable()) {
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setType(1);
            registrationParam.setUserName(editUserName.getText().toString());

            taggeryAPI.checkUserName(registrationParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    checkUserNameResponse = response.body();
                    if (response.body() != null) {
                        if (response.code() == 200 && checkUserNameResponse.getStatusCode() == 200) {
                            //Not available
                            //UserName check
                            isUserNameAvailable = false;
                            textInputUserName.setErrorEnabled(false);
                            editUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                                    R.mipmap.icon_correct, 0);
                            editUserName.setCompoundDrawablePadding(10);
                        } else {
                            //Already available
                            isUserNameAvailable = true;
                            editUserName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                        }
                    } else {
                        showShackError(getString(R.string.server_error), consEditProfile);
                    }

                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    showShackError(getString(R.string.server_error), consEditProfile);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), consEditProfile);
        }
    }

    private void editProfileValidation() {
        if (editFirstName.getText().toString().length() > 0 &&
                editLastName.getText().toString().length() > 0 &&
                editUserName.getText().toString().length() > 0 &&
                spinnerRelationShip.getSelectedItemPosition() != 0 &&
                spinnerGender.getSelectedItemPosition() != 0 &&
                editEmail.getText().toString().length() > 0 &&
                Util.isValidEmailAddress(editEmail.getText().toString()) && !isUserNameAvailable) {
            textInputFirstName.setErrorEnabled(false);
            textInputLastName.setErrorEnabled(false);
            textInputUserName.setErrorEnabled(false);
            textInputEmail.setErrorEnabled(false);
            spinnerGender.setEnableErrorLabel(false);
            spinnerRelationShip.setEnableErrorLabel(false);
            if (profilePicImageName != null && coverPicImageName != null) {
                //Changed pro and cover pic
                propicUploadArrayList.add(new PropicUpload(profilePicImagePath, profilePicImageName));
                propicUploadArrayList.add(new PropicUpload(coverPicImagePath, coverPicImageName));
                cloudinaryUpload(propicUploadArrayList, getString(R.string.cloudinary_download_profile_picture),
                        getString(R.string.image));
            } else if (profilePicImageName != null && coverPicImageName == null) {
                //Changed propic alone
                propicUploadArrayList.add(new PropicUpload(profilePicImagePath, profilePicImageName));
                coverPicImageName = sharedPreferences.getString(TaggeryConstants.USER_COVER_PIC, "");
                cloudinaryUpload(propicUploadArrayList, getString(R.string.cloudinary_download_profile_picture),
                        getString(R.string.image));
            } else if (profilePicImageName == null && coverPicImageName != null) {
                //Changed Cover image alone
                propicUploadArrayList.add(new PropicUpload(coverPicImagePath, coverPicImageName));
                profilePicImageName = sharedPreferences.getString(TaggeryConstants.USER_PROFILE_PIC, "");
                cloudinaryUpload(propicUploadArrayList, getString(R.string.cloudinary_download_profile_picture),
                        getString(R.string.image));
            } else {
                //No change
                showProgress();
                profilePicImageName = sharedPreferences.getString(TaggeryConstants.USER_PROFILE_PIC, "");
                coverPicImageName = sharedPreferences.getString(TaggeryConstants.USER_COVER_PIC, "");
                editProfile();
            }

        } else {
            if (editFirstName.getText().toString().length() == 0)
                textInputFirstName.setError(getString(R.string.first_name_error));
            else
                textInputFirstName.setErrorEnabled(false);
            if (editLastName.getText().toString().length() == 0)
                textInputLastName.setError(getString(R.string.last_name_error));
            else
                textInputLastName.setErrorEnabled(false);
            if (editUserName.getText().toString().length() == 0)
                textInputUserName.setError(getString(R.string.user_name_error));
            else
                textInputUserName.setErrorEnabled(false);
            if (spinnerRelationShip.getSelectedItemPosition() == 0) {
                spinnerRelationShip.setError(getString(R.string.relationship_status_error));
            } else {
                spinnerRelationShip.setEnableErrorLabel(false);
            }
            if (spinnerGender.getSelectedItemPosition() == 0) {
                spinnerGender.setError(getString(R.string.gender_error));
            } else {
                spinnerGender.setEnableErrorLabel(false);
            }
            if (editEmail.getText().toString().length() == 0) {
                textInputEmail.setError(getString(R.string.Email_error));
            } else {
                if (!Util.isValidEmailAddress(editEmail.getText().toString())) {
                    textInputEmail.setError(getString(R.string.Email_vaild_error));
                }
            }
            if (isUserNameAvailable)
                textInputUserName.setError(checkUserNameResponse.getMessage());
        }
    }

    private void getUserDetail() {
        editFirstName.setText(sharedPreferences.getString(TaggeryConstants.FIRST_NAME, ""));
        editLastName.setText(sharedPreferences.getString(TaggeryConstants.LAST_NAME, ""));
        editUserName.setText(sharedPreferences.getString(TaggeryConstants.USER_NAME, ""));
        editEmail.setText(sharedPreferences.getString(TaggeryConstants.USER_EMAIL, ""));
        editUserDecription.setText(sharedPreferences.getString(TaggeryConstants.USER_DECRIPTION, ""));
        picassoImageHolder(imageProPic, sharedPreferences.getString(TaggeryConstants.USER_PROFILE_PIC, ""),
                R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder, getString(R.string.cloudinary_download_profile_picture));
        picassoImageHolder(imgCoverPhoto, sharedPreferences.getString(TaggeryConstants.USER_COVER_PIC, ""),
                R.mipmap.image_snap_loading_place_holder, R.mipmap.icon_profile_cover_photo, getString(R.string.cloudinary_download_profile_picture));
        profileRingColor(sharedPreferences.getInt(TaggeryConstants.USER_LIKE_COUNT, 0), imageProPic);
        for (int i = 0; i < adapterRelationshipStatus.getCount(); i++) {
            if (adapterRelationshipStatus.getItem(i).equals(
                    sharedPreferences.getString(TaggeryConstants.USER_RELATION_SHIP_STATUS, ""))) {
                spinnerRelationShip.setSelection(i + 1);
                relationShipStatus = sharedPreferences.getString(TaggeryConstants.USER_RELATION_SHIP_STATUS, "");
                break;
            }

        }
        for (int i = 0; i < adapterGender.getCount(); i++) {
            if (adapterGender.getItem(i).equals(
                    sharedPreferences.getString(TaggeryConstants.USER_GENDER, ""))) {
                spinnerGender.setSelection(i + 1);
                gender = sharedPreferences.getString(TaggeryConstants.USER_GENDER, "");
                break;
            }

        }
    }

    private void editProfile() {
        if (Util.isNetworkAvailable()) {
            RegistrationParam registrationParam = new RegistrationParam();
            registrationParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            registrationParam.setFirstName(editFirstName.getText().toString());
            registrationParam.setLastName(editLastName.getText().toString());
            registrationParam.setUserName(editUserName.getText().toString());
            registrationParam.setEmailID(editEmail.getText().toString());
            registrationParam.setRelationShipStatus(relationShipStatus);
            registrationParam.setGender(gender);
            registrationParam.setUserPicture(profilePicImageName);
            registrationParam.setUserDescription(editUserDecription.getText().toString());
            registrationParam.setUserCoverPhoto(coverPicImageName);
            taggeryAPI.updateProfile(registrationParam).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    hideProgress();
                    loginResponse = response.body();
                    if (response.body() != null && response.isSuccessful() && response.code() == 200) {
                        if (loginResponse.getStatusCode() == 200) {
                            if (loginResponse.getUserDetails() != null) {
                                editor.putString(TaggeryConstants.USER_NAME,
                                        loginResponse.getUserDetails().getUserName() != null ?
                                                loginResponse.getUserDetails().getUserName() : "").commit();
                                editor.putString(TaggeryConstants.FIRST_NAME,
                                        loginResponse.getUserDetails().getUserFirstName() != null ?
                                                loginResponse.getUserDetails().getUserFirstName() : "").commit();
                                editor.putString(TaggeryConstants.LAST_NAME,
                                        loginResponse.getUserDetails().getUserLastName() != null ?
                                                loginResponse.getUserDetails().getUserLastName() : "").commit();
                                editor.putString(TaggeryConstants.USER_EMAIL,
                                        loginResponse.getUserDetails().getUserEmail() != null ?
                                                loginResponse.getUserDetails().getUserEmail() : "").commit();
                                editor.putString(TaggeryConstants.USER_GENDER,
                                        loginResponse.getUserDetails().getUserGender() != null ?
                                                loginResponse.getUserDetails().getUserGender() : "").commit();
                                editor.putString(TaggeryConstants.USER_RELATION_SHIP_STATUS,
                                        loginResponse.getUserDetails().getUserRelationshipStatus() != null ?
                                                loginResponse.getUserDetails().getUserRelationshipStatus() : "").commit();
                                editor.putInt(TaggeryConstants.USER_PROFILE_TYPE,
                                        loginResponse.getUserDetails().getUserProfileType()).commit();
                                editor.putString(TaggeryConstants.USER_PROFILE_PIC,
                                        loginResponse.getUserDetails().getUserPicture() != null ?
                                                loginResponse.getUserDetails().getUserPicture() : "").commit();
                                editor.putString(TaggeryConstants.USER_COVER_PIC,
                                        loginResponse.getUserDetails().getUserCoverPhoto() != null ?
                                                loginResponse.getUserDetails().getUserCoverPhoto() : "").commit();
                                editor.putString(TaggeryConstants.USER_DECRIPTION,
                                        loginResponse.getUserDetails().getUserDescription() != null ?
                                                loginResponse.getUserDetails().getUserDescription() : "").commit();
                                Toast.makeText(getActivity(), loginResponse.getMessage() != null ?
                                        loginResponse.getMessage() : "", Toast.LENGTH_SHORT).show();
                                getActivity().onBackPressed();
                            }
                        } else {
                            showShackError(loginResponse.getMessage(), consEditProfile);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consEditProfile);
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consEditProfile);
                }
            });


        } else {
            showShackError(getString(R.string.no_network), consEditProfile);
        }
    }

    private void cloudinaryUpload(final List<PropicUpload> propicUploadList, String folderName, String mediaType) {
        showProgress();
        initializeCloudinary();

        for (int i = 0; i < propicUploadList.size(); i++) {
            final int finalI = i;
            MediaManager.get().upload(propicUploadList.get(i).getImagePath())
                    .option("public_id", propicUploadList.get(i).getImageName())
                    .option("resource_type", mediaType)
                    .option("folder", folderName)
                    .callback(new UploadCallback() {
                        @Override
                        public void onStart(String requestId) {

                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {
                       /* Double progress = (double) bytes/totalBytes;
                        String numberD = String.valueOf(progress);
                        numberD = numberD.substring ( numberD.indexOf ( "." ) );
                        Toast.makeText(getActivity(), numberD, Toast.LENGTH_SHORT).show();*/
                        }

                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            if (propicUploadList.size() == finalI + 1)
                                editProfile();
                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {
                            hideProgress();
                            showShackError(getString(R.string.media_upload_error), consEditProfile);
                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {
                        }
                    }).dispatch();
        }


    }

    public void pickImageFromGalleryandCamera(final ImageView imageProPic, final int picType) {
        PickImageDialog.build(setup)
                .setOnPickResult(new IPickResult() {
                    @Override
                    public void onPickResult(PickResult r) {
                        if (r.getError() == null) {
                            //If you want the Uri.
                            //Mandatory to refresh image from Uri.
                            //getImageView().setImageURI(null);

                            //Setting the real returned image.
                            //getImageView().setImageURI(r.getUri());

                            //If you want the Bitmap.
                            imageProPic.setImageBitmap(r.getBitmap());
                            //Image path
                            //r.getPath();
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            if (picType == 1) {
                                profilePicImageName = "ProfileImg" + timeStamp;
                                profilePicImagePath = r.getPath();
                            } else {
                                coverPicImageName = "CoverImg" + timeStamp;
                                coverPicImagePath = r.getPath();
                            }
                        } else {
                            //Handle possible errors
                            //TODO: do what you have to do with r.getError();
                            Toast.makeText(getActivity(), r.getError().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setOnPickCancel(new IPickCancel() {
                    @Override
                    public void onCancelClick() {
                    }
                }).show(getActivity().getSupportFragmentManager());

    }
}
