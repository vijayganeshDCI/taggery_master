package com.taggery.model;

import java.util.List;

public class GetPostDetail {

    public String getPostPic() {
        return postPic;
    }

    public void setPostPic(String postPic) {
        this.postPic = postPic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public List<ViewedItem> getViewedItemList() {
        return viewedItemList;
    }

    public void setViewedItemList(List<ViewedItem> viewedItemList) {
        this.viewedItemList = viewedItemList;
    }

    public GetPostDetail(String postPic, String description, int likeCount, int viewCount, List<ViewedItem> viewedItemList) {
        this.postPic = postPic;
        this.description = description;
        this.likeCount = likeCount;
        this.viewCount = viewCount;
        this.viewedItemList = viewedItemList;
    }

    private String postPic;
    private String description;
    private int likeCount;
    private int viewCount;
    private List<ViewedItem> viewedItemList;


}
