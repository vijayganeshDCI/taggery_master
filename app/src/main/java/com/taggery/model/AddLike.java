package com.taggery.model;

public class AddLike {

    public int getLikeUserID() {
        return LikeUserID;
    }

    public void setLikeUserID(int likeUserID) {
        LikeUserID = likeUserID;
    }

    public int getLikePostID() {
        return LikePostID;
    }

    public void setLikePostID(int likePostID) {
        LikePostID = likePostID;
    }

    private int LikeUserID;
    private int LikePostID;

    public int getLikeStatus() {
        return LikeStatus;
    }

    public void setLikeStatus(int likeStatus) {
        LikeStatus = likeStatus;
    }

    private int LikeStatus;
}
