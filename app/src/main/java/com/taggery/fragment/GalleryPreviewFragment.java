package com.taggery.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.taggery.R;
import com.taggery.activity.GalleryPreviewActivity;
import com.taggery.application.TaggeryApplication;
import com.taggery.fragment.BaseFragment;
import com.taggery.fragment.CommentFragment;
import com.taggery.model.AddComment;
import com.taggery.model.AddLike;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.GetPostDetailResponse;
import com.taggery.model.LikeList;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomEditText;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GalleryPreviewFragment extends BaseFragment {


    @BindView(R.id.image_preview)
    ImageView imgUploadpic;
    @BindView(R.id.video_preview)
    SimpleExoPlayerView videoPlayerPost;
    @BindView(R.id.text_location)
    CustomTextView textLocation;
    @BindView(R.id.img_flag)
    ImageView imgFlag;
    @BindView(R.id.text_view_count)
    CustomTextView textViewCount;
    @BindView(R.id.relative_top)
    RelativeLayout relativeTop;
    @BindView(R.id.img_like)
    ImageView imgLike;
    @BindView(R.id.relative_like)
    ConstraintLayout relativeLike;
    @BindView(R.id.text_label_liked_by)
    CustomTextView textLabelLikedBy;
    @BindView(R.id.relative_likedby)
    ConstraintLayout relativeLikedby;
    @BindView(R.id.text_description)
    CustomTextView textDescription;
    @BindView(R.id.text_view_all_comments)
    CustomTextView textViewAllComments;
    @BindView(R.id.relative_viewallcomments)
    RelativeLayout relativeViewallcomments;
    @BindView(R.id.edit_add_comment)
    CustomEditText editAddComment;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.relative_add_comment)
    ConstraintLayout relativeAddComment;
    @BindView(R.id.img_comments)
    CircleImageView imgComments;
    @BindView(R.id.text_commentname)
    CustomTextView textCommentname;
    @BindView(R.id.text_comments)
    CustomTextView textComments;
    @BindView(R.id.relative_comments)
    RelativeLayout relativeComments;
    @BindView(R.id.relative_comment_layout)
    RelativeLayout relativeCommentLayout;
    @BindView(R.id.linear_bottom)
    LinearLayout linearBottom;
    @BindView(R.id.linear_child)
    LinearLayout linearChild;
    @BindView(R.id.scroll_bottom)
    ScrollView scrollBottom;
    @BindView(R.id.relative_parent)
    RelativeLayout relativeParent;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    int postID;
    @BindView(R.id.text_duration)
    CustomTextView textDuration;
    @BindView(R.id.text_comment_date)
    CustomTextView textCommentDate;
    private CheckUserNameResponse checkUserNameResponse;
    private boolean isLiked = false;
    private GetPostDetailResponse getPostDetailResponse;
    private SimpleExoPlayer player;
    private DefaultDataSourceFactory dataSourceFactory;
    private DefaultExtractorsFactory extractorsFactory;
    private boolean isReported = false;
    GalleryPreviewActivity galleryPreviewActivity;
    private String likeOne, likeTwo, likeBy, likeThree;
    private int likeCount;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery_preview, container, false);
        ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        galleryPreviewActivity = (GalleryPreviewActivity) getActivity();
        initializePlayer();
        Intent intent = getActivity().getIntent();
        if (intent != null)
            postID = intent.getIntExtra("postID", 0);
        getPostDetails();
        return view;
    }

    @OnClick({R.id.img_flag, R.id.img_like, R.id.relative_viewallcomments, R.id.img_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_flag:
                addReport();
                break;
            case R.id.img_like:
                addLike();
                break;
            case R.id.relative_viewallcomments:
                CommentFragment commentFragment = new CommentFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("postID", postID);
                commentFragment.setArguments(bundle);
                galleryPreviewActivity.push(commentFragment, "commentFragment");
                break;
            case R.id.img_send:
                if (editAddComment.getText().length() > 0)
                    addComment();
                else
                    editAddComment.setError(getString(R.string.add_comment));
                break;
        }
    }


    private void addLike() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddLike addLike = new AddLike();
            addLike.setLikePostID(postID);
            addLike.setLikeUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            if (isLiked)
                //unlike
                addLike.setLikeStatus(2);
            else
                //like
                addLike.setLikeStatus(1);

            taggeryAPI.addLike(addLike).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            if (getPostDetailResponse.getPost() != null)
                                likeCount = getPostDetailResponse.getPost().getLikestcount();
                            if (checkUserNameResponse.getLikeStatus() == 1) {
                                //like
                                isLiked = true;
                                imgLike.setImageResource(R.mipmap.icon_heart_like);
                                if (getPostDetailResponse.getPost() != null) {
                                    getPostDetailResponse.getPost().setLikes(1);
                                    getPostDetailResponse.getPost().setLikestcount(++likeCount);
                                    if (getPostDetailResponse.getPost().getLikestcount() == 1) {
                                        textLabelLikedBy.setText(getString(R.string.liked_by) + " " + getString(R.string.You));
                                    } else if (getPostDetailResponse.getPost().getLikestcount() == 2) {
                                        textLabelLikedBy.setText(getString(R.string.liked_by) + " " + getString(R.string.You) + " " +
                                                getString(R.string.And) + " " +
                                                likeBy);
                                    } else if (getPostDetailResponse.getPost().getLikestcount() > 2) {
                                        textLabelLikedBy.setText(getString(R.string.liked_by) + " " + getString(R.string.You) + "," + likeTwo + " " +
                                                getString(R.string.and_more));
                                    }
                                }


                            } else if (checkUserNameResponse.getLikeStatus() == 2) {
                                //unlike
                                isLiked = false;
                                imgLike.setImageResource(R.mipmap.icon_heart_un_like);
                                if (getPostDetailResponse.getPost() != null) {
                                    getPostDetailResponse.getPost().setLikes(2);
                                    getPostDetailResponse.getPost().setLikestcount(--likeCount);
                                    if (getPostDetailResponse.getPost().getLikestcount() == 0) {
                                        textLabelLikedBy.setText(getString(R.string.Add_your_like));
                                    } else if (getPostDetailResponse.getPost().getLikestcount() == 1) {
                                        textLabelLikedBy.setText(getString(R.string.liked_by) + " " + likeBy);
                                    } else if (getPostDetailResponse.getPost().getLikestcount() == 2) {
                                        textLabelLikedBy.setText(getString(R.string.liked_by) + " " + likeTwo + " "
                                                + getString(R.string.And) + " " + likeThree);
                                    } else if (getPostDetailResponse.getPost().getLikestcount() > 2) {
                                        likeBy = likeTwo + "," + likeThree;
                                        textLabelLikedBy.setText(getString(R.string.liked_by) + " " + likeBy + " " + getString(R.string.and_more));
                                    }
                                }

                            }
                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }
    }

    private void addComment() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            AddComment addComment = new AddComment();
            addComment.setCommentPostID(postID);
            addComment.setCommentText(editAddComment.getText().toString());
            addComment.setCommentUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            taggeryAPI.addComment(addComment).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            relativeCommentLayout.setVisibility(View.VISIBLE);
                            picassoImageHolder(imgComments, sharedPreferences.getString(TaggeryConstants.USER_PROFILE_PIC, ""),
                                    R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                                    getString(R.string.cloudinary_download_profile_picture));
                            profileRingColor((sharedPreferences.getInt(TaggeryConstants.USER_LIKE_COUNT, 0)), imgComments);
                            textCommentname.setText(getString(R.string.You));
                            textComments.setText(editAddComment.getText().toString());
                            textCommentDate.setText("- " + getString(R.string.now));
                            if (getPostDetailResponse.getPost() != null) {
                                int commentsCount = getPostDetailResponse.getPost().getCommentscount();
                                getPostDetailResponse.getPost().setCommentscount(++commentsCount);
                                if (commentsCount > 1) {
                                    textViewAllComments.setVisibility(View.VISIBLE);
                                    textViewAllComments.setText(getString(R.string.View_all) + " " +
                                            getPostDetailResponse.getPost().getCommentscount() + " " +
                                            getString(R.string.comments));
                                } else {
                                    textViewAllComments.setVisibility(View.GONE);
                                }
                            } else {
                                textViewAllComments.setVisibility(View.GONE);
                            }

                            editAddComment.setText("");
                            showShackError(checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", relativeParent);
                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }
    }


    private void getPostDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            contactListInputParam.setPostID(postID);
            taggeryAPI.getPostDetails(contactListInputParam).enqueue(new Callback<GetPostDetailResponse>() {
                @Override
                public void onResponse(Call<GetPostDetailResponse> call, Response<GetPostDetailResponse> response) {
                    hideProgress();
                    getPostDetailResponse = response.body();
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (getPostDetailResponse.getStatusCode() == 200) {
                            if (getPostDetailResponse.getPost() != null) {
                                if (getPostDetailResponse.getPost().getPostMediaType() != 2) {
                                    //image
                                    imgUploadpic.setVisibility(View.VISIBLE);
                                    videoPlayerPost.setVisibility(View.GONE);
                                    picassoImageHolder(imgUploadpic, getPostDetailResponse.getPost().getPostImage() != null ?
                                                    getPostDetailResponse.getPost().getPostImage() : "",
                                            R.mipmap.icon_loading_64, R.mipmap.icon_no_image_64,
                                            getString(R.string.cloudinary_download_post));
                                } else {
                                    //video
                                    // This is the MediaSource representing the media to be played.
                                    imgUploadpic.setVisibility(View.INVISIBLE);
                                    videoPlayerPost.setVisibility(View.VISIBLE);
                                    if (getPostDetailResponse.getPost().getPostImage() != null) {
                                        Uri videoUri = Uri.parse(getString(R.string.cloudinary_base_url) +
                                                getString(R.string.cloudinary_download_video_add_post) +
                                                getPostDetailResponse.getPost().getPostImage());
                                        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                                                dataSourceFactory, extractorsFactory, null, null);
                                        // Prepare the player with the source.
                                        player.prepare(videoSource);
                                        player.setPlayWhenReady(true);
                                    }
                                }


                                textViewCount.setText("" + getPostDetailResponse.getPost().getViewscount());
                                textDescription.setText(getPostDetailResponse.getPost().getPostText() != null ?
                                        getPostDetailResponse.getPost().getPostText() : "");
                                textLocation.setText(getPostDetailResponse.getPost().getPostLocation() != null ?
                                        getPostDetailResponse.getPost().getPostLocation() : "");
                                String[] postTime = new String[0];
                                if (getPostDetailResponse.getPost().getPostLocation() != null)
                                    postTime = getPostDetailResponse.getPost().getPostcreateddate().split(" ");
                                textDuration.setText((postTime[0] != null ? postTime[0] : "") + " " +
                                        (postTime[1] != null ? postTime[1] : ""));

                                ArrayList<LikeList> likeList = new ArrayList<LikeList>();
                                if (getPostDetailResponse.getPost().getViewed() != null &&
                                        getPostDetailResponse.getPost().getViewed().size() > 0) {
                                    for (int i = 0; i < getPostDetailResponse.getPost().getViewed().size(); i++) {
                                        if (getPostDetailResponse.getPost().getViewed().get(i).getLikes() == 1)
                                            likeList.add(new LikeList(getPostDetailResponse.getPost().getViewed().get(i).getUserFirstName() != null ?
                                                    getPostDetailResponse.getPost().getViewed().get(i).getUserFirstName() : "",
                                                    getPostDetailResponse.getPost().getViewed().get(i).getId()));
                                    }
                                }


                                if (getPostDetailResponse.getPost().getLikestcount() == 0) {
                                    textLabelLikedBy.setText(getString(R.string.Add_your_like));
                                } else if (getPostDetailResponse.getPost().getLikestcount() == 1) {
                                    likeBy = likeList.get(0).getUserID() ==
                                            sharedPreferences.getInt(TaggeryConstants.USER_ID, 0) ? getString(R.string.You)
                                            : likeList.get(0).getUserName();
                                    textLabelLikedBy.setText(getString(R.string.liked_by) + " " + likeBy);
                                } else if (getPostDetailResponse.getPost().getLikestcount() == 2) {
                                    likeOne = likeList.get(0).getUserID() == sharedPreferences.getInt(TaggeryConstants.USER_ID, 0)
                                            ? getString(R.string.You) : likeList.get(0).getUserName();
                                    likeTwo = likeList.get(1).getUserID() == sharedPreferences.getInt(TaggeryConstants.USER_ID, 0)
                                            ? getString(R.string.You) : likeList.get(1).getUserName();
                                    textLabelLikedBy.setText(getString(R.string.liked_by) + " " + likeOne + " " + getString(R.string.And) + " " + likeTwo);
                                } else if (getPostDetailResponse.getPost().getLikestcount() > 2) {
                                    likeOne = likeList.get(0).getUserID() == sharedPreferences.getInt(TaggeryConstants.USER_ID, 0)
                                            ? getString(R.string.You) : likeList.get(0).getUserName();
                                    likeTwo = likeList.get(1).getUserID() == sharedPreferences.getInt(TaggeryConstants.USER_ID, 0)
                                            ? getString(R.string.You) : likeList.get(1).getUserName();
                                    likeThree = likeList.get(2).getUserID() == sharedPreferences.getInt(TaggeryConstants.USER_ID, 0)
                                            ? getString(R.string.You) : likeList.get(2).getUserName();
                                    likeBy = likeOne + "," + likeTwo;
                                    textLabelLikedBy.setText(getString(R.string.liked_by) + " " + likeBy + " " + getString(R.string.and_more));
                                }

                                if (getPostDetailResponse.getPost().getLikes() == 1) {
                                    //like
                                    isLiked = true;
                                    imgLike.setImageResource(R.mipmap.icon_heart_like);
                                } else if (getPostDetailResponse.getPost().getLikes() == 2) {
                                    //unlike
                                    isLiked = false;
                                    imgLike.setImageResource(R.mipmap.icon_heart_un_like);
                                }
                                if (getPostDetailResponse.getPost().getReportStatus() == 1) {
                                    //Report
                                    isReported = true;
                                    imgFlag.setImageResource(R.mipmap.icon_flag_red);
                                } else if (getPostDetailResponse.getPost().getReportStatus() == 2) {
                                    //Revert
                                    isReported = false;
                                    imgFlag.setImageResource(R.mipmap.icon_flag_white);
                                }

                                if (getPostDetailResponse.getPost().getCommentsdetails() != null &&
                                        getPostDetailResponse.getPost().getCommentscount() > 0) {
                                    relativeCommentLayout.setVisibility(View.VISIBLE);
                                    textCommentname.setText(getPostDetailResponse.getPost().getCommentsdetails().get(
                                            getPostDetailResponse.getPost().getCommentscount() - 1
                                    ).getCommentUserFirstName());
                                    textComments.setText(getPostDetailResponse.getPost().getCommentsdetails().get(
                                            getPostDetailResponse.getPost().getCommentscount() - 1
                                    ).getCommentText());
                                    picassoImageHolder(imgComments, getPostDetailResponse.getPost().getCommentsdetails().get(
                                            getPostDetailResponse.getPost().getCommentscount() - 1
                                            ).getCommentPicture(),
                                            R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                                            getString(R.string.cloudinary_download_profile_picture));
                                    profileRingColor(getPostDetailResponse.getPost().getCommentsdetails().get(
                                            getPostDetailResponse.getPost().getCommentscount() - 1
                                    ).getUserTotalLikesCount(), imgComments);
                                    String[] postCommentTime = getPostDetailResponse.getPost().getCommentsdetails().get(
                                            getPostDetailResponse.getPost().getCommentscount() - 1).getCommentCreatedAt().split(" ");
                                    textCommentDate.setText("- " + (postCommentTime[0] != null ?
                                            postCommentTime[0] : "") + " " + (postCommentTime[1] != null ? postCommentTime[1] : ""));

                                } else {
                                    relativeCommentLayout.setVisibility(View.GONE);
                                }

                                if (getPostDetailResponse.getPost().getCommentscount() > 1) {
                                    textViewAllComments.setVisibility(View.VISIBLE);
                                    textViewAllComments.setText(getString(R.string.View_all) + " " +
                                            getPostDetailResponse.getPost().getCommentscount() + " " +
                                            getString(R.string.comments));
                                } else {
                                    textViewAllComments.setVisibility(View.GONE);
                                }
                            }


                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<GetPostDetailResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });


        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }


    }

    private void addReport() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setPostID(postID);
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            if (isReported)
                //revert
                contactListInputParam.setReportStatus(2);
            else
                //report
                contactListInputParam.setReportStatus(1);

            taggeryAPI.addReport(contactListInputParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            if (checkUserNameResponse.getReportStatus() == 1) {
                                //Report
                                isReported = true;
                                imgFlag.setImageResource(R.mipmap.icon_flag_red);
                            } else if (checkUserNameResponse.getReportStatus() == 2) {
                                //Revert
                                isLiked = false;
                                imgFlag.setImageResource(R.mipmap.icon_flag_white);
                            }
                            showShackError(checkUserNameResponse.getMessage() != null ?
                                    checkUserNameResponse.getMessage() : "", relativeParent);

                        } else {
                            showShackError(getString(R.string.server_error), relativeParent);
                        }

                    } else {
                        showShackError(getString(R.string.server_error), relativeParent);
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), relativeParent);
                }
            });

        } else {
            showShackError(getString(R.string.no_network), relativeParent);
        }
    }

    private void initializePlayer() {
        // Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        //Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        //Initialize simpleExoPlayerView
        videoPlayerPost.setPlayer(player);
        // Produces DataSource instances through which media data is loaded.
        dataSourceFactory =
                new DefaultDataSourceFactory(getActivity(),
                        com.google.android.exoplayer2.util.Util.getUserAgent(getActivity(),
                                "taggery"));
        // Produces Extractor instances for parsing the media data.
        extractorsFactory = new DefaultExtractorsFactory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
