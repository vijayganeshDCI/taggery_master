package com.taggery.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.taggery.R;
import com.taggery.adapter.ProfileRecycleAdapter;
import com.taggery.adapter.StatusAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.CheckUserNameResponse;
import com.taggery.model.FItem;
import com.taggery.model.FriendRequestInputParam;
import com.taggery.model.ProfileInputParam;
import com.taggery.model.ProfileResponse;
import com.taggery.model.ProfileUpload;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomButton;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileDetailActivity extends BaseActivity {

    @BindView(R.id.img_cover_photo)
    ImageView imgCoverPhoto;
    @BindView(R.id.img_statics)
    ImageView imgStatics;
    @BindView(R.id.img_settings)
    ImageView imgSettings;
    @BindView(R.id.image_pro_pic_profile)
    CircleImageView imageProPic;
    @BindView(R.id.text_like_count)
    CustomTextView textLikeCount;
    @BindView(R.id.text_label_likes)
    CustomTextView textLabelLikes;
    @BindView(R.id.text_followers_count)
    CustomTextView textFollowersCount;
    @BindView(R.id.text_label_followers)
    CustomTextView textLabelFollowers;
    @BindView(R.id.text_post_count)
    CustomTextView textPostCount;
    @BindView(R.id.text_label_post)
    CustomTextView textLabelPost;
    @BindView(R.id.text_following_count)
    CustomTextView textFollowingCount;
    @BindView(R.id.text_label_following)
    CustomTextView textLabelFollowing;
    @BindView(R.id.img_request)
    CircleImageView imgRequest;
    @BindView(R.id.text_label_request_count)
    CustomTextView textLabelRequestCount;
    @BindView(R.id.text_label_user_description)
    CustomTextView textLabelUserDescription;
    @BindView(R.id.img_edit_descrip)
    ImageView imgEditDescrip;
    @BindView(R.id.img_edit_user_name)
    ImageView imgEditUserName;
    @BindView(R.id.text_label_user_first_name)
    CustomTextView textLabelUserFirstName;
    @BindView(R.id.text_label_user_status)
    CustomTextView textLabelUserStatus;
    @BindView(R.id.text_label_user_gender)
    CustomTextView textLabelUserGender;
    @BindView(R.id.text_label_user_name)
    CustomTextView textLabelUserName;
    @BindView(R.id.recy_recent_post)
    RecyclerView recyRecentPost;
    @BindView(R.id.cons_cover_photo)
    ConstraintLayout consCoverPhoto;
    @BindView(R.id.cons_profile)
    ConstraintLayout consProfile;
    //    @BindView(R.id.app_bar_header)
//    AppBarLayout appBarHeader;
//    @BindView(R.id.list_snaps)
//    RecyclerView recyclerUploadList;
    //    @BindView(R.id.image_warn_icon)
//    ImageView imageWarnIcon;
//    @BindView(R.id.text_warn)
//    CustomTextView textWarn;
//    @BindView(R.id.cons_warn)
//    ConstraintLayout consWarn;
    Unbinder unbinder;
    @BindView(R.id.text_label_view_uploads)
    CustomTextView textLabelViewUploads;
//    @BindView(R.id.coordinator_profile)
//    CoordinatorLayout coordinatorProfile;
//    @BindView(R.id.nested_profile_uploads)
//    NestedScrollView nestedProfileUploads;


    private LinearLayoutManager mLayoutManager;
    private StatusAdapter mAdapter;
    private List<ProfileUpload> profileUpload = new ArrayList<>();
    private List<FItem> fItem = new ArrayList<>();
    private ProfileRecycleAdapter adapter;
    ProfileResponse profileResponse, profileUploadListResponse;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private boolean isStarted;
    private boolean isVisible;
    private int ownerID, viwerID;
    private CheckUserNameResponse checkUserNameResponse;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile_new);
        TaggeryApplication.getContext().getComponent().inject(this);
        unbinder = ButterKnife.bind(this);
        Intent bundle = getIntent();
        ownerID = sharedPreferences.getInt(TaggeryConstants.USER_ID, 0);
        if (bundle != null) {
            viwerID = bundle.getIntExtra("postUserID", 0);
        }
        getProfileList(ownerID, viwerID);

//        final GridLayoutManager mLayoutManager = new GridLayoutManager(ProfileDetailActivity.this, 3);
//        recyclerUploadList.setNestedScrollingEnabled(false);
//        recyclerUploadList.setLayoutManager(mLayoutManager);
//        recyclerUploadList.setItemAnimator(new DefaultItemAnimator());
//        adapter = new ProfileRecycleAdapter(ProfileDetailActivity.this, profileUpload);
//        recyclerUploadList.setAdapter(adapter);
//        nestedProfileUploads.setSmoothScrollingEnabled(true);
//        nestedProfileUploads.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                if (v.getChildAt(v.getChildCount() - 1) != null) {
//                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight()
//                            - v.getMeasuredHeight())) && scrollY > oldScrollY) {
//                        if (!isLoading) {
//                            if (!reachedLastEnd) {
//                                if (profileUpload.size() >= 23) {
//                                    getProfileMediaList(++paginationCount);
//                                }
//
//                            }
//                        }
//                    }
//                }
//            }
//        });

    }


    private void getProfileList(int ownerID, int viwerID) {
        if (Util.isNetworkAvailable()) {
            showProgress();
            final ProfileInputParam profileInputParam = new ProfileInputParam();
            profileInputParam.setOwner_id(ownerID);
            profileInputParam.setViewer_id(viwerID);
            fItem.clear();
            taggeryAPI.profileresponse(profileInputParam).enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    hideProgress();
                    profileResponse = response.body();
                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (profileResponse.getStatusCode() == 200) {
//                            consWarn.setVisibility(View.GONE);
                            if (profileResponse.getProfile() != null) {
                                if (profileResponse.getProfile().getProfileOwner() == 1) {
                                    //Same User
                                    imgStatics.setVisibility(View.VISIBLE);
                                    imgSettings.setVisibility(View.VISIBLE);
                                    imgEditDescrip.setVisibility(View.VISIBLE);
                                    imgEditUserName.setVisibility(View.VISIBLE);
                                    imgRequest.setImageResource(R.mipmap.icon_pro_own);
                                    if (profileResponse.getProfile().getUserDescription() != null &&
                                            !profileResponse.getProfile().getUserDescription().equals(""))
                                        textLabelUserDescription.setText(profileResponse.getProfile().getUserDescription());
                                    else
                                        textLabelUserDescription.setText(getString(R.string.add_description));

                                    picassoImageHolder(imgCoverPhoto, profileResponse.getProfile().getUserCoverPhoto() != null ?
                                                    profileResponse.getProfile().getUserCoverPhoto() : "",
                                            R.mipmap.image_snap_loading_place_holder, R.mipmap.icon_profile_cover_photo,
                                            getString(R.string.cloudinary_download_profile_picture));

                                    switch (profileResponse.getProfile().getUserProfileType()) {
                                        case 1:
                                            //public
                                            textLabelRequestCount.setVisibility(View.GONE);
                                            break;
                                        case 2:
                                            //private
                                            textLabelRequestCount.setVisibility(View.VISIBLE);
                                            if (profileResponse.getProfile().getOurfriendrequestscount() > 0) {
                                                textLabelRequestCount.setVisibility(View.VISIBLE);
                                                if (profileResponse.getProfile().getOurfriendrequestscount() > 9)
                                                    textLabelRequestCount.setText("9+");
                                                else
                                                    textLabelRequestCount.setText("" +
                                                            profileResponse.getProfile().getOurfriendrequestscount());
                                            } else
                                                textLabelRequestCount.setVisibility(View.GONE);
                                            break;
                                        case 3:
                                            //Business
                                            textLabelRequestCount.setVisibility(View.GONE);
                                            break;
                                        default:
                                            textLabelRequestCount.setVisibility(View.GONE);
                                            break;
                                    }

                                } else {
                                    //Other User
                                    imgStatics.setVisibility(View.INVISIBLE);
                                    imgSettings.setVisibility(View.INVISIBLE);
                                    imgEditDescrip.setVisibility(View.INVISIBLE);
                                    imgEditUserName.setVisibility(View.INVISIBLE);
                                    if (profileResponse.getProfile().getUserDescription() != null &&
                                            !profileResponse.getProfile().getUserDescription().equals(""))
                                        textLabelUserDescription.setText(profileResponse.getProfile().getUserDescription());
                                    else
                                        textLabelUserDescription.setText(getString(R.string.no_description));

                                    picassoImageHolder(imgCoverPhoto, profileResponse.getProfile().getUserCoverPhoto() != null ?
                                                    profileResponse.getProfile().getUserCoverPhoto() : "",
                                            R.mipmap.image_snap_loading_place_holder, R.mipmap.icon_no_cover_photo,
                                            getString(R.string.cloudinary_download_profile_picture));
                                    if (profileResponse.getProfile().getThosetwofriends() == 1) {
                                        //Friends
                                        imgRequest.setImageResource(R.mipmap.icon_pro_friend);
                                    } else if (profileResponse.getProfile().getThosetwofriends() == 2) {
                                        //Not Friends
                                        imgRequest.setImageResource(R.mipmap.icon_pro_friend_request);
                                    } else {
                                        //Requested
                                        imgRequest.setImageResource(R.mipmap.icon_pro_user_requested);
                                    }
                                }


                                textLabelUserName.setText(profileResponse.getProfile().getUserName() != null ?
                                        profileResponse.getProfile().getUserName() : "");
                                textLikeCount.setText(String.valueOf(profileResponse.getProfile().getLikes()));
                                textFollowersCount.setText(String.valueOf(profileResponse.getProfile().getFollowers()));
                                textFollowingCount.setText(String.valueOf(profileResponse.getProfile().getFollowings()));
                                textPostCount.setText(String.valueOf(profileResponse.getProfile().getPosts()));
                                textLabelUserFirstName.setText(profileResponse.getProfile().getUserFirstName() != null ?
                                        profileResponse.getProfile().getUserFirstName() : "");
                                textLabelUserStatus.setText(profileResponse.getProfile().getUserRelationshipStatus() != null ?
                                        profileResponse.getProfile().getUserRelationshipStatus() : "");
                                textLabelUserGender.setText(profileResponse.getProfile().getUserGender() != null ?
                                        profileResponse.getProfile().getUserGender() : "");

                                picassoImageHolder(imageProPic, profileResponse.getProfile().getUserPicture() != null ?
                                                profileResponse.getProfile().getUserPicture() : "",
                                        R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                                        getString(R.string.cloudinary_download_profile_picture));

                                if (profileResponse.getAfter12HrsTop3()!=null &&
                                        profileResponse.getAfter12HrsTop3().size() > 0) {
                                    for (int i = 0; i < profileResponse.getAfter12HrsTop3().size(); i++) {
                                        fItem.add(new FItem(profileResponse.getAfter12HrsTop3().get(i).getPostImage()!=null?
                                                profileResponse.getAfter12HrsTop3().get(i).getPostImage():"",
                                                profileResponse.getAfter12HrsTop3().get(i).getPostType()!=null?
                                                        profileResponse.getAfter12HrsTop3().get(i).getPostType():"",
                                                profileResponse.getAfter12HrsTop3().get(i).getPostID(),
                                                profileResponse.getAfter12HrsTop3().get(i).getPostUserID(),
                                                profileResponse.getAfter12HrsTop3().get(i).getId(),
                                                profileResponse.getAfter12HrsTop3().get(i).getPostMediaType(),
                                                profileResponse.getAfter12HrsTop3().get(i).getPostStatus()));
                                    }

                                }
                                LinearLayoutManager mLayoutManager1 = new LinearLayoutManager(ProfileDetailActivity.this,
                                        LinearLayoutManager.HORIZONTAL, false);
                                recyRecentPost.setLayoutManager(mLayoutManager1);
                                recyRecentPost.setItemAnimator(new DefaultItemAnimator());
                                mAdapter = new StatusAdapter(fItem, ProfileDetailActivity.this);
                                recyRecentPost.setAdapter(mAdapter);
                                recyRecentPost.setNestedScrollingEnabled(false);
//                            getProfileMediaList(1);
                            }


                        } else {
//                            consWarn.setVisibility(View.VISIBLE);
//                            textWarn.setText(profileResponse.getStatus());
//                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                            showShackError(profileResponse.getStatus(), consProfile);
                        }

                    } else {
//                        consWarn.setVisibility(View.VISIBLE);
//                        textWarn.setText(getString(R.string.server_error));
//                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        showShackError(getString(R.string.server_error), consProfile);
                    }

                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consProfile);
//                    consWarn.setVisibility(View.VISIBLE);
//                    textWarn.setText(getString(R.string.server_error));
//                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);

                }
            });


        } else {
            showShackError(getString(R.string.no_network), consProfile);
//            consWarn.setVisibility(View.VISIBLE);
//            textWarn.setText(getString(R.string.no_network));
//            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }
    }

//    private void getProfileMediaList(final int paginationCount) {
//        final ProfileInputParam profileInputParam = new ProfileInputParam();
//        profileInputParam.setOwner_id(ownerID);
//        profileInputParam.setViewer_id(viwerID);
//        profileInputParam.setPage(paginationCount);
//        isLoading = true;
//        if (paginationCount == 1)
//            profileUpload.clear();
//        taggeryAPI.getProfileMediaList(profileInputParam).enqueue(new Callback<ProfileResponse>() {
//            @Override
//            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
//                hideProgress();
//                isLoading = false;
//                profileUploadListResponse = response.body();
//                if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
//                    if (profileResponse.getStatusCode() == 200) {
//                        consWarn.setVisibility(View.GONE);
//                        if (profileUploadListResponse.getAfter12HrsRemaining().size() > 0) {
//                            for (int i = 0; i < profileUploadListResponse.getAfter12HrsRemaining().size(); i++) {
//                                profileUpload.add(new ProfileUpload(profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostImage(),
//                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostType(),
//                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostID(),
//                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostUserID(),
//                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getId(),
//                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostMediaType(),
//                                        profileUploadListResponse.getAfter12HrsRemaining().get(i).getPostStatus()));
//
//                            }
//                            adapter.notifyDataSetChanged();
//                        } else {
//                            consWarn.setVisibility(View.VISIBLE);
//                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
//                            textWarn.setText(getString(R.string.no_media));
//                        }
//
//                    } else if (profileUploadListResponse.getStatusCode() == 501) {
//                        if (paginationCount != 1) {
//                            reachedLastEnd = true;
//                            showShackError(profileUploadListResponse.getMessage(), consProfile);
//                        } else {
//                            recyclerUploadList.setVisibility(View.GONE);
//                            consWarn.setVisibility(View.VISIBLE);
//                            textWarn.setText(profileUploadListResponse.getMessage());
//                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
//                        }
//                    } else {
//                        consWarn.setVisibility(View.VISIBLE);
//                        textWarn.setText(profileUploadListResponse.getStatus());
//                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
//                    }
//
//                } else {
//                    consWarn.setVisibility(View.VISIBLE);
//                    textWarn.setText(getString(R.string.server_error));
//                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ProfileResponse> call, Throwable t) {
//                hideProgress();
//                isLoading = false;
//                consWarn.setVisibility(View.VISIBLE);
//                textWarn.setText(getString(R.string.server_error));
//                imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
//
//            }
//        });
//
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isLoading = false;
        reachedLastEnd = false;
    }

    @OnClick({R.id.img_statics, R.id.img_settings, R.id.image_pro_pic_profile, R.id.img_request,
            R.id.img_edit_descrip, R.id.img_edit_user_name, R.id.img_cover_photo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_statics:
                /*Intent intent = new Intent(ProfileDetailActivity.this, AnalyticalModuleActivity.class);
                startActivity(intent);*/
                break;
            case R.id.image_pro_pic_profile:
                Intent intentRecentPost = new Intent(ProfileDetailActivity.this, PreviewActivity.class);
                Gson gson = new Gson();
                if (profileResponse.getProfile().getRelatedPostsID() != null &&
                        profileResponse.getProfile().getRelatedPostsID().size() > 0) {
                    String listValue = gson.toJson(profileResponse.getProfile().getRelatedPostsID());
                    intentRecentPost.putExtra("relatedPostList", listValue);
                    startActivity(intentRecentPost);
                } else {
                    showShackError(getString(R.string.no_recent_post), consProfile);
                }
                break;
            case R.id.img_request:
                if (profileResponse.getProfile().getProfileOwner() == 1) {
                    //Same user and private or business profile
                    if (profileResponse.getProfile().getUserProfileType() != 1) {
                        //private profile
                        Intent intentFriendReq = new Intent(ProfileDetailActivity.this, FriendRequestActivity.class);
                        startActivity(intentFriendReq);
                    }
                } else {
                    //Other user
                    if (profileResponse.getProfile().getThosetwofriends() == 1) {
                        //Friends
                        //navigate to chat page
                    } else if (profileResponse.getProfile().getThosetwofriends() == 2) {
                        //Not Friends
                        friendRequest(viwerID);
                    } else if (profileResponse.getProfile().getThosetwofriends() == 3) {
                        // alreadyRequested
                        alertDialog();
                    }
                }


                break;
            case R.id.img_settings:
                if (profileResponse.getProfile().getProfileOwner() == 1) {
                    //Same user
                    Intent intent = new Intent(ProfileDetailActivity.this, SettingsActivity.class);
                    intent.putExtra("isfromProfile", false);
                    startActivity(intent);
                }
                break;
            case R.id.img_edit_descrip:
            case R.id.img_edit_user_name:
            case R.id.img_cover_photo:
                if (profileResponse.getProfile().getProfileOwner() == 1) {
                    //Same user
                    Intent intent = new Intent(ProfileDetailActivity.this, SettingsActivity.class);
                    intent.putExtra("isfromProfile", true);
                    startActivity(intent);
                }
                break;
        }
    }


    public void friendRequest(int id) {

        if (Util.isNetworkAvailable()) {
            showProgress();
            FriendRequestInputParam friendRequestInputParam = new FriendRequestInputParam();
            friendRequestInputParam.setFollowerUserID(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            friendRequestInputParam.setFollowingUserID(id);
            taggeryAPI.friendRequest(friendRequestInputParam).enqueue(new Callback<CheckUserNameResponse>() {
                @Override
                public void onResponse(Call<CheckUserNameResponse> call, Response<CheckUserNameResponse> response) {
                    hideProgress();
                    checkUserNameResponse = response.body();
                    if (response.body() != null && response.code() == 200 && response.isSuccessful()) {
                        if (checkUserNameResponse.getStatusCode() == 200) {
                            Snackbar snackbarError = Snackbar.make(consProfile,
                                    checkUserNameResponse.getMessage(), Snackbar.LENGTH_SHORT);
                            snackbarError.show();
                            if (checkUserNameResponse.getRequestAcceptStatus() == 1) {
                                //requested
                                imgRequest.setImageResource(R.mipmap.icon_pro_user_requested);
                                profileResponse.getProfile().setThosetwofriends(3);
                            } else if (checkUserNameResponse.getRequestAcceptStatus() == 2) {
                                // following
                                imgRequest.setImageResource(R.mipmap.icon_pro_friend);
                                profileResponse.getProfile().setThosetwofriends(1);
                            } else {
                                // follow
                                imgRequest.setImageResource(R.mipmap.icon_pro_friend_request);
                                profileResponse.getProfile().setThosetwofriends(2);
                            }

                        } else {
                            Snackbar snackbarError = Snackbar.make(consProfile,
                                    getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                            snackbarError.show();
                        }
                    } else {
                        Snackbar snackbarError = Snackbar.make(consProfile,
                                getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                        snackbarError.show();
                    }
                }

                @Override
                public void onFailure(Call<CheckUserNameResponse> call, Throwable t) {
                    hideProgress();
                    Snackbar snackbarError = Snackbar.make(consProfile,
                            getString(R.string.server_error), Snackbar.LENGTH_SHORT);
                    snackbarError.show();
                }
            });

        } else {
            Snackbar snackbarError = Snackbar.make(consProfile,
                    getString(R.string.no_network), Snackbar.LENGTH_SHORT);
            snackbarError.show();
        }
    }

    private void alertDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProfileDetailActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_unfollow, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();
        CustomTextView textUserName = (CustomTextView) dialogView.findViewById(R.id.text_user_name);
        CustomButton buttonCancel = (CustomButton) dialogView.findViewById(R.id.button_cancel);
        CustomButton button_Ok = (CustomButton) dialogView.findViewById(R.id.button_Ok);
        CircleImageView imageAlertProPic = (CircleImageView) dialogView.findViewById(R.id.image_pro_pic);
        button_Ok.setTypeface(Typeface.createFromAsset(getAssets(),
                "fontName/Montserrat-Regular.ttf"));
        buttonCancel.setTypeface(Typeface.createFromAsset(getAssets(),
                "fontName/Montserrat-Regular.ttf"));
        try {
            picassoImageHolder(imageAlertProPic,
                    profileResponse.getProfile().getUserPicture(),
                    R.mipmap.icon_profile_place_holder, R.mipmap.icon_profile_place_holder,
                    getString(R.string.cloudinary_download_profile_picture));
        } catch (Exception e) {
            e.printStackTrace();
        }
        profileRingColor(profileResponse.getProfile().getLikes(), imageProPic);
        //requested
        textUserName.setText(getString(R.string.Confirm_to_back_request));
        button_Ok.setText(getString(R.string.Undo));
        button_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                friendRequest(viwerID);
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @OnClick(R.id.text_label_view_uploads)
    public void onViewClicked() {
        Intent intent = new Intent(this, ProfileUploadsActivity.class);
        intent.putExtra("viewerID", viwerID);
        intent.putExtra("ownerID", ownerID);
        startActivity(intent);
    }


}
