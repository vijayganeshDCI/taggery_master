package com.taggery.utils;




public class TaggeryConstants {


    public static final String DEVICEID = "DEVICEID";
    public static String APPID="APPID";
    public static String APPVERSION="APPVERSION";
    public static String LOGIN_STATUS="LOGIN_STATUS";
    public static String PHONE_NUMBER="PHONE_NUMBER";
    public static String COUNTRY_CODE_PHONE_NUMBER="COUNTRY_CODE_PHONE_NUMBER";
    public static String FILTERS_CHECK="FILTERS_CHECK";
    public static String FILTERS_NEW="FILTERS_NEW";
    public static String FILE_POST="FILE_POST";
    public static String CHECK_GALLERY="CHECK_GALLERY";
    public static String FILTERS_APPLY="FILTERS_APPLY";
    public static String FCMKEYSHAREDPERFRENCES="FCMKEYSHAREDPERFRENCES";
    public static String FCMTOKEN="FCMTOKEN";
    public static String USER_ID="USER_ID";
    public static String FIRST_NAME="FIRST_NAME";
    public static String LAST_NAME="LAST_NAME";
    public static String USER_NAME="USER_NAME";
    public static String USER_EMAIL="USER_EMAIL";
    public static String USER_GENDER="USER_GENDER";
    public static String USER_RELATION_SHIP_STATUS="USER_RELATION_SHIP_STATUS";
    public static String USER_PROFILE_TYPE="USER_PROFILE_TYPE";
    public static String USER_PROFILE_PIC="USER_PROFILE_PIC";
    public static String USER_COVER_PIC="USER_COVER_PIC";
    public static String USER_CHAT_NAME="USER_CHAT_NAME";
    public static String USER_DECRIPTION="USER_DECRIPTION";
    /** Key into an Intent's extras data that points to a Channel object. */
    public static final String EXTRA_CHANNEL = "com.twilio.chat.Channel";
    /** Key into an Intent's extras data that contains Channel SID. */
    public static final String EXTRA_CHANNEL_SID = "C_SID";
    public static final String USER_LIKE_COUNT = "USER_LIKE_COUNT";
}


