package com.taggery.model;

import java.util.List;

public class ContactListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public List<SuggestionItem> getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(List<SuggestionItem> suggestion) {
		this.suggestion = suggestion;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private List<SuggestionItem> suggestion;

	public List<SuggestionItem> getFriends() {
		return Friends;
	}

	public void setFriends(List<SuggestionItem> friends) {
		Friends = friends;
	}

	private List<SuggestionItem> Friends;

	public List<SuggestionItem> getFollowings() {
		return Followings;
	}

	public void setFollowings(List<SuggestionItem> followings) {
		Followings = followings;
	}

	private List<SuggestionItem> Followings;

	public List<SuggestionItem> getFollowers() {
		return Followers;
	}

	public void setFollowers(List<SuggestionItem> followers) {
		Followers = followers;
	}

	private List<SuggestionItem> Followers;
	private int StatusCode;

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	private String Message;
}