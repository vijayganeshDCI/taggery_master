package com.taggery.fragment;


import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.taggery.R;
import com.taggery.activity.PreviewActivity;
import com.taggery.adapter.PostDetailAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.ContactListInputParam;
import com.taggery.model.GetPostDetailResponse;
import com.taggery.model.ViewedItem;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * UserProfileUploadAfter12Hrs simple {@link Fragment} subclass.
 */
public class StatsFragment extends BaseFragment {


    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.linear_close)
    LinearLayout linearClose;
    @BindView(R.id.img_uploadpic)
    ImageView imgUploadpic;
    @BindView(R.id.text_status)
    CustomTextView textStatus;
    @BindView(R.id.img_uparrow)
    ImageView imgUparrow;
    @BindView(R.id.img_eye)
    ImageView imgEye;
    @BindView(R.id.text_viewcount)
    CustomTextView textViewcount;
    @BindView(R.id.img_likeheart)
    ImageView imgLikeheart;
    @BindView(R.id.text_like_count)
    CustomTextView textLikecount;
    @BindView(R.id.relative_status)
    RelativeLayout relativeStatus;
    @BindView(R.id.relative_top)
    RelativeLayout relativeTop;
    @BindView(R.id.relative_child)
    RelativeLayout relativeChild;
    @BindView(R.id.app_bar_header)
    AppBarLayout appBarHeader;
    @BindView(R.id.recyclerview_status)
    RecyclerView recyclerviewStatus;
    //    @BindView(R.id.swipe_post_details)
//    SwipeRefreshLayout swipePostDetails;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    ConstraintLayout consWarn;
    @BindView(R.id.cons_post_details)
    CoordinatorLayout consPostDetails;
    Unbinder unbinder;
    @BindView(R.id.video_player_post)
    SimpleExoPlayerView videoPlayerPost;

    private LinearLayoutManager mLayoutManager;
    private PostDetailAdapter postDetailsAdapter;
    PreviewActivity previewActivity;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    private int postID;
    private GetPostDetailResponse getPostDetailResponse;
    private ArrayList<ViewedItem> getPostDetailArrayList;
    private SimpleExoPlayer player;
    private DefaultDataSourceFactory dataSourceFactory;
    private DefaultExtractorsFactory extractorsFactory;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stats, container, false);
        unbinder = ButterKnife.bind(this, view);
        previewActivity = (PreviewActivity) getActivity();
        getPostDetailArrayList = new ArrayList<ViewedItem>();
        TaggeryApplication.getContext().getComponent().inject(this);
        Bundle bundle = getArguments();
        if (getArguments() != null) {
            postID = bundle.getInt("postID");
        }
        initializePlayer();
        getPostDetails();
        return view;
    }


    private void initializePlayer() {
        // Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        //Initialize the player
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector);
        //Initialize simpleExoPlayerView
        videoPlayerPost.setPlayer(player);
        // Produces DataSource instances through which media data is loaded.
        dataSourceFactory =
                new DefaultDataSourceFactory(getActivity(), com.google.android.exoplayer2.util.Util.getUserAgent(getActivity(),
                        "taggery"));
        // Produces Extractor instances for parsing the media data.
        extractorsFactory = new DefaultExtractorsFactory();

    }

    private void getPostDetails() {
        if (Util.isNetworkAvailable()) {
            showProgress();
            ContactListInputParam contactListInputParam = new ContactListInputParam();
            contactListInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            contactListInputParam.setPostID(postID);
            taggeryAPI.getPostDetails(contactListInputParam).enqueue(new Callback<GetPostDetailResponse>() {
                @Override
                public void onResponse(Call<GetPostDetailResponse> call, Response<GetPostDetailResponse> response) {
                    hideProgress();
                    getPostDetailResponse = response.body();
                    if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                        if (getPostDetailResponse.getStatusCode() == 200) {
                            if (getPostDetailResponse.getPost() != null) {
                                if (getPostDetailResponse.getPost().getPostMediaType() != 2) {
                                    //image
                                    imgUploadpic.setVisibility(View.VISIBLE);
                                    videoPlayerPost.setVisibility(View.GONE);
                                    picassoImageHolder(imgUploadpic, getPostDetailResponse.getPost().getPostImage() != null ?
                                                    getPostDetailResponse.getPost().getPostImage() : "",
                                            R.mipmap.image_snap_loading_place_holder, R.mipmap.image_snap_no_image_place_holder,
                                            getString(R.string.cloudinary_download_post));
                                } else {
                                    //video
                                    // This is the MediaSource representing the media to be played.
                                    imgUploadpic.setVisibility(View.INVISIBLE);
                                    videoPlayerPost.setVisibility(View.VISIBLE);
                                    if (getPostDetailResponse.getPost().getPostImage() != null) {
                                        Uri videoUri = Uri.parse(getString(R.string.cloudinary_base_url) +
                                                getString(R.string.cloudinary_download_video_add_post) +
                                                getPostDetailResponse.getPost().getPostImage());
                                        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                                                dataSourceFactory, extractorsFactory, null, null);
                                        // Prepare the player with the source.
                                        player.prepare(videoSource);
                                        player.setPlayWhenReady(true);
                                    }
                                }

                                textLikecount.setText("" + getPostDetailResponse.getPost().getLikestcount());
                                textViewcount.setText("" + getPostDetailResponse.getPost().getViewscount());
                                textStatus.setText(getPostDetailResponse.getPost().getPostText());
                                if (getPostDetailResponse.getPost().getViewed() != null &&
                                        getPostDetailResponse.getPost().getViewed().size() > 0) {
                                    consWarn.setVisibility(View.GONE);
                                    recyclerviewStatus.setVisibility(View.VISIBLE);
                                    getPostDetailArrayList.addAll(getPostDetailResponse.getPost().getViewed());
                                    mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,
                                            false);
                                    recyclerviewStatus.setLayoutManager(mLayoutManager);
                                    recyclerviewStatus.setItemAnimator(new DefaultItemAnimator());
                                    postDetailsAdapter = new PostDetailAdapter(getPostDetailArrayList, getActivity());
                                    recyclerviewStatus.setAdapter(postDetailsAdapter);
                                } else {
                                    consWarn.setVisibility(View.VISIBLE);
                                    recyclerviewStatus.setVisibility(View.GONE);
                                    textWarn.setText(getString(R.string.no_one_seen));
                                    imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                                }
                            }


                        } else {
                            showShackError(getString(R.string.server_error), consPostDetails);
                            consWarn.setVisibility(View.VISIBLE);
                            recyclerviewStatus.setVisibility(View.GONE);
                            textWarn.setText(getString(R.string.no_one_seen));
                            imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                        }
                    } else {
                        showShackError(getString(R.string.server_error), consPostDetails);
                        consWarn.setVisibility(View.VISIBLE);
                        recyclerviewStatus.setVisibility(View.GONE);
                        textWarn.setText(getString(R.string.no_one_seen));
                        imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                    }
                }

                @Override
                public void onFailure(Call<GetPostDetailResponse> call, Throwable t) {
                    hideProgress();
                    showShackError(getString(R.string.server_error), consPostDetails);
                    consWarn.setVisibility(View.VISIBLE);
                    recyclerviewStatus.setVisibility(View.GONE);
                    textWarn.setText(getString(R.string.no_one_seen));
                    imageWarnIcon.setImageResource(R.mipmap.icon_no_contacts);
                }
            });


        } else {
            showShackError(getString(R.string.no_network), consPostDetails);
            consWarn.setVisibility(View.VISIBLE);
            recyclerviewStatus.setVisibility(View.GONE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.img_close)
    public void onViewClicked() {
    }
}
