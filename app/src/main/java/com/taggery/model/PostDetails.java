package com.taggery.model;

import java.util.List;

public class PostDetails {
    public String getPostImage() {
        return PostImage;
    }

    public void setPostImage(String postImage) {
        PostImage = postImage;
    }

    public String getPostLocation() {
        return PostLocation;
    }

    public void setPostLocation(String postLocation) {
        PostLocation = postLocation;
    }

    public String getPostType() {
        return PostType;
    }

    public void setPostType(String postType) {
        PostType = postType;
    }

    public String getViews() {
        return Views;
    }

    public void setViews(String views) {
        Views = views;
    }

    public int getLikestcount() {
        return likestcount;
    }

    public void setLikestcount(int likestcount) {
        this.likestcount = likestcount;
    }

    public int getViewscount() {
        return viewscount;
    }

    public void setViewscount(int viewscount) {
        this.viewscount = viewscount;
    }

    public int getPostID() {
        return PostID;
    }

    public void setPostID(int postID) {
        PostID = postID;
    }

    public int getPostUserID() {
        return PostUserID;
    }

    public void setPostUserID(int postUserID) {
        PostUserID = postUserID;
    }


    public String getPostLongitude() {
        return PostLongitude;
    }

    public void setPostLongitude(String postLongitude) {
        PostLongitude = postLongitude;
    }

    public String getPostText() {
        return PostText;
    }

    public void setPostText(String postText) {
        PostText = postText;
    }

    public int getPostTimeFrame() {
        return PostTimeFrame;
    }

    public void setPostTimeFrame(int postTimeFrame) {
        PostTimeFrame = postTimeFrame;
    }

    public int getLikes() {
        return Likes;
    }

    public void setLikes(int likes) {
        Likes = likes;
    }

    public int getPostStatus() {
        return PostStatus;
    }

    public void setPostStatus(int postStatus) {
        PostStatus = postStatus;
    }

    public String getPostLatitude() {
        return PostLatitude;
    }

    public void setPostLatitude(String postLatitude) {
        PostLatitude = postLatitude;
    }

    private String PostImage;
    private String PostLocation;
    private String PostType;
    private String Views;
    private int likestcount;
    private int viewscount;
    private int PostID;
    private int PostUserID;

    public List<String> getPostTagList() {
        return PostTagList;
    }

    public void setPostTagList(List<String> postTagList) {
        PostTagList = postTagList;
    }

    private List<String> PostTagList;
    private String PostLongitude;
    private String PostText;
    private int PostTimeFrame;
    private int Likes;
    private int PostStatus;
    private String PostLatitude;

    public String getFeedtime() {
        return Feedtime;
    }

    public void setFeedtime(String feedtime) {
        Feedtime = feedtime;
    }

    private String Feedtime;

    public int getPostMediaType() {
        return PostMediaType;
    }

    public void setPostMediaType(int postMediaType) {
        PostMediaType = postMediaType;
    }

    private int PostMediaType;

    public String getPostUserFName() {
        return PostUserFName;
    }

    public void setPostUserFName(String postUserFName) {
        PostUserFName = postUserFName;
    }

    public String getPostUserLName() {
        return PostUserLName;
    }

    public void setPostUserLName(String postUserLName) {
        PostUserLName = postUserLName;
    }

    public String getPostUserProfileImage() {
        return PostUserProfileImage;
    }

    public void setPostUserProfileImage(String postUserProfileImage) {
        PostUserProfileImage = postUserProfileImage;
    }

    private String PostUserFName;
    private String PostUserLName;
    private String PostUserProfileImage;

    public PostDetails(String postImage, int likestcount, int viewscount,
                       int postID, int postUserID, String postText, int postTimeFrame) {
        PostImage = postImage;
        this.likestcount = likestcount;
        this.viewscount = viewscount;
        PostID = postID;
        PostUserID = postUserID;
        PostText = postText;
        PostTimeFrame = postTimeFrame;
    }


}
