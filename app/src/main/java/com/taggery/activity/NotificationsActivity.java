package com.taggery.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.taggery.model.TaggeryPojo;
import com.taggery.model.horizontalpojo;
import com.taggery.R;
import com.taggery.adapter.TagAdapter;

import java.util.ArrayList;
import java.util.List;

public class NotificationsActivity extends BaseActivity {

    public RecyclerView recyclerview;
    public RecyclerView recyclerview1;
    private TagAdapter mAdapter;
    private List<TaggeryPojo> tagList=new ArrayList<>();
    private List<horizontalpojo> hrlist=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        recyclerview=(RecyclerView)findViewById(R.id.recycle);
        recyclerview.setHasFixedSize(true);
        recyclerview1=(RecyclerView)findViewById(R.id.footer_recycle);
recyclerview1.setHasFixedSize(true);



        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new TagAdapter(tagList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        recyclerview.setAdapter(mAdapter);
        Acceptdata();


        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerview1.setLayoutManager(mLayoutManager1);
        recyclerview1.setItemAnimator(new DefaultItemAnimator());
        horizontalrecyclerview();
        recyclerview.setNestedScrollingEnabled(false);
        recyclerview1.setNestedScrollingEnabled(false);

    }

    private void horizontalrecyclerview() {
        horizontalpojo s=new horizontalpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setButton("Follow");
        s.setText("David");
        hrlist.add(s);


        s=new horizontalpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setText("Daniel");
        s.setButton("Follow");
        hrlist.add(s);

        s=new horizontalpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setText("Caroline");
        s.setButton("Follow");
        hrlist.add(s);

        s=new horizontalpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setText("Albert");
        s.setButton("Follow");
        hrlist.add(s);

        s=new horizontalpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setText("Sofia");
        s.setButton("Follow");
        hrlist.add(s);

        s=new horizontalpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setText("Lucia");
        s.setButton("Follow");
        hrlist.add(s);


    }

    private void Acceptdata() {

        TaggeryPojo s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setBut("Follow");
        s.setName("David");
        tagList.add(s);


        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Daniel");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Caroline");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Albert");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Sofia");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Lucia");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Daniel");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Caroline");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Albert");
        s.setBut("Follow");
        tagList.add(s);

        s=new TaggeryPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Sofia");
        s.setBut("Follow");
        tagList.add(s);
    }
}
