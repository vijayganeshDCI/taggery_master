package com.taggery.model;

public class AnalyticalListResponse{
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public com.taggery.model.Analytical getAnalytical() {
		return Analytical;
	}

	public void setAnalytical(com.taggery.model.Analytical analytical) {
		Analytical = analytical;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	private String Status;
	private Analytical Analytical;
	private int StatusCode;
}
