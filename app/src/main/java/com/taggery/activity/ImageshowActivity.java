package com.taggery.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudinary.android.MediaManager;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.taggery.R;
import com.taggery.application.TaggeryApplication;
import com.taggery.fragment.FilterFragment;
import com.taggery.utils.TaggeryConstants;
import com.yashoid.instacropper.InstaCropperView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;


public class ImageshowActivity extends BaseActivity  {
    FrameLayout frame_imageshow_container;
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String mediaPath,mediaFilename;
    InstaCropperView sample_instacropper;
    TextView text_next;
    boolean crop=false;
    File imageFile;
    boolean isImage;
    RelativeLayout  relative_crop;
    String mCurrentPhotoPath,imageFilename;
    FFmpeg fFmpeg;
    String[] command;
    SharedPreferences share;
    SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageshow);
        frame_imageshow_container=findViewById(R.id.frame_imageshow_container);
        TaggeryApplication.getContext().getComponent().inject(this);
//        editor=sharedPreferences.edit();


        share = getSharedPreferences("mytaggery",Context.MODE_PRIVATE);
        edit = share.edit();
        edit.putString(TaggeryConstants.FILTERS_NEW, "newfilters").commit();


        text_next= findViewById(R.id.text_next);
        sample_instacropper = findViewById(R.id.sample_instacropper);
        relative_crop= findViewById(R.id.relative_crop);
        Intent intent = getIntent();

        if (intent!=null){
            isImage = intent.getBooleanExtra("isImage",true);
            mediaPath = intent.getStringExtra("mediaPath");
            Log.i("mediaPath",mediaPath);
            mediaFilename = intent.getStringExtra("mediaFilename");
           /* imageFilename = intent.getStringExtra("imageFilename");
            imageFile = new File(mediaPath);
            if (imageFile.exists()) {
                sample_instacropper.setImageUri(Uri.fromFile(new File(imageFile.getAbsolutePath())));
            }*/

            FilterFragment filterFragment = new FilterFragment();
            Bundle bundle = new Bundle();
            bundle.putString("mediaPath", mediaPath);
            bundle.putString("mediaFilename",mediaFilename);
            if (isImage){
                bundle.putBoolean("isImage",true);

            }
            else {
                /*try {
                    loadFFmepg();
                } catch (FFmpegNotSupportedException e) {
                    e.printStackTrace();
                }*/
                bundle.putBoolean("isImage",false);
            }

            filterFragment.setArguments(bundle);
            push(filterFragment);

        }

    }

    private void loadFFmepg() throws FFmpegNotSupportedException {
        if (fFmpeg==null){
            fFmpeg = FFmpeg.getInstance(this);
            fFmpeg.loadBinary(new FFmpegLoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    Toast.makeText(ImageshowActivity.this, "library not loaded", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess() {
                    Toast.makeText(ImageshowActivity.this, "library loaded", Toast.LENGTH_SHORT).show();
                    command = new String[]{"-y", "-i", "/storage/emulated/0/Taggery/temp/VID_1540989091987.mp4", "-s", "480x320",  "-vcodec", "mpeg4", "-acodec","aac", "-strict","-2","-ac","1","-ar","16000","-r","13","-ab","32000","-aspect","3:2" , "/storage/emulated/0/Taggery/save/ajith.mp4"};
                    try {
                        executeCommand(command);
                    } catch (FFmpegCommandAlreadyRunningException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });
        }
    }

    private void executeCommand(String command[]) throws FFmpegCommandAlreadyRunningException {
        fFmpeg.execute(command, new FFmpegExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                Toast.makeText(ImageshowActivity.this, "success", Toast.LENGTH_SHORT).show();
               /* filterFragment.setArguments(bundle);
                push(filterFragment);*/

            }

            @Override
            public void onProgress(String message) {
                Toast.makeText(ImageshowActivity.this, "progress", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(String message) {
                Toast.makeText(ImageshowActivity.this, "failure", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStart() {
                Toast.makeText(ImageshowActivity.this, "start", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                Toast.makeText(ImageshowActivity.this, "finish", Toast.LENGTH_SHORT).show();
            }
        });
    }





    private void saveImage(Bitmap finalImage) {
        File destFile;
        String state = Environment.getExternalStorageState();
        if (!state.equals(Environment.MEDIA_MOUNTED)) {
            return;
        } else {
            File folder_gui1 = new File(Environment.getExternalStorageDirectory() + File.separator + "Taggery" + File.separator +"Local");

            if (!folder_gui1.exists()) {
                folder_gui1.mkdirs();

            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "IMG" + timeStamp + "_";

            destFile = new File(folder_gui1, imageFilename + ".jpg");
            try {
                FileOutputStream out = new FileOutputStream(destFile,false);
                finalImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
                mCurrentPhotoPath = destFile.getAbsolutePath();
                out.flush();
                out.close();
                //  Toast.makeText(getActivity(), "filtered image saved ", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        relative_crop.setVisibility(View.GONE);
        frame_imageshow_container.setVisibility(View.VISIBLE);
        FilterFragment filterFragment = new FilterFragment();
        Bundle bundle = new Bundle();
        bundle.putString("mediaPath", mCurrentPhotoPath);
        if (isImage){
            bundle.putBoolean("isImage",true);
        }
        else {
            bundle.putBoolean("isImage",false);
        }
        filterFragment.setArguments(bundle);
        push(filterFragment);

    }

    public void popBackStackImmediate() {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.executePendingTransactions();
    }

    public void popAllBackstack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();
        if (backCount > 0) {
            fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).getId(),
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void push(Fragment fragment, String title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getCanonicalName();

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_imageshow_container, fragment, tag)
                        .addToBackStack(title)
                        .commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_imageshow_container, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss();
            }
        } else {
            try {
                fragmentManager.beginTransaction().replace(R.id.frame_imageshow_container, fragment, tag).commit();
            } catch (IllegalStateException ile) {
                fragmentManager.beginTransaction().replace(R.id.frame_imageshow_container, fragment, tag).commitAllowingStateLoss();
            }
        }

//        if (title != null) {
//            ActionBar actionBar = getSupportActionBar();
//            if (actionBar != null) {
//                actionBar.setTitle(title);
//            }
//        }
    }

    public void push(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        push(fragment, null);
    }

}

