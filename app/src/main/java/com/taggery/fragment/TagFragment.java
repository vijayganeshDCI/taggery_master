package com.taggery.fragment;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.taggery.R;
import com.taggery.adapter.TagPeopleAdapter;
import com.taggery.application.TaggeryApplication;
import com.taggery.model.TagInputParam;
import com.taggery.model.TagPeopleList;
import com.taggery.model.TagPeopleResponse;
import com.taggery.retrofit.TaggeryAPI;
import com.taggery.utils.TaggeryConstants;
import com.taggery.utils.Util;
import com.taggery.view.CustomTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import Utils.OnItemClickListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TagFragment extends BaseFragment implements OnItemClickListener {


    String mediaPath;
    @BindView(R.id.img_logo)
    ImageView imgLogo;
    @BindView(R.id.text_tagpeople)
    CustomTextView textTagpeople;
    @BindView(R.id.relative_top)
    RelativeLayout relativeTop;
    @BindView(R.id.img_tagpeople)
    ImageView imgTagpeople;
    @BindView(R.id.vid_tagpeople)
    VideoView vidTagpeople;
    @BindView(R.id.relative_tagpeople)
    RelativeLayout relativeTagpeople;
    @BindView(R.id.recyclerview_tagpeople)
    RecyclerView recyclerviewTagpeople;
    @BindView(R.id.relative_bottom)
    RelativeLayout relativeBottom;
    @BindView(R.id.relative_parent)
    RelativeLayout relativeParent;
    Unbinder unbinder;
    @BindView(R.id.image_warn_icon)
    ImageView imageWarnIcon;
    @BindView(R.id.text_warn)
    CustomTextView textWarn;
    @BindView(R.id.cons_warn)
    RelativeLayout consWarn;
    @BindView(R.id.search_tag)
    SearchView searchTag;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.text_nofrnds)
    CustomTextView textNofrnds;
    @BindView(R.id.text_tag)
    CustomTextView textTag;
    @BindView(R.id.img_tag)
    ImageView imgTag;
    @BindView(R.id.relative_tag)
    RelativeLayout relativeTag;
    private TagPeopleAdapter mAdapter;
    List<TagPeopleResponse.Friend> tagPeopleResposeList = new ArrayList<>();
    public static LinkedHashSet<TagPeopleList> tagPeopleStaticSelectedList = new LinkedHashSet<>();
    List<TagPeopleList> tempTagList = new ArrayList<>();
    boolean isSearch = false;
    @Inject
    TaggeryAPI taggeryAPI;
    @Inject
    SharedPreferences sharedPreferences;
    private int selectedCount = 0;
    TagPeopleResponse tagPeopleResponse;
    private boolean isLoading = false, reachedLastEnd = false;
    private int paginationCount = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tag, container, false);
        unbinder = ButterKnife.bind(this, view);
        TaggeryApplication.getContext().getComponent().inject(this);
        Bundle bundle = getArguments();

        if (getArguments() != null) {

            if (bundle.getBoolean("isImage")) {
                mediaPath = bundle.getString("mediaPath");
                vidTagpeople.setVisibility(View.GONE);
                //  filterimage = bundle.getParcelable("filterimage");
                File imgFile = new File(mediaPath);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imgTagpeople.setImageBitmap(myBitmap);
                    getTaglist("");

                }
            } else {
                mediaPath = bundle.getString("mediaPath");
                vidTagpeople.setVisibility(View.VISIBLE);
                try {
                    // videoPost.setMediaController(controller);
                    vidTagpeople.setVideoURI(Uri.parse(mediaPath));
                    getTaglist("");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                vidTagpeople.requestFocus();
                vidTagpeople.seekTo(100);
                //videoView.setZOrderOnTop(true);
                vidTagpeople.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    public void onPrepared(MediaPlayer mp) {

                        // vid_tagpeople.start();
                    }
                });
                vidTagpeople.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        //  videoView.start();
                    }
                });
            }

        }
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSearch) {
                    searchTag.setVisibility(View.GONE);
                    searchTag.onActionViewCollapsed();
                    isSearch = false;
                } else {
                    searchTag.setVisibility(View.VISIBLE);
                    searchTag.onActionViewExpanded();
                    isSearch = true;
                }

            }
        });
        searchTag.setQueryHint("search here");
        EditText searchEditText = (EditText) searchTag.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        ImageView searchIcon = searchTag.findViewById(android.support.v7.appcompat.R.id.search_button);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));
        searchIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.searchnew));
        searchTag.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getTaglist(newText);

                return false;
            }
        });
        relativeTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < tagPeopleResposeList.size(); i++) {
                    if (tagPeopleResposeList.get(i).getIsSelected() == 1) {
                        tagPeopleStaticSelectedList.add(new TagPeopleList(tagPeopleResposeList.get(i).getId(), tagPeopleResposeList.get(i).getUserName()));
                        tempTagList.add(new TagPeopleList(tagPeopleResposeList.get(i).getId(), tagPeopleResposeList.get(i).getUserName()));
                    }
                }
                Log.i("tag", "" + tagPeopleStaticSelectedList.toString());

                if (tempTagList.size()==0){
                    showShackError("select a friend", relativeParent);
                }
                else {
                    getActivity().onBackPressed();
                }

            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (selectedCount > 0) {
                        selectedCount = 0;
                        for (int i = 0; i < tagPeopleResposeList.size(); i++) {
                            if (tagPeopleResposeList.get(i).getIsSelected() == 1) {
                                tagPeopleResposeList.get(i).setIsSelected(0);
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                        return true;
                    } else {
                        return false;
                    }
                }
                return false;
            }
        });


        return view;
    }


    private void getTaglist(String newText) {
        if (Util.isNetworkAvailable()) {
             showProgress();
            final TagInputParam tagInputParam = new TagInputParam();
            tagInputParam.setUser_id(sharedPreferences.getInt(TaggeryConstants.USER_ID, 0));
            tagInputParam.setSearchvalue(newText);
            taggeryAPI.tagpeopleresponse(tagInputParam).enqueue(new Callback<TagPeopleResponse>() {
                @Override
                public void onResponse(Call<TagPeopleResponse> call, Response<TagPeopleResponse> response) {
                    hideProgress();
                    tagPeopleResponse = response.body();

                    if (response.code() == 200 && response.isSuccessful() && response.body() != null) {
                        if (tagPeopleResponse.getStatusCode() == 200) {
                            if (response.body().getFriends() != null &&
                                    response.body().getFriends().size() > 0) {
                                tagPeopleResposeList = response.body().getFriends();
                                recyclerviewTagpeople.setVisibility(View.VISIBLE);
                                textNofrnds.setVisibility(View.GONE);
                                consWarn.setVisibility(View.GONE);

                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                recyclerviewTagpeople.setLayoutManager(mLayoutManager);
                                recyclerviewTagpeople.setItemAnimator(new DefaultItemAnimator());
                                recyclerviewTagpeople.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
                                mAdapter = new TagPeopleAdapter(tagPeopleResposeList, getActivity());
                                mAdapter.setItemclickListener(TagFragment.this);
                                recyclerviewTagpeople.setAdapter(mAdapter);

                            } else {
                                recyclerviewTagpeople.setVisibility(View.GONE);
                                textNofrnds.setVisibility(View.VISIBLE);
                            }


                        } else {
                            recyclerviewTagpeople.setVisibility(View.GONE);
                            consWarn.setVisibility(View.VISIBLE);
                            textWarn.setText(getString(R.string.server_error));
                            imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                        }

                    } else {
                        recyclerviewTagpeople.setVisibility(View.GONE);
                        consWarn.setVisibility(View.VISIBLE);
                        textWarn.setText(getString(R.string.server_error));
                        imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);
                    }


                }

                @Override
                public void onFailure(Call<TagPeopleResponse> call, Throwable t) {

                    hideProgress();
                    recyclerviewTagpeople.setVisibility(View.GONE);
                    consWarn.setVisibility(View.VISIBLE);
                    textWarn.setText(getString(R.string.server_error));
                    imageWarnIcon.setImageResource(R.mipmap.icon_empty_post);

                }
            });

        } else {
            consWarn.setVisibility(View.VISIBLE);
            textWarn.setText(getString(R.string.no_network));
            imageWarnIcon.setImageResource(R.mipmap.icon_no_network);

        }


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void OnItemClick(int pos, View view) {
        /*selectedCount++;*/

    }


    @Override
    public void OnItemClickSecond(int position, View view) {

        if (tagPeopleResposeList.get(position).getIsSelected() == 0) {
            selectedCount++;

        } else {
            selectedCount--;

        }


    }

    /*@Override
    public void OnItemClick(int pos, View view) {
        TagPeopleResponse.Friend tag = tagPeopleResposeList.get(pos);

        tag.setIsSelected(1);
        mAdapter.notifyDataSetChanged();



    }

    @Override
    public void OnItemClickSecond(int position, View view) {
        TagPeopleResponse.Friend tagpeople = tagPeopleResposeList.get(position);

        if (tagPeopleResposeList.get(position).getIsSelected() == 0) {
            tagPeopleResposeList.get(position).setIsSelected(1);

        } else {
            tagPeopleResposeList.get(position).setIsSelected(0);

        }
        mAdapter.notifyDataSetChanged();


        Toast.makeText(getActivity(), tagpeople.getUserName(), Toast.LENGTH_SHORT).show();

    }*/
}
